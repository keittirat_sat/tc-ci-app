﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
//using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace Checklist
{
    public partial class preLoading : Form
    {
        Form1 parent;
        public preLoading(Form1 parseForm)
        {
            InitializeComponent();
            parent = parseForm;
            timer1.Interval = 20;
            timer1.Enabled = true;
            //timer1.Tick += new System.EventHandler(OnTimerEvent);
        }

        private void progressBar1_Click(object sender, EventArgs e)
        {

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            progressBar1.Value = parent.bootProgress;
            loadingState.Text = parent.loadingState;
            /*if (progressBar1.Value == 100)
                timer1.Stop();*/
        }
    }
}
