﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using Newtonsoft.Json.Linq;

namespace CI_Calculator
{
    public partial class profile_form : Form
    {
        private Checklist.Form1 formMain;
        public profile_form(Checklist.Form1 param)
        {
            InitializeComponent();
            weight_box.Text = param.weight_raws;
            formMain = param;
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            ProcessStartInfo sInfo = new ProcessStartInfo("http://ci.damlog.com/generate");
            Process.Start(sInfo);
        }

        private void save_btn_Click(object sender, EventArgs e)
        {
            String all_new = weight_box.Text;
            formMain.weight_raws = all_new;
            formMain.weight = JObject.Parse(all_new);
            System.IO.File.WriteAllText(formMain.appPath + @"\Profile\default.json", all_new);
            MessageBox.Show("บันทึกค่าน้ำหนักใหม่เรียบร้อยแล้ว");
        }
    }
}
