﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
//using System.Linq;
using System.Text;
using System.Threading;
//using System.Threading.Tasks;
using System.Windows.Forms;

namespace Checklist
{
    public partial class addNewDam : Form
    {
        public IDataReader db;
        private Form1 mainForm;
        private bool[] checklistInfo = new bool[160];
        public addNewDam(Form1 parseForm)
        {
            InitializeComponent();
            mainForm = parseForm;
            string c = DateTime.Now.ToString("yyMMddHHmmss");
            string sql = "select * from geography";
            ArrayList local_db = mainForm.getDB(sql);
            geo_id.Items.Clear();
            ComboboxItem itemList;
            foreach (ArrayList geo in local_db)
            {
                itemList = new ComboboxItem();
                itemList.Text = geo[1].ToString();
                itemList.Value = geo[0].ToString();
                geo_id.Items.Add(itemList);
            }
            geo_id.SelectedIndex = 0;
            dam_province.SelectedIndex = 0;
            dam_district.SelectedIndex = 0;
            dam_subdistrict.SelectedIndex = 0;
            dam_id.Text = c;

            addDam_panel_2.Hide();
            createDam_step1_panel.Show();

            mainForm.genRootView("1", DamPart_1, false, true);
            mainForm.genRootView("10", DamPart_2, false, true);
            mainForm.genRootView("89", DamPart_3, false, true);

            DamPart_1.ExpandAll();
            DamPart_2.ExpandAll();
            DamPart_3.ExpandAll();

            parseForm.Cursor = Cursors.Default;

            itemList = new ComboboxItem();
            itemList.Text = "ม. ร.ท.ก.";
            itemList.Value = "0";
            water_scale.Items.Clear();
            water_scale.Items.Add(itemList);

            itemList = new ComboboxItem();
            itemList.Text = "ม. ร.ส.ม.";
            itemList.Value = "1";
            water_scale.Items.Add(itemList);

            water_scale.SelectedIndex = 0;

        }

        private void geo_id_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            string _geo_id = (geo_id.SelectedItem as ComboboxItem).Value.ToString();
            string sql = "select * from province where GEO_ID = " + _geo_id;
            dam_province.ResetText();
            ArrayList local_db = mainForm.getDB(sql);
            dam_province.Items.Clear();
            ComboboxItem itemList;
            foreach (ArrayList c in local_db)
            {
                itemList = new ComboboxItem();
                itemList.Text = c[2].ToString();
                itemList.Value = c[0].ToString();
                dam_province.Items.Add(itemList);
            }

            dam_province.SelectedIndex = 0;
        }

        private void dam_province_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            string _province_id = (dam_province.SelectedItem as ComboboxItem).Value.ToString();
            string sql = "select * from amphur where PROVINCE_ID = " + _province_id;
            ArrayList local_db = mainForm.getDB(sql);
            dam_district.ResetText();
            dam_district.Items.Clear();
            ComboboxItem itemList;
            foreach (ArrayList c in local_db)
            {
                itemList = new ComboboxItem();
                itemList.Text = c[2].ToString();
                itemList.Value = c[0].ToString();
                dam_district.Items.Add(itemList);
            }
            dam_district.SelectedIndex = 0;
        }

        private void dam_district_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            string _dam_district = (dam_district.SelectedItem as ComboboxItem).Value.ToString();
            string sql = "select * from district where AMPHUR_ID = " + _dam_district;
            ArrayList local_db = mainForm.getDB(sql);
            dam_subdistrict.ResetText();
            dam_subdistrict.Items.Clear();
            ComboboxItem itemList;
            foreach (ArrayList c in local_db)
            {
                itemList = new ComboboxItem();
                itemList.Text = c[2].ToString();
                itemList.Value = c[0].ToString();
                dam_subdistrict.Items.Add(itemList);
            }
            dam_subdistrict.SelectedIndex = 0;
        }

        private void addDam_next_Click(object sender, EventArgs e)
        {
            if (dam_id.Text == "")
            {
                MessageBox.Show("กรุณากรอกรหัสของเขื่อนด้วยค่ะ");
            }
            else if (dam_name.Text == "")
            {
                MessageBox.Show("กรุณากรอกชื่อของเขื่อนด้วยค่ะ");
            }
            else
            {
                addDam_panel_2.Show();
                createDam_step1_panel.Hide();
            }
        }

        private void addDam_cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void addDam_step2_cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void addDam_previous_btn_Click(object sender, EventArgs e)
        {
            addDam_panel_2.Hide();
            createDam_step1_panel.Show();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            string sql = "INSERT INTO dam_info(dam_id, dam_name, dam_baan, dam_geo_id, dam_geo_id_index, dam_subdistrict, dam_subdistrict_index, dam_district, dam_district_index, dam_province, dam_province_index, dam_length, dam_height, dam_width, min_at_sea_level, normal_at_sea_level, max_at_sea_level, crest_level, min_cap, normal_cap, max_cap, water_scale) values('" + dam_id.Text + "', '" + dam_name.Text + "', '" + dam_baan.Text + "', '" + (geo_id.SelectedItem as ComboboxItem).Value.ToString() + "', '" + geo_id.SelectedIndex.ToString() + "', '" + (dam_subdistrict.SelectedItem as ComboboxItem).Value.ToString() + "','" + dam_subdistrict.SelectedIndex.ToString() + "', '" + (dam_district.SelectedItem as ComboboxItem).Value.ToString() + "', '" + dam_district.SelectedIndex.ToString() + "', '" + (dam_province.SelectedItem as ComboboxItem).Value.ToString() + "', '" + dam_province.SelectedIndex.ToString() + "', '" + length.Text + "', '" + height.Text + "', '" + width.Text + "', '" + min_sea.Text + "', '" + normal_sea.Text + "', '" + max_sea.Text + "', '" + crest_level.Text + "', '" + min_cap.Text + "', '" + normal_cap.Text + "', '" + max_cap.Text + "', '" + (water_scale.SelectedItem as ComboboxItem).Value.ToString() + "')";
            mainForm.insertDB(sql);
            System.Threading.Thread.Sleep(100);

            sql = "SELECT TOP 1, * FROM dam_info ORDER BY dam_info.id_auto DESC";
            ArrayList c = mainForm.getDB(sql);

            foreach (ArrayList temp in c)
            {
                string dam_just_save_id = temp[0].ToString();

                mainForm.dam_auto_id_global = Convert.ToInt32(temp[0]);

                mainForm.TreeView_Traversal(DamPart_1, dam_just_save_id, "1");
                mainForm.TreeView_Traversal(DamPart_2, dam_just_save_id, "2");
                mainForm.TreeView_Traversal(DamPart_3, dam_just_save_id, "3");
                mainForm.copyfile(dam_just_save_id, pictureBox3);

                sql = "SELECT fill_checklist.ID, * FROM fill_checklist WHERE (((fill_checklist.dam_auto_id)='" + mainForm.dam_auto_id_global + "')) ORDER BY fill_checklist.ID DESC;";
                ArrayList checklist_sql = mainForm.getDB(sql);
                mainForm.Checklist_Added(checklist_sql);
            }
            System.Threading.Thread.Sleep(500);
            mainForm.renderListView();
            MessageBox.Show("บันทึกเรียบร้อยแล้ว");
            this.Cursor = Cursors.Default;
            mainForm.Cursor = Cursors.Default;
            this.DestroyHandle();
            this.Close();
        }

        private void DamPart_1_AfterCheck(object sender, TreeViewEventArgs e)
        {
            mainForm.Checktree_Parent(e.Node, e.Node.Checked);
            mainForm.Checktree_Child(e.Node, e.Node.Checked);
        }

        private void DamPart_2_AfterCheck(object sender, TreeViewEventArgs e)
        {
            mainForm.Checktree_Parent(e.Node, e.Node.Checked);
            mainForm.Checktree_Child(e.Node, e.Node.Checked);
        }

        private void DamPart_3_AfterCheck(object sender, TreeViewEventArgs e)
        {
            mainForm.Checktree_Parent(e.Node, e.Node.Checked);
            mainForm.Checktree_Child(e.Node, e.Node.Checked);
        }

        private void Upload_plan_Click(object sender, EventArgs e)
        {
            mainForm.browse_file(pictureBox3);
        }

        private void pictureBox3_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                if (pictureBox3.BackgroundImage != null)
                {
                    Point p = new Point(e.X, e.Y);
                    contextMenuStrip1.Show(pictureBox3, p);
                }
            }
            else
            {
                if (pictureBox3.BackgroundImage == null)
                {
                    mainForm.browse_file(pictureBox3);
                }
            }
        }

        private void Toolstrip_DelImage_Click(object sender, EventArgs e)
        {
            pictureBox3.BackgroundImage = null;
            Upload_plan.Show();
        }
    }
}
