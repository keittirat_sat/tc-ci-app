﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Resources;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("โปรแกรมคำนวณค่าดัชนีเขื่อน")]
[assembly: AssemblyDescription("โปรแกรมคำนวณค่าดัชนีเขื่อน (CI-Calculator)")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Damsafety : Royal Irrigation Department")]
[assembly: AssemblyProduct("CI-Calculator : Royal Irrigation Department")]
[assembly: AssemblyCopyright("Copyright © RID 2013")]
[assembly: AssemblyTrademark("Royal Irrigation Department")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("436ac724-4e77-49aa-9682-8d8d645f82a8")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
[assembly: AssemblyVersion("1.0.2.0")]
[assembly: AssemblyFileVersion("1.0.2.0")]
[assembly: NeutralResourcesLanguageAttribute("th-TH")]
