﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
//using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace Checklist
{
    public partial class ViewPlan : Form
    {
        public ViewPlan(Bitmap img)
        {
            InitializeComponent();
            viewport_box.BackgroundImage = img;
        }

        private void viewport_box_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
