﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
//using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json.Linq;

namespace Checklist
{
    public partial class Form1 : Form
    {
        public String appPath = Path.GetDirectoryName(Application.ExecutablePath);
        public String db_path = String.Empty;
        public int bootProgress;
        public string loadingState = "";
        public int g;
        private string damlist_selected_ID;
        public ArrayList properties_list;
        public ArrayList properties_checked;
        public String file_upload = String.Empty;
        public int dam_auto_id_global = 0;
        public String pathfile = String.Empty;
        public String connection_str;
        public String checklist_id;
        private ArrayList province, geo_db;
        public JObject weight;
        public String weight_raws;
        public Form1()
        {
            //Reset Prgrogress bar
            bootProgress = 0;

            //Load Profile
            // Read the file as one string. 
            weight_raws = System.IO.File.ReadAllText(appPath + @"\Profile\default.json");
            weight = JObject.Parse(weight_raws);

            //Console.Write(weight);

            //Init Database Path
            Form.CheckForIllegalCrossThreadCalls = false;
            db_path = appPath + @"\Database\DamsafeCI.mdb";
            connection_str = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + db_path;
            if (!File.Exists(db_path))
            {
                MessageBox.Show("ไม่พบไฟล์ฐานข้อมูล");
                return;
            }
            //Loading Page
            Thread t = new Thread(new ThreadStart(preLoadScreen));

            t.Start();
            
            InitializeComponent();
            
            try
            {
                //Render Dam List by Province and Geo
                renderListView();
            }
            catch(Exception d)
            {
                MessageBox.Show(d.ToString());
            }

            ArrayList geoList = getDB("select * from geography");
            geo_id.Items.Clear();
            ComboboxItem itemList;

            foreach (ArrayList _geo in geoList)
            {
                itemList = new ComboboxItem();
                itemList.Text = _geo[1].ToString();
                itemList.Value = _geo[0].ToString();
                geo_id.Items.Add(itemList);
            }
            geo_id.SelectedIndex = 0;

            /* Panel */
            OperationTab.Hide();

            /* Generate Dam Properties */
            string sql = "select * from list_menu order by list_menu.parent, list_menu.order ASC";
            properties_list = getDB(sql);

            loadingState = "เสร็จสมบูรณ์เตรียมเปิดโปรแกรม ....";
            Thread.Sleep(1000);
            statusbar_label.Text = "";

            itemList = new ComboboxItem();
            itemList.Text = "ม. ร.ท.ก.";
            itemList.Value = "0";
            water_scale.Items.Clear();
            water_scale.Items.Add(itemList);

            itemList = new ComboboxItem();
            itemList.Text = "ม. ร.ส.ม.";
            itemList.Value = "1";
            water_scale.Items.Add(itemList);
            t.Abort();
        }

        public void preLoadScreen()
        {
            Application.Run(new preLoading(this));
        }

        public TreeNode nodeSet(string ImageKey, int SelectedImageIndex, string label)
        {
            return nodeSet(ImageKey, SelectedImageIndex, label, false);
        }

        public TreeNode nodeSet(string ImageKey, int SelectedImageIndex, string label, Boolean isCheck)
        {
            TreeNode objNode = new TreeNode();
            objNode.Text = label;
            objNode.ForeColor = Color.Black;
            objNode.BackColor = Color.White;
            objNode.SelectedImageIndex = SelectedImageIndex;
            objNode.ImageKey = ImageKey;
            objNode.Checked = isCheck;
            return objNode;  
        }

        public ArrayList getDB(string queryString)
        {
            System.Data.OleDb.OleDbConnection conn = new System.Data.OleDb.OleDbConnection();
            //conn.ConnectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + db_path + ";Persist Security Info=False;";
            conn.ConnectionString = connection_str;
            System.Data.OleDb.OleDbCommand sql = new System.Data.OleDb.OleDbCommand();
            IDataReader ir;
            ArrayList data,temp;
            data = new ArrayList();
            try
            {
                conn.Open();
                sql.CommandText = queryString;
                sql.Connection = conn;
                using (ir = sql.ExecuteReader())
                {
                    while (ir.Read())
                    {
                        temp = new ArrayList();
                        for (int i = 0; i < ir.FieldCount; i++)
                        {
                            temp.Add(ir.GetValue(i).ToString());
                        }
                        data.Add(temp);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return data;
        }

        public int insertDB(string query)
        {
            System.Data.OleDb.OleDbConnection conn = new System.Data.OleDb.OleDbConnection();
            //conn.ConnectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + db_path + ";Persist Security Info=False;";
            conn.ConnectionString = connection_str;
            System.Data.OleDb.OleDbCommand sql = new System.Data.OleDb.OleDbCommand();
            int f = 0;
            try
            {
                conn.Open();
                sql.CommandText = query;
                sql.Connection = conn;
                f = sql.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return f;
        }

        public void renderListView()
        {
            renderListView(false);
        }

        public void renderListView(bool counter)
        {
            loadingState = "กำลังดึงค่าข้อมูลเขื่อน ....";
            bootProgress = 0;
            TreeNode mainList, provinceList, damList;
            DamList.Nodes.Clear();
            ArrayList dam;

            geo_db = getDB("select * from geography");
            province = getDB("select * from province");
            dam = getDB("SELECT dam_info.*, province.PROVINCE_NAME FROM dam_info INNER JOIN province ON dam_info.dam_province = province.PROVINCE_ID ORDER BY province.PROVINCE_NAME;");

            int co = dam.Count;
            int i = 0;
            double f = 0.0;

            foreach (ArrayList geo_arList in geo_db)
            {
                mainList = nodeSet("geo", Convert.ToInt32(geo_arList[0]), geo_arList[1].ToString());
                DamList.Nodes.Add(mainList);
                foreach (ArrayList province_arList in province)
                {
                    if (province_arList[3].ToString() == geo_arList[0].ToString())
                    {
                        provinceList = nodeSet("province", Convert.ToInt32(province_arList[0]), province_arList[2].ToString());
                        mainList.Nodes.Add(provinceList);

                        foreach (ArrayList dam_arrList in dam)
                        {
                            if (dam_arrList[10].ToString() == province_arList[0].ToString())
                            {
                                damList = nodeSet("dam", Convert.ToInt32(dam_arrList[0]), dam_arrList[2].ToString());
                                provinceList.Nodes.Add(damList);
                                i++;
                                f = Convert.ToDouble(i) / Convert.ToDouble(co);
                                bootProgress = Convert.ToInt32(f * 100.0);
                            }
                        }
                    }
                }
            }

            AllDamList.Nodes.Clear();
            getAllDamList(dam);

            view_by_regist.Nodes.Clear();
            getView_by_regist(dam);
        }

        public void getAllDamList(ArrayList dam)
        {
            TreeNode damList;
            foreach (ArrayList dam_arrList in dam)
            {
                damList = nodeSet("dam", Convert.ToInt32(dam_arrList[0]), dam_arrList[2].ToString() + " (" + dam_arrList[23].ToString().Trim() + ")");
                AllDamList.Nodes.Add(damList);
            }
        }

        public void getView_by_regist(ArrayList dam)
        {
            TreeNode item, eachdam_1;
            for (int i = 1; i <= 17; i++)
            {
                item = nodeSet("regist", 0, "สำนักชลประทานที่ " + i.ToString());
                view_by_regist.Nodes.Add(item);

                foreach (ArrayList province_detail in province)
                {
                    if (province_detail[4].ToString() == i.ToString())
                    {
                        foreach (ArrayList eachdam in dam)
                        {
                            if (eachdam[10].ToString() == province_detail[0].ToString())
                            {
                                eachdam_1 = nodeSet("dam", Convert.ToInt32(eachdam[0]), eachdam[2].ToString());
                                item.Nodes.Add(eachdam_1);
                            }
                        }
                    }
                }
            }
        }

        private void operationTab_control(TreeView x)
        {
            ArrayList local_db;
            switch (x.SelectedNode.ImageKey)
            {
                case "dam":
                    {
                        /* Dam Information Render */
                        this.Cursor = Cursors.AppStarting;
                        local_db = getDB("select * from dam_info where id_auto = " + x.SelectedNode.SelectedImageIndex.ToString());
                        if (local_db.Count > 0)
                        {
                            statusbar_label.Text = "กำลังเปิดรายละเอียดเขื่อน : " + x.SelectedNode.Text;
                            foreach (ArrayList damInfo in local_db)
                            {
                                dam_auto_id_global = Convert.ToInt32(damInfo[0]);

                                dam_id.Text = damInfo[1].ToString();
                                dam_name.Text = damInfo[2].ToString();
                                //Set dam name for top right label
                                dam_name_label.Text = damInfo[2].ToString();

                                dam_baan.Text = damInfo[3].ToString();

                                length.Text = damInfo[12].ToString();
                                height.Text = damInfo[13].ToString();
                                width.Text = damInfo[14].ToString();
                                min_sea.Text = damInfo[15].ToString();
                                normal_sea.Text = damInfo[16].ToString();
                                max_sea.Text = damInfo[17].ToString();
                                crest_level.Text = damInfo[18].ToString();
                                min_cap.Text = damInfo[19].ToString();
                                normal_cap.Text = damInfo[20].ToString();
                                max_cap.Text = damInfo[21].ToString();
                                geo_id.SelectedIndex = Convert.ToInt32(damInfo[5]);
                                dam_province.SelectedIndex = Convert.ToInt32(damInfo[11]);
                                dam_district.SelectedIndex = Convert.ToInt32(damInfo[9]);
                                dam_subdistrict.SelectedIndex = Convert.ToInt32(damInfo[7]);

                                water_scale.SelectedIndex = Convert.ToInt32(damInfo[22]);

                                pathfile = appPath + "\\Plan\\" + damInfo[0].ToString();
                                if (File.Exists(pathfile + ".BMP"))
                                {
                                    pathfile += ".BMP";
                                    planBox.BackgroundImage = new Bitmap(pathfile);
                                }
                                else if (File.Exists(pathfile + ".bmp"))
                                {
                                    pathfile += ".bmp";
                                    planBox.BackgroundImage = new Bitmap(pathfile);
                                }
                                else if (File.Exists(pathfile + ".JPG"))
                                {
                                    pathfile += ".JPG";
                                    planBox.BackgroundImage = new Bitmap(pathfile);
                                }
                                else if (File.Exists(pathfile + ".jpg"))
                                {
                                    pathfile += ".jpg";
                                    planBox.BackgroundImage = new Bitmap(pathfile);
                                }
                                else
                                {
                                    pathfile = String.Empty;
                                    planBox.BackgroundImage = null;
                                }
                            }
                        }
                        else
                        {
                            MessageBox.Show("ระบบการดึงข้อมูลเขื่อนทำงานผิดพลาด : " + DamList.SelectedNode.SelectedImageIndex.ToString());
                        }

                        /*Properties Tab Render*/
                        reset_dam_properties_btn.Enabled = false;
                        update_dam_properties_btn.Enabled = false;
                        damlist_selected_ID = dam_auto_id_global.ToString();
                        retrieve_dam_properties(damlist_selected_ID);
                        OperationTab.Show();

                        string sql = "SELECT fill_checklist.ID, * FROM fill_checklist WHERE (((fill_checklist.dam_auto_id)='" + dam_auto_id_global + "')) ORDER BY fill_checklist.ID DESC;";
                        ArrayList checklist_sql = getDB(sql);
                        Checklist_Added(checklist_sql);
                        this.Cursor = Cursors.Default;
                    } break;
                case "province":
                case "regist":
                case "geo": x.SelectedNode.Expand();
                    break;
                default:
                    MessageBox.Show("ระบบการจัดการเมนูทำงานผิดพลาด");
                    break;
            }

            statusbar_label.Text = String.Empty;
        }

        private void DamList_AfterSelect(object sender, TreeViewEventArgs e)
        {
            operationTab_control(DamList);
        }

        public void retrieve_dam_properties(string dam_id)
        {
            string sql = "select * from checklist where dam_id = '" + dam_id + "'";

            properties_checked = getDB(sql);

            probBox_1.Nodes.Clear();
            probBox_2.Nodes.Clear();
            probBox_3.Nodes.Clear();

            genRootView("1", probBox_1, true);
            genRootView("10", probBox_2, true);
            genRootView("89", probBox_3, true);

            probBox_1.ExpandAll();
            probBox_2.ExpandAll();
            probBox_3.ExpandAll();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            addNewDam x = new addNewDam(this);
            x.Show();
        }

        private void geo_id_SelectedIndexChanged(object sender, EventArgs e)
        {
            string _geo_id = (geo_id.SelectedItem as ComboboxItem).Value.ToString();
            string sql = "select * from province where GEO_ID = " + _geo_id;
            dam_province.ResetText();

            ArrayList local_db = getDB(sql);
            dam_province.Items.Clear();
            ComboboxItem itemList;
            foreach (ArrayList c in local_db)
            {
                itemList = new ComboboxItem();
                itemList.Text = c[2].ToString();
                itemList.Value = c[0].ToString();
                dam_province.Items.Add(itemList);
            }
            dam_province.SelectedIndex = 0;
        }

        private void dam_province_SelectedIndexChanged(object sender, EventArgs e)
        {
            string _province_id = (dam_province.SelectedItem as ComboboxItem).Value.ToString();
            string sql = "select * from amphur where PROVINCE_ID = " + _province_id;
            ArrayList local_db = getDB(sql);
            dam_district.ResetText();
            dam_district.Items.Clear();
            ComboboxItem itemList;
            foreach (ArrayList c in local_db)
            {
                itemList = new ComboboxItem();
                itemList.Text = c[2].ToString();
                itemList.Value = c[0].ToString();
                dam_district.Items.Add(itemList);
            }
            dam_district.SelectedIndex = 0;
        }

        private void dam_district_SelectedIndexChanged(object sender, EventArgs e)
        {
            string _dam_district = (dam_district.SelectedItem as ComboboxItem).Value.ToString();
            string sql = "select * from district where AMPHUR_ID = " + _dam_district;
            ArrayList local_db = getDB(sql);
            dam_subdistrict.ResetText();
            dam_subdistrict.Items.Clear();
            ComboboxItem itemList;
            foreach (ArrayList c in local_db)
            {
                itemList = new ComboboxItem();
                itemList.Text = c[2].ToString();
                itemList.Value = c[0].ToString();
                dam_subdistrict.Items.Add(itemList);
            }
            dam_subdistrict.SelectedIndex = 0;
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            //OpenFileDialog browseFile = new OpenFileDialog();
            SaveFileDialog browseFile = new SaveFileDialog();
            browseFile.Filter = "PDF Files (*.pdf)|*.pdf";
            browseFile.Title = "ใบตรวจสอบดัชนีสภาพเขื่อน";
            if (browseFile.ShowDialog() == DialogResult.Cancel)
            {
                return;
            }
            else
            {
                //new cl_export(browseFile.FileName, this).Show();
            }
        }

        private void damProp_1_Click(object sender, EventArgs e)
        {
        }

        private void damProp_2_Click(object sender, EventArgs e)
        {
        }

        private void damProp_3_Click(object sender, EventArgs e)
        {
        }

        public void Checktree_Child(TreeNode node, Boolean isChecked)
        {
            /*foreach (TreeNode item in node.Nodes)
            {
                item.Checked = isChecked;

                if (item.Nodes.Count > 0)
                {
                    this.Checktree_Child(item, isChecked);
                }
            }*/

            foreach (TreeNode child in node.Nodes)
            {
                if (!isChecked)
                {
                    child.Checked = isChecked;
                    Checktree_Child(child, isChecked);
                }
            }
        }

        public void Checktree_Parent(TreeNode node, Boolean isChecked)
        {
            try
            {
                if (isChecked)
                {
                    node.Parent.Checked = true;
                    Checktree_Parent(node.Parent, isChecked);
                }
            }
            catch
            {

            }
        }

        public void TreeView_Traversal(TreeView root, string dam_id, string tab_id)
        {
            foreach (TreeNode x in root.Nodes)
            {
                if (x.Checked)
                {
                    string sql = "insert into checklist (checklist_id, dam_id, tab_index) values (" + x.SelectedImageIndex.ToString() + "," + dam_id + "," + tab_id + ")";
                    insertDB(sql);
                    TreeNode_Traversal(x, dam_id, tab_id);
                }
            }
        }

        public void TreeNode_Traversal(TreeNode root, string dam_id, string tab_id)
        {
            foreach (TreeNode x in root.Nodes)
            {
                if (x.Checked)
                {
                    string sql = "insert into checklist (checklist_id, dam_id, tab_index) values (" + x.SelectedImageIndex.ToString() + "," + dam_id + "," + tab_id + ")";
                    insertDB(sql);
                    TreeNode_Traversal(x, dam_id, tab_id);
                }
            }
        }
        
        //public void genRootView(string parent = "0", TreeView root = null, Boolean check = false, Boolean fcheck = false)
        public void genRootView(string parent, TreeView root)
        {
            genRootView(parent, root, false, false);
        }
        
        public void genRootView(string parent, TreeView root, Boolean check)
        {
            genRootView(parent, root, check, false);
        }

        public void genRootView(string parent, TreeView root, Boolean check, Boolean fcheck)
        {            
            foreach (ArrayList item in properties_list)
            {
                if (item[1].ToString() == parent)
                {
                    TreeNode x = new TreeNode();
                    if (check)
                    {
                        x = nodeSet(item[1].ToString(), Convert.ToInt16(item[0]), item[3].ToString().Trim());
                        foreach (ArrayList i in properties_checked)
                        {
                            if (i[1].ToString() == item[0].ToString())
                            {
                                x = nodeSet(item[1].ToString(), Convert.ToInt16(item[0]), item[3].ToString().Trim(), true);
                            }
                        }
                    }
                    else
                    {
                        x = nodeSet(item[1].ToString(), Convert.ToInt16(item[0]), item[3].ToString().Trim(), fcheck);
                    }

                    root.Nodes.Add(x);
                    genPropertiesList(item[0].ToString(), x, check, fcheck);
                }
            }
        }

        //public void genPropertiesList(string parent = "0", TreeNode initTree = null, Boolean check = false, Boolean fcheck = false)
        public void genPropertiesList(string parent, TreeNode initTree)
        {
            genPropertiesList(parent, initTree, false, false);
        }

        public void genPropertiesList(string parent, TreeNode initTree, Boolean check)
        {
            genPropertiesList(parent, initTree, check, false);
        }
        
        public void genPropertiesList(string parent, TreeNode initTree, Boolean check, Boolean fcheck)
        {
            foreach (ArrayList item in properties_list)
            {
                if (item[1].ToString() == parent)
                {
                    TreeNode x = new TreeNode();
                    if (check)
                    {
                        x = nodeSet(item[1].ToString(), Convert.ToInt16(item[0]), item[3].ToString().Trim());
                        foreach (ArrayList i in properties_checked)
                        {
                            if (i[1].ToString() == item[0].ToString())
                            {
                                x = nodeSet(item[1].ToString(), Convert.ToInt16(item[0]), item[3].ToString().Trim(), true);
                            }
                        }                            
                    }
                    else
                    {
                        x = nodeSet(item[1].ToString(), Convert.ToInt16(item[0]), item[3].ToString().Trim(), fcheck);
                    }
                    initTree.Nodes.Add(x);
                    genPropertiesList(item[0].ToString(), x, check, fcheck);
                }
            }
        }

        public void remove_dam_propertiesby_id(string dam_id)
        {
            string sql = "DELETE FROM checklist WHERE dam_id='"+dam_id+"'";
            getDB(sql);
            Thread.Sleep(100);

        }

        private void reset_dam_properties_btn_Click(object sender, EventArgs e)
        {
            retrieve_dam_properties(damlist_selected_ID);
            reset_dam_properties_btn.Enabled = false;
            update_dam_properties_btn.Enabled = false;
        }

        private void update_dam_properties_btn_Click(object sender, EventArgs e)
        {
            statusbar_label.Text = "กำลังบันทึก";
            this.Cursor = Cursors.WaitCursor;
            remove_dam_propertiesby_id(damlist_selected_ID);
            TreeView_Traversal(probBox_1, damlist_selected_ID, "1");
            TreeView_Traversal(probBox_2, damlist_selected_ID, "2");
            TreeView_Traversal(probBox_3, damlist_selected_ID, "3");

            reset_dam_properties_btn.Enabled = false;
            update_dam_properties_btn.Enabled = false;
            statusbar_label.Text = "บันทึกเรียบร้อยแล้ว";
            this.Cursor = Cursors.Default;
        }

        bool busyp1 = false;
        private void probBox_1_AfterCheck(object sender, TreeViewEventArgs e)
        {
            reset_dam_properties_btn.Enabled = true;
            update_dam_properties_btn.Enabled = true;

            if (busyp1) return;
            busyp1 = true;
            try
            {
                Checktree_Parent(e.Node, e.Node.Checked);
                Checktree_Child(e.Node, e.Node.Checked);
            }
            finally
            {
                busyp1 = false;
            }
        }

        bool busyp2 = false;
        private void probBox_2_AfterCheck(object sender, TreeViewEventArgs e)
        {
            reset_dam_properties_btn.Enabled = true;
            update_dam_properties_btn.Enabled = true;

            if (busyp2) return;
            busyp2 = true;
            try
            {
                Checktree_Parent(e.Node, e.Node.Checked);
                Checktree_Child(e.Node, e.Node.Checked);
            }
            finally
            {
                busyp2 = false;
            }
        }

        bool busyp3 = false;
        private void probBox_3_AfterCheck(object sender, TreeViewEventArgs e)
        {
            reset_dam_properties_btn.Enabled = true;
            update_dam_properties_btn.Enabled = true;

            if (busyp3) return;
            busyp3 = true;
            try
            {
                Checktree_Parent(e.Node, e.Node.Checked);
                Checktree_Child(e.Node, e.Node.Checked);
            }
            finally
            {
                busyp3 = false;
            }
        }

        private void Toolstrip_DelImage_Click(object sender, EventArgs e)
        {
            planBox.BackgroundImage = null;

            try
            {
                if (File.Exists(@"Plan/" + dam_auto_id_global + ".jpg"))
                {
                    File.Delete(@"Plan/" + dam_auto_id_global + ".jpg");
                }

                if (File.Exists(@"Plan/" + dam_auto_id_global + ".JPG"))
                {
                    File.Delete(@"Plan/" + dam_auto_id_global + ".JPG");
                }

                if (File.Exists(@"Plan/" + dam_auto_id_global + ".bmp"))
                {
                    File.Delete(@"Plan/" + dam_auto_id_global + ".bmp");
                }

                if (File.Exists(@"Plan/" + dam_auto_id_global + ".BMP"))
                {
                    File.Delete(@"Plan/" + dam_auto_id_global + ".BMP");
                }
            }
            catch
            {
                MessageBox.Show("ไม่สามารถลบได้ ภาพนี้ถูกใช้ในโปรแกรมอื่นอยู่");
            }
        }

        //public void browse_file(PictureBox x, Boolean copy = false, string dam_id = "")
        public void browse_file(PictureBox x)
        {
            browse_file(x, false, "");
        }

        public void browse_file(PictureBox x, Boolean copy)
        {
            browse_file(x, copy, "");
        }

        public void browse_file(PictureBox x, Boolean copy , string dam_id)
        {
            file_upload = string.Empty;
            MessageBox.Show("ระบบรองรับรูปภาพ *.jpg, *.bmp เท่านั้นและขนาดที่แนะนำคือ กว้างไม่เกิน 1024px โดยทำการจัดรูปแบบผ่าน Photoshop เท่านั้น");
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "ไฟล์รูปภาพ|*.JPG; *.BMP";
            dialog.Title = "เลือกรูปภาพ";
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                file_upload = dialog.FileName;
                System.Drawing.Image img = new Bitmap(file_upload);
                if (img.Width <= img.Height)
                {
                    x.BackgroundImage = img;
                    if (copy)
                    {
                        copyfile(dam_id, x);
                    }
                }
                else
                {
                    MessageBox.Show("อัตราส่วนรูปภาพไม่ถูกต้อง");
                }
            }
            if (file_upload == String.Empty)
                return; //user didn't select a file to opena
        }

        public String browse_db()
        {
            file_upload = string.Empty;
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "ไฟล์ฐานข้อมูล|*.mdb;";
            //dialog.InitialDirectory = "C:";
            dialog.Title = "ไฟล์ฐานข้อมูล";
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                return dialog.FileName;
            }
            else
            {
                return String.Empty;
            }
        }

        private void Toolstrip_ChangeImage_Click(object sender, EventArgs e)
        {
            browse_file(planBox, true, dam_auto_id_global.ToString());
        }

        private void planBox_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                Point p = new Point(e.X, e.Y);
                contextMenuStrip1.Show(planBox, p);
            }
            else
            {
                if (planBox.BackgroundImage == null)
                {
                    browse_file(planBox, true, dam_auto_id_global.ToString());
                }
                else
                {
                    Form viewBiggerscreen = new ViewPlan(new Bitmap(pathfile));
                    viewBiggerscreen.Show();
                }
            }
        }

        public void copyfile(String dam_auto_id, PictureBox x)
        {
            if (x.BackgroundImage != null)
            {
                String upload_dir = appPath + "/Plan";

                //Create upload path in root Dir
                if (!Directory.Exists(upload_dir))
                {
                    Directory.CreateDirectory(upload_dir);
                }

                //Re-Format name structure
                upload_dir += "/" + dam_auto_id + Path.GetExtension(file_upload);

                //Copy to plan folder
                try
                {
                    File.Copy(file_upload, upload_dir, true);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            }
        }

        private void update_btn_dam_info_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            string sql = "UPDATE dam_info SET dam_id = '" + dam_id.Text + "', dam_name = '" + dam_name.Text + "', dam_baan = '" + dam_baan.Text + "', dam_geo_id = '" + (geo_id.SelectedItem as ComboboxItem).Value.ToString() + "', dam_geo_id_index = '" + geo_id.SelectedIndex.ToString() + "', dam_subdistrict = '" + (dam_subdistrict.SelectedItem as ComboboxItem).Value.ToString() + "', dam_subdistrict_index = '" + dam_subdistrict.SelectedIndex.ToString() + "', dam_district = '" + (dam_district.SelectedItem as ComboboxItem).Value.ToString() + "', dam_district_index = '" + dam_district.SelectedIndex.ToString() + "', dam_province = '" + (dam_province.SelectedItem as ComboboxItem).Value.ToString() + "', dam_province_index = '" + dam_province.SelectedIndex.ToString() + "', dam_length = '" + length.Text + "', dam_height = '" + height.Text + "', dam_width = '" + width.Text + "', min_at_sea_level = '" + min_sea.Text + "', normal_at_sea_level = '" + normal_sea.Text + "', max_at_sea_level = '" + max_sea.Text + "', crest_level = '" + crest_level.Text + "', min_cap = '" + min_cap.Text + "', normal_cap = '" + normal_cap.Text + "', max_cap = '" + max_cap.Text + "', water_scale = '" + (water_scale.SelectedItem as ComboboxItem).Value.ToString() + "' WHERE id_auto = " + dam_auto_id_global.ToString();
            getDB(sql);
            copyfile(dam_auto_id_global.ToString(), planBox);
            statusbar_label.Text = "ทำการอัพเดตเรียบร้อยแล้ว";
            System.Threading.Thread.Sleep(1000);
            this.Cursor = Cursors.Default;
            statusbar_label.Text = "";
            renderListView();
            MessageBox.Show("ทำการอัพเดตเรียบร้อยแล้ว");
        }

        private void AllDamList_AfterSelect(object sender, TreeViewEventArgs e)
        {
            operationTab_control(AllDamList);
        }

        public void Checklist_Added(ArrayList x)
        {
            this.Cursor = Cursors.AppStarting;
            ChecklistAdded.Rows.Clear();
            double score;
            foreach (ArrayList c in x)
            {
                score = Convert.ToDouble(c[3]) * 20.0;
                string[] time = new string[6];
                string temp = c[2].ToString();
                /* * * * * * * * * * * *
                 * time index
                 * 0    =>  year
                 * 1    =>  month
                 * 2    =>  date
                 * 3    =>  hour
                 * 4    =>  min
                 * 5    =>  sec
                 * * * * * * * * * * */
                for (int i = 0; i < 6; i++ )
                {
                    time[i] = temp[(i * 2)].ToString() + temp[(i * 2) + 1].ToString();
                }

                string datetime_custom = time[3] + ":" + time[4] + ":" + time[5] + "น. " + time[2] + "/" + time[1] + "/" + time[0];
                ChecklistAdded.Rows.Add(c[2].ToString(), datetime_custom, String.Format("{0:F2}", score));
            }
            this.Cursor = Cursors.Default;
        }

        private void ChecklistAdded_CellClick(object sender, DataGridViewCellEventArgs e)
        {
        }

        private void RegistDam_Checklist_Click(object sender, EventArgs e)
        {
            Form x = new ChecklistPanel(this, dam_auto_id_global, checklist_id);
            x.Show();
        }

        private Int32[] coor_checklist_added;

        private void ChecklistAdded_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                try
                {
                    Point pc = new Point(e.X, e.Y);
                    var p = ChecklistAdded.HitTest(e.X, e.Y);
                    ChecklistAdded.Rows[p.RowIndex].Selected = true;
                    checklist_added_menu.Show(ChecklistAdded, pc);
                    checklist_id = ChecklistAdded.Rows[p.RowIndex].Cells[0].Value.ToString();

                    //Make it Global
                    coor_checklist_added = new Int32[2];
                    coor_checklist_added[0] = e.X;
                    coor_checklist_added[1] = e.Y;
                }
                catch
                {
                }
            }
        }

        private void delete_checklistsheet_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("ต้องการลบตารางตรวจสอบหมายเลข "+checklist_id+" หรือไม่ ?", "Confirm delete", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                //Delete Checklist Header
                String sql = "delete from [fill_checklist] where [checklist_id] = '" + checklist_id + "'";
                getDB(sql);

                //Delete from Layer6_Calculate
                sql = "delete from [Layer6_Calculate] where [checklist_id] = '" + checklist_id + "'";
                getDB(sql);

                //Delete in each sheet
                for (int i = 1; i <= 24; i++)
                {
                    sql = "delete from [sheet_" + i.ToString() + "] where [checklist_id] = '" + checklist_id + "'";
                    getDB(sql);
                }

                var p = ChecklistAdded.HitTest(coor_checklist_added[0], coor_checklist_added[1]);
                ChecklistAdded.Rows.RemoveAt(p.RowIndex);
            }

        }

        private void export_table_Click(object sender, EventArgs e)
        {
            Thread x = new Thread(delegate()
            {
                int i = 0;
                while (true)
                {
                    string str = "กำลัง Export PDF file ";
                    switch (i % 6)
                    {
                        case 0: str += ""; break;
                        case 1: str += "."; break;
                        case 2: str += ". ."; break;
                        case 3: str += ". . ."; break;
                        case 4: str += ". . . ."; break;
                        case 5: str += ". . . . ."; break;
                    }
                    statusbar_label.Text = str;
                    i++;
                    Thread.Sleep(1000);
                }
            });
            x.Start();

            export_no_data();
            x.Abort();
            statusbar_label.Text = "ส่งออกเรียบร้อยแล้ว";
            Thread.Sleep(1000);
            statusbar_label.Text = "";
        }

        public void export_no_data()
        {
            String sql = "select * from fill_checklist where fill_checklist.checklist_id = '" + checklist_id + "'";
            String dam_id;
            Boolean[] damProp = new Boolean[200];
            ArrayList data = getDB(sql);
            int i = 0;

            //ArrayList dam_info = new ArrayList();
            String dam_info = String.Empty;
            String ascii = String.Empty;

            foreach (ArrayList temp in data)
            {
                dam_id = temp[1].ToString();
                sql = "select * from checklist where dam_id = '" + dam_id + "'";
                ArrayList data2 = getDB(sql);

                foreach (ArrayList prop in data2)
                {
                    damProp[Convert.ToInt32(prop[1])] = true;
                }

                //sql = "select * from dam_info where id_auto = " + dam_id;
                //sql = "SELECT dam_info.*, province.PROVINCE_NAME AS province_name, province.water_management, district.DISTRICT_NAME as district_name, amphur.AMPHUR_NAME as amphur_name FROM ((dam_info LEFT JOIN province ON dam_info.dam_province = province.PROVINCE_ID) INNER JOIN amphur ON dam_info.dam_district = amphur.AMPHUR_ID) INNER JOIN district ON dam_info.dam_subdistrict = district.DISTRICT_ID WHERE (((dam_info.id_auto)=" + dam_id + "));";
                sql = "SELECT dam_info.*, province.PROVINCE_NAME AS province_name, province.water_management, district.DISTRICT_NAME AS district_name, amphur.AMPHUR_NAME AS amphur_name, fill_checklist.* FROM (((dam_info LEFT JOIN province ON dam_info.dam_province = province.PROVINCE_ID) INNER JOIN amphur ON dam_info.dam_district = amphur.AMPHUR_ID) INNER JOIN district ON dam_info.dam_subdistrict = district.DISTRICT_ID) INNER JOIN fill_checklist ON dam_info.id_auto = CInt(fill_checklist.dam_auto_id) WHERE (((dam_info.id_auto)=" + dam_id + ") AND ((fill_checklist.checklist_id)='" + checklist_id + "'));";
                data2 = getDB(sql);

                dam_info = "[";
                i = 0;
                dam_info += "[";
                foreach (ArrayList prop in data2)
                {
                    foreach (String tempx in prop)
                    {
                        if (i != 0)
                        {
                            dam_info += ",";
                        }
                        dam_info += "\"" + tempx.Trim() + "\"";
                        i++;
                    }
                }
                dam_info += "],[";
                sql = "SELECT member.* FROM member WHERE (((member.checklist_id)='" + checklist_id + "'));";
                data2 = getDB(sql);
                i = 0;
                foreach (ArrayList prop in data2)
                {
                    if (i != 0)
                    {
                        dam_info += ",";
                    }
                    dam_info += "[\"" + prop[1] + "\",\"" + prop[2] + "\",\"" + prop[3] + "\"]";
                    i++;
                }
                dam_info += "]";
                dam_info += "]";

            }

            String json = "[";
            for (i = 0; i < 200; i++)
            {
                if (i != 0)
                {
                    json += ",";
                }
                if (damProp[i])
                {
                    json += "1";
                }
                else
                {
                    json += "0";
                }
            }
            json += "]";

            generate_pdf(dam_info, json);
        }

        private void generate_pdf(String dam_info, String json)
        {
            SaveFileDialog browseFile = new SaveFileDialog();
            browseFile.Filter = "PDF Files (*.pdf)|*.pdf";
            browseFile.Title = "บันทึกใบดัชนีสภาพเขื่อน";
            if (browseFile.ShowDialog() == DialogResult.Cancel)
            {
                return;
            }
            else
            {
                this.Cursor = Cursors.AppStarting;
                String PFile = browseFile.FileName;
                String abpath = appPath + @"\Template";

                Thread genPDF = new Thread(delegate()
                {
                    if (File.Exists(PFile))
                    {
                        try
                        {
                            File.Delete(PFile);
                        }
                        catch
                        {
                            MessageBox.Show("ไม่สามารถเขียนข้อมูลได้ : Read-Only File");
                            return;
                        }
                    }
                    System.IO.File.WriteAllText(abpath + @"\Param.dat", dam_info);
                    String php_path = abpath + @"\bin\php\php5.3.13\php.exe";
                    //String exec = abpath + @"\test.php --C_FILE=" + abpath + " --SAVE_AS=" + PFile + " --DAM_PROP=" + json + " --PRO_PATH=" + appPath;
                    String output_path = appPath + @"\Output\temp.pdf";
                    String exec = abpath + @"\test.php --C_FILE=" + abpath + " --SAVE_AS=" + output_path + " --DAM_PROP=" + json + " --PRO_PATH=" + appPath;
                    Process process = new Process();
                    ProcessStartInfo startInfo = new ProcessStartInfo(php_path, exec);

                    startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;

                    process.StartInfo = startInfo;
                    process.Start();
                    process.WaitForExit();

                    if (File.Exists(output_path))
                    {
                        File.Move(output_path, PFile);
                    }
                    else
                    {
                        MessageBox.Show("ไม่สามารถออกรายงานได้");
                    }
                });

                genPDF.Start();
                genPDF.Join();
                this.Cursor = Cursors.Default;

                new Thread(delegate()
                {
                    if(File.Exists(PFile))
                    {
                        try
                        {
                            Process p = new Process();
                            ProcessStartInfo s = new ProcessStartInfo(PFile);
                            p.StartInfo = s;
                            p.Start();
                        }
                        catch
                        {
                            MessageBox.Show("ไม่มีโปรแกรมรองรับการอ่าน PDF File");
                        }
                    }
                    else
                    {
                        MessageBox.Show("ไม่สามารถเซฟ File ลงจุดที่เลือกได้");
                    }
                }).Start();
            }
        }

        private void add_checklist_sheet_wo_project_Click(object sender, EventArgs e)
        {
            add_new_checklist_wo_project();
            string checklist_id = ChecklistAdded.Rows[0].Cells[0].Value.ToString();
            openChecklist(dam_auto_id_global, checklist_id);
        }

        private void view_by_regist_AfterSelect(object sender, TreeViewEventArgs e)
        {
            operationTab_control(view_by_regist);
        }

        public void add_new_checklist_wo_project()
        {
            string checklist_id = DateTime.Now.ToString("yyMMddHHmmss");
            string sql = "insert into fill_checklist (dam_auto_id, checklist_id, score) values('" + dam_auto_id_global.ToString() + "', '" + checklist_id + "', '0')";
            insertDB(sql);

            sql = "SELECT fill_checklist.ID, * FROM fill_checklist WHERE (((fill_checklist.dam_auto_id)='" + dam_auto_id_global + "')) ORDER BY fill_checklist.ID DESC;";
            ArrayList checklist_sql = getDB(sql);
            Checklist_Added(checklist_sql);
        }

        private void view_checklistsheet_Click(object sender, EventArgs e)
        {
            openChecklist(dam_auto_id_global, checklist_id);
        }

        private void openChecklist(int dam_auto_id_global, string checklist_id)
        {
            this.Cursor = Cursors.WaitCursor;
            Form x = new ChecklistPanel(this, dam_auto_id_global, checklist_id);
            x.Show();
        }

        private void ChecklistAdded_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                Point pc = new Point(e.X, e.Y);
                var p = ChecklistAdded.HitTest(e.X, e.Y);
                ChecklistAdded.Rows[p.RowIndex].Selected = true;
                checklist_id = ChecklistAdded.Rows[p.RowIndex].Cells[0].Value.ToString();
                openChecklist(dam_auto_id_global, checklist_id);
            }
        }

        private void export_data_with_data_Click(object sender, EventArgs e)
        {
            Thread x = new Thread(delegate()
            {
                int i = 0;
                while (true)
                {
                    string str = "กำลัง Export PDF file ";
                    switch (i % 6)
                    {
                        case 0: str += ""; break;
                        case 1: str += "."; break;
                        case 2: str += ". ."; break;
                        case 3: str += ". . ."; break;
                        case 4: str += ". . . ."; break;
                        case 5: str += ". . . . ."; break;
                    }
                    statusbar_label.Text = str;
                    i++;
                    Thread.Sleep(1000);
                }
            });
            x.Start();
            export_table_with_data();
            x.Abort();
            statusbar_label.Text = "ส่งออกเรียบร้อยแล้ว";
            Thread.Sleep(1000);
            statusbar_label.Text = "";
        }

        public String data_preparing(ArrayList raws, int total_cols)
        {
            return data_preparing(raws, total_cols, 1);
        }

        public String data_preparing(ArrayList raws, int total_cols, int element)
        {
            String json = string.Empty;
            if (element != 0)
            {
                json += ",";
            }
            json += "\"c" + raws[2] + "\":[";
            int limit = total_cols + 3;
            json += "[";
            for (int i = 3; i < limit; i++)
            {
                if (i != 3)
                {
                    json += ",";
                }
                json += "\"" + raws[i].ToString() + "\"";
            }
            json += "]]";
            return json;
        }

        public void export_table_with_data()
        {
            String sql = String.Empty;
            ArrayList data;
            String json = string.Empty;
            int comma_counter = 0, element = 0;
            json = "{";
            for (int i = 1; i <= 24; i++)
            {
                comma_counter = 0;
                sql = "SELECT sheet_" + i + ".* FROM sheet_" + i + " WHERE (((sheet_" + i + ".checklist_id)='" + checklist_id + "')) ORDER BY sheet_" + i + ".ID;";
                data = getDB(sql);
                switch (i)
                {
                    case 1:
                        {
                            if (i != 1)
                            {
                                json += ",";
                            }
                            json += "\"c5\":[";
                            foreach (ArrayList item in data)
                            {
                                if (comma_counter != 0)
                                {
                                    json += ",";
                                }
                                comma_counter++;
                                json += "[\"" + item[3] + "\",\"" + item[4] + "\",\"" + item[5] + "\",\"" + item[6] + "\",\"" + item[7] + "\",\"" + item[8] + "\",\"" + item[9] + "\",\"" + item[10] + "\",\"" + item[11] + "\",\"" + item[12] + "\",\"" + item[13] + "\"]";
                            }
                            json += "]";
                            element++;
                        }
                        break;
                    case 2:
                        {
                            if (i != 1)
                            {
                                json += ",";
                            }
                            json += "\"c6\":[";
                            foreach (ArrayList item in data)
                            {
                                if (comma_counter != 0)
                                {
                                    json += ",";
                                }
                                comma_counter++;
                                json += "[\"" + item[3] + "\",\"" + item[4] + "\",\"" + item[5] + "\",\"" + item[6] + "\",\"" + item[7] + "\",\"" + item[8] + "\",\"" + item[9] + "\",\"" + item[10] + "\",\"" + item[11] + "\",\"" + item[12] + "\"]";
                            }
                            json += "]";
                            element++;
                        } 
                        break;
                    case 3:
                        {
                            if (i != 1)
                            {
                                json += ",";
                            }
                            json += "\"c7\":[";
                            foreach (ArrayList item in data)
                            {
                                if (comma_counter != 0)
                                {
                                    json += ",";
                                }
                                comma_counter++;
                                json += "[\"" + item[3] + "\",\"" + item[4] + "\",\"" + item[5] + "\",\"" + item[6] + "\",\"" + item[7] + "\",\"" + item[8] + "\",\"" + item[9] + "\",\"" + item[10] + "\",\"" + item[11] + "\",\"" + item[12] + "\",\"" + item[13] + "\"]";
                            }
                            json += "]";
                            element++;
                        }
                        break;
                    case 4:
                        {
                            String tab3 = "\"c4\":[";
                            int t3 = 0, comma = 0;
                            foreach (ArrayList item in data)
                            {
                                switch (Convert.ToInt16(item[2]))
                                {
                                    case 4:
                                        if (t3 != 0)
                                        {
                                            tab3 += ",";
                                        }
                                        t3++;
                                        tab3 += "[\"" + item[3] + "\",\"" + item[4] + "\",\"" + item[5] + "\",\"" + item[6] + "\",\"" + item[7] + "\",\"" + item[8] + "\",\"" + item[9] + "\",\"" + item[10] + "\",\"" + item[11] + "\",\"" + item[12] + "\",\"" + item[13] + "\",\"" + item[14] + "\",\"" + item[15] + "\"]";
                                        break;
                                    case 8:
                                        json += data_preparing(item, 10, element);
                                        element++;
                                        break;
                                    case 9:
                                        json += data_preparing(item, 10, element);
                                        element++;
                                        break;
                                }
                                comma++;
                            }
                            tab3 += "]";
                            element++;
                            if (i != 1)
                            {
                                json += ",";
                            }
                            json += tab3;
                        }
                        break;
                    case 5:
                        foreach (ArrayList item in data)
                        {
                            switch (Convert.ToInt16(item[2]))
                            {
                                case 12:
                                case 14:
                                case 23:
                                    json += data_preparing(item, 6, element);
                                    element++;
                                    break;
                                case 15:
                                case 16:
                                case 24:
                                    json += data_preparing(item, 4, element);
                                    element++;
                                    break;
                                case 18:
                                    json += data_preparing(item, 5, element);
                                    element++;
                                    break;
                            }
                        }
                        break;
                    case 6:
                        foreach (ArrayList item in data)
                        {
                            switch (Convert.ToInt16(item[2]))
                            {
                                case 25: 
                                    json += data_preparing(item, 3, element);
                                    element++;
                                    break;
                                case 26:
                                case 27:
                                case 29:
                                case 30:
                                case 31:
                                    json += data_preparing(item, 5, element);
                                    element++;
                                    break;
                                case 33:
                                    json += data_preparing(item, 11, element);
                                    element++;
                                    break;
                            }
                        }
                        break;
                    case 7:
                        foreach (ArrayList item in data)
                        {
                            switch (Convert.ToInt16(item[2]))
                            {
                                case 34:
                                case 36:
                                    json += data_preparing(item, 10, element);
                                    element++;
                                    break;
                                case 35:
                                    json += data_preparing(item, 11, element);
                                    element++;
                                    break;
                                case 22:
                                    json += data_preparing(item, 12, element);
                                    element++;
                                    break;
                            }
                        }
                        break;
                    case 8:
                        foreach (ArrayList item in data)
                        {
                            switch (Convert.ToInt16(item[2]))
                            {
                                case 46:
                                case 48:
                                case 52:
                                    json += data_preparing(item, 6, element);
                                    element++;
                                    break;
                                case 49:
                                case 50:
                                case 53:
                                    json += data_preparing(item, 4, element);
                                    element++;
                                    break;
                                case 51:
                                    json += data_preparing(item, 5, element);
                                    element++;
                                    break;
                            }
                        }
                        break;
                    case 9:
                        foreach (ArrayList item in data)
                        {
                            switch (Convert.ToInt16(item[2]))
                            {
                                case 54:
                                    json += data_preparing(item, 3, element);
                                    element++;
                                    break;
                                case 55:
                                case 56:
                                case 58:
                                case 59:
                                case 60:
                                    json += data_preparing(item, 5, element);
                                    element++;
                                    break;
                                case 62:
                                    json += data_preparing(item, 11, element);
                                    element++;
                                    break;
                            }
                        }
                        break;
                    case 10:
                        foreach (ArrayList item in data)
                        {
                            switch (Convert.ToInt16(item[2]))
                            {
                                case 63:
                                case 65:
                                    json += data_preparing(item, 10, element);
                                    element++;
                                    break;
                                case 64:
                                    json += data_preparing(item, 11, element);
                                    element++;
                                    break;
                                case 45:
                                    json += data_preparing(item, 12, element);
                                    element++;
                                    break;
                            }
                        }
                        break;
                    case 11:
                        foreach (ArrayList item in data)
                        {
                            switch (Convert.ToInt16(item[2]))
                            {
                                case 72:
                                case 74:
                                case 78:
                                    json += data_preparing(item, 6, element);
                                    element++;
                                    break;
                                case 75:
                                case 76:
                                case 79:
                                    json += data_preparing(item, 4, element);
                                    element++;
                                    break;
                                case 77:
                                    json += data_preparing(item, 5, element);
                                    element++;
                                    break;
                            }
                        }
                        break;
                    case 12:
                        foreach (ArrayList item in data)
                        {
                            switch (Convert.ToInt16(item[2]))
                            {
                                case 80:
                                    json += data_preparing(item, 3, element);
                                    element++;
                                    break;
                                case 81:
                                case 82:
                                case 150:
                                case 151:
                                case 152:
                                    json += data_preparing(item, 5, element);
                                    element++;
                                    break;
                                case 85:
                                    json += data_preparing(item, 11, element);
                                    element++;
                                    break;
                            }
                        }
                        break;
                    case 13:
                        foreach (ArrayList item in data)
                        {
                            switch (Convert.ToInt16(item[2]))
                            {
                                case 86:
                                case 88:
                                    json += data_preparing(item, 10, element);
                                    element++;
                                    break;
                                case 87:
                                case 71:
                                    json += data_preparing(item, 12, element);
                                    element++;
                                    break;
                            }
                        }
                        break;
                    case 14:
                        foreach (ArrayList item in data)
                        {
                            switch (Convert.ToInt16(item[2]))
                            {
                                case 101:
                                    json += data_preparing(item, 11, element);
                                    element++;
                                    break;
                                case 102:
                                    json += data_preparing(item, 10, element);
                                    element++;
                                    break;
                                case 103:
                                    json += data_preparing(item, 9, element);
                                    element++;
                                    break;
                            }
                        }
                        break;
                    case 15:
                        foreach (ArrayList item in data)
                        {
                            switch (Convert.ToInt16(item[2]))
                            {
                                case 104:
                                case 106:
                                    json += data_preparing(item, 11, element);
                                    element++;
                                    break;
                                case 105:
                                case 107:
                                    json += data_preparing(item, 10, element);
                                    element++;
                                    break;
                            }
                        }
                        break;
                    case 16:
                        foreach (ArrayList item in data)
                        {
                            switch (Convert.ToInt16(item[2]))
                            {
                                case 99:
                                    json += data_preparing(item, 12, element);
                                    element++;
                                    break;
                                case 108:
                                case 109:
                                case 110:
                                    json += data_preparing(item, 5, element);
                                    element++;
                                    break;
                            }
                        }
                        break;
                    case 17:
                        foreach (ArrayList item in data)
                        {
                            switch (Convert.ToInt16(item[2]))
                            {
                                case 117:
                                    json += data_preparing(item, 11, element);
                                    element++;
                                    break;
                                case 118:
                                    json += data_preparing(item, 10, element);
                                    element++;
                                    break;
                                case 119:
                                    json += data_preparing(item, 9, element);
                                    element++;
                                    break;
                                case 126:
                                    json += data_preparing(item, 5, element);
                                    element++;
                                    break;
                            }
                        }
                        break;
                    case 18:
                        foreach (ArrayList item in data)
                        {
                            switch (Convert.ToInt16(item[2]))
                            {
                                case 127:
                                case 128:
                                    json += data_preparing(item, 5, element);
                                    element++;
                                    break;
                                case 121:
                                    json += data_preparing(item, 11, element);
                                    element++;
                                    break;
                                case 122:
                                    json += data_preparing(item, 10, element);
                                    element++;
                                    break;
                            }
                        }
                        break;
                    case 19:
                        foreach (ArrayList item in data)
                        {
                            switch (Convert.ToInt16(item[2]))
                            {
                                case 123:
                                    json += data_preparing(item, 5, element);
                                    element++;
                                    break;
                                case 124:
                                    json += data_preparing(item, 4, element);
                                    element++;
                                    break;
                                case 125:
                                    json += data_preparing(item, 3, element);
                                    element++;
                                    break;
                                case 129:
                                    json += data_preparing(item, 11, element);
                                    element++;
                                    break;
                            }
                        }
                        break;
                    case 20:
                        foreach (ArrayList item in data)
                        {
                            switch (Convert.ToInt16(item[2]))
                            {
                                case 131:
                                    json += data_preparing(item, 11, element);
                                    element++;
                                    break;
                                case 130:
                                case 132:
                                    json += data_preparing(item, 10, element);
                                    element++;
                                    break;
                            }
                        }
                        break;
                    case 21:
                        foreach (ArrayList item in data)
                        {
                            switch (Convert.ToInt16(item[2]))
                            {
                                case 115:
                                    json += data_preparing(item, 12, element);
                                    element++;
                                    break;
                                case 134:
                                    json += data_preparing(item, 6, element);
                                    element++;
                                    break;
                                case 133:
                                case 135:
                                    json += data_preparing(item, 5, element);
                                    element++;
                                    break;
                            }
                        }
                        break;
                    case 22:
                        foreach (ArrayList item in data)
                        {
                            switch (Convert.ToInt16(item[2]))
                            {
                                case 136:
                                    json += data_preparing(item, 6, element);
                                    element++;
                                    break;
                                case 141:
                                    json += data_preparing(item, 5, element);
                                    element++;
                                    break;
                                case 142:
                                    json += data_preparing(item, 11, element);
                                    element++;
                                    break;
                                case 143:
                                    json += data_preparing(item, 10, element);
                                    element++;
                                    break;
                            }
                        }
                        break;
                    case 23:
                        foreach (ArrayList item in data)
                        {
                            switch (Convert.ToInt16(item[2]))
                            {
                                case 144:
                                    json += data_preparing(item, 11, element);
                                    element++;
                                    break;
                                case 145:
                                    json += data_preparing(item, 10, element);
                                    element++;
                                    break;
                                case 140:
                                    json += data_preparing(item, 12, element);
                                    element++;
                                    break;
                            }
                        }
                        break;
                    case 24:
                        foreach (ArrayList item in data)
                        {
                            switch (Convert.ToInt16(item[2]))
                            {
                                case 146:
                                case 147:
                                case 148:
                                    json += data_preparing(item, 6, element);
                                    element++;
                                    break;
                            }
                        }
                        break;
                }
            }
            json += "}";
            System.IO.File.WriteAllText(appPath + @"\Template\RawsData.dat", json);
            export_no_data();
        }

        private void cancel_btn_dam_info_Click(object sender, EventArgs e)
        {
            delete_dam();
        }

        public void delete_dam()
        {
            if (MessageBox.Show("ต้องการลบเขื่อนที่เลือกไว้ใช่หรือไม่ ?", "Confirm delete", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                this.Cursor = Cursors.WaitCursor;
                Thread x = new Thread(delegate()
                {
                    statusbar_label.Text = "กำลังลบข้อมูลพื้นฐานเขื่อน";
                    String sql = "delete from dam_info where [id_auto] = " + dam_auto_id_global;
                    getDB(sql);

                    statusbar_label.Text = "กำลังลบข้อมูลคุณลักษณะเขื่อน";
                    sql = "delete from checklist where [dam_id] = '" + dam_auto_id_global + "'";
                    getDB(sql);

                    statusbar_label.Text = "กำลังลบข้อมูล CI";
                    sql = "select * from fill_checklist where [dam_auto_id] = '" + dam_auto_id_global + "'";
                    ArrayList checklist = getDB(sql);

                    foreach (ArrayList ci in checklist)
                    {
                        string checklist_id = ci[2].ToString();

                        statusbar_label.Text = "กำลังลบข้อมูลคำนวน";
                        sql = "delete from Layer6_Calculate where [checklist_id] = '" + checklist_id + "'";
                        getDB(sql);

                        statusbar_label.Text = "กำลังลบข้อมูลชิ้นส่วนที่สึกหรอ";
                        sql = "delete from risk_part where [checklist_id] = '" + checklist_id + "'";
                        getDB(sql);

                        statusbar_label.Text = "กำลังลบข้อมูลดิบ CI";
                        for (int index = 1; index <= 24; index++)
                        {
                            sql = "delete from sheet_" + index + " where [checklist_id] = '" + checklist_id + "'";
                            getDB(sql);
                        }
                    }

                    statusbar_label.Text = "กำลังลบข้อมูล CI";
                    sql = "delete from fill_checklist where [dam_auto_id] = '" + dam_auto_id_global + "'";
                    getDB(sql);
                    statusbar_label.Text = "ลบข้อมูลสำเร็จ";
                });
                x.Start();
                x.Join();

                OperationTab.Hide();
                renderListView();
                Thread.Sleep(1000);
                statusbar_label.Text = "";

                this.Cursor = Cursors.Default;
            }
        }

        private void export_report_Click(object sender, EventArgs e)
        {
            Form export_form = new export_result(this, checklist_id);
            export_form.Show();
        }

        private void dam_subdistrict_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Console.WriteLine((dam_subdistrict.SelectedItem as ComboboxItem).Value.ToString());
        }

        private void profile_btn_Click(object sender, EventArgs e)
        {
            JArray current_weight = (JArray)weight.GetValue("5");
            Console.WriteLine(current_weight);
            foreach(double temp in current_weight){
                Console.WriteLine("Contents of WriteText.txt = {0} {1}", temp.ToString(), current_weight.Count);
            }
            Form change_profile = new CI_Calculator.profile_form(this);
            change_profile.Show();
        }
    }

    public class ComboboxItem
    {
        public string Text
        {
            get; set;
        }
        public string Value
        {
            get; set;
        }

        public override string ToString()
        {
            return Text;
        }
    }

    public class ComboboxItemWithParent
    {
        public string Text
        {
            get; set;
        }
        public Int32 Value
        {
            get; set;
        }
        public Int32 Parent
        {
            get; set;
        }
        public Int32 Order
        {
            get; set;
        }

        public override string ToString()
        {
            return Text;
        }
    }
}
