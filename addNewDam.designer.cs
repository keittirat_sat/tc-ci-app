﻿namespace Checklist
{
    partial class addNewDam
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(addNewDam));
            this.createDam_step1_panel = new System.Windows.Forms.Panel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dam_subdistrict = new System.Windows.Forms.ComboBox();
            this.dam_district = new System.Windows.Forms.ComboBox();
            this.dam_province = new System.Windows.Forms.ComboBox();
            this.dam_baan = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.addDam_next = new System.Windows.Forms.Button();
            this.addDam_cancel = new System.Windows.Forms.Button();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label21 = new System.Windows.Forms.Label();
            this.water_scale = new System.Windows.Forms.ComboBox();
            this.crest_level = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.max_sea = new System.Windows.Forms.TextBox();
            this.min_sea = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.normal_sea = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.max_cap = new System.Windows.Forms.TextBox();
            this.min_cap = new System.Windows.Forms.TextBox();
            this.normal_cap = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label9 = new System.Windows.Forms.Label();
            this.height = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.width = new System.Windows.Forms.TextBox();
            this.length = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.geo_id = new System.Windows.Forms.ComboBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.dam_id = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.dam_name = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.createDam_btn = new System.Windows.Forms.Button();
            this.addDam_panel_2 = new System.Windows.Forms.Panel();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.Upload_plan = new System.Windows.Forms.Button();
            this.CheckList_Property = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.DamPart_1 = new System.Windows.Forms.TreeView();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.DamPart_2 = new System.Windows.Forms.TreeView();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.DamPart_3 = new System.Windows.Forms.TreeView();
            this.label20 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.addDam_previous_btn = new System.Windows.Forms.Button();
            this.addDam_step2_cancel = new System.Windows.Forms.Button();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.Toolstrip_ChangeImage = new System.Windows.Forms.ToolStripMenuItem();
            this.Toolstrip_DelImage = new System.Windows.Forms.ToolStripMenuItem();
            this.createDam_step1_panel.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.addDam_panel_2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.CheckList_Property.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // createDam_step1_panel
            // 
            this.createDam_step1_panel.Controls.Add(this.groupBox2);
            this.createDam_step1_panel.Controls.Add(this.addDam_next);
            this.createDam_step1_panel.Controls.Add(this.addDam_cancel);
            this.createDam_step1_panel.Controls.Add(this.groupBox5);
            this.createDam_step1_panel.Controls.Add(this.groupBox4);
            this.createDam_step1_panel.Controls.Add(this.groupBox3);
            this.createDam_step1_panel.Controls.Add(this.groupBox1);
            this.createDam_step1_panel.Controls.Add(this.label2);
            this.createDam_step1_panel.Controls.Add(this.pictureBox1);
            this.createDam_step1_panel.Controls.Add(this.label1);
            this.createDam_step1_panel.Location = new System.Drawing.Point(6, 6);
            this.createDam_step1_panel.Name = "createDam_step1_panel";
            this.createDam_step1_panel.Size = new System.Drawing.Size(534, 461);
            this.createDam_step1_panel.TabIndex = 35;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dam_subdistrict);
            this.groupBox2.Controls.Add(this.dam_district);
            this.groupBox2.Controls.Add(this.dam_province);
            this.groupBox2.Controls.Add(this.dam_baan);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Location = new System.Drawing.Point(7, 165);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(259, 127);
            this.groupBox2.TabIndex = 39;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "สถานที่ตั้ง";
            // 
            // dam_subdistrict
            // 
            this.dam_subdistrict.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.dam_subdistrict.FormattingEnabled = true;
            this.dam_subdistrict.Location = new System.Drawing.Point(54, 43);
            this.dam_subdistrict.Name = "dam_subdistrict";
            this.dam_subdistrict.Size = new System.Drawing.Size(198, 21);
            this.dam_subdistrict.TabIndex = 9;
            // 
            // dam_district
            // 
            this.dam_district.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.dam_district.FormattingEnabled = true;
            this.dam_district.Location = new System.Drawing.Point(54, 70);
            this.dam_district.Name = "dam_district";
            this.dam_district.Size = new System.Drawing.Size(198, 21);
            this.dam_district.TabIndex = 10;
            this.dam_district.SelectedIndexChanged += new System.EventHandler(this.dam_district_SelectedIndexChanged_1);
            // 
            // dam_province
            // 
            this.dam_province.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.dam_province.FormattingEnabled = true;
            this.dam_province.Location = new System.Drawing.Point(54, 96);
            this.dam_province.Name = "dam_province";
            this.dam_province.Size = new System.Drawing.Size(198, 21);
            this.dam_province.TabIndex = 11;
            this.dam_province.SelectedIndexChanged += new System.EventHandler(this.dam_province_SelectedIndexChanged_1);
            // 
            // dam_baan
            // 
            this.dam_baan.Location = new System.Drawing.Point(54, 17);
            this.dam_baan.Name = "dam_baan";
            this.dam_baan.Size = new System.Drawing.Size(198, 20);
            this.dam_baan.TabIndex = 8;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(15, 47);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(32, 13);
            this.label6.TabIndex = 18;
            this.label6.Text = "ตำบล";
            this.label6.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(11, 73);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(36, 13);
            this.label7.TabIndex = 19;
            this.label7.Text = "อำเภอ";
            this.label7.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(8, 20);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(40, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "ชื่อบ้าน";
            this.label5.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(9, 99);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(38, 13);
            this.label8.TabIndex = 20;
            this.label8.Text = "จังหวัด";
            this.label8.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // addDam_next
            // 
            this.addDam_next.Location = new System.Drawing.Point(370, 430);
            this.addDam_next.Name = "addDam_next";
            this.addDam_next.Size = new System.Drawing.Size(75, 23);
            this.addDam_next.TabIndex = 45;
            this.addDam_next.Text = "ถัดไป";
            this.addDam_next.UseVisualStyleBackColor = true;
            this.addDam_next.Click += new System.EventHandler(this.addDam_next_Click);
            // 
            // addDam_cancel
            // 
            this.addDam_cancel.Location = new System.Drawing.Point(451, 430);
            this.addDam_cancel.Name = "addDam_cancel";
            this.addDam_cancel.Size = new System.Drawing.Size(75, 23);
            this.addDam_cancel.TabIndex = 44;
            this.addDam_cancel.Text = "ยกเลิก";
            this.addDam_cancel.UseVisualStyleBackColor = true;
            this.addDam_cancel.Click += new System.EventHandler(this.addDam_cancel_Click);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label21);
            this.groupBox5.Controls.Add(this.water_scale);
            this.groupBox5.Controls.Add(this.crest_level);
            this.groupBox5.Controls.Add(this.label18);
            this.groupBox5.Controls.Add(this.max_sea);
            this.groupBox5.Controls.Add(this.min_sea);
            this.groupBox5.Controls.Add(this.label17);
            this.groupBox5.Controls.Add(this.normal_sea);
            this.groupBox5.Controls.Add(this.label16);
            this.groupBox5.Controls.Add(this.label15);
            this.groupBox5.Location = new System.Drawing.Point(273, 274);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(253, 150);
            this.groupBox5.TabIndex = 42;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "ระดับน้ำ";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(6, 20);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(35, 13);
            this.label21.TabIndex = 36;
            this.label21.Text = "หน่วย";
            this.label21.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // water_scale
            // 
            this.water_scale.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.water_scale.FormattingEnabled = true;
            this.water_scale.Items.AddRange(new object[] {
            "ม. ร.ท.ก.",
            "ม. ร.ส.ม."});
            this.water_scale.Location = new System.Drawing.Point(47, 17);
            this.water_scale.Name = "water_scale";
            this.water_scale.Size = new System.Drawing.Size(197, 21);
            this.water_scale.TabIndex = 35;
            // 
            // crest_level
            // 
            this.crest_level.Location = new System.Drawing.Point(63, 122);
            this.crest_level.Name = "crest_level";
            this.crest_level.Size = new System.Drawing.Size(182, 20);
            this.crest_level.TabIndex = 33;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(11, 125);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(46, 13);
            this.label18.TabIndex = 34;
            this.label18.Text = "สันเขื่อน";
            this.label18.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // max_sea
            // 
            this.max_sea.Location = new System.Drawing.Point(47, 96);
            this.max_sea.Name = "max_sea";
            this.max_sea.Size = new System.Drawing.Size(198, 20);
            this.max_sea.TabIndex = 29;
            // 
            // min_sea
            // 
            this.min_sea.Location = new System.Drawing.Point(47, 44);
            this.min_sea.Name = "min_sea";
            this.min_sea.Size = new System.Drawing.Size(198, 20);
            this.min_sea.TabIndex = 27;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(13, 73);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(28, 13);
            this.label17.TabIndex = 31;
            this.label17.Text = "ปกติ";
            this.label17.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // normal_sea
            // 
            this.normal_sea.Location = new System.Drawing.Point(47, 70);
            this.normal_sea.Name = "normal_sea";
            this.normal_sea.Size = new System.Drawing.Size(198, 20);
            this.normal_sea.TabIndex = 28;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(10, 99);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(31, 13);
            this.label16.TabIndex = 32;
            this.label16.Text = "สูงสุด";
            this.label16.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(9, 47);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(32, 13);
            this.label15.TabIndex = 30;
            this.label15.Text = "ต่ำสุด";
            this.label15.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.max_cap);
            this.groupBox4.Controls.Add(this.min_cap);
            this.groupBox4.Controls.Add(this.normal_cap);
            this.groupBox4.Controls.Add(this.label12);
            this.groupBox4.Controls.Add(this.label14);
            this.groupBox4.Controls.Add(this.label13);
            this.groupBox4.Location = new System.Drawing.Point(8, 298);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(259, 103);
            this.groupBox4.TabIndex = 41;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "ปริมาตรความจุ (ล้าน ลบ.ม)";
            // 
            // max_cap
            // 
            this.max_cap.Location = new System.Drawing.Point(53, 73);
            this.max_cap.Name = "max_cap";
            this.max_cap.Size = new System.Drawing.Size(198, 20);
            this.max_cap.TabIndex = 17;
            // 
            // min_cap
            // 
            this.min_cap.Location = new System.Drawing.Point(53, 21);
            this.min_cap.Name = "min_cap";
            this.min_cap.Size = new System.Drawing.Size(198, 20);
            this.min_cap.TabIndex = 15;
            // 
            // normal_cap
            // 
            this.normal_cap.Location = new System.Drawing.Point(53, 47);
            this.normal_cap.Name = "normal_cap";
            this.normal_cap.Size = new System.Drawing.Size(198, 20);
            this.normal_cap.TabIndex = 16;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(15, 24);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(32, 13);
            this.label12.TabIndex = 24;
            this.label12.Text = "ต่ำสุด";
            this.label12.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(16, 76);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(31, 13);
            this.label14.TabIndex = 26;
            this.label14.Text = "สูงสุด";
            this.label14.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(19, 50);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(28, 13);
            this.label13.TabIndex = 25;
            this.label13.Text = "ปกติ";
            this.label13.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.height);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.width);
            this.groupBox3.Controls.Add(this.length);
            this.groupBox3.Location = new System.Drawing.Point(273, 165);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(253, 103);
            this.groupBox3.TabIndex = 40;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "รายละเอียดทางโครงสร้างเขื่อน (เมตร)";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(16, 50);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(25, 13);
            this.label9.TabIndex = 21;
            this.label9.Text = "ยาว";
            this.label9.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // height
            // 
            this.height.Location = new System.Drawing.Point(47, 73);
            this.height.Name = "height";
            this.height.Size = new System.Drawing.Size(199, 20);
            this.height.TabIndex = 14;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(11, 24);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(30, 13);
            this.label11.TabIndex = 23;
            this.label11.Text = "กว้าง";
            this.label11.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(23, 76);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(18, 13);
            this.label10.TabIndex = 22;
            this.label10.Text = "สูง";
            this.label10.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // width
            // 
            this.width.Location = new System.Drawing.Point(47, 21);
            this.width.Name = "width";
            this.width.Size = new System.Drawing.Size(199, 20);
            this.width.TabIndex = 12;
            // 
            // length
            // 
            this.length.Location = new System.Drawing.Point(47, 47);
            this.length.Name = "length";
            this.length.Size = new System.Drawing.Size(199, 20);
            this.length.TabIndex = 13;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.geo_id);
            this.groupBox1.Controls.Add(this.label19);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.dam_id);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.dam_name);
            this.groupBox1.Location = new System.Drawing.Point(171, 58);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(355, 100);
            this.groupBox1.TabIndex = 38;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "ชื่อและรหัส";
            // 
            // geo_id
            // 
            this.geo_id.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.geo_id.FormattingEnabled = true;
            this.geo_id.Location = new System.Drawing.Point(65, 72);
            this.geo_id.Name = "geo_id";
            this.geo_id.Size = new System.Drawing.Size(282, 21);
            this.geo_id.TabIndex = 8;
            this.geo_id.SelectedIndexChanged += new System.EventHandler(this.geo_id_SelectedIndexChanged_1);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(19, 75);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(40, 13);
            this.label19.TabIndex = 7;
            this.label19.Text = "ภูมิภาค";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "รหัสเขื่อน";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // dam_id
            // 
            this.dam_id.Location = new System.Drawing.Point(65, 19);
            this.dam_id.Name = "dam_id";
            this.dam_id.Size = new System.Drawing.Size(282, 20);
            this.dam_id.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(14, 48);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "ชื่อเขื่อน";
            this.label4.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // dam_name
            // 
            this.dam_name.Location = new System.Drawing.Point(65, 45);
            this.dam_name.Name = "dam_name";
            this.dam_name.Size = new System.Drawing.Size(282, 20);
            this.dam_name.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(169, 33);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 13);
            this.label2.TabIndex = 37;
            this.label2.Text = "กรมขลประทาน";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(7, 8);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(150, 150);
            this.pictureBox1.TabIndex = 36;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(166, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(337, 20);
            this.label1.TabIndex = 35;
            this.label1.Text = "แบบบันทึกการตรวจสภาพเขื่อน ส่วนความปลอดภัยเขื่อน";
            // 
            // createDam_btn
            // 
            this.createDam_btn.Location = new System.Drawing.Point(370, 430);
            this.createDam_btn.Name = "createDam_btn";
            this.createDam_btn.Size = new System.Drawing.Size(75, 23);
            this.createDam_btn.TabIndex = 43;
            this.createDam_btn.Text = "สร้าง";
            this.createDam_btn.UseVisualStyleBackColor = true;
            this.createDam_btn.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // addDam_panel_2
            // 
            this.addDam_panel_2.Controls.Add(this.pictureBox3);
            this.addDam_panel_2.Controls.Add(this.Upload_plan);
            this.addDam_panel_2.Controls.Add(this.CheckList_Property);
            this.addDam_panel_2.Controls.Add(this.label20);
            this.addDam_panel_2.Controls.Add(this.pictureBox2);
            this.addDam_panel_2.Controls.Add(this.addDam_previous_btn);
            this.addDam_panel_2.Controls.Add(this.addDam_step2_cancel);
            this.addDam_panel_2.Controls.Add(this.createDam_btn);
            this.addDam_panel_2.Location = new System.Drawing.Point(6, 6);
            this.addDam_panel_2.Name = "addDam_panel_2";
            this.addDam_panel_2.Size = new System.Drawing.Size(534, 461);
            this.addDam_panel_2.TabIndex = 46;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox3.Location = new System.Drawing.Point(7, 225);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(150, 199);
            this.pictureBox3.TabIndex = 54;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pictureBox3_MouseClick);
            // 
            // Upload_plan
            // 
            this.Upload_plan.Location = new System.Drawing.Point(22, 195);
            this.Upload_plan.Name = "Upload_plan";
            this.Upload_plan.Size = new System.Drawing.Size(118, 23);
            this.Upload_plan.TabIndex = 53;
            this.Upload_plan.Text = "อัพโหลดแปลนเขื่อน";
            this.Upload_plan.UseVisualStyleBackColor = true;
            this.Upload_plan.Click += new System.EventHandler(this.Upload_plan_Click);
            // 
            // CheckList_Property
            // 
            this.CheckList_Property.Controls.Add(this.tabPage1);
            this.CheckList_Property.Controls.Add(this.tabPage2);
            this.CheckList_Property.Controls.Add(this.tabPage3);
            this.CheckList_Property.Location = new System.Drawing.Point(163, 8);
            this.CheckList_Property.Name = "CheckList_Property";
            this.CheckList_Property.SelectedIndex = 0;
            this.CheckList_Property.Size = new System.Drawing.Size(364, 416);
            this.CheckList_Property.TabIndex = 52;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.DamPart_1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(356, 390);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "เขื่อน (Dam)";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // DamPart_1
            // 
            this.DamPart_1.CheckBoxes = true;
            this.DamPart_1.Location = new System.Drawing.Point(4, 3);
            this.DamPart_1.Name = "DamPart_1";
            this.DamPart_1.Size = new System.Drawing.Size(352, 387);
            this.DamPart_1.TabIndex = 0;
            this.DamPart_1.AfterCheck += new System.Windows.Forms.TreeViewEventHandler(this.DamPart_1_AfterCheck);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.DamPart_2);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(356, 390);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "อาคารส่งน้ำ/ระบายน้ำ (Outlet)";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // DamPart_2
            // 
            this.DamPart_2.CheckBoxes = true;
            this.DamPart_2.Location = new System.Drawing.Point(2, 2);
            this.DamPart_2.Name = "DamPart_2";
            this.DamPart_2.Size = new System.Drawing.Size(352, 387);
            this.DamPart_2.TabIndex = 1;
            this.DamPart_2.AfterCheck += new System.Windows.Forms.TreeViewEventHandler(this.DamPart_2_AfterCheck);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.DamPart_3);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(356, 390);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "อาคารระบายน้ำล้น (Spillways)";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // DamPart_3
            // 
            this.DamPart_3.CheckBoxes = true;
            this.DamPart_3.Location = new System.Drawing.Point(2, 2);
            this.DamPart_3.Name = "DamPart_3";
            this.DamPart_3.Size = new System.Drawing.Size(352, 387);
            this.DamPart_3.TabIndex = 1;
            this.DamPart_3.AfterCheck += new System.Windows.Forms.TreeViewEventHandler(this.DamPart_3_AfterCheck);
            // 
            // label20
            // 
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(7, 165);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(150, 23);
            this.label20.TabIndex = 51;
            this.label20.Text = "คุณสมบัติ";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox2.BackgroundImage")));
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox2.Location = new System.Drawing.Point(7, 8);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(150, 150);
            this.pictureBox2.TabIndex = 50;
            this.pictureBox2.TabStop = false;
            // 
            // addDam_previous_btn
            // 
            this.addDam_previous_btn.Location = new System.Drawing.Point(289, 430);
            this.addDam_previous_btn.Name = "addDam_previous_btn";
            this.addDam_previous_btn.Size = new System.Drawing.Size(75, 23);
            this.addDam_previous_btn.TabIndex = 48;
            this.addDam_previous_btn.Text = "ย้อนกลับ";
            this.addDam_previous_btn.UseVisualStyleBackColor = true;
            this.addDam_previous_btn.Click += new System.EventHandler(this.addDam_previous_btn_Click);
            // 
            // addDam_step2_cancel
            // 
            this.addDam_step2_cancel.Location = new System.Drawing.Point(451, 430);
            this.addDam_step2_cancel.Name = "addDam_step2_cancel";
            this.addDam_step2_cancel.Size = new System.Drawing.Size(75, 23);
            this.addDam_step2_cancel.TabIndex = 47;
            this.addDam_step2_cancel.Text = "ยกเลิก";
            this.addDam_step2_cancel.UseVisualStyleBackColor = true;
            this.addDam_step2_cancel.Click += new System.EventHandler(this.addDam_step2_cancel_Click);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Toolstrip_ChangeImage,
            this.Toolstrip_DelImage});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(137, 48);
            // 
            // Toolstrip_ChangeImage
            // 
            this.Toolstrip_ChangeImage.Name = "Toolstrip_ChangeImage";
            this.Toolstrip_ChangeImage.Size = new System.Drawing.Size(136, 22);
            this.Toolstrip_ChangeImage.Text = "เปลี่ยนรูปภาพ";
            this.Toolstrip_ChangeImage.Click += new System.EventHandler(this.Upload_plan_Click);
            // 
            // Toolstrip_DelImage
            // 
            this.Toolstrip_DelImage.Name = "Toolstrip_DelImage";
            this.Toolstrip_DelImage.Size = new System.Drawing.Size(136, 22);
            this.Toolstrip_DelImage.Text = "ลบภาพออก";
            this.Toolstrip_DelImage.Click += new System.EventHandler(this.Toolstrip_DelImage_Click);
            // 
            // addNewDam
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(544, 471);
            this.Controls.Add(this.createDam_step1_panel);
            this.Controls.Add(this.addDam_panel_2);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "addNewDam";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "เพิ่มเขื่อนใหม่";
            this.TopMost = true;
            this.createDam_step1_panel.ResumeLayout(false);
            this.createDam_step1_panel.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.addDam_panel_2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.CheckList_Property.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel createDam_step1_panel;
        private System.Windows.Forms.Button addDam_next;
        private System.Windows.Forms.Button addDam_cancel;
        private System.Windows.Forms.Button createDam_btn;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox crest_level;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox max_sea;
        private System.Windows.Forms.TextBox min_sea;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox normal_sea;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox max_cap;
        private System.Windows.Forms.TextBox min_cap;
        private System.Windows.Forms.TextBox normal_cap;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox height;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox width;
        private System.Windows.Forms.TextBox length;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox dam_subdistrict;
        private System.Windows.Forms.ComboBox dam_district;
        private System.Windows.Forms.ComboBox dam_province;
        private System.Windows.Forms.TextBox dam_baan;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox geo_id;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox dam_id;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox dam_name;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel addDam_panel_2;
        private System.Windows.Forms.Button addDam_step2_cancel;
        private System.Windows.Forms.Button addDam_previous_btn;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.TabControl CheckList_Property;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TreeView DamPart_1;
        private System.Windows.Forms.TreeView DamPart_2;
        private System.Windows.Forms.TreeView DamPart_3;
        private System.Windows.Forms.Button Upload_plan;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem Toolstrip_ChangeImage;
        private System.Windows.Forms.ToolStripMenuItem Toolstrip_DelImage;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.ComboBox water_scale;

    }
}