﻿namespace Checklist
{
    partial class ViewPlan
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ViewPlan));
            this.viewport_box = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.viewport_box)).BeginInit();
            this.SuspendLayout();
            // 
            // viewport_box
            // 
            this.viewport_box.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.viewport_box.Location = new System.Drawing.Point(0, 0);
            this.viewport_box.Name = "viewport_box";
            this.viewport_box.Size = new System.Drawing.Size(480, 640);
            this.viewport_box.TabIndex = 0;
            this.viewport_box.TabStop = false;
            this.viewport_box.Click += new System.EventHandler(this.viewport_box_Click);
            // 
            // ViewPlan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(480, 640);
            this.Controls.Add(this.viewport_box);
            this.Cursor = System.Windows.Forms.Cursors.Hand;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "ViewPlan";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ดูตัวอย่างแผนผังเขื่อน";
            this.TopMost = true;
            ((System.ComponentModel.ISupportInitialize)(this.viewport_box)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox viewport_box;
    }
}