﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
//using System.Threading.Tasks;
using System.Windows.Forms;
using System.Web;
using System.Net;
using System.IO;
using System.Diagnostics;

namespace Checklist
{
    public partial class export_result : Form
    {
        private string checklist_id;
        private Form1 mainForm;
        private ArrayList lgrade, hgrade, raws_properties, daminfo, log_calculation, list_menu;
        private Boolean[] dam_poperties;
        public export_result(Form1 main, String CI_id)
        {
            mainForm = main;
            checklist_id = CI_id;
            dam_poperties = new Boolean[160];
            InitializeComponent();

            checklist_id_label.Text = checklist_id;
            this.Text = "รายงานใบตรวจดัชนีสภาพเขื่อนเลขที่ : " + checklist_id;
            get_listmenu();
            get_daminfo();
            getScore();
            get_properties();
            insert_datagrid();
            set_properties();
            set_text();
            status_label.Text = "";
        }

        private void get_listmenu()
        {
            String sql = "SELECT list_menu.* FROM list_menu ORDER BY list_menu.list_id;";
            list_menu = mainForm.getDB(sql);
        }

        private void getScore()
        {
            String sql = "SELECT * FROM Layer6_Calculate INNER JOIN list_menu ON CInt(Layer6_Calculate.element_id) = list_menu.list_id WHERE Layer6_Calculate.Val >= '4' AND  Layer6_Calculate.checklist_id ='" + checklist_id + "' ORDER BY Layer6_Calculate.Val DESC;";
            hgrade = mainForm.getDB(sql);

            sql = "SELECT * FROM Layer6_Calculate INNER JOIN list_menu ON CInt(Layer6_Calculate.element_id) = list_menu.list_id WHERE Layer6_Calculate.Val < '4' AND  Layer6_Calculate.checklist_id ='" + checklist_id + "' ORDER BY Layer6_Calculate.Val DESC;";
            lgrade = mainForm.getDB(sql);
        }

        private void get_properties()
        {
            String sql = "SELECT checklist.* FROM fill_checklist INNER JOIN checklist ON fill_checklist.dam_auto_id = checklist.dam_id WHERE (((fill_checklist.checklist_id)='" + checklist_id + "'));";
            raws_properties = mainForm.getDB(sql);

            //ArrayList list_menu = new ArrayList();
            foreach (ArrayList rec in raws_properties)
            {
                dam_poperties[Convert.ToInt32(rec[1])] = true;
                //list_menu[Convert.ToInt32(rec[1])] = rec;
            }
        }

        private void get_daminfo()
        {
            String sql = "SELECT dam_info.*, province.PROVINCE_NAME, fill_checklist.score FROM (fill_checklist INNER JOIN dam_info ON CInt(fill_checklist.dam_auto_id) = dam_info.id_auto) INNER JOIN province ON dam_info.dam_province = province.PROVINCE_ID WHERE ((fill_checklist.checklist_id)='" + checklist_id + "');";
            daminfo = mainForm.getDB(sql);
        }

        private void set_color(Label temp, double score)
        {
            if (score < 80)
            {
                temp.ForeColor = System.Drawing.Color.Red;
            }
            else
            {
                temp.ForeColor = System.Drawing.Color.Green;
            }
        }

        private void set_properties()
        {
            foreach (ArrayList rec in daminfo)
            {
                dam_name_label.Text = rec[2].ToString();
                province_label.Text = rec[23].ToString();
                double score = (Convert.ToDouble(rec[24]) * 20);
                dam_score_label.Text = String.Format("{0:F2}", score) + " %";
                set_color(dam_score_label, score);

                string sql = "select * from calculation_log where [checklist_id] = '"+checklist_id+"'";
                log_calculation = mainForm.getDB(sql);

                if (log_calculation.Count == 0)
                {
                    MessageBox.Show("ไม่พบประวัติการบันทึกแบบสำรวจเขื่อน");
                    score_p1.Text = "0 %";
                    set_color(score_p1, 0);
                    score_p2.Text = "0 %";
                    set_color(score_p2, 0);
                    score_p3.Text = "0 %";
                    set_color(score_p3, 0);

                }

                foreach (ArrayList item in log_calculation)
                {
                    switch (Convert.ToInt16(item[1]))
                    {
                        case 1:
                            score = Convert.ToDouble(item[3]) * 20;
                            score_p1.Text = String.Format("{0:F2}", score) + " %";
                            set_color(score_p1, score);
                            break;
                        case 10:
                            score = Convert.ToDouble(item[3]) * 20;
                            score_p2.Text = String.Format("{0:F2}", score) + " %";
                            set_color(score_p2, score);
                            break;
                        case 89:
                            score = Convert.ToDouble(item[3]) * 20;
                            score_p3.Text = String.Format("{0:F2}", score) + " %";
                            set_color(score_p3, score);
                            break;
                    }
                }
            }
        }
        private string find_name(int id)
        {
            return find_name(id, "");
        }
        private string find_name(int id, string name)
        {
            String str = string.Empty;
            foreach (ArrayList item in list_menu)
            {
                if (Convert.ToInt32(item[0]) == id)
                {
                    if (name == "")
                    {
                        str = item[3].ToString().Trim();
                    }
                    else
                    {
                        str = item[3].ToString() + " » " + name;
                        str = str.Trim();
                    }
                    if (Convert.ToInt32(item[1]) == 0)
                    {
                        return str;
                    }else
                    {
                        return find_name(Convert.ToInt32(item[1]), str);
                    }
                }
            }
            return "";
        }

        private void insert_datagrid()
        {
            String str = string.Empty;
            foreach (ArrayList rec in hgrade)
            {
                double score = (Convert.ToDouble(rec[4]) * 20);
                if (dam_poperties[Convert.ToInt32(rec[2])])
                {
                    str = find_name(Convert.ToInt32(rec[2]));
                    hi_grade.Rows.Add(rec[2].ToString(), str, String.Format("{0:F2}", score));
                }
            }

            foreach (ArrayList rec in lgrade)
            {
                double score = (Convert.ToDouble(rec[4]) * 20);
                if (dam_poperties[Convert.ToInt32(rec[2])])
                {
                    if (score > 0.0)
                    {
                        str = find_name(Convert.ToInt32(rec[2]));
                        low_grade.Rows.Add(rec[2].ToString(), str, String.Format("{0:F2}", score));
                    }
                }
            }
        }

        private void set_text()
        {
            String sql = "select * from final_report where [checklist_id] = '" + checklist_id + "'";
            ArrayList memo = mainForm.getDB(sql);
            if (memo.Count > 0)
            {
                foreach (ArrayList rec in memo)
                {
                    String c = rec[2].ToString();
                    String s = rec[3].ToString();
                    String sv = rec[4].ToString();

                    conclusion.Text = c.Replace("<br/>", "\r\n");
                    suggestion.Text = s.Replace("<br/>", "\r\n");
                    solve.Text = sv.Replace("<br/>", "\r\n");
                }
            }
        }

        private void save_final_report()
        {
            String conclusion_str = conclusion.Text;
            String suggestion_str = suggestion.Text;
            String solve_str = solve.Text;
            String sql = "delete from final_report where [checklist_id] = '" + checklist_id + "'";
            mainForm.getDB(sql);
            sql = "insert into final_report (checklist_id, conclusion, suggestion, solve) values('" + checklist_id + "','" + conclusion_str.Replace("\r\n", "<br/>") + "', '" + suggestion_str.Replace("\r\n", "<br/>") + "', '" + solve_str.Replace("\r\n", "<br/>") + "')";
            mainForm.insertDB(sql);
        }

        private void export_report_btn_Click(object sender, EventArgs e)
        {
            Thread x = new Thread(delegate()
            {
                save_final_report();
            });

            x.Start();
            x.Join();
            export_json();
            make_middle_file();
        }

        private void save_report_btn_Click(object sender, EventArgs e)
        {
            Thread x = new Thread(delegate()
            {
                save_final_report();
            });
            x.Start();
            x.Join();
            MessageBox.Show("บันทึกเรียบร้อย");
        }

        private void export_json()
        {
            String Json = "{";
            double score;
            //---Get Dam Info
            String sql = "SELECT dam_info.dam_name, dam_info.dam_baan, province.PROVINCE_NAME, fill_checklist.score, amphur.AMPHUR_NAME, district.DISTRICT_NAME, fill_checklist.checkatday, fill_checklist.checkatmonth, fill_checklist.checkatyear FROM fill_checklist INNER JOIN (((dam_info INNER JOIN province ON dam_info.dam_province = province.PROVINCE_ID) INNER JOIN amphur ON dam_info.dam_district = amphur.AMPHUR_ID) INNER JOIN district ON dam_info.dam_subdistrict = district.DISTRICT_ID) ON CInt(fill_checklist.dam_auto_id) = dam_info.id_auto WHERE (((fill_checklist.checklist_id)='" + checklist_id + "'));";
            ArrayList dam_info = mainForm.getDB(sql);
            Json += "\"dam_info\":{";
            foreach (ArrayList x in dam_info)
            {
                Json += "\"dam_name\":\"" + x[0].ToString().Trim() + "\",\"baan\":\"" + x[1].ToString().Trim() + "\",\"province\":\"" + x[2].ToString().Trim() + "\",\"score\":\"" + String.Format("{0:F2}", (Convert.ToDouble(x[3]) * 20)) + "\",\"district\":\"" + x[4].ToString().Trim() + "\",\"subdistrict\":\"" + x[5].ToString().Trim() + "\",\"day\":\"" + x[6].ToString().Trim() + "\",\"month\":\"" + x[7].ToString().Trim() + "\",\"year\":\"" + x[8].ToString().Trim() + "\"";
            }
            Json += "}";
            //---End Dam Info

            Json+=",";

            //---Get All Properties
            //sql = "SELECT list_menu.* FROM list_menu ORDER BY list_menu.list_id;";
            //ArrayList prop = mainForm.getDB(sql);

            ArrayList prop = list_menu;
            Json += "\"Properties\":[{\"id\":0,\"parent\":0,\"name\":\"Dummy\"},";
            int i = 0;
            foreach (ArrayList x in prop)
            {
                if (i != 0)
                {
                    Json += ",";
                }
                i++;
                Json += "{\"id\":" + x[0] + ",\"parent\":" + x[1] + ",\"name\":\"" + x[3].ToString().Trim() + "\"}";
            }
            Json += "]";
            //---End All Properties

            Json += ",";

            //---Get Top5
            i = 0;
            Json += "\"Top5\":[";
            foreach (ArrayList rec in hgrade)
            {
                score = (Convert.ToDouble(rec[4]) * 20);
                if (dam_poperties[Convert.ToInt32(rec[2])])
                {
                    if (i != 0)
                    {
                        Json += ",";
                    }
                    i++;
                    Json += "{\"id\":" + rec[2] + ",\"score\":" + String.Format("{0:F2}", score) + "}";
                }
            }
            Json += "]";
            //---End Top5

            Json += ",";

            //---Get last5
            i = 0;
            Json += "\"last5\":[";
            foreach (ArrayList rec in lgrade)
            {
                score = (Convert.ToDouble(rec[4]) * 20);
                if (dam_poperties[Convert.ToInt32(rec[2])])
                {
                    if (score > 0)
                    {
                        if (i != 0)
                        {
                            Json += ",";
                        }
                        i++;
                        Json += "{\"id\":" + rec[2] + ",\"score\":" + String.Format("{0:F2}", score) + "}";
                    }
                }
            }
            Json += "]";
            //---End last5

            Json += ",";

            //---Get Main Structure
            Json += "\"main_part\":[";
            i = 0;
            foreach (ArrayList item in log_calculation)
            {
                switch (Convert.ToInt16(item[1]))
                {
                    case 1:
                    case 10:
                    case 89:
                        if (i != 0)
                        {
                            Json += ",";
                        }
                        i++;

                        score = Convert.ToDouble(item[3]) * 20;
                        Json += "{\"id\":" + item[1] + ",\"score\":" + score + "}";
                        break;
                }
            }
            Json += "]";
            //---End Main Structure

            Json += ",";

            //---Set Solve
            String txt = solve.Text;
            txt = html_encode(txt);
            Json += "\"solve\":\"" + txt + "\"";
            //---End Solve

            Json += ",";

            //---Set Conclusion
            txt = conclusion.Text;
            txt = html_encode(txt);
            Json += "\"conclusion\":\"" + txt + "\"";
            //---End Conclusion

            Json += ",";

            //---Set Suggest
            txt = suggestion.Text;
            txt = html_encode(txt);
            Json += "\"suggestion\":\"" + txt + "\"";
            //---End Solve

            Json += "}";

            System.IO.File.WriteAllText(mainForm.appPath + @"\Template\final_param.dat", Json);
        }

        private void make_middle_file()
        {
            SaveFileDialog browseFile = new SaveFileDialog();
            browseFile.Filter = "PDF Files (*.pdf)|*.pdf";
            browseFile.Title = "ใบตรวจสอบดัชนีสภาพเขื่อน";
            if (browseFile.ShowDialog() == DialogResult.Cancel)
            {
                return;
            }
            else
            {
                String savefile = browseFile.FileName;
                Thread x = new Thread(delegate()
                {
                    int i = 0;
                    while (true)
                    {
                        string str = "กำลัง Export PDF file ";
                        switch (i % 6)
                        {
                            case 0: str += ""; break;
                            case 1: str += "."; break;
                            case 2: str += ". ."; break;
                            case 3: str += ". . ."; break;
                            case 4: str += ". . . ."; break;
                            case 5: str += ". . . . ."; break;
                        }
                        status_label.Text = str;
                        i++;
                        Thread.Sleep(1000);
                    }
                });

                Thread genReport = new Thread(delegate()
                {
                    if (File.Exists(savefile))
                    {
                        try
                        {
                            File.Delete(savefile);
                        }
                        catch
                        {
                            MessageBox.Show("ไม่สามารถเขียนข้อมูลได้ : Read-Only File");
                            return;
                        }
                    }

                    String abpath = mainForm.appPath + @"\Template";
                    String php_path = abpath + @"\bin\php\php5.3.13\php.exe";
                    String output_path = mainForm.appPath + @"\Output\temp.pdf";
                    //String exec = abpath + @"\make_final_report.php --C_FILE=" + abpath + " --SAVE_FILE=" + savefile;
                    String exec = abpath + @"\make_final_report.php --C_FILE=" + abpath + " --SAVE_FILE=" + output_path;
                    Process process = new Process();
                    ProcessStartInfo startInfo = new ProcessStartInfo(php_path, exec);

                    startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;

                    process.StartInfo = startInfo;
                    process.Start();
                    process.WaitForExit();
                    
                    if (File.Exists(output_path))
                    {
                        File.Move(output_path, savefile);
                    }
                    else
                    {
                        MessageBox.Show("ไม่สามารถออกรายงานได้");
                    }
                });

                x.Start();
                genReport.Start();
                genReport.Join();
                x.Abort();

                Thread o = new Thread(delegate()
                {
                    if (File.Exists(savefile))
                    {
                        try
                        {
                            Process p = new Process();
                            ProcessStartInfo s = new ProcessStartInfo(savefile);
                            p.StartInfo = s;
                            p.Start();
                        }
                        catch
                        {
                            MessageBox.Show("ไม่มีโปรแกรมรองรับการอ่าน PDF File");
                        }
                    }
                    else
                    {
                        MessageBox.Show("ไม่สามารถเซฟ File ลงจุดที่เลือกได้");
                    }
                });
                o.Start();
            }
        }

        private string html_encode(string input)
        {
            input = HttpUtility.HtmlEncode(input);
            input = input.Replace("(", "&#40;");
            input = input.Replace(")", "&#41;");
            input = input.Replace("%", "&#37;");
            input = input.Replace("[", "&#91;");
            input = input.Replace("]", "&#93;");
            input = input.Replace("{", "&#123;");
            input = input.Replace("}", "&#125;");
            input = input.Replace("\t", "&#160;");
            input = input.Replace(" ", "&#160;");

            input = "<li>" + input.Replace("\r\n", "</li><li>") + "</li>";
            input = input.Replace("<li></li>", "");
            input = input.Trim();

            return input;
        }
    }
}
