<?php
	function score_marker($score, $txt){
		if($score >= 4.0){
			return "<font style='color: green;'>{$txt}</font>";
		}else{
			if($score > 0.0){
				return "<font style='color: red;'>{$txt}</font>";
			}else{
				return "<font style='color: #efefef;'>{$txt}</font>";
			}
		}
	}
	
	date_default_timezone_set('Asia/Bangkok');
	
	$root = getcwd ();
	$filepath = $root."\\Log\\Log.txt";
	$fp = fopen($filepath, "r");
	$raws = fread($fp, filesize($filepath));
	fclose($fp);

	$all_data = json_decode($raws);
	$data = $all_data->data;

	$filepath = $root."\\Log\\template.php";
	$fp2 = fopen($filepath, "r");
	$template = fread($fp2, filesize($filepath));
	fclose($fp2);

	$fdata = "";
	foreach($data as $item){
		$fdata .= "<tr>"; 
		$fdata .= "<td class='textinfo'>{$item->id}</td>"; 
		$fdata .= "<td>{$item->title}</td>"; 
		$fdata .= "<td class='textinfo'>".score_marker($item->info->score, $item->info->score)."</td>"; 
		$fdata .= "<td class='textinfo'>".score_marker($item->info->score, sprintf('%.2f',($item->info->score * 20)))."</td>"; 
		$fdata .= "<td class='textinfo'>".score_marker($item->info->score, $item->info->weight)."</td>";
		$fdata .= "</tr>"; 
	}
	
	$template = str_replace("{title}", $all_data->dam_name, $template);
	$template = str_replace("{date_create}", date("l j F Y H:i:s"), $template);
	$data = str_replace("{replace}", $fdata, $template);

	$fp3 = fopen($root."\\Log\\Log_{$all_data->survey_id}.html", "w+");
	fwrite($fp3, $data);
	fclose($fp3);
?>