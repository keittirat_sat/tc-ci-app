<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <meta charset="UTF-8">
        <title>รายละเอียดการคำนวน {title}</title>
		<style>
			body{
				font-size: 12px;
			}
			table{
                border-spacing: 0px;
                border: 1px solid;
                border-collapse:collapse;
			}
			th{
				text-align: center;
				font-weight: bold;
				background-color: #cfcfcf;
                border: 1px solid;
				padding: 2px;
			}
			td{
                border: 1px solid;
				paddding: 2px;
			}
			.textinfo{
				text-align: center;
			}
		</style>
	</head>
	<body>
		<h2>{title}</h2>
		<h3>ทำการคำนวณเมื่อ: {date_create}</h3>
		<table>
			<thead>
				<tr>
					<th>ID</th>
					<th>ชิ้นส่วนชื่อ</th>
					<th>คะแนนที่ได้</th>
					<th>คะแนนที่ได้ % </th>
					<th>ค่าน้ำหนัก</th>
				</tr>
			</thead>
			<tbody>
				{replace}
			</tbody>
		</table>
	</body>
</html>