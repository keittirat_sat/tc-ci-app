
<?php if ($data[8] || $data[9]): ?>
    <table>
        <thead>
            <tr>
                <th class="headtitle" colspan="31">1. เขื่อน (Dam) : 1.2  ฐานยัน (Abutment)</th>
            </tr>
            <tr>
                <th class="menu" rowspan ="2">รายการ</th>
                <th colspan="5">การกัดเซาะ<sup>1</sup></th>
                <th colspan="5">การเกิดร่องน้ำ</th>
                <th colspan="5">การระบายน้ำ</th>
                <th colspan="5">การซึม</th>
                <th colspan="5">การเลื่อนไถล</th>
                <th colspan="5">การเสื่อมสภาพ<sup>1</sup></th>
            </tr>
            <tr>
                <?php for ($i = 0; $i < 6; $i++): ?>
                    <?php for ($j = 1; $j <= 5; $j++): ?>
                        <th class="score_template"><?php echo $j; ?></th>
                    <?php endfor; ?>
                <?php endfor; ?>
            </tr>
        </thead>
        <tbody>
            <?php if ($data[8]): ?>
                <tr>
                    <td>ฝั่งซ้าย</td>
                    <?php if ($score == ""): ?>
                        <td class="first"></td>
                        <td class="block"></td>
                        <td></td>
                        <td class="block"></td>
                        <td></td>

                        <td class="first block"></td>
                        <td></td>
                        <td class="block"></td>
                        <td></td>
                        <td></td>

                        <td class="first"></td>
                        <td></td>
                        <td></td>
                        <td class="block"></td>
                        <td></td>

                        <td class="first block"></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>

                        <td class="first block"></td>
                        <td></td>
                        <td class="block"></td>
                        <td class="block"></td>
                        <td></td>

                        <td class="first"></td>
                        <td class="block"></td>
                        <td></td>
                        <td class="block"></td>
                        <td></td>
                    <?php else: ?>
                        <?php $item = getsingle($score->c8); ?>
                        <td class="first"><?php getans($item[1], 1); ?></td>
                        <td class="block"></td>
                        <td><?php getans($item[1], 3); ?></td>
                        <td class="block"></td>
                        <td><?php getans($item[1], 5); ?></td>

                        <td class="first block"></td>
                        <td><?php getans($item[2], 2); ?></td>
                        <td class="block"></td>
                        <td><?php getans($item[2], 4); ?></td>
                        <td><?php getans($item[2], 5); ?></td>

                        <td class="first"><?php getans($item[3], 1); ?></td>
                        <td><?php getans($item[3], 2); ?></td>
                        <td><?php getans($item[3], 3); ?></td>
                        <td class="block"></td>
                        <td><?php getans($item[3], 5); ?></td>

                        <td class="first block"></td>
                        <td><?php getans($item[4], 2); ?></td>
                        <td><?php getans($item[4], 3); ?></td>
                        <td><?php getans($item[4], 4); ?></td>
                        <td><?php getans($item[4], 5); ?></td>

                        <td class="first block"></td>
                        <td><?php getans($item[5], 2); ?></td>
                        <td class="block"></td>
                        <td class="block"></td>
                        <td><?php getans($item[5], 5); ?></td>

                        <td class="first"><?php getans($item[6], 1); ?></td>
                        <td class="block"></td>
                        <td><?php getans($item[6], 3); ?></td>
                        <td class="block"></td>
                        <td><?php getans($item[6], 5); ?></td>
                    <?php endif; ?>
                </tr>
            <?php endif; ?>
            <?php if ($data[9]): ?>
                <tr>
                    <td>ฝั่งขวา</td>
                    <?php if ($score == ""): ?>
                        <td class="first"></td>
                        <td class="block"></td>
                        <td></td>
                        <td class="block"></td>
                        <td></td>

                        <td class="first block"></td>
                        <td></td>
                        <td class="block"></td>
                        <td></td>
                        <td></td>

                        <td class="first"></td>
                        <td></td>
                        <td></td>
                        <td class="block"></td>
                        <td></td>

                        <td class="first block"></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>

                        <td class="first block"></td>
                        <td></td>
                        <td class="block"></td>
                        <td class="block"></td>
                        <td></td>

                        <td class="first"></td>
                        <td class="block"></td>
                        <td></td>
                        <td class="block"></td>
                        <td></td>
                    <?php else: ?>
                        <?php $item = getsingle($score->c9); ?>
                        <td class="first"><?php getans($item[1], 1); ?></td>
                        <td class="block"></td>
                        <td><?php getans($item[1], 3); ?></td>
                        <td class="block"></td>
                        <td><?php getans($item[1], 5); ?></td>

                        <td class="first block"></td>
                        <td><?php getans($item[2], 2); ?></td>
                        <td class="block"></td>
                        <td><?php getans($item[2], 4); ?></td>
                        <td><?php getans($item[2], 5); ?></td>

                        <td class="first"><?php getans($item[3], 1); ?></td>
                        <td><?php getans($item[3], 2); ?></td>
                        <td><?php getans($item[3], 3); ?></td>
                        <td class="block"></td>
                        <td><?php getans($item[3], 5); ?></td>

                        <td class="first block"></td>
                        <td><?php getans($item[4], 2); ?></td>
                        <td><?php getans($item[4], 3); ?></td>
                        <td><?php getans($item[4], 4); ?></td>
                        <td><?php getans($item[4], 5); ?></td>

                        <td class="first block"></td>
                        <td><?php getans($item[5], 2); ?></td>
                        <td class="block"></td>
                        <td class="block"></td>
                        <td><?php getans($item[5], 5); ?></td>

                        <td class="first"><?php getans($item[6], 1); ?></td>
                        <td class="block"></td>
                        <td><?php getans($item[6], 3); ?></td>
                        <td class="block"></td>
                        <td><?php getans($item[6], 5); ?></td>
                    <?php endif; ?>
                </tr>
            <?php endif; ?>
            <tr>
                <th rowspan="2">รายการ</th>
                <th colspan="5">ต้นไม้</th>
                <th colspan="5">วัชพืช</th>
                <th colspan="20" rowspan ="2">หมายเหตุ</th>
            </tr>
            <tr>
                <?php for ($i = 0; $i < 2; $i++): ?>
                    <?php for ($j = 1; $j <= 5; $j++): ?>
                        <th class="score_template"><?php echo $j; ?></th>
                    <?php endfor; ?>
                <?php endfor; ?>
            </tr>
            <?php if ($data[8]): ?>
                <tr>
                    <td>ฝั่งซ้าย</td>
                    <?php if ($score != ""): ?>
                        <?php $item = getsingle($score->c8); ?>
                        <td class="first"><?php getans($item[7], 1); ?></td>
                        <td><?php getans($item[7], 2); ?></td>
                        <td><?php getans($item[7], 3); ?></td>
                        <td><?php getans($item[7], 4); ?></td>
                        <td><?php getans($item[7], 5); ?></td>

                        <td class="first block"></td>
                        <td><?php getans($item[8], 2); ?></td>
                        <td class="block"></td>
                        <td><?php getans($item[8], 4); ?></td>
                        <td><?php getans($item[8], 5); ?></td>

                        <td colspan="20" class="first"><?php echo $item[9]; ?></td>
                    <?php else: ?>
                        <td class="first"></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>

                        <td class="first block"></td>
                        <td></td>
                        <td class="block"></td>
                        <td></td>
                        <td></td>

                        <td colspan="20" class="first"></td>
                    <?php endif; ?>
                </tr>
            <?php endif; ?>
            <?php if ($data[9]): ?>
                <tr>
                    <td>ฝั่งขวา</td>
                    <?php if ($score != ""): ?>
                        <?php $item = getsingle($score->c9); ?>
                        <td class="first"><?php getans($item[7], 1); ?></td>
                        <td><?php getans($item[7], 2); ?></td>
                        <td><?php getans($item[7], 3); ?></td>
                        <td><?php getans($item[7], 4); ?></td>
                        <td><?php getans($item[7], 5); ?></td>

                        <td class="first block"></td>
                        <td><?php getans($item[8], 2); ?></td>
                        <td class="block"></td>
                        <td><?php getans($item[8], 4); ?></td>
                        <td><?php getans($item[8], 5); ?></td>

                        <td colspan="20" class="first"><?php echo $item[9]; ?></td>
                    <?php else: ?>
                        <td class="first"></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>

                        <td class="first block"></td>
                        <td></td>
                        <td class="block"></td>
                        <td></td>
                        <td></td>

                        <td colspan="20" class="first"></td>
                    <?php endif; ?>
                </tr>
            <?php endif; ?>
        </tbody>
    </table>
<?php endif; ?>

<?php if ($data[4]): ?>
    <table>
        <thead>
            <tr>
                <th class="headtitle" colspan="32">1. เขื่อน (Dam) : 1.3  ฐานเขื่อน (Rockfill Toe, Toe Drain, Contact Drain, Open Drain)</th>
            </tr>
            <tr>
                <th colspan="2">รายการ</th>
                <th colspan="5">การกัดเซาะ<sup>1</sup></th>
                <th colspan="5">การทรุดตัว<sup>1</sup></th>
                <th colspan="5">การระบายน้ำ</th>
                <th colspan="5">การซึม</th>
                <th colspan="5">การเลื่อนไถล</th>
                <th colspan="5">การเสื่อมสภาพ<sup>1</sup></th>
            </tr>
            <tr>
                <th class="distance">จาก (กม.)</th>
                <th class="distance">ถึง (กม.)</th>

                <?php for ($i = 0; $i < 6; $i++): ?>
                    <?php for ($j = 1; $j <= 5; $j++): ?>
                        <th class="score_template"><?php echo $j; ?></th>
                    <?php endfor; ?>
                <?php endfor; ?>

            </tr>
        </thead>
        <tbody>
            <?php if ($score == ""): ?>
                <?php if (!$have_distance): ?>
                    <?php for ($i = 0; $i < 9; $i++): ?>
                        <tr>
                            <td class="distance"></td>
                            <td class="distance"></td>

                            <td class="first"></td>
                            <td class="block"></td>
                            <td></td>
                            <td class="block"></td>
                            <td></td>

                            <td class="first block"></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>

                            <td class="first"></td>
                            <td></td>
                            <td></td>
                            <td class="block"></td>
                            <td></td>

                            <td class="first block"></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>

                            <td class="first block"></td>
                            <td></td>
                            <td class="block"></td>
                            <td class="block"></td>
                            <td></td>

                            <td class="first"></td>
                            <td class="block"></td>
                            <td></td>
                            <td class="block"></td>
                            <td></td>
                        </tr>
                    <?php endfor; ?>
                <?php else: ?>
                    <?php foreach ($distance as $item): ?>
                        <tr>
                            <td class="distance"><?php echo $item[0]; ?></td>
                            <td class="distance"><?php echo $item[1]; ?></td>

                            <td class="first"></td>
                            <td class="block"></td>
                            <td></td>
                            <td class="block"></td>
                            <td></td>

                            <td class="first block"></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>

                            <td class="first"></td>
                            <td></td>
                            <td></td>
                            <td class="block"></td>
                            <td></td>

                            <td class="first block"></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>

                            <td class="first block"></td>
                            <td></td>
                            <td class="block"></td>
                            <td class="block"></td>
                            <td></td>

                            <td class="first"></td>
                            <td class="block"></td>
                            <td></td>
                            <td class="block"></td>
                            <td></td>
                        </tr>
                    <?php endforeach; ?>
                <?php endif; ?>
            <?php else: ?>
                <?php foreach ($score->c4 as $item): ?>
                    <tr>
                        <td class="distance"><?php echo $item[0]; ?></td>
                        <td class="distance"><?php echo $item[1]; ?></td>

                        <td class="first"><?php getans($item[2], 1); ?></td>
                        <td class="block"></td>
                        <td><?php getans($item[2], 3); ?></td>
                        <td class="block"></td>
                        <td><?php getans($item[2], 5); ?></td>

                        <td class="first block"></td>
                        <td><?php getans($item[3], 2); ?></td>
                        <td><?php getans($item[3], 3); ?></td>
                        <td><?php getans($item[3], 4); ?></td>
                        <td><?php getans($item[3], 5); ?></td>

                        <td class="first"><?php getans($item[4], 1); ?></td>
                        <td><?php getans($item[4], 2); ?></td>
                        <td><?php getans($item[4], 3); ?></td>
                        <td class="block"></td>
                        <td><?php getans($item[4], 5); ?></td>

                        <td class="first block"></td>
                        <td><?php getans($item[5], 2); ?></td>
                        <td><?php getans($item[5], 3); ?></td>
                        <td><?php getans($item[5], 4); ?></td>
                        <td><?php getans($item[5], 5); ?></td>

                        <td class="first block"></td>
                        <td><?php getans($item[6], 2); ?></td>
                        <td class="block"></td>
                        <td class="block"></td>
                        <td><?php getans($item[6], 5); ?></td>

                        <td class="first"><?php getans($item[7], 1); ?></td>
                        <td class="block"></td>
                        <td><?php getans($item[7], 3); ?></td>
                        <td class="block"></td>
                        <td><?php getans($item[7], 5); ?></td>
                    </tr>
                <?php endforeach; ?>
            <?php endif; ?>
        </tbody>
    </table>
    <table>
        <thead>
            <tr>
                <th colspan="2">รายการ</th>
                <th colspan="5">รอยแตกร้าว</th>
                <th colspan="5">รูโพรง</th>
                <th colspan="5">ต้นไม้</th>
                <th colspan="5">วัชพืช</th>
                <th colspan="10" rowspan="2">หมายเหตุ</th>
            </tr>
            <tr>
                <th class="distance">จาก (กม.)</th>
                <th class="distance">ถึง (กม.)</th>

                <?php for ($i = 0; $i < 4; $i++): ?>
                    <?php for ($j = 1; $j <= 5; $j++): ?>
                        <th class="score_template"><?php echo $j; ?></th>
                    <?php endfor; ?>
                <?php endfor; ?>

            </tr>
        </thead>
        <tbody>
            <?php if ($score == ""): ?>
                <?php if (!$have_distance): ?>
                    <?php for ($i = 0; $i < 9; $i++): ?>
                        <tr>
                            <td class="distance"></td>
                            <td class="distance"></td>

                            <td class="first block"></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>

                            <td class="first block"></td>
                            <td></td>
                            <td class="block"></td>
                            <td></td>
                            <td></td>

                            <td class="first"></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>

                            <td class="first block"></td>
                            <td></td>
                            <td class="block"></td>
                            <td></td>
                            <td></td>

                            <td colspan="10" class="first"></td>

                        </tr>
                    <?php endfor; ?>
                <?php else: ?>
                    <?php foreach ($distance as $item): ?>
                        <tr>
                            <td class="distance"><?php echo $item[0]; ?></td>
                            <td class="distance"><?php echo $item[1]; ?></td>

                            <td class="first block"></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>

                            <td class="first block"></td>
                            <td></td>
                            <td class="block"></td>
                            <td></td>
                            <td></td>

                            <td class="first"></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>

                            <td class="first block"></td>
                            <td></td>
                            <td class="block"></td>
                            <td></td>
                            <td></td>

                            <td colspan="10" class="first"></td>

                        </tr>
                    <?php endforeach; ?>
                <?php endif; ?>
            <?php else: ?>
                <?php foreach ($score->c4 as $item): ?>
                    <tr>
                        <td class="distance"><?php echo $item[0]; ?></td>
                        <td class="distance"><?php echo $item[1]; ?></td>

                        <td class="first block"></td>
                        <td><?php getans($item[8], 2); ?></td>
                        <td><?php getans($item[8], 3); ?></td>
                        <td><?php getans($item[8], 4); ?></td>
                        <td><?php getans($item[8], 5); ?></td>

                        <td class="first block"></td>
                        <td><?php getans($item[9], 2); ?></td>
                        <td class="block"></td>
                        <td><?php getans($item[9], 4); ?></td>
                        <td><?php getans($item[9], 5); ?></td>

                        <td class="first"><?php getans($item[10], 1); ?></td>
                        <td><?php getans($item[10], 2); ?></td>
                        <td><?php getans($item[10], 3); ?></td>
                        <td><?php getans($item[10], 4); ?></td>
                        <td><?php getans($item[10], 5); ?></td>

                        <td class="first block"></td>
                        <td><?php getans($item[11], 2); ?></td>
                        <td class="block"></td>
                        <td><?php getans($item[11], 4); ?></td>
                        <td><?php getans($item[11], 5); ?></td>

                        <td colspan="10" class="first"><?php echo $item[12]; ?></td>

                    </tr>
                <?php endforeach; ?>
            <?php endif; ?>
        </tbody>
    </table>
<?php endif; ?>

<?php if ($data[8] || $data[9] || $data[4]): ?>
    <table class="desc">
        <tbody>
            <tr>
                <td class="col1"><b>หมายเหตุ</b></td>
                <td class="col2">การกัดเซาะ<sup>1</sup></td>
                <td class="col3">(1) กัดเซาะเสียหาย &gt;50%  (3) กัดเซาะเสียหาย &lt; 50% (5) ไม่เกิดการกัดเซาะ</td>
            </tr>
            <tr>
                <td></td>
                <td>การทรุดตัว<sup>1</sup></td>
                <td>(2) ทรุดตัวประมาณเอว/ >เอว (3) ทรุดตัวลึกประมาณเข่าแต่ไม่ถึงเอว (4) ทรุดตัวลึกประมาณข้อเท้าแต่ไม่ถึงเข่า (5) ไม่เกิดการทรุดตัว</td>
            </tr>
            <tr>
                <td></td>
                <td>การซึม</td>
                <td>(2) เกิดการซึมและมีการไหลรวม (3) เกิดการซึมเป็นความยาวมากกว่า 30% (4) เกิดการซึมเป็นทางยาวน้อยกว่า 30% (5) ไม่เกิดการซึม</td>
            </tr>
            <tr>
                <td></td>
                <td>การเลื่อนไถล</td>
                <td>(2) เกิดการเลื่อนไถล (5) ไม่เกิดการเลื่อนไถล</td>
            </tr>
            <tr>
                <td></td>
                <td>การเสื่อมสภาพ<sup>1</sup></td>
                <td>(1) เสื่อมสภาพเสียหาย &gt;50%  (3) เสื่อมสภาพเสียหาย &lt; 50% (5) ไม่เกิดการเสื่อมสภาพ</td>
            </tr>
            <tr>
                <td></td>
                <td>ต้นไม้</td>
                <td>(1) ต้นไม้สูงกว่าหัว (2) ต้นไม้สูงกว่าเอว (3) ต้นไม้สูงต่ำกว่าเอว (4) ต้นไม้ต่ำกว่าเข่า (5) ไม่มีต้นไม้</td>
            </tr>
            <tr>
                <td></td>
                <td>วัชพืช</td>
                <td>(2) มีวัชพืชปกคลุมมากกว่า 50% ของพื้นที่ (4) มีวัชพืชปกคลุมน้อยกว่า 50% ของพื้นที่ (5) ไม่มีวัชพืชปกคลุม</td>
            </tr>
            <tr>
                <td></td>
                <td>รูโพรง</td>
                <td>(2) มีรูโพรงลึกมากกว่าเข่า (4) มีรูโพรงลึกน้อยกว่าเข่า (5) ไม่มีรูโพรง</td>
            </tr>
            <tr>
                <td></td>
                <td>รอยแตกร้าว</td>
                <td>(2) เกิดรอยร้าวมีความกว้างและความลึกเป็นทางยาว (3) เกิดรอยร้าวมีความกว้างและความลึกบางจุด  (4) เกิดรอยร้าวเนื่องจากอุณหูมิ (5) ไม่เกิดรอยร้าว</td>
            </tr>
            <tr>
                <td></td>
                <td>การระบายน้ำ</td>
                <td>(1) ท่อระบายน้ำ/รางระบายน้ำอุดตัน ไม่สามารถระบายน้ำได้โดยสิ้นเชิง (2) น้ำขุ่นแต่ไม่เกิดการอุดตัน (3) น้ำไหลมีตะกอน แต่ไม่เกิดการอุดตัน (5) สามารถดี/น้ำไหลปกติ</td>
            </tr>
            <tr>
                <td></td>
                <td>การเกิดร่องน้ำ</td>
                <td>(2) เกิดร่องน้ำลึกประมาณเข่า (4) เกิดร่องน้ำลึกประมาณข้อเท้า (5) ไม่เกิดร่องน้ำ</td>
            </tr>
        </tbody>
    </table>
    <pagebreak />
<?php endif; ?>