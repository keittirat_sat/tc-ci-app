<?php if ($data[5]): ?>
    <table>
        <thead>
            <tr>
                <th class="headtitle" colspan="32">1. เขื่อน (Dam) : 1.1 ตัวเขื่อน (Dam Body) : 1.1.1 สันเขื่อน (Crest)</th>
            </tr>
            <tr>
                <th colspan="2">รายการ</th>
                <th colspan="5">รอยแตกตามขวาง</th>
                <th colspan="5">รอยแตกตามยาว</th>
                <th colspan="5">การทรุดตัว<sup>1</sup></th>
                <th colspan="5">ความทนทาน</th>
                <th colspan="5">รูโพรง</th>
                <th colspan="5">ต้นไม้</th>
            </tr>
            <tr>
                <th class="distance">จาก (กม.)</th>
                <th class="distance">ถึง (กม.)</th>

                <?php for ($i = 0; $i < 6; $i++): ?>
                    <?php for ($j = 1; $j <= 5; $j++): ?>
                        <th class="score_template"><?php echo $j; ?></th>
                    <?php endfor; ?>
                <?php endfor; ?>

            </tr>
        </thead>
        <?php if ($score == ""): ?>
            <tbody>
                <?php if (!$have_distance): ?>
                    <?php for ($i = 0; $i < 15; $i++): ?>
                        <tr>
                            <td class="distance"></td>
                            <td class="distance"></td>

                            <td class="first"></td>
                            <td class="block"></td>
                            <td></td>
                            <td></td>
                            <td></td>

                            <td class="first block"></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>

                            <td class="first block"></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>

                            <td class="first"></td>
                            <td class="block"></td>
                            <td></td>
                            <td></td>
                            <td></td>

                            <td class="first block"></td>
                            <td></td>
                            <td class="block"></td>
                            <td></td>
                            <td></td>

                            <td class="first"></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    <?php endfor; ?>
                <?php else: ?>
                    <?php foreach ($distance as $item): ?>
                        <tr>
                            <td class="distance"><?php echo $item[0]; ?></td>
                            <td class="distance"><?php echo $item[1]; ?></td>

                            <td class="first"></td>
                            <td class="block"></td>
                            <td></td>
                            <td></td>
                            <td></td>

                            <td class="first block"></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>

                            <td class="first block"></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>

                            <td class="first"></td>
                            <td class="block"></td>
                            <td></td>
                            <td></td>
                            <td></td>

                            <td class="first block"></td>
                            <td></td>
                            <td class="block"></td>
                            <td></td>
                            <td></td>

                            <td class="first"></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    <?php endforeach; ?>
                <?php endif; ?>
            </tbody>
        </table>
        <table>
            <thead>
                <tr>
                    <th colspan="2">รายการ</th>
                    <th colspan="5">วัชพืช</th>
                    <th colspan="5">ระบบรักษาความปลอดภัย</th>
                    <th colspan="20" rowspan="2">หมายเหตุ</th>
                </tr>
                <tr>
                    <th class="distance">จาก (กม.)</th>
                    <th class="distance">ถึง (กม.)</th>

                    <?php for ($i = 0; $i < 2; $i++): ?>
                        <?php for ($j = 1; $j <= 5; $j++): ?>
                            <th class="score_template"><?php echo $j; ?></th>
                        <?php endfor; ?>
                    <?php endfor; ?>

                </tr>
            </thead>
            <tbody>
                <?php if (!$have_distance): ?>
                    <?php for ($i = 0; $i < 15; $i++): ?>
                        <tr>
                            <td class="distance"></td>
                            <td class="distance"></td>

                            <td class="first block"></td>
                            <td></td>
                            <td class="block"></td>
                            <td></td>
                            <td></td>

                            <td class="first"></td>
                            <td></td>
                            <td class="block"></td>
                            <td class="block"></td>
                            <td></td>

                            <td colspan="20" class="first"></td>

                        </tr>
                    <?php endfor; ?>
                <?php else: ?>
                    <?php foreach ($distance as $item): ?>
                        <tr>
                            <td class="distance"><?php echo $item[0]; ?></td>
                            <td class="distance"><?php echo $item[1]; ?></td>

                            <td class="first block"></td>
                            <td></td>
                            <td class="block"></td>
                            <td></td>
                            <td></td>

                            <td class="first"></td>
                            <td></td>
                            <td class="block"></td>
                            <td class="block"></td>
                            <td></td>

                            <td colspan="20" class="first"></td>

                        </tr>
                    <?php endforeach; ?>
                <?php endif; ?>
            </tbody>
        <?php else: ?>
            <tbody>
                <?php foreach ($score->c5 as $item): ?>
                    <tr>
                        <td class="distance"><?php echo $item[0]; ?></td>
                        <td class="distance"><?php echo $item[1]; ?></td>

                        <td class="first"><?php getans($item[2], 1); ?></td>
                        <td class="block"></td>
                        <td><?php getans($item[2], 3); ?></td>
                        <td><?php getans($item[2], 4); ?></td>
                        <td><?php getans($item[2], 5); ?></td>

                        <td class="first block"></td>
                        <td><?php getans($item[3], 2); ?></td>
                        <td><?php getans($item[3], 3); ?></td>
                        <td><?php getans($item[3], 4); ?></td>
                        <td><?php getans($item[3], 5); ?></td>

                        <td class="first block"></td>
                        <td><?php getans($item[4], 2); ?></td>
                        <td><?php getans($item[4], 3); ?></td>
                        <td><?php getans($item[4], 4); ?></td>
                        <td><?php getans($item[4], 5); ?></td>

                        <td class="first"><?php getans($item[5], 1); ?></td>
                        <td class="block"></td>
                        <td><?php getans($item[5], 3); ?></td>
                        <td><?php getans($item[5], 4); ?></td>
                        <td><?php getans($item[5], 5); ?></td>

                        <td class="first block"></td>
                        <td><?php getans($item[6], 2); ?></td>
                        <td class="block"></td>
                        <td><?php getans($item[6], 4); ?></td>
                        <td><?php getans($item[6], 5); ?></td>

                        <td class="first"><?php getans($item[7], 1); ?></td>
                        <td><?php getans($item[7], 2); ?></td>
                        <td><?php getans($item[7], 3); ?></td>
                        <td><?php getans($item[7], 4); ?></td>
                        <td><?php getans($item[7], 5); ?></td>
                    </tr>
                <?php endforeach; ?>
                <tr>
                    <th colspan="2">รายการ</th>
                    <th colspan="5">วัชพืช</th>
                    <th colspan="5">ระบบรักษาความปลอดภัย</th>
                    <th colspan="20" rowspan="2">หมายเหตุ</th>
                </tr>
                <tr>
                    <th class="distance">จาก (กม.)</th>
                    <th class="distance">ถึง (กม.)</th>

                    <?php for ($i = 0; $i < 2; $i++): ?>
                        <?php for ($j = 1; $j <= 5; $j++): ?>
                            <th class="score_template"><?php echo $j; ?></th>
                        <?php endfor; ?>
                    <?php endfor; ?>

                </tr>
                <?php foreach ($score->c5 as $item): ?>
                    <tr>
                        <td class="distance"><?php echo $item[0]; ?></td>
                        <td class="distance"><?php echo $item[1]; ?></td>

                        <td class="first block"></td>
                        <td><?php getans($item[8], 2); ?></td>
                        <td class="block"></td>
                        <td><?php getans($item[8], 4); ?></td>
                        <td><?php getans($item[8], 5); ?></td>

                        <td class="first"><?php getans($item[9], 1); ?></td>
                        <td><?php getans($item[9], 2); ?></td>
                        <td class="block"></td>
                        <td class="block"></td>
                        <td><?php getans($item[9], 5); ?></td>

                        <td colspan="20" class="first"><?php echo $item[10]; ?></td>

                    </tr>
                <?php endforeach; ?>
            </tbody>
        <?php endif; ?>
        <tfoot>
            <tr>
                <td class="notice"><b>หมายเหตุ</b></td>
                <td colspan="3">รอยแตกร้าวตามขวาง</td>
                <td colspan="28">(1) เกิดรอยแตกร้าวมีความกว้างและความลึกเป็นทางยาว (3) เกิดรอยแตกร้าวมีความกว้างและความลึกบางจุด (4) เกิดรอยร้าว (5) ไม่เกิดรอยแตกร้าว</td>
            </tr>
            <tr>
                <td></td>
                <td colspan="3">รอยแตกร้าวตามยาว</td>
                <td colspan="28">(2) เกิดรอยแตกร้าวมีความกว้างและความลึกเป็นทางยาว (3) เกิดรอยแตกร้าวมีความกว้างและความลึกบางจุด (4) เกิดรอยร้าว (5) ไม่เกิดรอยแตกร้าว</td>
            </tr>
            <tr>
                <td></td>
                <td colspan="3">การทรุดตัว<sup>1</sup></td>
                <td colspan="28">(2) ทรุดตัวประมาณเอว/ >เอว (3) ทรุดตัวลึกประมาณเข่าแต่ไม่ถึงเอว (4) ทรุดตัวลึกประมาณข้อเท้าแต่ไม่ถึงเข่า (5) ไม่เกิดการทรุดตัว</td>
            </tr>
            <tr>
                <td></td>
                <td colspan="3">ความทนทาน</td>
                <td colspan="28">(1) เกิดร่องล้อ/ผิวจราจรสูญเสียกำลัง (3) เกิดเป็นแอ่งน้ำ (4) ผิวจราจรหลุดร่อน  (5) ไม่เกิดร่องล้อ</td>
            </tr>
            <tr>
                <td></td>
                <td colspan="3">รูโพรง</td>
                <td colspan="28">(2) มีรูโพรงลึกมากกว่าเข่า (4) มีรูโพรงลึกน้อยกว่าเข่า (5) ไม่มีรูโพรง</td>
            </tr>
            <tr>
                <td></td>
                <td colspan="3">ต้นไม้</td>
                <td colspan="28">(1) ต้นไม้สูงกว่าหัว (2) ต้นไม้สูงกว่าเอว (3) ต้นไม้สูงต่ำกว่าเอว (4) ต้นไม้ต่ำกว่าเข่า (5) ไม่มีต้นไม้</td>
            </tr>
            <tr>
                <td></td>
                <td colspan="3">วัชพืช</td>
                <td colspan="28">(2) มีวัชพืชปกคลุมมากกว่า 50% ของพื้นที่ (4) มีวัชพืชปกคลุมน้อยกว่า 50% ของพื้นที่ (5) ไม่มีวัชพืชปกคลุม</td>
            </tr>
            <tr>
                <td></td>
                <td colspan="3">ระบบรักษาความปลอดภัย</td>
                <td colspan="28">(1) ไม่มีระบบรักษาความปลอดภัย (2) มีระบบรักษาความปลอดภัย แต่ใช้การไม่ได้ (5) มีระบบรักษาความปลอดภัย ใช้การได้</td>
            </tr>
        </tfoot>
    </table>
    <pagebreak />
<?php endif; ?>