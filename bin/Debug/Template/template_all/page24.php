<?php if ($data[146]): ?>
    <table>
        <tbody>
            <tr>
                <th class="headtitle" colspan="27">
                    <b>3. อาคารระบายน้ำล้น (Spillways)</b> : 3.2 อาคารระบายน้ำล้นฉุกเฉิน (Emergency Spillway) :
                    3.2.1 <b><i><u>ส่วนทางน้ำเข้า</u></i></b> <br><b>[&nbsp;&nbsp;&nbsp;]</b> <b><i>ไม่มีอาคารระบายน้ำล้นฉุกเฉิน </i></b>
                </th>
            </tr>

            <tr>
                <th class="menu">รายการ</th>
                <th colspan="5"><b>การกัดเซาะ</b></th>
                <th colspan="5"><b>การเลื่อนไถล</b></th>
                <th colspan="5"><b>ต้นไม้</b></th>
                <th colspan="5"><b>วัชพีช</b></th>
                <th colspan="5"><b>สิ่งกีดขวางทางน้ำ</b></th>
                <th  rowspan="2"><b>หมายเหตุ</b></th>
            </tr>
            <tr>
                <td class="first"></td>
                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>
                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>
                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>
                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>
                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>
            </tr>
            <tr>
                <?php if ($score == ""): //start blank table?>
                    <td><b>[&nbsp;&nbsp;&nbsp;]</b> มองไม่เห็น</td>
                    <td class="first"></td>
                    <td class="block"></td>
                    <td ></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first block"></td>
                    <td ></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    <td class="first block"></td>
                    <td ></td>
                    <td class="block" ></td>
                    <td ></td>
                    <td></td>

                    <td class="first"></td>
                    <td class="block"></td>
                    <td ></td>
                    <td class="block"></td>
                    <td></td>
                    <td class="first"></td>
                <?php else: //end blank table, start data table?>

                    <td><b>[<?php notsee($score->c146); ?>]</b> มองไม่เห็น</td>
                    <?php $item = getsingle($score->c146); ?>

                    <td class="first"><?php getans($item[0], 1); ?></td>
                    <td class="block"></td>
                    <td ><?php getans($item[0], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[0], 5); ?></td>

                    <td class="first block"></td>
                    <td ><?php getans($item[1], 2); ?></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td><?php getans($item[1], 5); ?></td>

                    <td class="first"><?php getans($item[2], 1); ?></td>
                    <td><?php getans($item[2], 2); ?></td>
                    <td><?php getans($item[2], 3); ?></td>
                    <td><?php getans($item[2], 4); ?></td>
                    <td><?php getans($item[2], 5); ?></td>

                    <td class="first block"></td>
                    <td ><?php getans($item[3], 2); ?></td>
                    <td class="block" ></td>
                    <td ><?php getans($item[3], 4); ?></td>
                    <td><?php getans($item[3], 5); ?></td>

                    <td class="first"><?php getans($item[4], 1); ?></td>
                    <td class="block"></td>
                    <td ><?php getans($item[4], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[4], 5); ?></td>

                    <td class="first"><?php echo $item[5]; ?></td>

                <?php endif; //end data table?>
            </tr>
        </tbody>
    </table>
<?php endif ?>

<?php if ($data[147]): ?>
    <table>
        <tbody>
            <tr>
                <th class="headtitle" colspan="31">
                    <b> 3. อาคารระบายน้ำล้น (Spillways)</b> : 3.2 อาคารระบายน้ำล้นฉุกเฉิน (Emergency Spillway) :3.2.2 <b><u><i>ส่วนควบคุม </i></u></b>
                </th>
            </tr>

            <tr>
                <th class="menu">รายการ</th>
                <th colspan="5"><b>การกัดเซาะ<sup>2</sup></b></th>
                <th colspan="5"><b>การทรุดตัว<sup>2</sup></b></th>
                <th colspan="5"><b>รอยแตกร้าว</b></th>
                <th colspan="5"><b>ต้นไม้</b></th>
                <th colspan="5"><b>วัชพืช</b></th>
                <th colspan="5"><b>สิ่งกีดขวางทางน้ำ</b></th>
            </tr>

            <tr>
                <td class="first"></td>
                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>
            </tr>
            <tr>

                <?php if ($score == ""): //start blank table?>
                    <th><b>[&nbsp;&nbsp;&nbsp;]</b>  มองไม่เห็น</th>
                    <td class="first"></td>
                    <td class="block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    <td class="first"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    <td class="first"></td>
                    <td class="block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>


                    <td class="first"></td>
                    <td class="block" ></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>

                <?php else: //end blank table, start data table?>

                    <th><b>[<?php notsee($score->c147); ?>]</b>  มองไม่เห็น</th>
                    <?php $item = getsingle($score->c147); ?>

                    <td class="first"><?php getans($item[0], 1); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[0], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[0], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[1], 2); ?></td>
                    <td><?php getans($item[1], 3); ?></td>
                    <td><?php getans($item[1], 4); ?></td>
                    <td><?php getans($item[1], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[2], 2); ?></td>
                    <td><?php getans($item[2], 3); ?></td>
                    <td><?php getans($item[2], 4); ?></td>
                    <td><?php getans($item[2], 5); ?></td>

                    <td class="first"><?php getans($item[3], 1); ?></td>
                    <td><?php getans($item[3], 2); ?></td>
                    <td><?php getans($item[3], 3); ?></td>
                    <td><?php getans($item[3], 4); ?></td>
                    <td><?php getans($item[3], 5); ?></td>

                    <td class="first"><?php getans($item[4], 1); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[4], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[4], 5); ?></td>


                    <td class="first"><?php getans($item[5], 1); ?></td>
                    <td class="block" ></td>
                    <td><?php getans($item[5], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[5], 5); ?></td>

                <?php endif; //end data table?>

            </tr>
        </tbody>
    </table>	
<?php endif ?>

<?php if ($data[148]): ?>		
    <table>
        <tbody>
            <tr>
                <th class="headtitle" colspan="27">
                    <b>  3. อาคารระบายน้ำล้น (Spillways)</b> : 3.2 อาคารระบายน้ำล้นฉุกเฉิน (Emergency Spillway) :
                    3.2.3 <b><u><i>ส่วนทางน้ำออก </i></u></b>
                </th>
            </tr>

            <tr>
                <th class="menu">รายการ</th>
                <th colspan="5"><b>การกัดเซาะ<sup>1</sup></b></th>
                <th colspan="5"><b>การเลื่อนไถล</b></th>
                <th colspan="5"><b>ต้นไม้</b></th>
                <th colspan="5"><b>วัชพืช</b></th>
                <th colspan="5"><b>สิ่งกีขวางทางน้ำ</b></th>
                <th rowspan="2" class="menu" ><b>หมายเหตุ</b></th>
            </tr>
            <tr>
                <td class="first"></td>
                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>												
            </tr>
            <tr>
                <?php if ($score == ""): //start blank table?>
                    <td>  <b>[&nbsp;&nbsp;&nbsp;]</b>  มองไม่เห็น</td>
                    <td class="first"></td>
                    <td class="block"></td>
                    <td ></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first block"></td>
                    <td ></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    <td class="first block"></td>
                    <td ></td>
                    <td  class="block"></td>
                    <td ></td>
                    <td></td>

                    <td class="first"></td>
                    <td class="block"></td>
                    <td ></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first"></td>		

                <?php else: //end blank table, start data table?>

                    <td>  <b>[<?php notsee($score->c148); ?>]</b>  มองไม่เห็น</td>
                    <?php $item = getsingle($score->c148); ?>

                    <td class="first"><?php getans($item[0], 1); ?></td>
                    <td class="block"></td>
                    <td ><?php getans($item[0], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[0], 5); ?></td>

                    <td class="first block"></td>
                    <td ><?php getans($item[1], 2); ?></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td><?php getans($item[1], 5); ?></td>

                    <td class="first"><?php getans($item[2], 1); ?></td>
                    <td><?php getans($item[2], 2); ?></td>
                    <td><?php getans($item[2], 3); ?></td>
                    <td><?php getans($item[2], 4); ?></td>
                    <td><?php getans($item[2], 5); ?></td>

                    <td class="first block"></td>
                    <td ><?php getans($item[3], 2); ?></td>
                    <td  class="block"></td>
                    <td ><?php getans($item[3], 4); ?></td>
                    <td><?php getans($item[3], 5); ?></td>

                    <td class="first"><?php getans($item[4], 1); ?></td>
                    <td class="block"></td>
                    <td ><?php getans($item[4], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[4], 5); ?></td>

                    <td class="first"><?php echo $item[5]; ?></td>

                <?php endif; //end data table?>
            </tr>
        </tbody>
    </table>			
<?php endif; ?>

<?php if ($data[146] || $data[147] || $data[148]): ?>
    <table class="desc">
        <tbody>
            <tr>
                <td class="col1"><b><u>หมายเหตุ</u></b></td>
                <td class="col2">การกัดเซาะ <sup>1</sup></td>
                <td class="col3">(1)กัดเซาะเสียหาย &gt; 50% (3)กัดเซาะเสียหาย &lt;50% (5) ไม่เกิดการกัดเซาะ</td>
            </tr>

            <tr>
                <td></td>
                <td>การเลื่อนไถล</td>
                <td>(2) เกิดการเลื่อนไถล (5) ไม่เกิดการเลื่อนไถล</td>
            </tr>
            <tr>
                <td></td>
                <td>ต้นไม้</td>
                <td>(1) ต้นไม้สูงกว่าหัว (2)ต้นไม้สูงกว่าเอว (3)ต้นไม้สูงต่ำกว่าเอว (4)ต้นไม้ตำกว่าเข่า (ไม่มีต้นไม้)</td>
            </tr>
            <tr>
                <td></td>
                <td>วัชพืช</td>
                <td>(2) มีวัชพืชปกคลุมมากกว่า 50% ของพื้นที่ (4)มีวัชพืชปกคลุมน้อยกว่า 50 ของพื้นที่ (5)ไม่มีวัชพืชปกคลุม</td>
            </tr>
            <tr>
                <td></td>
                <td>สิ่งกีดขวามทางน้ำ</td>
                <td>(1) มีสิ่งกีดขวางทางน้ำ และขวางทางเดินน้ำทั้งหมด (3)มีสิ่งกีดขวางทางน้ำ แต่ปิดขวางทางเดินน้ำบางส่วน (5) ไม่มีสิ่งกีดขวางทางน้ำ</td>
            </tr>
            <tr>
                <td></td>
                <td>การกัดเซาะ<sup>2</sup></td>
                <td>(1) กัดเซาะเห็นเนื้อเหล็ก (3) เกิดการกัดเซาะ (5) ไม่เกิดการกัดเซาะ</td> 
            </tr>
            <tr>
                <td></td>
                <td>การทรุดตัว<sup>2</sup></td>
                <td>(2) ทรุดตัว&gt;5 ซม. (3) ทรุดตัวลึกอยู่ระหว่าง 2-5 ซม. (4) ทรุดตัว&lt;2 ซม. (5) ไม่เกิดการทรุดตัว</td>
            </tr>

            <tr>
                <td></td>
                <td>รอยแตกร้าว</td>
                <td>(2) เกิดรอยร้าวมีควมกว้างและความลึกเป็นทางยาว (3) เกิดรอยร้วมีความกว้างและความลึกบางจุด (4) เกิดรอยร้าวเนื่องจาก (5) ไม่เกิดรอยร้าว</td> 
            </tr>
        </tbody>
    </table>
<?php endif; ?>