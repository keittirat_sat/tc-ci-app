<?php if ($data[54]): ?> <!--Function 54-->
    <table> 
        <tbody>
            <tr>
                <th class="headtitle" colspan="12">
                    <b>2. อาคารส่งน้ำ/ระบายน้ำ (Outlets)</b> : 2.2 ท่อส่งน้ำ (Canal Outlet) : 2.2.1 ท่อส่งน้ำฝั่งซ้าย : 2.2.1.3 ส่วนควบคุม :
                    2.2.1.3.3 <b><i><u>อุปกรณ์ไฟฟ้า</u></i></b>
                </th>
            </tr>
            <!--Header 1--><tr> 
                <th class="menu" rowspan="2">รายการ</th>
                <th colspan="5"> การเสื่อมสภาพ<sup>2</sup></th>
                <th colspan="5">สภาพการใช้งาน</th>
                <th rowspan="2"> หมายเหตุ</th>
            </tr><!--End Header 1-->
            <tr>
                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>
            </tr>
            <tr>
                <?php if ($score == ""): ?>
                    <td><b>[&nbsp;&nbsp;&nbsp;]</b> มองไม่เห็น</td>
                    <td class="first"></td>
                    <td class="block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first"></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first"></td>
                <?php else: ?>
                    <td><b>[<?php notsee($score->c54); ?>]</b> มองไม่เห็น</td>
                    <?php $item = getsingle($score->c54); ?>
                    <td class="first"><?php getans($item[0], 1); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[0], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[0], 5); ?></td>

                    <td class="first"><?php getans($item[1], 1); ?></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td><?php getans($item[1], 5); ?></td>

                    <td class="first"><?php echo $item[2]; ?></td>
                <?php endif; ?>
            </tr>
        </tbody>
    </table>
<?php endif; ?> <!--End Function 54-->

<?php if ($data[55]): ?> <!--Function 55-->
    <table> 
        <tbody>
            <tr>
                <th class="headtitle" colspan="22">
                    <b>2. อาคารส่งน้ำ/ระบายน้ำ (Outlets)</b> : 2.2 ท่อส่งน้ำ (Canal Outlet) : 2.2.1 ท่อส่งน้ำฝั่งซ้าย : 2.2.1.3 ส่วนควบคุม : 
                    2.2.1.3.4 <b><i><u>Guard Gate</u></i></b>
                </th>
            </tr>
            <!--Header 1--><tr> 
                <th class="menu" rowspan="2">รายการ</th>
                <th colspan="5">การกัดเซาะ<sup>3</sup></th>
                <th colspan="5">การรั่ว</th>
                <th colspan="5"> การเสื่อมสภาพ<sup>2</sup></th>
                <th colspan="5">สภาพการใช้งาน</th>
                <th rowspan="2"> หมายเหตุ</th>
            </tr><!--End Header 1-->
            <tr>
                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>
            </tr>
            <tr>
                <?php if ($score == ""): ?>
                    <td><b>[&nbsp;&nbsp;&nbsp;]</b> มองไม่เห็น</td>
                    <td class="first"></td>
                    <td class="block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first"></td>
                    <td class="block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first"></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first"></td>
                <?php else: ?>
                    <td><b>[<?php notsee($score->c55) ?>]</b> มองไม่เห็น</td>
                    <?php $item = getsingle($score->c55); ?>
                    <td class="first"><?php getans($item[0], 1); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[0], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[0], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[1], 2); ?></td>
                    <td><?php getans($item[1], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[1], 5); ?></td>

                    <td class="first"><?php getans($item[2], 1); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[2], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[2], 5); ?></td>

                    <td class="first"><?php getans($item[3], 1); ?></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td><?php getans($item[3], 5); ?></td>

                    <td class="first"><?php echo $item[4]; ?></td>
                <?php endif; ?>
            </tr>
        </tbody>
    </table>
<?php endif; ?> <!--End Function 55-->	

<?php if ($data[56]): ?> <!--Function 56-->
    <table> 
        <tbody>
            <tr>
                <th class="headtitle" colspan="22">
                    <b>2. อาคารส่งน้ำ/ระบายน้ำ (Outlets)</b> : 2.2 ท่อส่งน้ำ (Canal Outlet) : 2.2.1 ท่อส่งน้ำฝั่งซ้าย : 2.2.1.3 ส่วนควบคุม : 2.2.1.3.3 อุปกรณ์ไฟฟ้า 
                    2.2.1.3.5 <b><i><u>Operating Gate</u></i></b>
                </th>
            </tr>
            <!--Header 1--><tr> 
                <th class="menu" rowspan="2">รายการ</th>
                <th colspan="5">การกัดเซาะ<sup>3</sup></th>
                <th colspan="5">การรั่ว</th>
                <th colspan="5"> การเสื่อมสภาพ<sup>2</sup></th>
                <th colspan="5">สภาพการใช้งาน</th>
                <th   rowspan="2"> หมายเหตุ</th>
            </tr><!--End Header 1-->
            <tr>
                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>
            </tr>
            <tr>
                <?php if ($score == ""): ?>
                    <td><b>[&nbsp;&nbsp;&nbsp;]</b> มองไม่เห็น</td>
                    <td class="first"></td>
                    <td class="block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first"></td>
                    <td class="block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first"></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first"></td>
                <?php else: ?>
                    <td><b>[<?php notsee($score->c56); ?>]</b> มองไม่เห็น</td>
                    <?php $item = getsingle($score->c56); ?>
                    <td class="first"><?php getans($item[0], 1); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[0], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[0], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[1], 2); ?></td>
                    <td><?php getans($item[1], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[1], 5); ?></td>

                    <td class="first"><?php getans($item[2], 1); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[2], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[2], 5); ?></td>

                    <td class="first"><?php getans($item[3], 1); ?></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td><?php getans($item[3], 5); ?></td>

                    <td class="first"><?php echo $item[4]; ?></td>
                <?php endif; ?>
            </tr>
        </tbody>
    </table>
<?php endif; ?> <!--End Function 56-->	

<?php if ($data[58]): ?> <!--Function 58-->
    <table> 
        <tbody>
            <tr>
                <th class="headtitle" colspan="22">
                    <b>2. อาคารส่งน้ำ/ระบายน้ำ (Outlets)</b> : 2.2 ท่อส่งน้ำ (Canal Outlet) : 2.2.1 ท่อส่งน้ำฝั่งซ้าย : 2.2.1.3 ส่วนควบคุม : 2.2.1.3.6 สะพาน (Access Bridge) 
                    2.2.1.3.6.1 <b><i><u>พื้น</u></i></b>
                </th>
            </tr>
            <!--Header 1--><tr> 
                <th class="menu" rowspan="2">รายการ</th>
                <th colspan="5">การกัดเซาะ<sup>2,3</sup></th>
                <th colspan="5">การเคลื่อนตัว</th>
                <th colspan="5"> การทรุดตัว<sup>2</sup></th>
                <th colspan="5">รอยแตกร้าว</th>
                <th rowspan="2"> หมายเหตุ</th>
            </tr><!--End Header 1-->
            <tr>
                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>
            </tr>
            <tr>
                <?php if ($score == ""): ?>
                    <td><b>[&nbsp;&nbsp;&nbsp;]</b> มองไม่เห็น</td>
                    <td class="first"></td>
                    <td class="block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    <td class="first"></td>
                <?php else: ?>
                    <td><b>[<?php notsee($score->c58); ?>]</b> มองไม่เห็น</td>
                    <?php $item = getsingle($score->c58); ?>
                    <td class="first"><?php getans($item[0], 1); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[0], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[0], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[1], 2); ?></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td><?php getans($item[1], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[2], 2); ?></td>
                    <td><?php getans($item[2], 3); ?></td>
                    <td><?php getans($item[2], 4); ?></td>
                    <td><?php getans($item[2], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[3], 2); ?></td>
                    <td><?php getans($item[3], 3); ?></td>
                    <td><?php getans($item[3], 4); ?></td>
                    <td><?php getans($item[3], 5); ?></td>

                    <td class="first"><?php echo $item[4]; ?></td>
                <?php endif; ?>
            </tr>
        </tbody>
    </table>
<?php endif; ?> <!--End Function 58-->	

<?php if ($data[59]): ?> <!--Function 59-->
    <table> 
        <tbody>
            <tr>
                <th class="headtitle" colspan="22">
                    <b>2. อาคารส่งน้ำ/ระบายน้ำ (Outlets)</b> : 2.2 ท่อส่งน้ำ (Canal Outlet) : 2.2.1 ท่อส่งน้ำฝั่งซ้าย : 2.2.1.3 ส่วนควบคุม : 2.2.1.3.6 สะพาน (Access Bridge) 
                    2.2.1.3.6.2 <b><i><u>ตอม่อ</u></i></b>
                </th>
            </tr>
            <!--Header 1--><tr> 
                <th class="menu" rowspan="2">รายการ</th>
                <th colspan="5">การกัดเซาะ<sup>2,3</sup></th>
                <th colspan="5">การเคลื่อนตัว</th>
                <th colspan="5"> การทรุดตัว<sup>2</sup></th>
                <th colspan="5">รอยแตกร้าว</th>
                <th   rowspan="2"> หมายเหตุ</th>
            </tr><!--End Header 1-->
            <tr>
                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>
            </tr>
            <tr>
                <?php if ($score == ""): ?>
                    <td><b>[&nbsp;&nbsp;&nbsp;]</b> มองไม่เห็น</td>
                    <td class="first"></td>
                    <td class="block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    <td class="first"></td>
                <?php else: ?>
                    <td><b>[<?php notsee($score->c59); ?>]</b> มองไม่เห็น</td>
                    <?php $item = getsingle($score->c59); ?>
                    <td class="first"><?php getans($item[0], 1); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[0], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[0], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[1], 2); ?></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td><?php getans($item[1], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[2], 2); ?></td>
                    <td><?php getans($item[2], 3); ?></td>
                    <td><?php getans($item[2], 4); ?></td>
                    <td><?php getans($item[2], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[3], 2); ?></td>
                    <td><?php getans($item[3], 3); ?></td>
                    <td><?php getans($item[3], 4); ?></td>
                    <td><?php getans($item[3], 5); ?></td>

                    <td class="first"><?php echo $item[4]; ?></td>
                <?php endif; ?>
            </tr>
        </tbody>
    </table>
<?php endif; ?> <!--End Function 59-->		

<?php if ($data[60]): ?> <!--Function 60-->
    <table> 
        <tbody>
            <tr>
                <th class="headtitle" colspan="22">
                    <b>2. อาคารส่งน้ำ/ระบายน้ำ (Outlets)</b> : 2.2 ท่อส่งน้ำ (Canal Outlet) : 2.2.1 ท่อส่งน้ำฝั่งซ้าย : 2.2.1.3 ส่วนควบคุม : 2.2.1.3.6 สะพาน (Access Bridge) 
                    2.2.1.3.6.3 <b><i><u>คาน</u></i></b>
                </th>
            </tr>
            <!--Header 1--><tr> 
                <th class="menu" rowspan="2">รายการ</th>
                <th colspan="5">การกัดเซาะ<sup>2,3</sup></th>
                <th colspan="5">การเคลื่อนตัว</th>
                <th colspan="5"> การทรุดตัว<sup>2</sup></th>
                <th colspan="5">รอยแตกร้าว</th>
                <th rowspan="2"> หมายเหตุ</th>
            </tr><!--End Header 1-->
            <tr>
                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>
            </tr>
            <tr>
                <?php if ($score == ""): ?>
                    <td><b>[&nbsp;&nbsp;&nbsp;]</b> มองไม่เห็น</td>
                    <td class="first"></td>
                    <td class="block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    <td class="first"></td>
                <?php else: ?>
                    <td><b>[<?php notsee($score->c60); ?>]</b> มองไม่เห็น</td>
                    <?php $item = getsingle($score->c60); ?>
                    <td class="first"><?php getans($item[0], 1); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[0], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[0], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[1], 2); ?></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td><?php getans($item[1], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[2], 2); ?></td>
                    <td><?php getans($item[2], 3); ?></td>
                    <td><?php getans($item[2], 4); ?></td>
                    <td><?php getans($item[2], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[3], 2); ?></td>
                    <td><?php getans($item[3], 3); ?></td>
                    <td><?php getans($item[3], 4); ?></td>
                    <td><?php getans($item[3], 5); ?></td>

                    <td class="first"><?php echo $item[4]; ?></td>
                <?php endif; ?>
            </tr>
        </tbody>
    </table>
<?php endif; ?> <!--End Function 60-->		

<?php if ($data[62]): ?> <!--Function 62-->
    <table> 
        <tbody>
            <tr>
                <th class="headtitle" colspan="31">
                    <b>2. อาคารส่งน้ำ/ระบายน้ำ (Outlets)</b> : 2.2 ท่อส่งน้ำ (Canal Outlet) : 2.2.1 ท่อส่งน้ำฝั่งซ้าย : 2.2.1.4 ส่วนทางน้ำออก : 2.2.1.4.1 รางเท :
                    2.2.1.4.1.1 <b><i><u>พื้น</u></i></b>
                </th>
            </tr>

            <!--Header 1--><tr> 
                <th class="menu" rowspan="2">รายการ</th>
                <th colspan="5"> การกัดเซาะ<sup>2</sup></th>
                <th colspan="5">การเคลื่อนตัว</th>
                <th colspan="5"> การทรุดตัว<sup>2</sup></th>
                <th colspan="5">การบวมตัว</th>
                <th colspan="5"> การระบายน้ำ</th>
                <th colspan="5"> การรั่ว</th>
            </tr><!--End Header 1-->		
            <tr>
                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>
            </tr>
            <tr>
                <?php if ($score == ""): ?>
                    <td><b>[&nbsp;&nbsp;&nbsp;]</b> มองไม่เห็น</td>
                    <td class="first"></td>
                    <td class="block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first"></td>
                    <td></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>
                <?php else: ?>
                    <td><b>[<?php notsee($score->c62); ?>]</b> มองไม่เห็น</td>
                    <?php $item = getsingle($score->c62); ?>
                    <td class="first"><?php getans($item[0], 1); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[0], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[0], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[1], 2); ?></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td><?php getans($item[1], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[2], 2); ?></td>
                    <td><?php getans($item[2], 3); ?></td>
                    <td><?php getans($item[2], 4); ?></td>
                    <td><?php getans($item[2], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[3], 2); ?></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td><?php getans($item[3], 5); ?></td>

                    <td class="first"><?php getans($item[4], 1); ?></td>
                    <td><?php getans($item[4], 2); ?></td>
                    <td><?php getans($item[4], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[4], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[5], 2); ?></td>
                    <td><?php getans($item[5], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[5], 5); ?></td>
                <?php endif; ?>
            </tr>	

            <!--Header 2--><tr> 
                <th class="menu" rowspan="2">รายการ</th>
                <th colspan="5"> รอยแตกร้าว</th>
                <th colspan="5"> ต้นไม้</th>
                <th colspan="5"> วัชพืช</th>
                <th colspan="5"> สิ่งกีดขวางทางน้ำ</th>
                <th colspan="10" rowspan="2">หมายเหตุ</th>
            </tr><!--End Header 2-->
            <tr>
                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

            </tr>
            <tr>
                <?php if ($score == ""): ?>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    <td class="first"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>
                    <td></td>

                    <td class="first"></td>
                    <td class="block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>

                    <th colspan="10"> </th>
                <?php else: ?>
                    <td></td>

                    <td class="first block"></td>
                    <td><?php getans($item[6], 2); ?></td>
                    <td><?php getans($item[6], 3); ?></td>
                    <td><?php getans($item[6], 4); ?></td>
                    <td><?php getans($item[6], 5); ?></td>

                    <td class="first"><?php getans($item[7], 1); ?></td>
                    <td><?php getans($item[7], 2); ?></td>
                    <td><?php getans($item[7], 3); ?></td>
                    <td><?php getans($item[7], 4); ?></td>
                    <td><?php getans($item[7], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[8], 2); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[8], 4); ?></td>
                    <td><?php getans($item[8], 5); ?></td>

                    <td class="first"><?php getans($item[9], 1); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[9], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[9], 5); ?></td>

                    <th colspan="10"><?php echo $item[10]; ?></th>
                <?php endif; ?>
            </tr>
        </tbody>
    </table>
<?php endif; ?> <!--End Function 62-->		

<?php if ($data[54] | $data[55] || $data[56] || $data[58] || $data[59] || $data[60] || $data[62]): ?> <!--Function detail print-->
    <table class="desc">
        <tbody>
            <tr> 
                <td class="col1"><b><u>หมายเหตุ</u></b></td>
                <td class="col2">ระบบรักษาความปลอดภัย</td>
                <td class="col3">(1) ไม่มีระบบรักษาความปลอดภัย (2) มีระบบรักษาความปลอดภัย แต่ใช้การไม่ได้ (5) มีระบบรักษาความปลอดภัย ใช้การได้</td>
            </tr>
            <tr>
                <td class="col1" rowspan="10"></td>
                <td class="col2">การเสื่อมสภาพ<sup>2</sup></td>
                <td class="col3">(1) เกิดการเสื่อมสภาพใช้งานไม่ได้ (3) เกิดการเสื่อมสภาพใช้งานได้ (5) ไม่เกิดการเสื่อมสภาพ</td>
            </tr>
            <tr>
                <td class="col2">สภาพการใช้งาน</td>
                <td class="col3">(1) ใช้งานไม่ได้ (5) ใช้งานได้</td>
            </tr>
            <tr>
                <td class="col2">การกัดเซาะ<sup>3</sup></td>
                <td class="col3">(1) เกิดสนิมกัดกร่อนถึงเนื้อใน (3) เกิดสนิมที่ผิวเหล็ก (5) ไม่เกิดสนิม</td>
            </tr>
            <tr>
                <td class="col2">การรั่ว</td>
                <td class="col3">(2) เกิดการรั่วและมีน้ำไหลพุ่งออกมา (3) เกิดการรั่วและมีน้ำไหลซึม (5) ไม่เกิดการรั่ว</td>
            </tr>
            <tr>
                <td class="col2">การกัดเซาะ<sup>2</sup></td>
                <td class="col3">(1) กัดกร่อนเห็นเนื้อเหล็ก  (3) เกิดการกัดกร่อน (5) ไม่เกิดการกัดกร่อน</td>
            </tr>
            <tr>
                <td class="col2">การเคลื่อนตัว</td>
                <td class="col3">(2) เกิดการเคลื่อนตัว (5) ไม่เกิดการเคลื่อนตัว</td>
            </tr>
            <tr>
                <td class="col2">การทรุดตัว<sup>2</sup></td>
                <td class="col3">(2) ทรุดตัว &gt; 5 ซม. (3) ทรุดตัวลึกอยู่ระหว่าง 2-5 ซม. (4) ทรุดตัว &lt; 2 ซม. (5) ไม่เกิดการทรุดตัว</td>
            </tr>
            <tr>
                <td class="col2">รอยแตกร้าว</td>
                <td class="col3">(2) เกิดรอยร้าวมีความกว้างและความลึกเป็นทางยาว (3) เกิดรอยร้าวมีความกว้างและความลึกบางจุด  (4) เกิดรอยร้าวเนื่องจากอุณหูมิ (5) ไม่เกิดรอยร้าว</td>
            </tr>
            <tr>
                <td class="col2">การบวมตัว</td>
                <td class="col3">(2) บวมตัว (5) ไม่บวมตัว</td>
            </tr>
            <tr>
                <td class="col2">สิ่งกีดขวางทางน้ำ</td>
                <td class="col3">(1) มีสิ่งกีดขวางทางน้ำ และขวางทางเดินน้ำทั้งหมด (3) มีสิ่งกีดขวางทางน้ำ แต่ปิดขวางทางเดินน้ำบางส่วน (5) ไม่มีสิ่งกีดขวางทางน้ำ</td>
            </tr>
        </tbody>
    </table>
    <pagebreak />
<?php endif; ?> <!--End Function detail print-->