
<?php if ($data[25]): ?>
    <table>
        <tbody>
            <tr>
                <th class="headtitle" colspan="12">2. อาคารส่งน้ำ/ระบายน้ำ (Outlets) : 2.1 ท่อระบายน้ำลงลำน้ำเดิม (River Outlet) : 2.1.3 ส่วนควบคุมน้ำ : 2.1.3.3 อุปกรณ์ไฟฟ้า</th>
            </tr>

            <tr>
                <th class="menu" rowspan="2" >รายการ</th>
                <th colspan="5">การเสื่อมสภาพ<sup>2</sup></th>
                <th colspan="5">สภาพการใช้งาน</th>
                <th rowspan="2">หมายเหตุ</th>
            </tr>
            <tr>
                <?php for ($i = 0; $i < 2; $i++): ?>
                    <?php for ($j = 1; $j <= 5; $j++): ?>
                        <th class="score_template"><?php echo $j; ?></th>
                    <?php endfor; ?>
                <?php endfor; ?>
            </tr>
            <tr>
                <?php if ($score == ""): ?>
                    <td><b>[&nbsp;&nbsp;&nbsp;]</b>  มองไม่เห็น</td>

                    <td class="first"></td>
                    <td class="block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first"></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first"></td>
                <?php else: ?>
                    <?php $item = getsingle($score->c25); ?>
                    <td><b>[<?php notsee($score->c25); ?>]</b> มองไม่เห็น</td>

                    <td class="first"><?php getans($item[0], 1); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[0], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[0], 5); ?></td>

                    <td class="first"><?php getans($item[1], 1); ?></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td><?php getans($item[1], 5); ?></td>

                    <td class="first"><?php echo $item[2]; ?></td>
                <?php endif; ?>
            </tr>
        </tbody>
    </table>
<?php endif; ?>

<?php if ($data[26]): ?>
    <table>
        <tbody>
            <tr>
                <th class="headtitle" colspan="22">2. อาคารส่งน้ำ/ระบายน้ำ (Outlets) : 2.1 ท่อระบายน้ำลงลำน้ำเดิม (River Outlet) : 2.1.3 ส่วนควบคุมน้ำ : 2.1.3.4 Guard Gate</th>
            </tr>

            <tr>
                <th class="menu" rowspan="2" >รายการ</th>
                <th colspan="5">การกัดเซาะ<sup>3</sup></th>
                <th colspan="5">การรั่ว</th>
                <th colspan="5">การเสื่อมสภาพ<sup>2</sup></th>
                <th colspan="5">สภาพการใช้งาน</th>
                <th rowspan="2">หมายเหตุ</th>
            </tr>
            <tr>
                <?php for ($i = 0; $i < 4; $i++): ?>
                    <?php for ($j = 1; $j <= 5; $j++): ?>
                        <th class="score_template"><?php echo $j; ?></th>
                    <?php endfor; ?>
                <?php endfor; ?>
            </tr>
            <tr>
                <?php if ($score == ""): ?>
                    <td><b>[&nbsp;&nbsp;&nbsp;]</b>  มองไม่เห็น</td>

                    <td class="first"></td>
                    <td class="block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first"></td>
                    <td class="block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first"></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first"></td>
                <?php else: ?>
                    <td><b>[<?php notsee($score->c26); ?>]</b>  มองไม่เห็น</td>
                    <?php $item = getsingle($score->c26); ?>
                    <td class="first"><?php getans($item[0], 1); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[0], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[0], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[1], 2); ?></td>
                    <td><?php getans($item[1], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[1], 5); ?></td>

                    <td class="first"><?php getans($item[2], 1); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[2], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[2], 5); ?></td>

                    <td class="first"><?php getans($item[3], 1); ?></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td><?php getans($item[3], 5); ?></td>

                    <td class="first"><?php echo $item[4]; ?></td>
                <?php endif; ?>
            </tr>
        </tbody>
    </table>
<?php endif; ?>

<?php if ($data[27]): ?>
    <table>
        <tbody>
            <tr>
                <th class="headtitle" colspan="22">2. อาคารส่งน้ำ/ระบายน้ำ (Outlets) : 2.1 ท่อระบายน้ำลงลำน้ำเดิม (River Outlet) : 2.1.3 ส่วนควบคุมน้ำ : 2.1.3.5 Operating Gate</th>
            </tr>

            <tr>
                <th class="menu" rowspan="2" >รายการ</th>
                <th colspan="5">การกัดเซาะ<sup>3</sup></th>
                <th colspan="5">การรั่ว</th>
                <th colspan="5">การเสื่อมสภาพ<sup>2</sup></th>
                <th colspan="5">สภาพการใช้งาน</th>
                <th rowspan="2">หมายเหตุ</th>
            </tr>
            <tr>
                <?php for ($i = 0; $i < 4; $i++): ?>
                    <?php for ($j = 1; $j <= 5; $j++): ?>
                        <th class="score_template"><?php echo $j; ?></th>
                    <?php endfor; ?>
                <?php endfor; ?>
            </tr>
            <tr>
                <?php if ($score == ""): ?>
                    <td><b>[&nbsp;&nbsp;&nbsp;]</b>  มองไม่เห็น</td>

                    <td class="first"></td>
                    <td class="block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first"></td>
                    <td class="block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first"></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first"></td>
                <?php else: ?>
                    <td><b>[<?php notsee($score->c27); ?>]</b>  มองไม่เห็น</td>
                    <?php $item = getsingle($score->c27); ?>
                    <td class="first"><?php getans($item[0], 1); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[0], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[0], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[1], 2); ?></td>
                    <td><?php getans($item[1], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[1], 5); ?></td>

                    <td class="first"><?php getans($item[2], 1); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[2], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[2], 5); ?></td>

                    <td class="first"><?php getans($item[3], 1); ?></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td><?php getans($item[3], 5); ?></td>

                    <td class="first"><?php echo $item[4]; ?></td>
                <?php endif; ?>
            </tr>
        </tbody>
    </table>
<?php endif; ?>

<?php if ($data[29]): ?>
    <table>
        <tbody>
            <tr>
                <th class="headtitle" colspan="22">2. อาคารส่งน้ำ/ระบายน้ำ (Outlets) : 2.1 ท่อระบายน้ำลงลำน้ำเดิม (River Outlet) : 2.1.3 ส่วนควบคุมน้ำ : 2.1.3.6 สะพาน (Access Bridge) : 2.1.3.6.1 พื้น</th>
            </tr>

            <tr>
                <th class="menu" rowspan="2" >รายการ</th>
                <th colspan="5">การกัดเซาะ<sup>2,3</sup></th>
                <th colspan="5">การเคลื่อนตัว</th>
                <th colspan="5">การทรุดตัว<sup>2</sup></th>
                <th colspan="5">รอยแตกร้าว</th>
                <th rowspan="2">หมายเหตุ</th>
            </tr>
            <tr>
                <?php for ($i = 0; $i < 4; $i++): ?>
                    <?php for ($j = 1; $j <= 5; $j++): ?>
                        <th class="score_template"><?php echo $j; ?></th>
                    <?php endfor; ?>
                <?php endfor; ?>
            </tr>
            <tr>
                <?php if ($score == ""): ?>
                    <td><b>[&nbsp;&nbsp;&nbsp;]</b>  มองไม่เห็น</td>

                    <td class="first"></td>
                    <td class="block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    <td class="first"></td>
                <?php else: ?>
                    <td><b>[<?php notsee($score->c29) ?>]</b>  มองไม่เห็น</td>
                    <?php $item = getsingle($score->c29); ?>
                    <td class="first"><?php getans($item[0], 1) ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[0], 3) ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[0], 5) ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[1], 2) ?></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td><?php getans($item[1], 5) ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[2], 2) ?></td>
                    <td><?php getans($item[2], 3) ?></td>
                    <td><?php getans($item[2], 4) ?></td>
                    <td><?php getans($item[2], 5) ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[3], 2) ?></td>
                    <td><?php getans($item[3], 3) ?></td>
                    <td><?php getans($item[3], 4) ?></td>
                    <td><?php getans($item[3], 5) ?></td>

                    <td class="first"><?php echo $item[4]; ?></td>
                <?php endif; ?>
            </tr>
        </tbody>
    </table>
<?php endif; ?>

<?php if ($data[30]): ?>
    <table>
        <tbody>
            <tr>
                <th class="headtitle" colspan="22">2. อาคารส่งน้ำ/ระบายน้ำ (Outlets) : 2.1 ท่อระบายน้ำลงลำน้ำเดิม (River Outlet) : 2.1.3 ส่วนควบคุมน้ำ : 2.1.3.6 สะพาน (Access Bridge) : 2.1.3.6.2 ตอม่อ</th>
            </tr>

            <tr>
                <th class="menu" rowspan="2" >รายการ</th>
                <th colspan="5">การกัดเซาะ<sup>2,3</sup></th>
                <th colspan="5">การเคลื่อนตัว</th>
                <th colspan="5">การทรุดตัว<sup>2</sup></th>
                <th colspan="5">รอยแตกร้าว</th>
                <th rowspan="2">หมายเหตุ</th>
            </tr>
            <tr>
                <?php for ($i = 0; $i < 4; $i++): ?>
                    <?php for ($j = 1; $j <= 5; $j++): ?>
                        <th class="score_template"><?php echo $j; ?></th>
                    <?php endfor; ?>
                <?php endfor; ?>
            </tr>
            <tr>
                <?php if ($score == ""): ?>
                    <td><b>[&nbsp;&nbsp;&nbsp;]</b>  มองไม่เห็น</td>

                    <td class="first"></td>
                    <td class="block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    <td class="first"></td>
                <?php else: ?>
                    <td><b>[<?php notsee($score->c30); ?>]</b>  มองไม่เห็น</td>
                    <?php $item = getsingle($score->c30); ?>
                    <td class="first"><?php getans($item[0], 1) ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[0], 3) ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[0], 5) ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[1], 2) ?></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td><?php getans($item[1], 5) ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[2], 2) ?></td>
                    <td><?php getans($item[2], 3) ?></td>
                    <td><?php getans($item[2], 4) ?></td>
                    <td><?php getans($item[2], 5) ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[3], 2) ?></td>
                    <td><?php getans($item[3], 3) ?></td>
                    <td><?php getans($item[3], 4) ?></td>
                    <td><?php getans($item[3], 5) ?></td>

                    <td class="first"><?php echo $item[4]; ?></td>
                <?php endif; ?>
            </tr>
        </tbody>
    </table>
<?php endif; ?>

<?php if ($data[31]): ?>
    <table>
        <tbody>
            <tr>
                <th class="headtitle" colspan="22">2. อาคารส่งน้ำ/ระบายน้ำ (Outlets) : 2.1 ท่อระบายน้ำลงลำน้ำเดิม (River Outlet) : 2.1.3 ส่วนควบคุมน้ำ : 2.1.3.6 สะพาน (Access Bridge) : 2.1.3.6.3 คาน</th>
            </tr>

            <tr>
                <th class="menu" rowspan="2" >รายการ</th>
                <th colspan="5">การกัดเซาะ<sup>2,3</sup></th>
                <th colspan="5">การเคลื่อนตัว</th>
                <th colspan="5">การทรุดตัว<sup>2</sup></th>
                <th colspan="5">รอยแตกร้าว</th>
                <th rowspan="2">หมายเหตุ</th>
            </tr>
            <tr>
                <?php for ($i = 0; $i < 4; $i++): ?>
                    <?php for ($j = 1; $j <= 5; $j++): ?>
                        <th class="score_template"><?php echo $j; ?></th>
                    <?php endfor; ?>
                <?php endfor; ?>
            </tr>
            <tr>
                <?php if ($score == ""): ?>
                    <td><b>[&nbsp;&nbsp;&nbsp;]</b>  มองไม่เห็น</td>

                    <td class="first"></td>
                    <td class="block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    <td class="first"></td>
                <?php else: ?>

                    <td><b>[<?php notsee($score->c31); ?>]</b>  มองไม่เห็น</td>
                    <?php $item = getsingle($score->c31); ?>
                    <td class="first"><?php getans($item[0], 1); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[0], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[0], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[1], 2); ?></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td><?php getans($item[1], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[2], 2); ?></td>
                    <td><?php getans($item[2], 3); ?></td>
                    <td><?php getans($item[2], 4); ?></td>
                    <td><?php getans($item[2], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[3], 2); ?></td>
                    <td><?php getans($item[3], 3); ?></td>
                    <td><?php getans($item[3], 4); ?></td>
                    <td><?php getans($item[3], 5); ?></td>

                    <td class="first"><?php echo $item[4]; ?></td>
                <?php endif; ?>
            </tr>
        </tbody>
    </table>
<?php endif; ?>

<?php if ($data[33]): ?>
    <table>
        <tbody>
            <tr>
                <th class="headtitle" colspan="31">2. อาคารส่งน้ำ/ระบายน้ำ (Outlets) : 2.1 ท่อระบายน้ำลงลำน้ำเดิม (River Outlet) : 2.1.4 ส่วนทางน้ำออก : 2.1.4.1 รางเท : 2.1.4.1.1 พื้น</th>
            </tr>

            <tr>
                <th class="menu" rowspan="2" >รายการ</th>
                <th colspan="5">การกัดเซาะ<sup>2,3</sup></th>
                <th colspan="5">การเคลื่อนตัว</th>
                <th colspan="5">การทรุดตัว<sup>2</sup></th>
                <th colspan="5">การบวมตัว</th>
                <th colspan="5">การระบายน้ำ</th>
                <th colspan="5">การรั่ว</th>
            </tr>
            <tr>
                <?php for ($i = 0; $i < 6; $i++): ?>
                    <?php for ($j = 1; $j <= 5; $j++): ?>
                        <th class="score_template"><?php echo $j; ?></th>
                    <?php endfor; ?>
                <?php endfor; ?>
            </tr>
            <tr>
                <?php if ($score == ""): ?>
                    <td><b>[&nbsp;&nbsp;&nbsp;]</b>  มองไม่เห็น</td>

                    <td class="first"></td>
                    <td class="block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first"></td>
                    <td></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>
                <?php else: ?>
                    <td><b>[<?php notsee($score->c33) ?>]</b>  มองไม่เห็น</td>
                    <?php $item = getsingle($score->c33); ?>
                    <td class="first"><?php getans($item[0], 1); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[0], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[0], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[1], 2); ?></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td><?php getans($item[1], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[2], 2); ?></td>
                    <td><?php getans($item[2], 3); ?></td>
                    <td><?php getans($item[2], 4); ?></td>
                    <td><?php getans($item[2], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[3], 2); ?></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td><?php getans($item[3], 5); ?></td>

                    <td class="first"><?php getans($item[4], 1); ?></td>
                    <td><?php getans($item[4], 2); ?></td>
                    <td><?php getans($item[4], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[4], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[5], 2); ?></td>
                    <td><?php getans($item[5], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[5], 5); ?></td>
                <?php endif; ?>
            </tr>

            <tr>
                <th class="menu" rowspan="2" >รายการ</th>
                <th colspan="5">รอยแตกร้าว</th>
                <th colspan="5">ต้นไม้</th>
                <th colspan="5">วัชพืช</th>
                <th colspan="5">สิ่งกีดขวางทางน้ำ</th>
                <th colspan="10" rowspan="2">หมายเหตุ</th>
            </tr>
            <tr>
                <?php for ($i = 0; $i < 4; $i++): ?>
                    <?php for ($j = 1; $j <= 5; $j++): ?>
                        <th class="score_template"><?php echo $j; ?></th>
                    <?php endfor; ?>
                <?php endfor; ?>
            </tr>
            <tr>
                <td></td>
                <?php if ($score == ""): ?>
                    <td class="first block"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    <td class="first"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>
                    <td></td>

                    <td class="first"></td>
                    <td class="block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first" colspan="10"></td>
                <?php else: ?>
                    <td class="first block"></td>
                    <td><?php getans($item[6], 2); ?></td>
                    <td><?php getans($item[6], 3); ?></td>
                    <td><?php getans($item[6], 4); ?></td>
                    <td><?php getans($item[6], 5); ?></td>

                    <td class="first"><?php getans($item[7], 1); ?></td>
                    <td><?php getans($item[7], 2); ?></td>
                    <td><?php getans($item[7], 3); ?></td>
                    <td><?php getans($item[7], 4); ?></td>
                    <td><?php getans($item[7], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[8], 2); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[8], 4); ?></td>
                    <td><?php getans($item[8], 5); ?></td>

                    <td class="first"><?php getans($item[9], 1); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[9], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[9], 5); ?></td>

                    <td class="first" colspan="10"><?php echo $item[10]; ?></td>
                <?php endif; ?>
            </tr>
        </tbody>
    </table>
<?php endif; ?>

<?php if ($data[25] || $data[26] || $data[27] || $data[29] || $data[30] || $data[31] || $data[33]): ?>
    <table class="desc">
        <tbody>
            <tr>
                <td class="col1">หมายเหตุ</td>
                <td class="col2">ระบบรักษาความปลอดภัย</td>
                <td class="col3">(1) ไม่มีระบบรักษาความปลอดภัย (2) มีระบบรักษาความปลอดภัย แต่ใช้การไม่ได้ (5) มีระบบรักษาความปลอดภัย ใช้การได้</td>
            </tr>
            <tr>
                <td></td>
                <td>การเสื่อมสภาพ<sup>2</sup></td>
                <td>(1) เกิดการเสื่อมสภาพใช้งานไม่ได้ (3) เกิดการเสื่อมสภาพใช้งานได้ (5) ไม่เกิดการเสื่อมสภาพ</td>
            </tr>
            <tr>
                <td></td>
                <td>สภาพการใช้งาน</td>
                <td>(1) ใช้งานไม่ได้ (5) ใช้งานได้</td>
            </tr>
            <tr>
                <td></td>
                <td>การกัดเซาะ<sup>3</sup></td>
                <td>(1) เกิดสนิมกัดกร่อนถึงเนื้อใน (3) เกิดสนิมที่ผิวเหล็ก (5) ไม่เกิดสนิม</td>
            </tr>
            <tr>
                <td></td>
                <td>การรั่ว</td>
                <td>(2) เกิดการรั่วและมีน้ำไหลพุ่งออกมา (3) เกิดการรั่วและมีน้ำไหลซึม (5) ไม่เกิดการรั่ว</td>
            </tr>
            <tr>
                <td></td>
                <td>การกัดเซาะ<sup>2</sup></td>
                <td>(1) กัดกร่อนเห็นเนื้อเหล็ก  (3) เกิดการกัดกร่อน (5) ไม่เกิดการกัดกร่อน</td>
            </tr>
            <tr>
                <td></td>
                <td>การเคลื่อนตัว</td>
                <td>(2) เกิดการเคลื่อนตัว (5) ไม่เกิดการเคลื่อนตัว</td>
            </tr>
            <tr>
                <td></td>
                <td>การทรุดตัว<sup>2</sup></td>
                <td>(2) ทรุดตัว > 5 ซม. (3) ทรุดตัวลึกอยู่ระหว่าง 2-5 ซม. (4) ทรุดตัว < 2 ซม. (5) ไม่เกิดการทรุดตัว</td>
            </tr>
            <tr>
                <td></td>
                <td>รอยแตกร้าว</td>
                <td>(2) เกิดรอยร้าวมีความกว้างและความลึกเป็นทางยาว (3) เกิดรอยร้าวมีความกว้างและความลึกบางจุด  (4) เกิดรอยร้าวเนื่องจากอุณหูมิ (5) ไม่เกิดรอยร้าว</td>
            </tr>
            <tr>
                <td></td>
                <td>การบวมตัว</td>
                <td>(2) บวมตัว (5) ไม่บวมตัว</td>
            </tr>
            <tr>
                <td></td>
                <td>สิ่งกีดขวางทางน้ำ</td>
                <td>(1) มีสิ่งกีดขวางทางน้ำ และขวางทางเดินน้ำทั้งหมด (3) มีสิ่งกีดขวางทางน้ำ แต่ปิดขวางทางเดินน้ำบางส่วน (5) ไม่มีสิ่งกีดขวางทางน้ำ</td>
            </tr>
        </tbody>
    </table>
    <pagebreak />
<?php endif; ?>