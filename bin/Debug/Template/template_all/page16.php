<!--Function 99-->
<?php if ($data[99]): ?>
    <table>		
        <tbody>
            <tr>
                <th class="headtitle" colspan="31">
                    <b>3. อาคารระบายน้ำล้น (Spillways)</b> :  3.1 อาคารระบายน้ำล้นใช้งาน (Service Spillway) : 3.1.1 แบบไม่มีบาน (Ungated Spillway): 3.1.1.5 
                    <b><i><u>ส่วนคลองระบายน้ำ</u></i></b>
                </th>
            </tr>

            <tr>
                <th class="menu">รายการ</th>
                <th colspan="5"><b>การกัดเซาะ<sup>1,2</sup></b></th>
                <th colspan="5"><b>การเกิดร่องน้ำ</b></th>
                <th colspan="5"><b>การทรุดตัว<sup>1</sup></b></th>
                <th colspan="5"><b>การซึม</b></th>
                <th colspan="5"><b>การรั่ว</b></th>
                <th colspan="5"><b>การเลื่อนไถล</b></th>
            </tr>
            <tr>
                <td class="first"></td>
                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>
            </tr>
            <tr>
                <?php if ($score == ""): //start blank table?>
                    <td><b>[&nbsp;&nbsp;&nbsp;]</b> มองไม่เห็น</td>
                    <td class="first"></td>
                    <td class="block"></td>
                    <td ></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first block"></td>
                    <td ></td>
                    <td class="block"></td>
                    <td ></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    <td class="first block"></td>
                    <td ></td>
                    <td ></td>
                    <td ></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td ></td>
                    <td class="block"></td>
                    <td ></td>


                    <td class="first block"></td>
                    <td ></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td ></td>
                <?php else: //end blank table, start data table?>
                    <td><b>[<?php notsee($score->c99); ?>]</b> มองไม่เห็น</td>
                    <?php $item = getsingle($score->c99); ?>
                    <td class="first"><?php getans($item[0], 1); ?></td>
                    <td class="block"></td>
                    <td ><?php getans($item[0], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[0], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[1], 2); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[1], 4); ?></td>
                    <td><?php getans($item[1], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[2], 2); ?></td>
                    <td><?php getans($item[2], 3); ?></td>
                    <td><?php getans($item[2], 4); ?></td>
                    <td><?php getans($item[2], 5); ?></td>

                    <td class="first block"></td>
                    <td ><?php getans($item[3], 2); ?></td>
                    <td ><?php getans($item[3], 3); ?></td>
                    <td ><?php getans($item[3], 4); ?></td>
                    <td ><?php getans($item[3], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[4], 2); ?></td>
                    <td ><?php getans($item[4], 3); ?></td>
                    <td class="block"></td>
                    <td ><?php getans($item[4], 5); ?></td>


                    <td class="first block"></td>
                    <td ><?php getans($item[5], 2); ?></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td ><?php getans($item[5], 5); ?></td>
                <?php endif; //end data table?>
            </tr>

            <!--Section 2-->
            <tr>
                <th class="menu" rowspan="2">รายการ</th>
                <th colspan="5"><b>การเสื่อมสภาพ<sup>1</sup></b></th>
                <th colspan="5"><b>รอยแตกร้าว</b></th>
                <th colspan="5"><b>รูโพรง<sup>1</sup></b></th>
                <th colspan="5"><b>ต้นไม้</b></th>
                <th colspan="5"><b>วัชพืช</b></th>
                <th  colspan="5"><b>สิ่งกีดขวางทางน้ำ</b></th>
            </tr>
            <tr>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>
            </tr>
            <tr>
                <?php if ($score == ""): //start blank table?>
                    <td class="first"></td>
                    <td class="first"></td>
                    <td class="block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>
                    <td></td>

                    <td class="first"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>
                    <td></td>


                    <td class="first"></td>
                    <td class="block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>		
                <?php else: //end blank table, start data table?>
                    <td class="first"></td>

                    <td class="first"><?php getans($item[6], 1); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[6], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[6], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[7], 2); ?></td>
                    <td><?php getans($item[7], 3); ?></td>
                    <td><?php getans($item[7], 4); ?></td>
                    <td><?php getans($item[7], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[8], 2); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[8], 4); ?></td>
                    <td><?php getans($item[8], 5); ?></td>

                    <td class="first"><?php getans($item[9], 1); ?></td>
                    <td><?php getans($item[9], 2); ?></td>
                    <td><?php getans($item[9], 3); ?></td>
                    <td><?php getans($item[9], 4); ?></td>
                    <td><?php getans($item[9], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[10], 2); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[10], 4); ?></td>
                    <td><?php getans($item[10], 5); ?></td>


                    <td class="first"><?php getans($item[11], 1); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[11], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[11], 5); ?></td>	

                <?php endif; //end data table?>
            </tr>
        </tbody>
    </table>
<?php endif ?> <!--End Function 99-->


<!--Function 108-->
<?php if ($data[108]): ?>
    <table>
        <tbody>
            <tr>
                <th class="headtitle" colspan="22"><b>  
                        3. อาคารระบายน้ำล้น (Spillways)</b> : 3.1 อาคารระบายน้ำล้นใช้งาน (Service Spillway) :3.1.1 แบบไม่มีบาน (Ungate Spillway): 3.1.1.6 สะพานรถยนต์ : 
                    3.1.1.6.1 <b><i><u>พื้น</u></i></b>
                </th>
            </tr>		
            <tr>
                <th class="menu">รายการ</th>
                <th colspan="5"><b>การกัดเซาะ<sup>2</sup></b></th>
                <th colspan="5"><b>การเคลื่อนตัว</b></th>
                <th colspan="5"><b>การทรุดตัว<sup>2</sup></b></th>
                <th colspan="5"><b>รอยแตกร้าว</b></th>
                <th rowspan="2"><b>หมายเหตุ</b></th>
            </tr>
            <tr>
                <td class="first"></td>
                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>												
            </tr>
            <tr>
                <?php if ($score == ""): //start blank table?>
                    <td><b>[&nbsp;&nbsp;&nbsp;]</b> มองไม่เห็น</td>
                    <td class="first"></td>
                    <td class="block"></td>
                    <td ></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first block"></td>
                    <td ></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    <td class="first"></td>
                <?php else: //end blank table, start data table?>
                    <td><b>[<?php notsee($score->c108); ?>]</b> มองไม่เห็น</td>
                    <?php $item = getsingle($score->c108); ?>
                    <td class="first"><?php getans($item[0], 1); ?></td>
                    <td class="block"></td>
                    <td ><?php getans($item[0], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[0], 5); ?></td>

                    <td class="first block"></td>
                    <td ><?php getans($item[1], 2); ?></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td><?php getans($item[1], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[2], 2); ?></td>
                    <td><?php getans($item[2], 3); ?></td>
                    <td><?php getans($item[2], 4); ?></td>
                    <td><?php getans($item[2], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[3], 2); ?></td>
                    <td><?php getans($item[3], 3); ?></td>
                    <td><?php getans($item[3], 4); ?></td>
                    <td><?php getans($item[3], 5); ?></td>

                    <td class="first"><?php echo $item[4]; ?></td>
                <?php endif; ?>
            </tr>
        </tbody>
    </table>
<?php endif ?> <!--End Function 108-->


<!--Function 109-->
<?php if ($data[109]): ?>
    <table>
        <tbody>
            <tr>
                <th class="headtitle" colspan="22"><b>  
                        3. อาคารระบายน้ำล้น (Spillways)</b> : 3.1 อาคารระบายน้ำล้นใช้งาน (Service Spillway) :3.1.1 แบบไม่มีบาน (Ungate Spillway): 3.1.1.6 สะพานรถยนต์ : 
                    3.1.1.6.2 <b><i><u>ต่อม่อ</u></i></b>
                </th>
            </tr>

            <tr>
                <th class="menu">รายการ</th>
                <th colspan="5"><b>การกัดเซาะ<sup>2</sup></b></th>
                <th colspan="5"><b>การเคลื่อนตัว</b></th>
                <th colspan="5"><b>การทรุดตัว<sup>2</sup></b></th>
                <th colspan="5"><b>รอยแตกร้าว</b></th>
                <th rowspan="2"><b>หมายเหตุ</b></th>
            </tr>
            <tr>
                <td class="first"></td>
                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>											
            </tr>
            <tr>
                <?php if ($score == ""): //start blank table?>
                    <td><b>[&nbsp;&nbsp;&nbsp;]</b> มองไม่เห็น</td>

                    <td class="first"></td>
                    <td class="block"></td>
                    <td ></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first block"></td>
                    <td ></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    <td class="first"></td>
                <?php else: //end blank table, start data table?>
                    <td><b>[<?php notsee($score->c109); ?>]</b> มองไม่เห็น</td>
                    <?php $item = getsingle($score->c109); ?>
                    <td class="first"><?php getans($item[0], 1); ?></td>
                    <td class="block"></td>
                    <td ><?php getans($item[0], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[0], 5); ?></td>

                    <td class="first block"></td>
                    <td ><?php getans($item[1], 2); ?></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td><?php getans($item[1], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[2], 2); ?></td>
                    <td><?php getans($item[2], 3); ?></td>
                    <td><?php getans($item[2], 4); ?></td>
                    <td><?php getans($item[2], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[3], 2); ?></td>
                    <td><?php getans($item[3], 3); ?></td>
                    <td><?php getans($item[3], 4); ?></td>
                    <td><?php getans($item[3], 5); ?></td>

                    <td class="first"><?php echo $item[4]; ?></td>
                <?php endif; //end data table?>
            </tr>
        </tbody>
    </table>
<?php endif ?> <!--Function 109-->


<!--Function 110-->
<?php if ($data[110]): ?>
    <table>
        <tbody>
            <tr>
                <th class="headtitle" colspan="22"><b>  
                    3. อาคารระบายน้ำล้น (Spillways)</b> : 3.1 อาคารระบายน้ำล้นใช้งาน (Service Spillway) :3.1.1 แบบไม่มีบาน (Ungate Spillway): 3.1.1.6 สะพานรถยนต์ : 
                    3.1.1.6.3 <b><i><u>คาน</u></i></b>
                </th>
            </tr>

            <tr>
                <th class="menu">รายการ</th>
                <th colspan="5"><b>การกัดเซาะ<sup>2</sup></b></th>
                <th colspan="5"><b>การเคลื่อนตัว</b></th>
                <th colspan="5"><b>การทรุดตัว<sup>2</sup></b></th>
                <th colspan="5"><b>รอยแตกร้าว</b></th>
                <th rowspan="2"><b>หมายเหตุ</b></th>
            </tr>
            <tr>
                <td class="first"></td>
                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>											
            </tr>
            <tr>
                <?php if ($score == ""): //start blank table?>
                    <td><b>[&nbsp;&nbsp;&nbsp;]</b> มองไม่เห็น</td>

                    <td class="first"></td>
                    <td class="block"></td>
                    <td ></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first block"></td>
                    <td ></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    <td class="first"></td>
                <?php else: //end blank table, start data table?>
                    <td><b>[<?php notsee($score->c110); ?>]</b> มองไม่เห็น</td>
                    <?php $item = getsingle($score->c110); ?>
                    <td class="first"><?php getans($item[0], 1); ?></td>
                    <td class="block"></td>
                    <td ><?php getans($item[0], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[0], 5); ?></td>

                    <td class="first block"></td>
                    <td ><?php getans($item[1], 2); ?></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td><?php getans($item[1], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[2], 2); ?></td>
                    <td><?php getans($item[2], 3); ?></td>
                    <td><?php getans($item[2], 4); ?></td>
                    <td><?php getans($item[2], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[3], 2); ?></td>
                    <td><?php getans($item[3], 3); ?></td>
                    <td><?php getans($item[3], 4); ?></td>
                    <td><?php getans($item[3], 5); ?></td>

                    <td class="first"><?php echo $item[4]; ?></td>
                <?php endif; //end data table?>

            </tr>
        </tbody>
    </table>
<?php endif ?> <!--Function 110-->

<!--Function detail print-->
<?php if ($data[99] || $data[108] || $data[109] || $data[110]): ?>
    <table class="desc">
        <tbody>
            <tr>
                <td class="col1"><b><u><i>หมายเหตุ</i></u></b></td>
                <td class="col2">การกัดเซาะ <sup>1</sup></td>
                <td class="col3">(1)กัดเซาะเสียหาย &gt; 50% (3)กัดเซาะเสียหาย &lt; 50% (5) ไม่เกิดการกัดเซาะ</td>
            </tr>    
            <tr>
                <td></td>
                <td class="col2">การเกิดร่องน้ำ</td>
                <td class="col3">(2) เกิดร่องน้ำลึกประมาณเข่า (4) เกิดร่องน้ำลึกประมาณข้อเท้า (5) ไม่เกิดร่องน้ำ</td>
            </tr>
            <tr>
                <td></td>
                <td class="col2">การทรุดตัว<sup>1</sup></td>
                <td>(2) ทรุดตัวประมาณเอว/ &gt; เอว (3) ทรุดตัวลึกประมาณเข่า/ &gt; แต่ไม่ถึงเอว (4) ทรุดตัวลึกประมาณข้อเท้า/ &gt; แต่ไม่ถึงเข่า (5) ไม่เกิดการทรุดตัว</td>
            </tr>
            <tr>
                <td></td>
                <td class="col2">การซึม</td>
                <td>(2) เกิดการซึมและมีการไหลรวม (3) เกิดการซึมเป็นความยาวมากกว่า 30% (4) เกิดการซึมเป็นทางยาวน้อยกว่า 30% (5) ไม่เกิดการซึม</td>
            </tr>
            <tr>
                <td></td>
                <td class="col2">การรั่ว</td>
                <td>(2) เกิดการรั่วและมีน้ำไหลพุ่งออกมา (3) เกิดการรั่วและมีน้ำไหลซึม (5) ไม่เกิดการรั่ว</td>
            </tr>
            <tr>
                <td></td>
                <td class="col2">การเลื่อนไถล</td>
                <td>(2) เกิดการเลื่อนไถล (5) ไม่เกิดการเลื่อนไถล</td> 
            </tr>
            <tr>
                <td></td>
                <td class="col2">การเสื่อมสภาพ<sup>1</sup></td>
                <td>(1) เสื่อมสภาพเสียหาย &gt;50%  (3) เสื่อมสภาพเสียหาย &lt; 50% (5)ไม่เกิดการเสื่อมสภาพ</td>
            </tr>				
            <tr>
                <td></td>
                <td class="col2">รอยแตกร้าว</td>
                <td>(2) เกิดรอยร้าวมีควมกว้างและความลึกเป็นทางยาว (3)เกิดรอยร้วมีความกว้างและความลึกบางจุด (4)เกิดรอยร้าวเนื่องจาก (5)ไม่เกิดรอยร้าว</td>
            </tr>

            <tr>
                <td></td>
                <td class="col2">รู โพรง</td>
                <td>(2) มีรูโพรงลึกมากกว่าเข่า (4) มีรูโพรงลึกน้อยกว่าเข่า (5) ไม่มีรูโพรง</td>  
            </tr>

            <tr>
                <td></td>
                <td class="col2">ต้นไม้</td>
                <td>(1) ต้นไม้สูงกว่าหัว (2) ต้นไม้สูงกว่าเอว (3) ต้นไม้สูงต่ำกว่าเอว (4) ต้นไม้ต่ำกว่าเข่า (5) ไม่มีต้นไม้</td>
            </tr>

            <tr>
                <td></td>
                <td class="col2">วัชพืช</td>
                <td>(2) มีวัชพืชปกคลุมมากกว่า 50% ของพื้นที่ (4) มีวัชพืชปกคลุมน้อยกว่า 50% ของพื้นที่ (5) ไม่มีวัชพืชปกคลุม</td>
            </tr>

            <tr>
                <td></td>
                <td class="col2">สิ่งกีดขวางทางน้ำ</td>
                <td>(1) มีสิ่งกีดขวางทางน้ำ และขวางทางเดินน้ำทั้งหมด (3) มีสิ่งกีดขวางทางน้ำ แต่ปิดขวางทางเดินน้ำบางส่วน (5) ไม่มีสิ่งกีดขวางทางน้ำ</td>
            </tr>

            <tr>
                <td></td>
                <td class="col2">การกัดเซาะ<sup>2</sup></td>
                <td>(1) กัดเซาะเห็นเนื้อเหล็ก  (3) เกิดการกัดเซาะ (5) ไม่เกิดการกัดเซาะ</td>
            </tr>

            <tr>
                <td></td>
                <td class="col2">การเคลื่อนตัว</td>
                <td>(2) เกิดการเคลื่อนตัว (5) ไม่เกิดการเคลื่อนตัว</td>
            </tr>
            <tr>
                <td></td>
                <td class="col2">การทรุดตัว<sup>2</sup></td>
                <td>(2) ทรุดตัว &gt; 5 ซม. (3) ทรุดตัวลึกอยู่ระหว่าง 2-5 ซม. (4) ทรุดตัว &lt; 2 ซม. (5) ไม่เกิดการทรุดตัว</td>
            </tr>
        </tbody>
    </table>	
    <pagebreak />
<?php endif ?> <!--Function detail print-->