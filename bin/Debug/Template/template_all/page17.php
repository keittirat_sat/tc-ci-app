<?php if ($data[117]): ?>
    <table>

        <tbody>
            <tr>
                <th class="headtitle" colspan="31">
                    <b>3. อาคารระบายน้ำล้น (Spillways)</b> :  3.1 อาคารระบายน้ำล้นใช้งาน (Service Spillway) : 3.1.2 แบบมีบาน (Gated Spillway): 3.1.2.1 ส่วนทางน้ำเข้า 3.1.2.1.1 
                    <b><i><u>ฟื้น</u></i></b><b>[&nbsp;&nbsp;&nbsp;]</b><b>ไม่ใช้ชนิดนี้</b>
                </th>
            </tr>

            <tr>
                <th class="menu" rowspan="2">รายการ</th>
                <th colspan="5"><b>การกัดเซาะ<sup>2</sup></b></th>
                <th colspan="5"><b>การเคลื่อนตัว</b></th>
                <th colspan="5"><b>การทรุดตัว<sup>2</sup></b></th>
                <th colspan="5"><b>การบวมตัว</b></th>
                <th colspan="5"><b>การระบายน้ำ</b></th>
                <th colspan="5"><b>การรั่ว</b></th>
            </tr>
            <tr>
                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>
                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>
                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>
                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>
                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>
                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>
            </tr>
            <tr>
                <?php if ($score == ""): //start blank table?>
                    <td><b>[&nbsp;&nbsp;&nbsp;]</b> มองไม่เห็น</td>
                    <td class="first"></td>
                    <td class="block"></td>
                    <td ></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first block"></td>
                    <td ></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    <td class="first block"></td>
                    <td ></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first"></td>
                    <td></td>
                    <td ></td>
                    <td class="block"></td>
                    <td ></td>


                    <td class="first block"></td>
                    <td ></td>
                    <td ></td>
                    <td class="block"></td>
                    <td ></td>
                <?php else: //end blank table, start data table?>
                    <td><b>[<?php notsee($score->c117); ?>]</b> มองไม่เห็น</td>
                    <?php $item = getsingle($score->c117); ?>
                    <td class="first"><?php getans($item[0], 1); ?></td>
                    <td class="block"></td>
                    <td ><?php getans($item[0], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[0], 5); ?></td>

                    <td class="first block"></td>
                    <td ><?php getans($item[1], 2); ?></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td><?php getans($item[1], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[2], 2); ?></td>
                    <td><?php getans($item[2], 3); ?></td>
                    <td><?php getans($item[2], 4); ?></td>
                    <td><?php getans($item[2], 5); ?></td>

                    <td class="first block"></td>
                    <td ><?php getans($item[3], 2); ?></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td><?php getans($item[3], 5); ?></td>

                    <td class="first"><?php getans($item[4], 1); ?></td>
                    <td><?php getans($item[4], 2); ?></td>
                    <td ><?php getans($item[4], 3); ?></td>
                    <td class="block"></td>
                    <td ><?php getans($item[4], 5); ?></td>


                    <td class="first block"></td>
                    <td ><?php getans($item[5], 2); ?></td>
                    <td ><?php getans($item[5], 3); ?></td>
                    <td class="block"></td>
                    <td ><?php getans($item[5], 5); ?></td>
                <?php endif; //end data table?>
            </tr>

            <!--Section 2-->
            <tr>



                <th class="menu" rowspan="3">รายการ</th>
                <th colspan="5"><b>รอยแตกร้าว</b></th>
                <th colspan="5"><b>ต้นไม้</b></th>
                <th colspan="5"><b>วัชพืช</b></th>
                <th colspan="5"><b>สิ่งกีดขวางทางน้ำ</b></th>
                <th colspan="10" rowspan="2"><b>หมายเหตุ</b></th>
            </tr>
            <tr>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>
                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>
                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>
                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>



            </tr>
            <tr>
                <?php if ($score == ""): //start blank table?>
                    <td class="first block"></td>
                    <td ></td>
                    <td ></td>
                    <td ></td>
                    <td></td>

                    <td class="first"></td>
                    <td ></td>
                    <td ></td>
                    <td ></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>
                    <td></td>

                    <td class="first"></td>
                    <td ></td>
                    <td ></td>
                    <td ></td>
                    <td></td>

                    <td colspan="10" class="first"></td>
                <?php else: //end blank table, start data table?>
                    <td class="first block"></td>
                    <td ><?php getans($item[6], 2); ?></td>
                    <td ><?php getans($item[6], 3); ?></td>
                    <td ><?php getans($item[6], 4); ?></td>
                    <td><?php getans($item[6], 5); ?></td>

                    <td class="first"><?php getans($item[7], 1); ?></td>
                    <td ><?php getans($item[7], 2); ?></td>
                    <td ><?php getans($item[7], 3); ?></td>
                    <td ><?php getans($item[7], 4); ?></td>
                    <td><?php getans($item[7], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[8], 2); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[8], 4); ?></td>
                    <td><?php getans($item[8], 5); ?></td>

                    <td class="first"><?php getans($item[9], 1); ?></td>
                    <td ><?php getans($item[9], 2); ?></td>
                    <td ><?php getans($item[9], 3); ?></td>
                    <td ><?php getans($item[9], 4); ?></td>
                    <td><?php getans($item[9], 5); ?></td>

                    <td colspan="10" class="first"><?php echo $item[10]; ?></td>
                <?php endif; //end data table?>

            </tr>

        </tbody>
    </table>
<?php endif ?>

<?php if ($data[118]): ?>
    <table>

        <tbody>
            <tr>
                <th class="headtitle" colspan="31"><b>
                        3. อาคารระบายน้ำล้น (Spillways)</b> :  3.1 อาคารระบายน้ำล้นใช้งาน (Service Spillway) : 3.1.2 แบบมีบาน (Gated Spillway): 3.1.2.1 ส่วนทางน้ำเข้า 
                    3.1.2.1.1 <b><i><u>ลาดด้านข้าง</u></i></b>
                </th>
            </tr>

            <tr>
                <th class="menu" rowspan="2">รายการ</th>
                <th colspan="5"><b>การกัดเซาะ<sup>2</sup></b></th>
                <th colspan="5"><b>การเคลื่อนตัว</b></th>
                <th colspan="5"><b>การทรุดตัว<sup>2</sup></b></th>
                <th colspan="5"><b>การระบายน้ำ</b></th>
                <th colspan="5"><b>การรั่ว</b></th>
                <th colspan="5"><b>รอยแตกร้าว</b></th>
            </tr>
            <tr>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>
                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>
                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>
                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>
                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>
                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>
            </tr>
            <tr>
                <?php if ($score == ""): //start blank table?>

                    <td><b>[&nbsp;&nbsp;&nbsp;]</b> มองไม่เห็น</td>
                    <td class="first"></td>
                    <td class="block"></td>
                    <td ></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first block"></td>
                    <td ></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    <td class="first"></td>
                    <td ></td>
                    <td ></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td ></td>
                    <td class="block"></td>
                    <td ></td>


                    <td class="first block"></td>
                    <td ></td>
                    <td ></td>
                    <td ></td>
                    <td ></td>
                <?php else: //end blank table, start data table?>
                    <td><b>[<?php notsee($score->c118); ?>]</b> มองไม่เห็น</td>
                    <?php $item = getsingle($score->c118); ?>
                    <td class="first"><?php getans($item[0], 1); ?></td>
                    <td class="block"></td>
                    <td ><?php getans($item[0], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[0], 5); ?></td>

                    <td class="first block"></td>
                    <td ><?php getans($item[1], 2); ?></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td><?php getans($item[1], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[2], 2); ?></td>
                    <td><?php getans($item[2], 3); ?></td>
                    <td><?php getans($item[2], 4); ?></td>
                    <td><?php getans($item[2], 5); ?></td>

                    <td class="first"><?php getans($item[3], 1); ?></td>
                    <td ><?php getans($item[3], 2); ?></td>
                    <td ><?php getans($item[3], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[3], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[4], 2); ?></td>
                    <td ><?php getans($item[4], 3); ?></td>
                    <td class="block"></td>
                    <td ><?php getans($item[4], 5); ?></td>


                    <td class="first block"></td>
                    <td ><?php getans($item[5], 2); ?></td>
                    <td ><?php getans($item[5], 3); ?></td>
                    <td ><?php getans($item[5], 4); ?></td>
                    <td ><?php getans($item[5], 5); ?></td>
                <?php endif; //end data table?>
            </tr>

            <!--Section 2-->



            <tr>
                <th class="menu" rowspan="3">รายการ</th>
                <th colspan="5"><b>รูโพรง</b></th>
                <th colspan="5"><b>ต้นไม้</b></th>
                <th colspan="5"><b>วัชพืช</b></th>
                <th colspan="15" rowspan="2"><b>หมายเหตุ</b></th>
            </tr>
            <tr>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>
                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>
                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>




            </tr>
            <tr>
                <?php if ($score == ""): //start blank table?>
                    <td class="first block"></td>
                    <td ></td>
                    <td class="block"></td>
                    <td ></td>
                    <td></td>

                    <td class="first"></td>
                    <td ></td>
                    <td ></td>
                    <td ></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>
                    <td></td>

                    <td colspan="15" class="first"></td>

                <?php else: //end blank table, start data table?>
                    <td class="first block"></td>
                    <td ><?php getans($item[6], 2); ?></td>
                    <td class="block"></td>
                    <td ><?php getans($item[6], 4); ?></td>
                    <td><?php getans($item[6], 5); ?></td>

                    <td class="first"><?php getans($item[7], 1); ?></td>
                    <td ><?php getans($item[7], 2); ?></td>
                    <td ><?php getans($item[7], 3); ?></td>
                    <td ><?php getans($item[7], 4); ?></td>
                    <td><?php getans($item[7], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[8], 2); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[8], 4); ?></td>
                    <td><?php getans($item[8], 5); ?></td>

                    <td colspan="15" class="first"><?php echo $item[9]; ?></td>

                <?php endif; //end data table?>

            </tr>

        </tbody>
    </table>
<?php endif ?>

<?php if ($data[119]): ?>
    <table>

        <tbody>
            <tr>
                <th class="headtitle" colspan="31"><b>
                        3. อาคารระบายน้ำล้น (Spillways)</b> :  3.1 อาคารระบายน้ำล้นใช้งาน (Service Spillway) : 
                    3.1.2 แบบมีบาน (Gated Spillway): 3.1.2.2 ส่วนควบคุมน้ำ 3.1.2.2.1 <b><i><u>ฝายคอนกรีต</u></i></b>
                </th>
            </tr>

            <tr>
                <th class="menu" rowspan="2">รายการ</th>
                <th colspan="5"><b>การกัดเซาะ<sup>2</sup></b></th>
                <th colspan="5"><b>การเคลื่อนตัว</b></th>
                <th colspan="5"><b>การทรุดตัว<sup>2</sup></b></th>
                <th colspan="5"><b>การรั่ว</b></th>
                <th colspan="5"><b>รอยแตกร้าว</b></th>
                <th colspan="5"><b>ต้นไม้</b></th>
            </tr>
            <tr>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>
                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>
                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>
                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>
                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>
                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>
            </tr>
            <tr>
                <?php if ($score == ""): //start blank table?>
                    <td><b>[&nbsp;&nbsp;&nbsp;]</b> มองไม่เห็น</td>
                    <td class="first"></td>
                    <td class="block"></td>
                    <td ></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first block"></td>
                    <td ></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    <td class="first block"></td>
                    <td ></td>
                    <td ></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td ></td>
                    <td ></td>
                    <td ></td>


                    <td class="first"></td>
                    <td ></td>
                    <td ></td>
                    <td ></td>
                    <td ></td>
                <?php else: //end blank table, start data table?>
                    <td><b>[<?php notsee($score->c119); ?>]</b> มองไม่เห็น</td>
                    <?php $item = getsingle($score->c119); ?>
                    <td class="first"><?php getans($item[0], 1); ?></td>
                    <td class="block"></td>
                    <td ><?php getans($item[0], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[0], 5); ?></td>

                    <td class="first block"></td>
                    <td ><?php getans($item[1], 2); ?></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td><?php getans($item[1], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[2], 2); ?></td>
                    <td><?php getans($item[2], 3); ?></td>
                    <td><?php getans($item[2], 4); ?></td>
                    <td><?php getans($item[2], 5); ?></td>

                    <td class="first block"></td>
                    <td ><?php getans($item[3], 2); ?></td>
                    <td ><?php getans($item[3], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[3], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[4], 2); ?></td>
                    <td ><?php getans($item[4], 3); ?></td>
                    <td ><?php getans($item[4], 4); ?></td>
                    <td ><?php getans($item[4], 5); ?></td>


                    <td class="first"><?php getans($item[5], 1); ?></td>
                    <td ><?php getans($item[5], 2); ?></td>
                    <td ><?php getans($item[5], 3); ?></td>
                    <td ><?php getans($item[5], 4); ?></td>
                    <td ><?php getans($item[5], 5); ?></td>
                <?php endif; //end data table?>
            </tr>

            <!--Section 2-->



            <tr>
                <th class="menu" rowspan="3">รายการ</th>
                <th colspan="5"><b>วัชพืช</b></th>
                <th colspan="5"><b>สิ่งกีดขวาง</b></th>

                <th colspan="20" rowspan="2"><b>หมายเหตุ</b></th>
            </tr>
            <tr>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>
                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>
            </tr>
            <tr>
                <?php if ($score == ""): //start blank table?>
                    <td class="first block"></td>
                    <td ></td>
                    <td class="block"></td>
                    <td ></td>
                    <td></td>

                    <td class="first"></td>
                    <td class="block" ></td>
                    <td ></td>
                    <td class="block"></td>
                    <td></td>

                    <td colspan="20" class="first"></td>

                <?php else: //end blank table, start data table?>
                    <td class="first block"><?php getans($item[6], 1); ?></td>
                    <td ><?php getans($item[6], 2); ?></td>
                    <td class="block"></td>
                    <td ><?php getans($item[6], 4); ?></td>
                    <td><?php getans($item[6], 5); ?></td>

                    <td class="first"><?php getans($item[7], 1); ?></td>
                    <td class="block" ></td>
                    <td ><?php getans($item[7], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[7], 5); ?></td>

                    <td colspan="20" class="first"><?php echo $item[8]; ?></td>
                <?php endif; //end data table?>
            </tr>

        </tbody>
    </table>
<?php endif ?>

<?php if ($data[126]): ?>

    <table>

        <tbody>
            <tr>
                <th class="headtitle" colspan="22"><b>
                        3. อาคารระบายน้ำล้น (Spillways)</b> :  3.1 อาคารระบายน้ำล้นใช้งาน (Service Spillway) : 
                    3.1.2 แบบมีบาน (Gated Spillway): 3.1.2.2 ส่วนควบคุมน้ำ 3.1.2.2.2.1 <b><i><u>พื้น</u></i></b>
                </th>
            </tr>

            <tr>
                <th class="menu" rowspan="2">รายการ</th>
                <th colspan="5"><b>การกัดเซาะ<sup>2</sup></b></th>
                <th colspan="5"><b>การเคลื่อนตัว</b></th>
                <th colspan="5"><b>การทรุดตัว<sup>2</sup></b></th>
                <th colspan="5"><b>รอยแตกร้าว</b></th>
                <th rowspan="2" ><b>หมายเหตุ</b></th>

            </tr>
            <tr>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>
                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>
                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>
                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

            </tr>

            <tr>
                <?php if ($score == ""): //start blank table?>
                    <td><b>[&nbsp;&nbsp;&nbsp;]</b> มองไม่เห็น</td>
                    <td class="first"></td>
                    <td class="block"></td>
                    <td ></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first block"></td>
                    <td ></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    <td class="first block"></td>
                    <td ></td>
                    <td ></td>
                    <td ></td>
                    <td></td>

                    <td class="first" ></td>
                <?php else: //end blank table, start data table?>
                    <td><b>[<?php notsee($score->c126); ?>]</b> มองไม่เห็น</td>
                    <?php $item = getsingle($score->c126); ?>
                    <td class="first"><?php getans($item[0], 1); ?></td>
                    <td class="block"></td>
                    <td ><?php getans($item[0], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[0], 5); ?></td>

                    <td class="first block"></td>
                    <td ><?php getans($item[2], 2); ?></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td><?php getans($item[2], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[3], 2); ?></td>
                    <td><?php getans($item[3], 3); ?></td>
                    <td><?php getans($item[3], 4); ?></td>
                    <td><?php getans($item[3], 5); ?></td>

                    <td class="first block"></td>
                    <td ><?php getans($item[4], 2); ?></td>
                    <td ><?php getans($item[4], 3); ?></td>
                    <td ><?php getans($item[4], 4); ?></td>
                    <td><?php getans($item[4], 5); ?></td>
                <?php endif; //end data table?>

            </tr>
    </table>
<?php endif ?>

<?php if ($data[117] || $data[118] || $data[119] || $data[126]): ?>
    <table class="desc">
        <tbody>
            <tr>
                <td class="col1"><b><u><i>หมายเหตุ</i></u></b></td>
                <td class="col2">การกัดเซาะ <sup>2</sup></td>
                <td class="col3">(1) กัดเซาะเห็นเนื้อเหล็ก (3) เกิดการกัดเซาะ (5) ไม่เกิดการกัดเซาะ</td>
            </tr>
            <tr>
                <td></td>
                <td class="col2">การเคลื่อนตัว</td>
                <td class="col3">(2) เกิดการเคลื่อนตัว (5) ไม่เกิดการเคลื่อนตัว</td>
            </tr>
            <tr>
                <td></td>
                <td class="col2">การทรุดตัว<sup>2</sup></td>
                <td>(2) ทรุดตัว &gt; 5 ซม. (3) ทรุดตัวลึกอยู่ระหว่าง 2-5 ซม. (4) ทรุดตัว &lt; 2 ซม. (5) ไม่เกิดการทรุดตัว</td>

            </tr>
            <tr>
                <td></td>
                <td class="col2">การบวมตัว</td>
                <td>(2) บวมตัว (5) ไม่บวมตัว</td>
            </tr>
            <tr>
                <td></td>
                <td class="col2">การระบายน้ำ</td>
                <td>(1) ท่อระบายน้ำ/รางระบายน้ำอุดตัน ไม่สามารถระบายน้ำได้โดยสิ้นเชิง (2) น้ำขุ่นแต่ไม่เกิดการอุดตัน (3) น้ำไหลมีตะกอน แต่ไม่เกิดการอุดตัน</td>

            </tr>
            <tr>
                <td></td>
                <td class="col2">การรั่ว</td>
                <td>(2) เกิดการรั่วและมีน้ำไหลพุ่งออกมา (3) เกิดการรั่วและมีน้ำไหลซึม (5) ไม่เกิดการรั่ว</td>

            </tr>
            <tr>
                <td></td>
                <td class="col2">รอยแตกร้าว</td>
                <td>(2) เกิดรอยร้าวมีความกว้างและความลึกเป็นทางยาว (3) เกิดรอยร้าวมีความกว้างและความลึกบางจุด  (4) เกิดรอยร้าวเนื่องจากอุณหูมิ (5) ไม่เกิดรอยร้าว</td>
            </tr>
            <tr>
                <td></td>
                <td class="col2">ต้นไม้</td>
                <td>(1) ต้นไม้สูงกว่าหัว (2) ต้นไม้สูงกว่าเอว (3) ต้นไม้สูงต่ำกว่าเอว (4) ต้นไม้ต่ำกว่าเข่า (5) ไม่มีต้นไม้</td>
            </tr>

            <tr>
                <td></td>
                <td class="col2">วัชพืช</td>
                <td>(2) มีวัชพืชปกคลุมมากกว่า 50% ของพื้นที่ (4) มีวัชพืชปกคลุมน้อยกว่า 50% ของพื้นที่ (5) ไม่มีวัชพืชปกคลุม</td>
            </tr>

            <tr>
                <td></td>
                <td class="col2">สิ่งกีดขวางทางน้ำ</td>
                <td>(1) มีสิ่งกีดขวางทางน้ำ และขวางทางเดินน้ำทั้งหมด (3) มีสิ่งกีดขวางทางน้ำ แต่ปิดขวางทางเดินน้ำบางส่วน (5) ไม่มีสิ่งกีดขวางทางน้ำ</td>
            </tr>
            <tr>
                <td></td>
                <td class="col2">รู โพรง</td>
                <td>(2) มีรูโพรงลึกมากกว่าเข่า (4) มีรูโพรงลึกน้อยกว่าเข่า (5) ไม่มีรูโพรง</td>
            </tr>


        </tbody>
    </table>
    <pagebreak />
<?php endif; ?>