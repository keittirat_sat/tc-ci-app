
<?php if ($data[34]): ?>
    <table>
        <tbody>
            <tr>
                <th class="headtitle" colspan="31">2. อาคารส่งน้ำ/ระบายน้ำ (Outlets) : 2.1 ท่อระบายน้ำลงลำน้ำเดิม (River Outlet) : 2.1.4 ส่วนทางน้ำออก : 2.1.4.1 รางเท : 2.1.4.1.2 กำแพง</th>
            </tr>

            <tr>
                <th class="menu" rowspan="2" >รายการ</th>
                <th colspan="5">การกัดเซาะ<sup>2</sup></th>
                <th colspan="5">การเคลื่อนตัว</th>
                <th colspan="5">การทรุดตัว<sup>2</sup></th>
                <th colspan="5">การระบายน้ำ</th>
                <th colspan="5">การรั่ว</th>
                <th colspan="5">รอยแตกร้าว</th>
            </tr>
            <tr>
                <?php for ($i = 0; $i < 6; $i++): ?>
                    <?php for ($j = 1; $j <= 5; $j++): ?>
                        <th class="score_template"><?php echo $j; ?></th>
                    <?php endfor; ?>
                <?php endfor; ?>
            </tr>
            <tr>
                <?php if ($score == ""): ?>
                    <td><b>[&nbsp;&nbsp;&nbsp;]</b>  มองไม่เห็น</td>

                    <td class="first"></td>
                    <td class="block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    <td class="first"></td>
                    <td></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                <?php else: ?>
                    <td><b>[<?php notsee($score->c34); ?>]</b>  มองไม่เห็น</td>
                    <?php $item = getsingle($score->c34); ?>
                    <td class="first"><?php getans($item[0], 1); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[0], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[0], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[1], 2); ?></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td><?php getans($item[1], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[2], 2); ?></td>
                    <td><?php getans($item[2], 3); ?></td>
                    <td><?php getans($item[2], 4); ?></td>
                    <td><?php getans($item[2], 5); ?></td>

                    <td class="first"><?php getans($item[3], 1); ?></td>
                    <td><?php getans($item[3], 2); ?></td>
                    <td><?php getans($item[3], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[3], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[4], 2); ?></td>
                    <td><?php getans($item[4], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[4], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[5], 2); ?></td>
                    <td><?php getans($item[5], 3); ?></td>
                    <td><?php getans($item[5], 4); ?></td>
                    <td><?php getans($item[5], 5); ?></td>
                <?php endif; ?>
            </tr>

            <tr>
                <th class="menu" rowspan="2" >รายการ</th>
                <th colspan="5">รูโพรง</th>
                <th colspan="5">ต้นไม้</th>
                <th colspan="5">วัชพืช</th>
                <th colspan="15" rowspan="2">หมายเหตุ</th>
            </tr>
            <tr>
                <?php for ($i = 0; $i < 3; $i++): ?>
                    <?php for ($j = 1; $j <= 5; $j++): ?>
                        <th class="score_template"><?php echo $j; ?></th>
                    <?php endfor; ?>
                <?php endfor; ?>
            </tr>
            <tr>
                <td></td>
                <?php if ($score == ""): ?>
                    <td class="first block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>
                    <td></td>

                    <td class="first"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>
                    <td></td>

                    <td class="first" colspan="15"></td>
                <?php else: ?>
                    <td class="first block"></td>
                    <td><?php getans($item[6], 2); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[6], 4); ?></td>
                    <td><?php getans($item[6], 5); ?></td>

                    <td class="first"><?php getans($item[7], 1); ?></td>
                    <td><?php getans($item[7], 2); ?></td>
                    <td><?php getans($item[7], 3); ?></td>
                    <td><?php getans($item[7], 4); ?></td>
                    <td><?php getans($item[7], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[8], 2); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[8], 4); ?></td>
                    <td><?php getans($item[8], 5); ?></td>

                    <td class="first" colspan="15"><?php echo $item[9]; ?></td>
                <?php endif; ?>
            </tr>
        </tbody>
    </table>
<?php endif; ?>

<?php if ($data[35]): ?>
    <table>
        <tbody>
            <tr>
                <th class="headtitle" colspan="31">2. อาคารส่งน้ำ/ระบายน้ำ (Outlets) : 2.1 ท่อระบายน้ำลงลำน้ำเดิม (River Outlet) : 2.1.5 ส่วนสลายพลังงาน : 2.1.5.1 พื้น (รวมฟันจระเข้)</th>
            </tr>

            <tr>
                <th class="menu" rowspan="2" >รายการ</th>
                <th colspan="5">การกัดเซาะ<sup>2,3</sup></th>
                <th colspan="5">การเคลื่อนตัว</th>
                <th colspan="5">การทรุดตัว<sup>2</sup></th>
                <th colspan="5">การบวมตัว</th>
                <th colspan="5">การระบายน้ำ</th>
                <th colspan="5">การรั่ว</th>
            </tr>
            <tr>
                <?php for ($i = 0; $i < 6; $i++): ?>
                    <?php for ($j = 1; $j <= 5; $j++): ?>
                        <th class="score_template"><?php echo $j; ?></th>
                    <?php endfor; ?>
                <?php endfor; ?>
            </tr>
            <tr>
                <?php if ($score == ""): ?>
                    <td><b>[&nbsp;&nbsp;&nbsp;]</b>  มองไม่เห็น</td>

                    <td class="first"></td>
                    <td class="block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first"></td>
                    <td></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>
                <?php else: ?>
                    <td><b>[<?php notsee($score->c35); ?>]</b>  มองไม่เห็น</td>
                    <?php $item = getsingle($score->c35); ?>
                    <td class="first"><?php getans($item[0], 1); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[0], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[0], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[1], 2); ?></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td><?php getans($item[1], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[2], 2); ?></td>
                    <td><?php getans($item[2], 3); ?></td>
                    <td><?php getans($item[2], 4); ?></td>
                    <td><?php getans($item[2], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[3], 2); ?></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td><?php getans($item[3], 5); ?></td>

                    <td class="first"><?php getans($item[4], 1); ?></td>
                    <td><?php getans($item[4], 2); ?></td>
                    <td><?php getans($item[4], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[4], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[5], 2); ?></td>
                    <td><?php getans($item[5], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[5], 5); ?></td>
                <?php endif; ?>
            </tr>

            <tr>
                <th class="menu" rowspan="2" >รายการ</th>
                <th colspan="5">รอยแตกร้าว</th>
                <th colspan="5">ต้นไม้</th>
                <th colspan="5">วัชพืช</th>
                <th colspan="5">สิ่งกีดขวางทางน้ำ</th>
                <th colspan="10" rowspan="2">หมายเหตุ</th>
            </tr>
            <tr>
                <?php for ($i = 0; $i < 4; $i++): ?>
                    <?php for ($j = 1; $j <= 5; $j++): ?>
                        <th class="score_template"><?php echo $j; ?></th>
                    <?php endfor; ?>
                <?php endfor; ?>
            </tr>
            <tr>
                <td></td>
                <?php if ($score != ""): ?>
                    <td class="first block"></td>
                    <td><?php getans($item[6], 2); ?></td>
                    <td><?php getans($item[6], 3); ?></td>
                    <td><?php getans($item[6], 4); ?></td>
                    <td><?php getans($item[6], 5); ?></td>

                    <td class="first"><?php getans($item[7], 1); ?></td>
                    <td><?php getans($item[7], 2); ?></td>
                    <td><?php getans($item[7], 3); ?></td>
                    <td><?php getans($item[7], 4); ?></td>
                    <td><?php getans($item[7], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[8], 2); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[8], 4); ?></td>
                    <td><?php getans($item[8], 5); ?></td>

                    <td class="first"><?php getans($item[9], 1); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[9], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[9], 5); ?></td>

                    <td class="first" colspan="10"><?php echo $item[10]; ?></td>
                <?php else: ?>
                    <td class="first block"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    <td class="first"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>
                    <td></td>

                    <td class="first"></td>
                    <td class="block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first" colspan="10"></td>
                <?php endif; ?>
            </tr>
        </tbody>
    </table>
<?php endif; ?>

<?php if ($data[36]): ?>
    <table>
        <tbody>
            <tr>
                <th class="headtitle" colspan="31">2. อาคารส่งน้ำ/ระบายน้ำ (Outlets) : 2.1 ท่อระบายน้ำลงลำน้ำเดิม (River Outlet) : 2.1.5 ส่วนสลายพลังงาน : 2.1.5.2 กำแพง</th>
            </tr>

            <tr>
                <th class="menu" rowspan="2" >รายการ</th>
                <th colspan="5">การกัดเซาะ<sup>2</sup></th>
                <th colspan="5">การเคลื่อนตัว</th>
                <th colspan="5">การทรุดตัว<sup>2</sup></th>
                <th colspan="5">การระบายน้ำ</th>
                <th colspan="5">การรั่ว</th>
                <th colspan="5">รอยแตกร้าว</th>
            </tr>
            <tr>
                <?php for ($i = 0; $i < 6; $i++): ?>
                    <?php for ($j = 1; $j <= 5; $j++): ?>
                        <th class="score_template"><?php echo $j; ?></th>
                    <?php endfor; ?>
                <?php endfor; ?>
            </tr>
            <tr>
                <?php if ($score == ""): ?>
                    <td><b>[&nbsp;&nbsp;&nbsp;]</b>  มองไม่เห็น</td>

                    <td class="first"></td>
                    <td class="block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    <td class="first"></td>
                    <td></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                <?php else: ?>
                    <td><b>[<?php notsee($score->c36); ?>]</b>  มองไม่เห็น</td>
                    <?php $item = getsingle($score->c36); ?>
                    <td class="first"><?php getans($item[0], 1); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[0], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[0], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[1], 2); ?></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td><?php getans($item[1], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[2], 2); ?></td>
                    <td><?php getans($item[2], 3); ?></td>
                    <td><?php getans($item[2], 4); ?></td>
                    <td><?php getans($item[2], 5); ?></td>

                    <td class="first"><?php getans($item[3], 1); ?></td>
                    <td><?php getans($item[3], 2); ?></td>
                    <td><?php getans($item[3], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[3], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[4], 2); ?></td>
                    <td><?php getans($item[4], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[4], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[5], 2); ?></td>
                    <td><?php getans($item[5], 3); ?></td>
                    <td><?php getans($item[5], 4); ?></td>
                    <td><?php getans($item[5], 5); ?></td>
                <?php endif; ?>
            </tr>

            <tr>
                <th class="menu" rowspan="2" >รายการ</th>
                <th colspan="5">รูโพรง</th>
                <th colspan="5">ต้นไม้</th>
                <th colspan="5">วัชพืช</th>
                <th colspan="15" rowspan="2">หมายเหตุ</th>
            </tr>
            <tr>
                <?php for ($i = 0; $i < 3; $i++): ?>
                    <?php for ($j = 1; $j <= 5; $j++): ?>
                        <th class="score_template"><?php echo $j; ?></th>
                    <?php endfor; ?>
                <?php endfor; ?>
            </tr>
            <tr>
                <?php if ($score == ""): ?>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>
                    <td></td>

                    <td class="first"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>
                    <td></td>

                    <td class="first" colspan="15"></td>
                <?php else: ?>
                    <td></td>

                    <td class="first block"></td>
                    <td><?php getans($item[6], 2); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[6], 4); ?></td>
                    <td><?php getans($item[6], 5); ?></td>

                    <td class="first"><?php getans($item[7], 1); ?></td>
                    <td><?php getans($item[7], 2); ?></td>
                    <td><?php getans($item[7], 3); ?></td>
                    <td><?php getans($item[7], 4); ?></td>
                    <td><?php getans($item[7], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[8], 2); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[8], 4); ?></td>
                    <td><?php getans($item[8], 5); ?></td>

                    <td class="first" colspan="15"><?php echo $item[9]; ?></td> 

                <?php endif; ?>
            </tr>
        </tbody>
    </table>
<?php endif; ?>

<?php if ($data[22]): ?>
    <table>
        <tbody>
            <tr>
                <th class="headtitle" colspan="31">2. อาคารส่งน้ำ/ระบายน้ำ (Outlets) : 2.1 ท่อระบายน้ำลงลำน้ำเดิม (River Outlet) : 2.1.6 ส่วนคลองระบายน้ำ</th>
            </tr>

            <tr>
                <th class="menu" rowspan="2" >รายการ</th>
                <th colspan="5">การกัดเซาะ<sup>1,2</sup></th>
                <th colspan="5">การเกิดร่องน้ำ</th>
                <th colspan="5">การทรุดตัว<sup>1</sup></th>
                <th colspan="5">การซึม</th>
                <th colspan="5">การรั่ว</th>
                <th colspan="5">การเลื่อนไถล</th>
            </tr>
            <tr>
                <?php for ($i = 0; $i < 6; $i++): ?>
                    <?php for ($j = 1; $j <= 5; $j++): ?>
                        <th class="score_template"><?php echo $j; ?></th>
                    <?php endfor; ?>
                <?php endfor; ?>
            </tr>
            <tr>
                <?php if ($score == ""): ?>
                    <td><b>[&nbsp;&nbsp;&nbsp;]</b>  มองไม่เห็น</td>

                    <td class="first"></td>
                    <td class="block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td></td>
                <?php else: ?>
                    <td><b>[<?php notsee($score->c22); ?>]</b>  มองไม่เห็น</td>
                    <?php $item = getsingle($score->c22); ?>
                    <td class="first"><?php getans($item[0], 1); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[0], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[0], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[1], 2); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[1], 4); ?></td>
                    <td><?php getans($item[1], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[2], 2); ?></td>
                    <td><?php getans($item[2], 3); ?></td>
                    <td><?php getans($item[2], 4); ?></td>
                    <td><?php getans($item[2], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[3], 2); ?></td>
                    <td><?php getans($item[3], 3); ?></td>
                    <td><?php getans($item[3], 4); ?></td>
                    <td><?php getans($item[3], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[4], 2); ?></td>
                    <td><?php getans($item[4], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[4], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[5], 2); ?></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td><?php getans($item[5], 5); ?></td>
                <?php endif; ?>
            </tr>

            <tr>
                <th class="menu" rowspan="2" >รายการ</th>
                <th colspan="5">การเสื่อมสภาพ<sup>1</sup></th>
                <th colspan="5">รอยแตกร้าว</th>
                <th colspan="5">รูโพรง</th>
                <th colspan="5">ต้นไม้</th>
                <th colspan="5">วัชพืช</th>
                <th colspan="5">สิ่งกีดขวางทางน้ำ</th>
            </tr>
            <tr>
                <?php for ($i = 0; $i < 6; $i++): ?>
                    <?php for ($j = 1; $j <= 5; $j++): ?>
                        <th class="score_template"><?php echo $j; ?></th>
                    <?php endfor; ?>
                <?php endfor; ?>
            </tr>
            <tr>
                <td></td>
                <?php if ($score == ""): ?>
                    <td class="first"></td>
                    <td class="block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>
                    <td></td>

                    <td class="first"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>
                    <td></td>

                    <td class="first"></td>
                    <td class="block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>
                <?php else: ?>
                    <td class="first"><?php getans($item[6], 1); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[6], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[6], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[7], 2); ?></td>
                    <td><?php getans($item[7], 3); ?></td>
                    <td><?php getans($item[7], 4); ?></td>
                    <td><?php getans($item[7], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[8], 2); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[8], 4); ?></td>
                    <td><?php getans($item[8], 5); ?></td>

                    <td class="first"><?php getans($item[9], 1); ?></td>
                    <td><?php getans($item[9], 2); ?></td>
                    <td><?php getans($item[9], 3); ?></td>
                    <td><?php getans($item[9], 4); ?></td>
                    <td><?php getans($item[9], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[10], 2); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[10], 4); ?></td>
                    <td><?php getans($item[10], 5); ?></td>

                    <td class="first"><?php getans($item[11], 1); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[11], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[11], 5); ?></td>
                <?php endif; ?>
            </tr>
        </tbody>
    </table>
<?php endif; ?>

<?php if ($data[34] || $data[35] || $data[36] || $data[22]): ?>
    <table class="desc">
        <tbody>
            <tr>
                <td class="col1">หมายเหตุ</td>
                <td class="col2">การกัดเซาะ<sup>2</sup></td>
                <td class="col3">(1) กัดกร่อนเห็นเนื้อเหล็ก  (3) เกิดการกัดกร่อน (5) ไม่เกิดการกัดกร่อน</td>
            </tr>
            <tr>
                <td></td>
                <td>การเคลื่อนตัว</td>
                <td>(2) เกิดการเคลื่อนตัว (5) ไม่เกิดการเคลื่อนตัว</td>
            </tr>
            <tr>
                <td></td>
                <td>การทรุดตัว<sup>2</sup></td>
                <td>(2) ทรุดตัว > 5 ซม. (3) ทรุดตัวลึกอยู่ระหว่าง 2-5 ซม. (4) ทรุดตัว < 2 ซม. (5) ไม่เกิดการทรุดตัว</td>
            </tr>
            <tr>
                <td></td>
                <td>การระบายน้ำ</td>
                <td>(1) ท่อระบายน้ำ/รางระบายน้ำอุดตัน ไม่สามารถระบายน้ำได้โดยสิ้นเชิง (2) น้ำขุ่นแต่ไม่เกิดการอุดตัน (3) น้ำไหลมีตะกอน แต่ไม่เกิดการอุดตัน (5) สามารถดี/น้ำไหลปกติ</td>
            </tr>
            <tr>
                <td></td>
                <td>การรั่ว</td>
                <td>(2) เกิดการรั่วและมีน้ำไหลพุ่งออกมา (3) เกิดการรั่วและมีน้ำไหลซึม (5) ไม่เกิดการรั่ว</td>
            </tr>
            <tr>
                <td></td>
                <td>รอยแตกร้าว</td>
                <td>(2) เกิดรอยร้าวมีความกว้างและความลึกเป็นทางยาว (3) เกิดรอยร้าวมีความกว้างและความลึกบางจุด  (4) เกิดรอยร้าวเนื่องจากอุณหูมิ (5) ไม่เกิดรอยร้าว</td>
            </tr>
            <tr>
                <td></td>
                <td>รูโพรง</td>
                <td>(2) มีรูโพรงลึกมากกว่าเข่า (4) มีรูโพรงลึกน้อยกว่าเข่า (5) ไม่มีรูโพรง</td>
            </tr>
            <tr>
                <td></td>
                <td>ต้นไม้</td>
                <td>(1) ต้นไม้สูงกว่าหัว (2) ต้นไม้สูงกว่าเอว (3) ต้นไม้สูงต่ำกว่าเอว (4) ต้นไม้ต่ำกว่าเข่า (5) ไม่มีต้นไม้</td>
            </tr>
            <tr>
                <td></td>
                <td>วัชพืช</td>
                <td>(2) มีวัชพืชปกคลุมมากกว่า 50% ของพื้นที่ (4) มีวัชพืชปกคลุมน้อยกว่า 50% ของพื้นที่ (5) ไม่มีวัชพืชปกคลุม</td>
            </tr>
            <tr>
                <td></td>
                <td>การกัดเซาะ<sup>1</sup></td>
                <td>(1) กัดเซาะเสียหาย >50%  (3) กัดเซาะเสียหาย < 50% (5) ไม่เกิดการกัดเซาะ</td>
            </tr>
            <tr>
                <td></td>
                <td>การเกิดร่องน้ำ</td>
                <td>(2) เกิดร่องน้ำลึกประมาณเข่า (4) เกิดร่องน้ำลึกประมาณข้อเท้า (5) ไม่เกิดร่องน้ำ </td>
            </tr>
            <tr>
                <td></td>
                <td>การทรุดตัว<sup>1</sup></td>
                <td>(2) ทรุดตัวประมาณเอว/ >เอว (3) ทรุดตัวลึกประมาณเข่า/ >แต่ไม่ถึงเอว (4) ทรุดตัวลึกประมาณข้อเท้า/ >แต่ไม่ถึงเข่า (5) ไม่เกิดการทรุดตัว</td>
            </tr>
            <tr>
                <td></td>
                <td>การซึม</td>
                <td>(2) เกิดการซึมและมีการไหลรวม (3) เกิดการซึมเป็นความยาวมากกว่า 30% (4) เกิดการซึมเป็นทางยาวน้อยกว่า 30% (5) ไม่เกิดการซึม</td>
            </tr>
            <tr>
                <td></td>
                <td>การเลื่อนไถล</td>
                <td>(2) เกิดการเลื่อนไถล (5) ไม่เกิดการเลื่อนไถล</td>
            </tr>
            <tr>
                <td></td>
                <td>สิ่งกีดขวางทางน้ำ</td>
                <td>(1) มีสิ่งกีดขวางทางน้ำ และขวางทางเดินน้ำทั้งหมด (3) มีสิ่งกีดขวางทางน้ำ แต่ปิดขวางทางเดินน้ำบางส่วน (5) ไม่มีสิ่งกีดขวางทางน้ำ</td>
            </tr>
        </tbody>
    </table>
    <pagebreak />
<?php endif; ?>
