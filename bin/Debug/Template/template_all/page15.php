<?php if ($data[104]): ?> <!--Function 104-->
    <table> 
        <tbody>
            <tr>
                <th class="headtitle" colspan="31">
                    <b>3.อาคารระบายน้ำล้น (Spillways)</b>  : 3.1 อาคารระบายน้ำล้นใช้งาน (Service Spillway) : 3.1.1 แบบไม่มีบาน (Ungated Spillway) : 3.1.1.3 ส่วนลำเลียงน้ำ (Chute) :<br>
                    3.1.1.3.1 <b><i><u>พื้น</u></i></b>  
                </th>
            </tr>

            <!--Header 1--><tr> 
                <th class="menu" rowspan="2">รายการ</th>
                <th colspan="5">การกัดเซาะ<sup>2</sup></th>
                <th colspan="5">การเคลื่อนตัว</th>
                <th colspan="5">การทรุดตัว<sup>2</sup></th>
                <th colspan="5">การบวมตัว</th>
                <th colspan="5">การระบายน้ำ</th>
                <th colspan="5">การรั่ว</th>
            </tr><!--End Header 1-->		
            <tr>
                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>
            </tr>
            <tr>
                <?php if ($score == ""): ?>
                    <td><b>[&nbsp;&nbsp;&nbsp;]</b> มองไม่เห็น</td>
                    <td class="first"></td>
                    <td class="block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first"></td>
                    <td></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>
                <?php else: ?>
                    <?php $item = getsingle($score->c104); ?>
                    <td><b>[<?php notsee($score->c104); ?>]</b> มองไม่เห็น</td>
                    <td class="first"><?php getans($item[0], 1); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[0], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[0], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[1], 2); ?></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td><?php getans($item[1], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[2], 2); ?></td>
                    <td><?php getans($item[2], 3); ?></td>
                    <td><?php getans($item[2], 4); ?></td>
                    <td><?php getans($item[2], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[3], 2); ?></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td><?php getans($item[3], 5); ?></td>

                    <td class="first"><?php getans($item[4], 1); ?></td>
                    <td><?php getans($item[4], 2); ?></td>
                    <td><?php getans($item[4], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[4], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[5], 2); ?></td>
                    <td><?php getans($item[5], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[5], 5); ?></td>
                <?php endif; ?>
            </tr>	

            <!--Header 2--><tr> 
                <th class="menu" rowspan="2">รายการ</th>
                <th colspan="5"> รอยแตกร้าว</th>
                <th colspan="5"> ต้นไม้</th>
                <th colspan="5"> วัชพืช</th>
                <th colspan="5"> สิ่งกีดขวางทางน้ำ</th>
                <th colspan="10" rowspan="2">หมายเหตุ</th>
            </tr><!--End Header 2-->
            <tr>
                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

            </tr>
            <tr>
                <td></td>
                <?php if ($score != ""): ?>
                    <td class="first block"></td>
                    <td><?php getans($item[6], 2); ?></td>
                    <td><?php getans($item[6], 3); ?></td>
                    <td><?php getans($item[6], 4); ?></td>
                    <td><?php getans($item[6], 5); ?></td>

                    <td class="first"><?php getans($item[7], 1); ?></td>
                    <td><?php getans($item[7], 2); ?></td>
                    <td><?php getans($item[7], 3); ?></td>
                    <td><?php getans($item[7], 4); ?></td>
                    <td><?php getans($item[7], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[8], 2); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[8], 4); ?></td>
                    <td><?php getans($item[8], 5); ?></td>

                    <td class="first"><?php getans($item[9], 1); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[9], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[9], 5); ?></td>

                    <td colspan="10" class="first"><?php echo $item[10]; ?></td>
                <?php else: ?>
                    <td class="first block"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    <td class="first"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>
                    <td></td>

                    <td class="first"></td>
                    <td class="block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>

                    <td colspan="10" class="first"> </td>
                <?php endif; ?>
            </tr>
        </tbody>
    </table>
<?php endif; ?> <!--End Function 104-->

<?php if ($data[105]): ?> <!--Function 105-->
    <table>
        <tbody>
            <tr>
                <th class="headtitle" colspan="31">
                    <b>3.อาคารระบายน้ำล้น (Spillways)</b>: 3.1 อาคารระบายน้ำล้นใช้งาน (Service Spillway) : 3.1.1 แบบไม่มีบาน (Ungated Spillway) : 3.1.1.3 ส่วนลำเลียงน้ำ (Chute) : <br>
                    3.1.1.3.2<b><i><u>กำแพง</u></i></b>
                </th>	
            </tr>
                <!--Header 1--><tr> 
                <th class="menu" rowspan="2">รายการ</th>
                <th colspan="5"> การกัดเซาะ<sup>2</sup></th>
                <th colspan="5">การเคลื่อนตัว</th>
                <th colspan="5"> การทรุดตัว<sup>2</sup></th>
                <th colspan="5">การระบายน้ำ</th>
                <th colspan="5"> การรั่ว</th>
                <th colspan="5"> รอยแตกร้าว</th>
            </tr><!--End Header 1-->
            <tr>
                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th> 
            </tr>
            <tr>
                <?php if ($score == ""): ?>
                    <td><b>[&nbsp;&nbsp;&nbsp;]</b> มองไม่เห็น</td>
                    <td class="first"></td>
                    <td class="block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    <td class="first"></td>
                    <td></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                <?php else: ?>
                    <?php $item = getsingle($score->c105); ?>
                    <td><b>[<?php notsee($score->c105); ?>]</b> มองไม่เห็น</td>
                    <td class="first"><?php getans($item[0], 1); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[0], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[0], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[1], 2); ?></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td><?php getans($item[1], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[2], 2); ?></td>
                    <td><?php getans($item[2], 3); ?></td>
                    <td><?php getans($item[2], 4); ?></td>
                    <td><?php getans($item[2], 5); ?></td>

                    <td class="first"><?php getans($item[3], 1); ?></td>
                    <td><?php getans($item[3], 2); ?></td>
                    <td><?php getans($item[3], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[3], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[4], 2); ?></td>
                    <td><?php getans($item[4], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[4], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[5], 2); ?></td>
                    <td><?php getans($item[5], 3); ?></td>
                    <td><?php getans($item[5], 4); ?></td>
                    <td><?php getans($item[5], 5); ?></td>
                <?php endif; ?>
            </tr>
            <!--Header 2--><tr> 
                <th class="Menu" rowspan="2">รายการ</th>
                <th colspan="5"> รูโพรง</th>
                <th colspan="5"> ต้นไม้</th>
                <th colspan="5"> วัชพืช</th>
                <th colspan="15" rowspan="2">หมายเหตุ</th>
            </tr><!--Enf Header 2-->
            <tr>
                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>
            </tr>
            <tr>
                <td class="first"></td>
                <?php if ($score != ""): ?>
                    <td class="first block"></td>
                    <td><?php getans($item[6], 2); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[6], 4); ?></td>
                    <td><?php getans($item[6], 5); ?></td>

                    <td class="first"><?php getans($item[7], 1); ?></td>
                    <td><?php getans($item[7], 2); ?></td>
                    <td><?php getans($item[7], 3); ?></td>
                    <td><?php getans($item[7], 4); ?></td>
                    <td><?php getans($item[7], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[8], 2); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[8], 4); ?></td>
                    <td><?php getans($item[8], 5); ?></td>

                    <td colspan="15" class="first"><?php echo $item[9]; ?></td>
                <?php else: ?>
                    <td class="first block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>
                    <td></td>

                    <td class="first"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>
                    <td></td>

                    <td colspan="15" class="first"> </td>
                <?php endif; ?>
            </tr>
        </tbody>	
    </table>
<?php endif; ?> <!--End Function 105-->

<?php if ($data[106]): ?> <!--Function 106-->
    <table> 
        <tbody>
            <tr>
                <th class="headtitle" colspan="31">
                    <b>3.อาคารระบายน้ำล้น (Spillways)</b>: 3.1 อาคารระบายน้ำล้นใช้งาน (Service Spillway) : 3.1.1 แบบไม่มีบาน (Ungated Spillway) : 3.1.1.4 ส่วนสลายพลังงาน : <br>
                    3.1.1.4.1 <b><i><u>พื้น(รวมฟันจระเข้)</u></i></b>  
                </th>
            </tr>

            <!--Header 1--><tr> 
                <th class="menu" rowspan="2">รายการ</th>
                <th colspan="5"> การกัดเซาะ<sup>2</sup></th>
                <th colspan="5">การเคลื่อนตัว</th>
                <th colspan="5"> การทรุดตัว<sup>2</sup></th>
                <th colspan="5">การบวมตัว</th>
                <th colspan="5"> การระบายน้ำ</th>
                <th colspan="5"> การรั่ว</th>
            </tr><!--End Header 1-->		
            <tr>
                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>
            </tr>
            <tr>
                <?php if ($score == ""): ?>
                    <td><b>[&nbsp;&nbsp;&nbsp;]</b> มองไม่เห็น</td>
                    <td class="first"></td>
                    <td class="block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first"></td>
                    <td></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>
                <?php else: ?>
                    <?php $item = getsingle($score->c106); ?>
                    <td><b>[<?php notsee($score->c106); ?>]</b> มองไม่เห็น</td>
                    <td class="first"><?php getans($item[0], 1); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[0], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[0], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[1], 2); ?></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td><?php getans($item[1], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[2], 2); ?></td>
                    <td><?php getans($item[2], 3); ?></td>
                    <td><?php getans($item[2], 4); ?></td>
                    <td><?php getans($item[2], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[3], 2); ?></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td><?php getans($item[3], 5); ?></td>

                    <td class="first"><?php getans($item[4], 1); ?></td>
                    <td><?php getans($item[4], 2); ?></td>
                    <td><?php getans($item[4], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[4], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[5], 2); ?></td>
                    <td><?php getans($item[5], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[5], 5); ?></td>
                <?php endif; ?>
            </tr>	

            <!--Header 2--><tr> 
                <th class="menu" rowspan="2">รายการ</th>
                <th colspan="5"> รอยแตกร้าว</th>
                <th colspan="5"> ต้นไม้</th>
                <th colspan="5"> วัชพืช</th>
                <th colspan="5"> สิ่งกีดขวางทางน้ำ</th>
                <th colspan="10" rowspan="2">หมายเหตุ</th>
            </tr><!--End Header 2-->
            <tr>
                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

            </tr>
            <tr>
                <td></td>
                <?php if ($score != ""): ?>
                    <td class="first block"></td>
                    <td><?php getans($item[6], 2); ?></td>
                    <td><?php getans($item[6], 3); ?></td>
                    <td><?php getans($item[6], 4); ?></td>
                    <td><?php getans($item[6], 5); ?></td>

                    <td class="first"><?php getans($item[7], 1); ?></td>
                    <td><?php getans($item[7], 2); ?></td>
                    <td><?php getans($item[7], 3); ?></td>
                    <td><?php getans($item[7], 4); ?></td>
                    <td><?php getans($item[7], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[8], 2); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[8], 4); ?></td>
                    <td><?php getans($item[8], 5); ?></td>

                    <td class="first"><?php getans($item[9], 1); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[9], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[9], 5); ?></td>

                    <td colspan="10" class="first"><?php echo $item[10]; ?></td>
                <?php else: ?>
                    <td class="first block"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    <td class="first"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>
                    <td></td>

                    <td class="first"></td>
                    <td class="block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>

                    <td colspan="10" class="first"> </td>
                <?php endif; ?>
            </tr>
        </tbody>
    </table>
<?php endif; ?> <!--End Function 106-->
<?php if ($data[107]): ?> <!--Function 107-->
    <table>
        <tbody>
            <tr>
                <th class="headtitle" colspan="31">
                    <b>3.อาคารระบายน้ำล้น (Spillways)</b>: 3.1 อาคารระบายน้ำล้นใช้งาน (Service Spillway) : 3.1.1 แบบไม่มีบาน (Ungated Spillway) : 3.1.1.4 ส่วนสลายพลังงาน :  <br>
                    3.1.1.4.2<b><i><u>กำแพง</u></i></b>
                </th>	
            </tr>
                <!--Header 1-->
                <tr> 
                <th class="menu" rowspan="2">รายการ</th>
                <th colspan="5"> การกัดเซาะ<sup>2</sup></th>
                <th colspan="5">การเคลื่อนตัว</th>
                <th colspan="5"> การทรุดตัว<sup>2</sup></th>
                <th colspan="5">การระบายน้ำ</th>
                <th colspan="5"> การรั่ว</th>
                <th colspan="5"> รอยแตกร้าว</th>
            </tr><!--End Header 1-->
            <tr>
                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th> 
            </tr>
            <tr>
                <?php if ($score == ""): ?>
                    <td><b>[&nbsp;&nbsp;&nbsp;]</b> มองไม่เห็น</td>
                    <td class="first"></td>
                    <td class="block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    <td class="first"></td>
                    <td></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                <?php else: ?>
                    <?php $item = getsingle($score->c107); ?>
                    <td><b>[<?php notsee($score->c107); ?>]</b> มองไม่เห็น</td>
                    <td class="first"><?php getans($item[0], 1); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[0], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[0], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[1], 2); ?></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td><?php getans($item[1], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[2], 2); ?></td>
                    <td><?php getans($item[2], 3); ?></td>
                    <td><?php getans($item[2], 4); ?></td>
                    <td><?php getans($item[2], 5); ?></td>

                    <td class="first"><?php getans($item[3], 1); ?></td>
                    <td><?php getans($item[3], 2); ?></td>
                    <td><?php getans($item[3], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[3], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[4], 2); ?></td>
                    <td><?php getans($item[4], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[4], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[5], 2); ?></td>
                    <td><?php getans($item[5], 3); ?></td>
                    <td><?php getans($item[5], 4); ?></td>
                    <td><?php getans($item[5], 5); ?></td>
                <?php endif; ?>
            </tr>
            <!--Header 2--><tr> 
                <th class="Menu" rowspan="2">รายการ</th>
                <th colspan="5"> รูโพรง</th>
                <th colspan="5"> ต้นไม้</th>
                <th colspan="5"> วัชพืช</th>
                <th colspan="15" rowspan="2">หมายเหตุ</th>
            </tr><!--Enf Header 2-->
            <tr>
                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>
            </tr>
            <tr>
                <td class="first"></td>
                <?php if ($score == ""): ?>
                    <td class="first block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>
                    <td></td>

                    <td class="first"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>
                    <td></td>

                    <td colspan="15" class="first"> </td>
                <?php else: ?>
                    <td class="first block"></td>
                    <td><?php getans($item[6], 2); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[6], 4); ?></td>
                    <td><?php getans($item[6], 5); ?></td>

                    <td class="first"><?php getans($item[7], 1); ?></td>
                    <td><?php getans($item[7], 2); ?></td>
                    <td><?php getans($item[7], 3); ?></td>
                    <td><?php getans($item[7], 4); ?></td>
                    <td><?php getans($item[7], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[8], 2); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[8], 4); ?></td>
                    <td><?php getans($item[8], 5); ?></td>

                    <td colspan="15" class="first"><?php echo $item[9]; ?></td>
                <?php endif; ?>
            </tr>
        </tbody>	
    </table>
<?php endif; ?> <!--End Function 107-->

<?php if ($data[104] || $data[105] || $data[106] || $data[107]): ?> <!--Function detail print-->
    <table class="desc">
        <tbody>
            <tr> 
                <td class="col1"><b><u>หมายเหตุ</u></b></td>
                <td class="col2">การกัดเซาะ<sup>2</sup></td>
                <td class="col3">(1) กัดเซาะเห็นเนื้อเหล็ก  (3) เกิดการกัดเซาะ (5) ไม่เกิดการกัดเซาะ</td>
            </tr>
            <tr>
                <td class="col1" rowspan="9"></td>
                <td class="col2">การเคลื่อนตัว</td>
                <td class="col3">(2) เกิดการเคลื่อนตัว (5) ไม่เกิดการเคลื่อนตัว</td>
            </tr>
            <tr>
                <td class="col2">การทรุดตัว<sup>2</sup></td>
                <td class="col3">(2) ทรุดตัว &gt; 5 ซม. (3) ทรุดตัวลึกอยู่ระหว่าง 2-5 ซม. (4) ทรุดตัว &lt; 2 ซม. (5) ไม่เกิดการทรุดตัว</td>
            </tr>
            <tr>
                <td class="col2">การบวมตัว</td>
                <td class="col3">(2) บวมตัว (5) ไม่บวมตัว</td>
            </tr>
            <tr>
                <td class="col2">การระบายน้ำ</td>
                <td class="col3">(1) ท่อระบายน้ำ/รางระบายน้ำอุดตัน ไม่สามารถระบายน้ำได้โดยสิ้นเชิง (2) น้ำขุ่นแต่ไม่เกิดการอุดตัน (3) น้ำไหลมีตะกอน แต่ไม่เกิดการอุดตัน</td>
            </tr>
            <tr>
                <td class="col2"> </td>
                <td class="col3">(5) สามารถดี/น้ำไหลปกติ</td>
            </tr>
            <tr>
                <td class="col2">การรั่ว</td>
                <td class="col3">(2) เกิดการรั่วและมีน้ำไหลพุ่งออกมา (3) เกิดการรั่วและมีน้ำไหลซึม (5) ไม่เกิดการรั่ว</td>
            </tr>
            <tr>
                <td class="col2">รอยแตกร้าว</td>
                <td class="col3">(2) เกิดรอยร้าวมีความกว้างและความลึกเป็นทางยาว (3) เกิดรอยร้าวมีความกว้างและความลึกบางจุด  (4) เกิดรอยร้าวเนื่องจากอุณหูมิ (5) ไม่เกิดรอยร้าว</td>
            </tr>
            <tr>
                <td class="col2">ต้นไม้</td>
                <td class="col3">(1) ต้นไม้สูงกว่าหัว (2) ต้นไม้สูงกว่าเอว (3) ต้นไม้สูงต่ำกว่าเอว (4) ต้นไม้ต่ำกว่าเข่า (5) ไม่มีต้นไม้</td>
            </tr>
            <tr>
                <td class="col2">วัชพืช</td>
                <td class="col3">(2) มีวัชพืชปกคลุมมากกว่า 50% ของพื้นที่ (4) มีวัชพืชปกคลุมน้อยกว่า 50% ของพื้นที่ (5) ไม่มีวัชพืชปกคลุม</td>
            </tr>
        </tbody>
    </table>
    <pagebreak />
<?php endif; ?> <!--End Function detail print-->