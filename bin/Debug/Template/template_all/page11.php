<?php if ($data[72]): ?> <!--Function 72-->
    <table> 
        <tbody>
            <tr>
                <th class="headtitle" colspan="31">
                    <b> 2.2 ท่อส่งน้ำ (Canal Outlet)</b>: 2.2.2 ท่อส่งน้ำฝั่งขวา : 2.2.2.1 ส่วนทางน้ำเข้า : 2.2.2.1.1 <b><i><u>คลองชักน้ำ</u></i></b>
                    <b>[&nbsp;&nbsp;&nbsp;]</b> <b><i>ไม่มีท่อส่งน้ำฝั่งขวา</i></b>
                </th>
            </tr>
            <!--Header 1--><tr> 
                <th class="menu" rowspan="2">รายการ</th>
                <th colspan="5"> การกัดเซาะ<sup>1</sup></th>
                <th colspan="5">การเลื่อนไถล</th>
                <th colspan="5">การเสื่อมสภาพ<sup>1</sup></th>
                <th colspan="5"> ต้นไม้</th>
                <th colspan="5"> วัชพืช</th>
                <th colspan="5"> สิ่งกีดขวางทางน้ำ</th>
            </tr><!--End Header 1-->
            <tr>
                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>
            </tr>
            <tr>
                <?php if ($score == ""): ?>
                    <td><b>[&nbsp;&nbsp;&nbsp;]</b> มองไม่เห็น</td>
                    <td class="first"></td>
                    <td class="block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first"></td>
                    <td class="block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>
                    <td></td>

                    <td class="first"></td>
                    <td class="block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>
                <?php else: ?>
                    <td><b>[<?php notsee($score->c72); ?>]</b> มองไม่เห็น</td>
                    <?php $item = getsingle($score->c72); ?>
                    <td class="first"><?php getans($item[0], 1); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[0], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[0], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[1], 2); ?></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td><?php getans($item[1], 5); ?></td>

                    <td class="first"><?php getans($item[2], 1); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[2], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[2], 5); ?></td>

                    <td class="first"><?php getans($item[3], 1); ?></td>
                    <td><?php getans($item[3], 2); ?></td>
                    <td><?php getans($item[3], 3); ?></td>
                    <td><?php getans($item[3], 4); ?></td>
                    <td><?php getans($item[3], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[4], 2); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[4], 4); ?></td>
                    <td><?php getans($item[4], 5); ?></td>

                    <td class="first"><?php getans($item[5], 1); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[5], 3); ?></td>
                    <td class="block"></td>
                    <td></td>
                <?php endif; ?>
            </tr>
        </tbody>
    </table>
<?php endif; ?> <!--End Function 72-->		

<?php if ($data[74]): ?> <!--Function 74-->
    <table> 
        <tbody>
            <tr>
                <th class="headtitle" colspan="27">
                    <b>2.2 ท่อส่งน้ำ (Canal Outlet)</b>: 2.2.2 ท่อส่งน้ำฝั่งขวา : 2.2.2.1 ส่วนทางน้ำเข้า : 2.2.2.1.2 อาคารรับน้ำ (Intake) : 2.2.2.1.2.1<b><i><u>พื้นและกำแพง</u></i></b>
                </th>
            </tr>
            <!--Header 1--><tr> 
                <th class="menu" rowspan="2">รายการ</th>
                <th colspan="5"> การกัดเซาะ<sup>2</sup></th>
                <th colspan="5">การเคลื่อนตัว</th>
                <th colspan="5">การทรุดตัว<sup>2</sup></th>
                <th colspan="5">รอยแตกร้าว</th>
                <th colspan="5"> สิ่งกีดขวางทางน้ำ</th>
                <th class="menu" rowspan="2"> หมายเหตุ</th>
            </tr><!--End Header 1-->
            <tr>
                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>
            </tr>
            <tr>
                <?php if ($score == ""): ?>
                    <td><b>[&nbsp;&nbsp;&nbsp;]</b> มองไม่เห็น</td>
                    <td class="first"></td>
                    <td class="block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    <td class="first"></td>
                    <td class="block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>

                    <td  class="first"> </td>	
                <?php else: ?>
                    <?php $item = getsingle($score->c74); ?>
                    <td><b>[<?php notsee($score->c74); ?>]</b> มองไม่เห็น</td>
                    <td class="first"><?php getans($item[0], 1); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[0], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[0], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[1], 2); ?></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td><?php getans($item[1], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[2], 2); ?></td>
                    <td><?php getans($item[2], 3); ?></td>
                    <td><?php getans($item[2], 4); ?></td>
                    <td><?php getans($item[2], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[3], 2); ?></td>
                    <td><?php getans($item[3], 3); ?></td>
                    <td><?php getans($item[3], 4); ?></td>
                    <td><?php getans($item[3], 5); ?></td>

                    <td class="first"><?php getans($item[4], 1); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[4], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[4], 5); ?></td>

                    <td  class="first"><?php echo $item[5]; ?></td>	
                <?php endif; ?>
            </tr>
        </tbody>
    </table>
<?php endif; ?> <!--End Function 74-->

<?php if ($data[75]): ?> <!--Function 75-->
    <table> 
        <tbody>
            <tr>
                <th class="headtitle" colspan="17">
                    <b>2.2 ท่อส่งน้ำ (Canal Outlet)</b>: 2.2.2 ท่อส่งน้ำฝั่งขวา : 2.2.2.1 ส่วนทางน้ำเข้า : 2.2.2.1.2 อาคารรับน้ำ (Intake) : 2.2.2.1.2.2<b><i><u>ตะแกรง(Trashrack)</u></i></b>
                </th>
            </tr>
            <!--Header 1--><tr> 
                <th class="menu" rowspan="2">รายการ</th>
                <th colspan="5"> การกัดเซาะ<sup>3</sup></th>
                <th colspan="5">การเปลี่ยนรูป</th>
                <th colspan="5"> สิ่งกีดขวางทางน้ำ</th>
                <th rowspan="2"> หมายเหตุ</th>
            </tr><!--End Header 1-->
            <tr>
                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

            </tr>
            <tr>
                <?php if ($score == ""): ?>
                    <td><b>[&nbsp;&nbsp;&nbsp;]</b> มองไม่เห็น</td>
                    <td class="first"></td>
                    <td class="block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first"></td>
                    <td class="block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first"></td>	
                <?php else: ?>
                    <td><b>[<?php notsee($score->c75); ?>]</b> มองไม่เห็น</td>
                    <?php $item = getsingle($score->c75); ?>
                    <td class="first"><?php getans($item[0], 1); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[0], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[0], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[1], 2); ?></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td><?php getans($item[1], 5); ?></td>

                    <td class="first"><?php getans($item[2], 1); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[2], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[2], 5); ?></td>

                    <td class="first"><?php echo $item[3]; ?></td>	
                <?php endif; ?>
            </tr>
        </tbody>
    </table>
<?php endif; ?> <!--End Function 75-->

<?php if ($data[76]): ?> <!--Function 76-->
    <table> 
        <tbody>
            <tr>
                <th class="headtitle" colspan="17">
                    <b>2.2 ท่อส่งน้ำ (Canal Outlet)</b>: 2.2.2 ท่อส่งน้ำฝั่งขวา : 2.2.2.1 ส่วนทางน้ำเข้า : 2.2.2.1.2 อาคารรับน้ำ (Intake) : 2.2.2.1.2.3<b><i><u>ประตูกั้นน้ำ (Bulkhead Gate)</u></i></b>
                </th>
            </tr>
            <!--Header 1--><tr> 
                <th class="menu" rowspan="2">รายการ</th>
                <th colspan="5"> การกัดเซาะ<sup>3</sup></th>
                <th colspan="5">การเปลี่ยนรูป</th>
                <th colspan="5"> สภาพการใช้งาน</th>
                <th rowspan="2"> หมายเหตุ</th>
            </tr><!--End Header 1-->
            <tr>
                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

            </tr>
            <tr>
                <?php if ($score == ""): ?>
                    <td><b>[&nbsp;&nbsp;&nbsp;]</b> มองไม่เห็น</td>
                    <td class="first"></td>
                    <td class="block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first"></td>
                    <td class="block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first"></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first"></td>	
                <?php else: ?>
                    <?php $item = getsingle($score->c76); ?>
                    <td><b>[<?php notsee($score->c76); ?>]</b> มองไม่เห็น</td>
                    <td class="first"><?php getans($item[0], 1); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[0], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[0], 5); ?></td>

                    <td class="first"><?php getans($item[1], 1); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[1], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[1], 5); ?></td>

                    <td class="first"><?php getans($item[2], 1); ?></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td><?php getans($item[2], 5); ?></td>

                    <td class="first"><?php echo $item[3]; ?></td>	
                <?php endif; ?>
            </tr>
        </tbody>
    </table>
<?php endif; ?> <!--End Function 76-->

<?php if ($data[77]): ?> <!--Function 77-->
    <table> 
        <tbody>
            <tr>
                <th class="headtitle" colspan="22">
                    <b>2.2 ท่อส่งน้ำ (Canal Outlet)</b> : 2.2 ท่อส่งน้ำ (Canal Outlet) : 2.2.2 ท่อส่งน้ำฝั่งขวา : 2.2.2.2 ส่วนท่อลำเลียงน้ำ : 2.2.2.2.1 <b><i><u>ท่อลำเลียงน้ำ</u></i></b>
                </th>
            </tr>
            <!--Header 1--><tr> 
                <th class="menu" rowspan="2">รายการ</th>
                <th colspan="5"> การกัดเซาะ<sup>2</sup></th>
                <th colspan="5">การรั่ว</th>
                <th colspan="5"> รอยแตกร้าว</th>
                <th colspan="5"> สิ่งกีดขวางทางน้ำ</th>
                <th rowspan="2"> หมายเหตุ</th>
            </tr><!--End Header 1-->
            <tr>
                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>		
            </tr>
            <tr>
                <?php if ($score != ""): ?>
                    <?php $item = getsingle($score->c77); ?>
                    <td><b>[<?php notsee($score->c77); ?>]</b> มองไม่เห็น</td>
                    <td class="first"><?php getans($item[0], 1); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[0], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[0], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[1], 2); ?></td>
                    <td><?php getans($item[1], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[1], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[2], 2); ?></td>
                    <td><?php getans($item[2], 3); ?></td>
                    <td><?php getans($item[2], 4); ?></td>
                    <td><?php getans($item[2], 5); ?></td>

                    <td class="first"><?php getans($item[3], 1); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[3], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[3], 5); ?></td>

                    <td class="first"><?php echo $item[4]; ?></td>	
                <?php else: ?>
                    <td><b>[&nbsp;&nbsp;&nbsp;]</b> มองไม่เห็น</td>
                    <td class="first"></td>
                    <td class="block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    <td class="first"></td>
                    <td class="block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first"></td>	
                <?php endif; ?>
            </tr>
        </tbody>
    </table>
<?php endif; ?> <!--End Function 77-->

<?php if ($data[78]): ?> <!--Function 78-->
    <table> 
        <tbody>
            <tr>
                <th class="headtitle" colspan="27">
                    <b> 2.2 ท่อส่งน้ำ (Canal Outlet)</b> : 2.2 ท่อส่งน้ำ (Canal Outlet) : 2.2.2 ท่อส่งน้ำฝั่งขวา : 2.2.2.3 ส่วนควบคุม : 2.2.2.3.1 <b><i><u>อาคารควบคุม</u></i></b>
                    <b>[&nbsp;&nbsp;&nbsp;]</b>
                </th>
            </tr>
            <!--Header 1--><tr> 
                <th class="menu" rowspan="2">รายการ</th>
                <th colspan="5"> การกัดเซาะ<sup>2</sup></th>
                <th colspan="5">การทรุดตัว<sup>2</sup></th>
                <th colspan="5">การรั่ว</th>
                <th colspan="5"> รอบแตกร้าว</th>
                <th colspan="5"> ระบบรักษาความปลอดภัย</th>
                <th rowspan="2"> หมายเหตุ</th>
            </tr><!--End Header 1-->
            <tr>
                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

            </tr>
            <tr>
                <?php if ($score == ""): ?>
                    <td><b>[&nbsp;&nbsp;&nbsp;]</b> มองไม่เห็น</td>
                    <td class="first"></td>
                    <td class="block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    <td class="first"></td>
                    <td></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first"></td>
                <?php else: ?>
                    <?php $item = getsingle($score->c78); ?>
                    <td><b>[<?php notsee($score->c78); ?>]</b> มองไม่เห็น</td>
                    <td class="first"><?php getans($item[0], 1); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[0], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[0], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[1], 2); ?></td>
                    <td><?php getans($item[1], 3); ?></td>
                    <td><?php getans($item[1], 4); ?></td>
                    <td><?php getans($item[1], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[2], 2); ?></td>
                    <td><?php getans($item[2], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[2], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[3], 2); ?></td>
                    <td><?php getans($item[3], 3); ?></td>
                    <td><?php getans($item[3], 4); ?></td>
                    <td><?php getans($item[3], 5); ?></td>

                    <td class="first"><?php getans($item[4], 1); ?></td>
                    <td><?php getans($item[4], 2); ?></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td><?php getans($item[4], 5); ?></td>

                    <td class="first"><?php echo $item[5]; ?></td>
                <?php endif; ?>

            </tr>
        </tbody>
    </table>
<?php endif; ?> <!--End Function 78-->	

<?php if ($data[79]): ?> <!--Function 79-->
    <table> 
        <tbody>
            <tr>
                <th class="headtitle" colspan="17">
                    <b>2.2 ท่อส่งน้ำ (Canal Outlet)</b> : 2.2 ท่อส่งน้ำ (Canal Outlet) : 2.2.2 ท่อส่งน้ำฝั่งขวา : 2.2.2.3 ส่วนควบคุม : 2.2.2.3.2 <b><i><u> อุปกรณ์เครื่องกล</u></i></b>
                </th>
            </tr>
            <!--Header 1--><tr> 
                <th class="menu" rowspan="2">รายการ</th>
                <th colspan="5"> การกัดเซาะ<sup>3</sup></th>
                <th colspan="5">การเปลี่ยนรูป</th>
                <th colspan="5"> สภาพการใช้งาน</th>
                <th rowspan="2"> หมายเหตุ</th>
            </tr><!--End Header 1-->
            <tr>
                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

            </tr>
            <tr>
                <?php if ($score == ""): ?>
                    <td><b>[&nbsp;&nbsp;&nbsp;]</b> มองไม่เห็น</td>
                    <td class="first"></td>
                    <td class="block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first"></td>
                    <td class="block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first"></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first"></td>	
                <?php else: ?>
                    <td><b>[<?php notsee($score->c79); ?>]</b> มองไม่เห็น</td>
                    <?php $item = getsingle($score->c79); ?>
                    <td class="first"><?php getans($item[0], 1); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[0], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[0], 5); ?></td>

                    <td class="first"><?php getans($item[1], 1); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[1], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[1], 5); ?></td>

                    <td class="first"><?php getans($item[2], 1); ?></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td><?php getans($item[2], 5); ?></td>

                    <td class="first"><?php echo $item[3]; ?></td>	
                <?php endif; ?>
            </tr>
        </tbody>
    </table>
<?php endif; ?> <!--End Function 79-->

<?php if ($data[72] | $data[74] || $data[75] || $data[76] || $data[77] || $data[78] || $data[79]): ?> <!--Function detail print-->
    <table class="desc">
        <tbody>
            <tr> 
                <td class="col1"><b><u>หมายเหตุ</u></b></td>
                <td class="col2">การกัดเซาะ<sup>1</sup></td>
                <td class="col3">(1) กัดเซาะเสียหาย &gt;50%  (3) กัดเซาะเสียหาย &lt; 50% (5) ไม่เกิดการกัดเซาะ</td>
            </tr>
            <tr>
                <td class="col1" rowspan="13"></td>
                <td class="col2">การเลื่อนไถล</td>
                <td class="col3">(2) เกิดการเลื่อนไถล (5) ไม่เกิดการเลื่อนไถล</td>
            </tr>
            <tr>
                <td class="col2">การเสื่อมสภาพ<sup>2</sup></td>
                <td class="col3">(1) เกิดการเสื่อมสภาพใช้งานไม่ได้ (3) เกิดการเสื่อมสภาพใช้งานได้ (5) ไม่เกิดการเสื่อมสภาพ</td>
            </tr>
            <tr>
                <td class="col2">ต้นไม้</td>
                <td class="col3">(1) ต้นไม้สูงกว่าหัว (2) ต้นไม้สูงกว่าเอว (3) ต้นไม้สูงต่ำกว่าเอว (4) ต้นไม้ต่ำกว่าเข่า (5) ไม่มีต้นไม้</td>
            </tr>
            <tr>
                <td class="col2">วัชพืช</td>
                <td class="col3">(2) มีวัชพืชปกคลุมมากกว่า 50% ของพื้นที่ (4) มีวัชพืชปกคลุมน้อยกว่า 50% ของพื้นที่ (5) ไม่มีวัชพืชปกคลุม</td>
            </tr>
            <tr>
                <td class="col2">สิ่งกีดขวางทางน้ำ</td>
                <td class="col3">(1) มีสิ่งกีดขวางทางน้ำ และขวางทางเดินน้ำทั้งหมด (3) มีสิ่งกีดขวางทางน้ำ แต่ปิดขวางทางเดินน้ำบางส่วน (5) ไม่มีสิ่งกีดขวางทางน้ำ</td>
            </tr>
            <tr> 
                <td class="col2">การกัดเซาะ<sup>2</sup></td>
                <td class="col3">(1) กัดเซาะเห็นเนื้อเหล็ก  (3) เกิดการกัดเซาะ (5) ไม่เกิดการกัดเซาะ</td>
            </tr>
            <tr>
                <td class="col2">การเคลื่อนตัว</td>
                <td class="col3">(2) เกิดการเคลื่อนตัว (5) ไม่เกิดการเคลื่อนตัว</td>
            </tr>
            <tr>
                <td class="col2">การทรุดตัว<sup>2</sup></td>
                <td class="col3">(2) ทรุดตัว &gt; 5 ซม. (3) ทรุดตัวลึกอยู่ระหว่าง 2-5 ซม. (4) ทรุดตัว &lt; 2 ซม. (5) ไม่เกิดการทรุดตัว</td>
            </tr>
            <tr>
                <td class="col2">รอยแตกร้าว</td>
                <td class="col3">(2) เกิดรอยร้าวมีความกว้างและความลึกเป็นทางยาว (3) เกิดรอยร้าวมีความกว้างและความลึกบางจุด  (4) เกิดรอยร้าวเนื่องจากอุณหูมิ (5) ไม่เกิดรอยร้าว</td>
            </tr>
            <tr> 
                <td class="col2">การกัดเซาะ<sup>3</sup></td>
                <td class="col3">(1) เกิดสนิมกัดกร่อนถึงเนื้อใน (3) เกิดสนิมที่ผิวเหล็ก (5) ไม่เกิดสนิม</td>
            </tr>
            <tr>
                <td class="col2">การเปลี่ยนรูป</td>
                <td class="col3">(2) เกิดการเปลี่ยนรูป (5) ไม่เกิดการเปลี่ยนรูป</td>
            </tr>
            <tr>
                <td class="col2">สภาพการใช้งาน</td>
                <td class="col3">(1) ใช้งานไม่ได้ (5) ใช้งานได้</td>
            </tr>
            <tr>
                <td class="col2">การรั่ว</td>
                <td class="col3">(2) เกิดการรั่วและมีน้ำไหลพุ่งออกมา (3) เกิดการรั่วและมีน้ำไหลซึม (5) ไม่เกิดการรั่ว</td>
            </tr>

        </tbody>
    </table>
    <pagebreak />
<?php endif; ?> <!--End Function detail print-->