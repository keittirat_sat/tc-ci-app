
<?php if ($data[46]): ?>
    <table>
        <tbody>
            <tr>
                <th class="headtitle" colspan="31">2. อาคารส่งน้ำ/ระบายน้ำ (Outlets) : 2.2 ท่อส่งน้ำ (Canal Outlet) : 2.2.1 ท่อส่งน้ำฝั่งซ้าย : 2.2.1.1 ส่วนทางน้ำเข้า : 2.2.1.1.1 คลองชักน้ำ <b>[&nbsp;&nbsp;&nbsp;]</b> ไม่มีท่อส่งน้ำฝั่งซ้าย</th>
            </tr>

            <tr>
                <th class="menu" rowspan="2" >รายการ</th>
                <th colspan="5">การกัดเซาะ<sup>1</sup></th>
                <th colspan="5">การเลื่อนไถล</th>
                <th colspan="5">การเสื่อมสภาพ<sup>1</sup></th>
                <th colspan="5">ต้นไม้</th>
                <th colspan="5">วัชพืช</th>
                <th colspan="5">สิ่งกีดขวางทางน้ำ</th>
            </tr>
            <tr>
                <?php for ($i = 0; $i < 6; $i++): ?>
                    <?php for ($j = 1; $j <= 5; $j++): ?>
                        <th class="score_template"><?php echo $j; ?></th>
                    <?php endfor; ?>
                <?php endfor; ?>
            </tr>
            <tr>
                <?php if ($score == ""): ?>
                    <td><b>[&nbsp;&nbsp;&nbsp;]</b>  มองไม่เห็น</td>

                    <td class="first"></td>
                    <td class="block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first"></td>
                    <td class="block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>
                    <td></td>

                    <td class="first"></td>
                    <td class="block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>
                <?php else: ?>
                    <td><b>[<?php notsee($score->c46); ?>]</b>  มองไม่เห็น</td>
                    <?php $item = getsingle($score->c46); ?>
                    <td class="first"><?php getans($item[0], 1); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[0], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[0], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[1], 2); ?></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td><?php getans($item[1], 5); ?></td>

                    <td class="first"><?php getans($item[2], 1); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[2], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[2], 5); ?></td>

                    <td class="first"><?php getans($item[3], 1); ?></td>
                    <td><?php getans($item[3], 2); ?></td>
                    <td><?php getans($item[3], 3); ?></td>
                    <td><?php getans($item[3], 4); ?></td>
                    <td><?php getans($item[3], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[4], 2); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[4], 4); ?></td>
                    <td><?php getans($item[4], 5); ?></td>

                    <td class="first"><?php getans($item[5], 1); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[5], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[5], 5); ?></td>
                <?php endif; ?>
            </tr>
        </tbody>
    </table>
<?php endif; ?>

<?php if ($data[48]): ?>
    <table>
        <tbody>
            <tr>
                <th class="headtitle" colspan="27">2. อาคารส่งน้ำ/ระบายน้ำ (Outlets) : 2.2 ท่อส่งน้ำ (Canal Outlet) : 2.2.1 ท่อส่งน้ำฝั่งซ้าย : 2.2.1.1 ส่วนทางน้ำเข้า : 2.2.1.1.2 อาคารรับน้ำ (Intake) : 2.2.1.1.2.1 พื้นและกำแพง</th>
            </tr>

            <tr>
                <th class="menu" rowspan="2" >รายการ</th>
                <th colspan="5">การกัดเซาะ<sup>2</sup></th>
                <th colspan="5">การเคลื่อนตัว</th>
                <th colspan="5">การทรุดตัว<sup>2</sup></th>
                <th colspan="5">รอยแตกร้าว</th>
                <th colspan="5">สิ่งกีดขวางทางน้ำ</th>
                <th class="menu" rowspan="2">หมายเหตุ</th>
            </tr>
            <tr>
                <?php for ($i = 0; $i < 5; $i++): ?>
                    <?php for ($j = 1; $j <= 5; $j++): ?>
                        <th class="score_template"><?php echo $j; ?></th>
                    <?php endfor; ?>
                <?php endfor; ?>
            </tr>
            <tr>
                <?php if ($score == ""): ?>
                    <td><b>[&nbsp;&nbsp;&nbsp;]</b>  มองไม่เห็น</td>

                    <td class="first"></td>
                    <td class="block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    <td class="first"></td>
                    <td class="block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first"></td>
                <?php else: ?>
                    <?php $item = getsingle($score->c48); ?>
                    <td><b>[<?php notsee($score->c48); ?>]</b>  มองไม่เห็น</td>                    

                    <td class="first"><?php getans($item[0], 1); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[0], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[0], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[1], 2); ?></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td><?php getans($item[1], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[2], 2); ?></td>
                    <td><?php getans($item[2], 3); ?></td>
                    <td><?php getans($item[2], 4); ?></td>
                    <td><?php getans($item[2], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[3], 2); ?></td>
                    <td><?php getans($item[3], 3); ?></td>
                    <td><?php getans($item[3], 4); ?></td>
                    <td><?php getans($item[3], 5); ?></td>

                    <td class="first"><?php getans($item[4], 1); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[4], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[4], 5); ?></td>

                    <td class="first"><?php echo $item[5]; ?></td>
                <?php endif; ?>
            </tr>
        </tbody>
    </table>
<?php endif; ?>

<?php if ($data[49]): ?>
    <table>
        <tbody>
            <tr>
                <th class="headtitle" colspan="17">2. อาคารส่งน้ำ/ระบายน้ำ (Outlets) : 2.2 ท่อส่งน้ำ (Canal Outlet) : 2.2.1 ท่อส่งน้ำฝั่งซ้าย : 2.2.1.1 ส่วนทางน้ำเข้า : 2.2.1.1.2 อาคารรับน้ำ (Intake) : 2.2.1.1.2.2 ตะแกรง (Trashrack)</th>
            </tr>

            <tr>
                <th class="menu" rowspan="2" >รายการ</th>
                <th colspan="5">การกัดเซาะ<sup>3</sup></th>
                <th colspan="5">การเปลี่ยนรูป</th>
                <th colspan="5">สิ่งกีดขวางทางน้ำ</th>
                <th rowspan="2">หมายเหตุ</th>
            </tr>
            <tr>
                <?php for ($i = 0; $i < 3; $i++): ?>
                    <?php for ($j = 1; $j <= 5; $j++): ?>
                        <th class="score_template"><?php echo $j; ?></th>
                    <?php endfor; ?>
                <?php endfor; ?>
            </tr>
            <tr>
                <?php if ($score == ""): ?>
                    <td><b>[&nbsp;&nbsp;&nbsp;]</b>  มองไม่เห็น</td>

                    <td class="first"></td>
                    <td class="block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first"></td>
                    <td class="block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first"></td>
                <?php else: ?>

                    <td><b>[<?php notsee($score->c49); ?>]</b>  มองไม่เห็น</td>
                    <?php $item = getsingle($score->c49); ?>
                    <td class="first"><?php getans($item[0], 1); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[0], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[0], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[1], 2); ?></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td><?php getans($item[1], 5); ?></td>

                    <td class="first"><?php getans($item[2], 1); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[2], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[2], 5); ?></td>

                    <td class="first"><?php echo $item[3]; ?></td>
                <?php endif; ?>
            </tr>
        </tbody>
    </table>
<?php endif; ?>

<?php if ($data[50]): ?>
    <table>
        <tbody>
            <tr>
                <th class="headtitle" colspan="17">2. อาคารส่งน้ำ/ระบายน้ำ (Outlets) : 2.2 ท่อส่งน้ำ (Canal Outlet) : 2.2.1 ท่อส่งน้ำฝั่งซ้าย : 2.2.1.1 ส่วนทางน้ำเข้า : 2.2.1.1.2 อาคารรับน้ำ (Intake) : 2.2.1.1.2.3 ประตูกั้นน้ำ (Bulkhead Gate)</th>
            </tr>

            <tr>
                <th class="menu" rowspan="2" >รายการ</th>
                <th colspan="5">การกัดเซาะ<sup>3</sup></th>
                <th colspan="5">การเสื่อมสภาพ<sup>2</sup></th>
                <th colspan="5">สภาพการใช้งาน</th>
                <th rowspan="2">หมายเหตุ</th>
            </tr>
            <tr>
                <?php for ($i = 0; $i < 3; $i++): ?>
                    <?php for ($j = 1; $j <= 5; $j++): ?>
                        <th class="score_template"><?php echo $j; ?></th>
                    <?php endfor; ?>
                <?php endfor; ?>
            </tr>
            <tr>
                <?php if ($score == ""): ?>
                    <td><b>[&nbsp;&nbsp;&nbsp;]</b>  มองไม่เห็น</td>

                    <td class="first"></td>
                    <td class="block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first"></td>
                    <td class="block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first"></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first"></td>
                <?php else: ?>
                    <td><b>[<?php notsee($score->c50); ?>]</b>  มองไม่เห็น</td>
                    <?php $item = getsingle($score->c50); ?>
                    <td class="first"><?php getans($item[0], 1); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[0], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[0], 5); ?></td>

                    <td class="first"><?php getans($item[1], 1); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[1], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[1], 5); ?></td>

                    <td class="first"><?php getans($item[2], 1); ?></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td><?php getans($item[2], 5); ?></td>

                    <td class="first"><?php echo $item[3]; ?></td>
                <?php endif; ?>
            </tr>
        </tbody>
    </table>
<?php endif; ?>

<?php if ($data[51]): ?>
    <table>
        <tbody>
            <tr>
                <th class="headtitle" colspan="22">2. อาคารส่งน้ำ/ระบายน้ำ (Outlets) : 2.2 ท่อส่งน้ำ (Canal Outlet) : 2.2.1 ท่อส่งน้ำฝั่งซ้าย : 2.2.1.2 ส่วนท่อลำเลียงน้ำ : 2.2.1.2.1 ท่อลำเลียงน้ำ</th>
            </tr>

            <tr>
                <th class="menu" rowspan="2" >รายการ</th>
                <th colspan="5">การกัดเซาะ<sup>2,3</sup></th>
                <th colspan="5">การรั่ว</th>
                <th colspan="5">รอยแตกร้าว</th>
                <th colspan="5">สิ่งกีดขวางทางน้ำ</th>
                <th rowspan="2">หมายเหตุ</th>
            </tr>
            <tr>
                <?php for ($i = 0; $i < 4; $i++): ?>
                    <?php for ($j = 1; $j <= 5; $j++): ?>
                        <th class="score_template"><?php echo $j; ?></th>
                    <?php endfor; ?>
                <?php endfor; ?>
            </tr>
            <tr>
                <?php if ($score == ""): ?>
                    <td><b>[&nbsp;&nbsp;&nbsp;]</b>  มองไม่เห็น</td>

                    <td class="first"></td>
                    <td class="block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    <td class="first"></td>
                    <td class="block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first"></td>
                <?php else: ?>

                    <td><b>[<?php notsee($score->c51); ?>]</b>  มองไม่เห็น</td>
                    <?php $item = getsingle($score->c51); ?>
                    <td class="first"><?php getans($item[0], 1); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[0], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[0], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[1], 2); ?></td>
                    <td><?php getans($item[1], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[1], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[2], 2); ?></td>
                    <td><?php getans($item[2], 3); ?></td>
                    <td><?php getans($item[2], 4); ?></td>
                    <td><?php getans($item[2], 5); ?></td>

                    <td class="first"><?php getans($item[3], 1); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[3], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[3], 5); ?></td>

                    <td class="first"><?php echo $item[4]; ?></td>
                <?php endif; ?>
            </tr>
        </tbody>
    </table>
<?php endif; ?>

<?php if ($data[52]): ?>
    <table>
        <tbody>
            <tr>
                <th class="headtitle" colspan="27">2. อาคารส่งน้ำ/ระบายน้ำ (Outlets) : 2.2 ท่อส่งน้ำ (Canal Outlet) : 2.2.1 ท่อส่งน้ำฝั่งซ้าย : 2.2.1.3 ส่วนควบคุม : 2.2.1.3.1 อาคารควบคุม</th>
            </tr>

            <tr>
                <th class="menu" rowspan="2" >รายการ</th>
                <th colspan="5">การกัดเซาะ<sup>2</sup></th>
                <th colspan="5">การทรุดตัว<sup>2</sup></th>
                <th colspan="5">การรั่ว</th>
                <th colspan="5">รอยแตกร้าว</th>
                <th colspan="5">ระบบรักษาความปลอดภัย</th>
                <th rowspan="2" class="menu">หมายเหตุ</th>
            </tr>
            <tr>
                <?php for ($i = 0; $i < 5; $i++): ?>
                    <?php for ($j = 1; $j <= 5; $j++): ?>
                        <th class="score_template"><?php echo $j; ?></th>
                    <?php endfor; ?>
                <?php endfor; ?>
            </tr>
            <tr>
                <?php if ($score == ""): ?>
                    <td><b>[&nbsp;&nbsp;&nbsp;]</b>  มองไม่เห็น</td>

                    <td class="first"></td>
                    <td class="block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    <td class="first"></td>
                    <td></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first"></td>
                <?php else: ?>
                    <td><b>[<?php notsee($score->c52); ?>]</b>  มองไม่เห็น</td>
                    <?php $item = getsingle($score->c52); ?>
                    <td class="first"><?php getans($item[0], 1); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[0], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[0], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[1], 2); ?></td>
                    <td><?php getans($item[1], 3); ?></td>
                    <td><?php getans($item[1], 4); ?></td>
                    <td><?php getans($item[1], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[2], 2); ?></td>
                    <td><?php getans($item[2], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[2], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[3], 2); ?></td>
                    <td><?php getans($item[3], 3); ?></td>
                    <td><?php getans($item[3], 4); ?></td>
                    <td><?php getans($item[3], 5); ?></td>

                    <td class="first"><?php getans($item[4], 1); ?></td>
                    <td><?php getans($item[4], 2); ?></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td><?php getans($item[4], 5); ?></td>

                    <td class="first"><?php echo $item[5]; ?></td>
                <?php endif; ?>
            </tr>
        </tbody>
    </table>
<?php endif; ?>

<?php if ($data[53]): ?>
    <table>
        <tbody>
            <tr>
                <th class="headtitle" colspan="17">2. อาคารส่งน้ำ/ระบายน้ำ (Outlets) : 2.2 ท่อส่งน้ำ (Canal Outlet) : 2.2.1 ท่อส่งน้ำฝั่งซ้าย : 2.2.1.3 ส่วนควบคุม : 2.2.1.3.2 อุปกรณ์เครื่องกล</th>
            </tr>

            <tr>
                <th class="menu" rowspan="2" >รายการ</th>
                <th colspan="5">การกัดเซาะ<sup>3</sup></th>
                <th colspan="5">การเสื่อมสภาพ<sup>2</sup></th>
                <th colspan="5">สภาพการใช้งาน</th>
                <th rowspan="2">หมายเหตุ</th>
            </tr>
            <tr>
                <?php for ($i = 0; $i < 3; $i++): ?>
                    <?php for ($j = 1; $j <= 5; $j++): ?>
                        <th class="score_template"><?php echo $j; ?></th>
                    <?php endfor; ?>
                <?php endfor; ?>
            </tr>
            <tr>
                <?php if ($score == ""): ?>
                    <td><b>[&nbsp;&nbsp;&nbsp;]</b>  มองไม่เห็น</td>

                    <td class="first"></td>
                    <td class="block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first"></td>
                    <td class="block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first"></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first"></td>
                <?php else: ?>
                    <td><b>[<?php notsee($score->c53); ?>]</b>  มองไม่เห็น</td>
                    <?php $item = getsingle($score->c53); ?>
                    <td class="first"><?php getans($item[0], 1); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[0], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[0], 5); ?></td>

                    <td class="first"><?php getans($item[2], 1); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[1], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[1], 5); ?></td>

                    <td class="first"><?php getans($item[2], 1); ?></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td><?php getans($item[2], 5); ?></td>

                    <td class="first"><?php echo $item[3]; ?></td>
                <?php endif; ?>
            </tr>
        </tbody>
    </table>
<?php endif; ?>

<?php if ($data[46] || $data[48] || $data[49] || $data[50] || $data[51] || $data[52] || $data[53]): ?>
    <table class="desc">
        <tbody>
            <tr>
                <td class="col1">หมายเหตุ</td>
                <td class="col2">การกัดเซาะ<sup>1</sup></td>
                <td class="col3">(1) กัดเซาะเสียหาย >50%  (3) กัดเซาะเสียหาย < 50% (5) ไม่เกิดการกัดเซาะ</td>
            </tr>
            <tr>
                <td></td>
                <td>การเลื่อนไถล</td>
                <td>(2) เกิดการเลื่อนไถล (5) ไม่เกิดการเลื่อนไถล</td>
            </tr>
            <tr>
                <td></td>
                <td>การเสื่อมสภาพ<sup>2</sup></td>
                <td>(1) เกิดการเสื่อมสภาพใช้งานไม่ได้ (3) เกิดการเสื่อมสภาพใช้งานได้ (5) ไม่เกิดการเสื่อมสภาพ</td>
            </tr>
            <tr>
                <td></td>
                <td>ต้นไม้</td>
                <td>(1) ต้นไม้สูงกว่าหัว (2) ต้นไม้สูงกว่าเอว (3) ต้นไม้สูงต่ำกว่าเอว (4) ต้นไม้ต่ำกว่าเข่า (5) ไม่มีต้นไม้</td>
            </tr>
            <tr>
                <td></td>
                <td>วัชพืช</td>
                <td>(2) มีวัชพืชปกคลุมมากกว่า 50% ของพื้นที่ (4) มีวัชพืชปกคลุมน้อยกว่า 50% ของพื้นที่ (5) ไม่มีวัชพืชปกคลุม</td>
            </tr>
            <tr>
                <td></td>
                <td>สิ่งกีดขวางทางน้ำ</td>
                <td>(1) มีสิ่งกีดขวางทางน้ำ และขวางทางเดินน้ำทั้งหมด (3) มีสิ่งกีดขวางทางน้ำ แต่ปิดขวางทางเดินน้ำบางส่วน (5) ไม่มีสิ่งกีดขวางทางน้ำ</td>
            </tr>
            <tr>
                <td></td>
                <td>การกัดเซาะ<sup>2</sup></td>
                <td>(1) กัดกร่อนเห็นเนื้อเหล็ก  (3) เกิดการกัดกร่อน (5) ไม่เกิดการกัดกร่อน</td>
            </tr>
            <tr>
                <td></td>
                <td>การเคลื่อนตัว</td>
                <td>(2) เกิดการเคลื่อนตัว (5) ไม่เกิดการเคลื่อนตัว</td>
            </tr>
            <tr>
                <td></td>
                <td>การทรุดตัว<sup>2</sup></td>
                <td>(2) ทรุดตัว > 5 ซม. (3) ทรุดตัวลึกอยู่ระหว่าง 2-5 ซม. (4) ทรุดตัว < 2 ซม. (5) ไม่เกิดการทรุดตัว</td>
            </tr>
            <tr>
                <td></td>
                <td>รอยแตกร้าว</td>
                <td>(2) เกิดรอยร้าวมีความกว้างและความลึกเป็นทางยาว (3) เกิดรอยร้าวมีความกว้างและความลึกบางจุด  (4) เกิดรอยร้าวเนื่องจากอุณหูมิ (5) ไม่เกิดรอยร้าว</td>
            </tr>
            <tr>
                <td></td>
                <td>การกัดเซาะ<sup>3</sup></td>
                <td>(1) เกิดสนิมกัดกร่อนถึงเนื้อใน (3) เกิดสนิมที่ผิวเหล็ก (5) ไม่เกิดสนิม</td>
            </tr>
            <tr>
                <td></td>
                <td>การเปลี่ยนรูป</td>
                <td>(2) เกิดการเปลี่ยนรูป (5) ไม่เกิดการเปลี่ยนรูป</td>
            </tr>
            <tr>
                <td></td>
                <td>สภาพการใช้งาน</td>
                <td>(1) ใช้งานไม่ได้ (5) ใช้งานได้</td>
            </tr>
            <tr>
                <td></td>
                <td>การรั่ว</td>
                <td>(2) เกิดการรั่วและมีน้ำไหลพุ่งออกมา (3) เกิดการรั่วและมีน้ำไหลซึม (5) ไม่เกิดการรั่ว</td>
            </tr>
        </tbody>
    </table>
    <pagebreak />
<?php endif; ?>