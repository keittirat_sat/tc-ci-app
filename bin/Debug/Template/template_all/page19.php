<?php if ($data[123]): ?>
    <table>
        <tbody>
            <tr>
                <th class="headtitle" colspan="22"><b>  
                        3. อาคารระบายน้ำล้น (Spillways)</b> : 3.2 อาคารระบายน้ำล้นฉุกเฉิน (Service Spillway) :3.1.2 แบบมีบาน (Gated Spillway):3.1.2.2 ส่วนควบคุม 
                    3.1.2.2.4 <b><u><i>บานระบาย (รวมอุปกรณ์อื่น ๆ)</i></u></b>
                </th>
            </tr>

            <tr>
                <th rowspan="2" class="menu">รายการ</th>
                <th colspan="5"><b>การกัดเซาะ<sup>3</sup></b></th>
                <th colspan="5"><b>การรั่ว</b></th>
                <th colspan="5"><b>การเสื่อมสภาพ<sup>2</sup></b></th>
                <th colspan="5"><b>สภาพการใช้งาน</b></th>

                <th   rowspan="2"><b>หมายเหตุ</b></th>
            </tr>
            <tr>
                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

            </tr>
            <tr>
                <?php if ($score == ""): //start blank table?>
                    <td><b>[&nbsp;&nbsp;&nbsp;]</b> มองไม่เห็น</td>
                    <td class="first"></td>
                    <td class="block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first"></td>
                    <td class="block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first"></td>
                    <td class="block"></td>
                    <td class="block" ></td>
                    <td class="block"></td>
                    <td></td>


                    <td class="first"></td>

                <?php else: //end blank table, start data table?>

                    <td><b>[<?php notsee($score->c123); ?>]</b> มองไม่เห็น</td>
                    <?php $item = getsingle($score->c123); ?>

                    <td class="first"><?php getans($item[0], 1); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[0], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[0], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[1], 2); ?></td>
                    <td><?php getans($item[1], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[1], 5); ?></td>

                    <td class="first"><?php getans($item[2], 1); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[2], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[2], 5); ?></td>

                    <td class="first"><?php getans($item[3], 1); ?></td>
                    <td class="block"></td>
                    <td class="block" ></td>
                    <td class="block"></td>
                    <td><?php getans($item[3], 5); ?></td>


                    <td class="first"><?php echo $item[4]; ?></td>

                <?php endif; //end data table?>
            </tr>
        </tbody>
    </table>
<?php endif; ?>

<?php if ($data[124]): ?>
    <table>
        <tbody>
            <tr>
                <th class="headtitle" colspan="17"><b>  
                    3. อาคารระบายน้ำล้น (Spillways)</b> : 3.2 อาคารระบายน้ำล้นฉุกเฉิน (Service Spillway) :3.1.2 แบบมีบาน (Gated Spillway):3.1.2.2 ส่วนควบคุม 
                    3.1.2.2.6 <b><u><i>อุปกรณ์เครื่องกล</i></u></b>
                </th>
            </tr>

            <tr>
                <th rowspan="2" class="menu">รายการ</th>
                <th colspan="5"><b>การกัดเซาะ<sup>3</sup></b></th>
                <th colspan="5"><b>การเสื่อมสภาพ<sup>2</sup></b></th>
                <th colspan="5"><b>สภาพการใช้งาน</b></th>

                <th rowspan="2"><b>หมายเหตุ</b></th>
            </tr>
            <tr>
                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>						
            </tr>
            <tr>
                <?php if ($score == ""): //start blank table?>
                    <td><b>[&nbsp;&nbsp;&nbsp;]</b> มองไม่เห็น</td>
                    <td class="first"></td>
                    <td class="block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first"></td>
                    <td class="block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>	
                    <td class="first"></td>
                <?php else: //end blank table, start data table?>

                    <td><b>[<?php notsee($score->c124); ?>]</b> มองไม่เห็น</td>
                    <?php $item = getsingle($score->c124); ?>

                    <td class="first"><?php getans($item[0], 1); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[0], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[0], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[1], 2); ?></td>
                    <td><?php getans($item[1], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[1], 5); ?></td>

                    <td class="first"><?php getans($item[2], 1); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[2], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[2], 5); ?></td>	

                    <td class="first"><?php echo $item[3]; ?></td>

                <?php endif; //end data table?>		
            </tr>
        </tbody>
    </table>
<?php endif; ?>

<?php if ($data[125]): ?>
    <table>
        <tbody>
            <tr>
                <th class="headtitle" colspan="12"><b>  
                        3. อาคารระบายน้ำล้น (Spillways)</b> : 3.2 อาคารระบายน้ำล้นฉุกเฉิน (Service Spillway) :3.1.2 แบบมีบาน (Gated Spillway):3.1.2.2 ส่วนควบคุม 
                    3.1.2.2.6 <b><u><i>อุปกรณ์เครื่องกล</i></u></b>
                </th>
            </tr>

            <tr>
                <th rowspan="2" class="menu">รายการ</th>
                <th colspan="5"><b>การเสื่อมสภาพ<sup>2</sup></b></th>
                <th colspan="5"><b>สภาพการใช้งาน</b></th>

                <th rowspan="2"><b>หมายเหตุ</b></th>
            </tr>
            <tr>
                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>
            </tr>
            <tr>
                <?php if ($score == ""): //start blank table?>
                    <td><b>[&nbsp;&nbsp;&nbsp;]</b> มองไม่เห็น</td>
                    <td class="first"></td>
                    <td class="block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>


                    <td class="first"></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td></td>	
                    <td class="first"></td>
                <?php else: //end blank table, start data table?>
                    <td><b>[<?php notsee($score->c125); ?>]</b> มองไม่เห็น</td>
                    <?php $item = getsingle($score->c125); ?>

                    <td class="first"><?php getans($item[0], 1); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[0], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[0], 5); ?></td>


                    <td class="first"><?php getans($item[1], 1); ?></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td><?php getans($item[1], 5); ?></td>

                    <td class="first"><?php echo $item[2]; ?></td>

                <?php endif; //end data table?>
            </tr>
        </tbody>
    </table>
<?php endif; ?>

<?php if ($data[129]): ?>
    <table>
        <tbody>
            <tr>
                <th class="headtitle" colspan="31">
                    <b>3. อาคารระบายน้ำล้น (Spillways)</b> :  3.1 อาคารระบายน้ำล้นใช้งาน (Service Spillway) : 3.1.2 แบบมีบาน (Gated Spillway): 3.1.2.3 ส่วนทางน้ำเข้า 
                    3.1.2.3.1 <b><u><i>ฟื้น<</i></u></b>
                </th>
            </tr>			
            <tr>
                <th class="menu" rowspan="2">รายการ</th>
                <th colspan="5"><b>การกัดเซาะ<sup>2</sup></b></th>
                <th colspan="5"><b>การเคลื่อนตัว</b></th>
                <th colspan="5"><b>การทรุดตัว<sup>2</sup></b></th>
                <th colspan="5"><b>การบวมตัว</b></th>
                <th colspan="5"><b>การระบายน้ำ</b></th>
                <th colspan="5"><b>การรั่ว</b></th>
            </tr>
            <tr>
                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>

                <th class="score_template">5</th>
                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>

                <th class="score_template">5</th>
                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>
            </tr>
            <tr>
                <?php if ($score == ""): //start blank table?>
                    <td><b>[&nbsp;&nbsp;&nbsp;]</b> มองไม่เห็น</td>
                    <td class="first"></td>
                    <td class="block"></td>
                    <td ></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td></td>

                    <td class="first"></td>
                    <td></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>


                    <td class="first block"></td>
                    <td></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>		
                <?php else: //end blank table, start data table?>

                    <td><b>[<?php notsee($score->c129); ?>]</b> มองไม่เห็น</td>
                    <?php $item = getsingle($score->c129); ?>

                    <td class="first"><?php getans($item[0], 1); ?></td>
                    <td class="block"></td>
                    <td ><?php getans($item[0], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[0], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[1], 2); ?></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td><?php getans($item[1], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[2], 2); ?></td>
                    <td><?php getans($item[2], 3); ?></td>
                    <td><?php getans($item[2], 4); ?></td>
                    <td><?php getans($item[2], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[3], 2); ?></td>
                    <td class="block"></td>
                    <td class="block"></td>
                    <td><?php getans($item[3], 5); ?></td>

                    <td class="first"><?php getans($item[4], 1); ?></td>
                    <td><?php getans($item[4], 2); ?></td>
                    <td><?php getans($item[4], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[4], 5); ?></td>


                    <td class="first block"></td>
                    <td><?php getans($item[5], 2); ?></td>
                    <td><?php getans($item[5], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[5], 5); ?></td>

                <?php endif; //end data table?>
            </tr>

            <!--Section 2-->
            <tr>
                <th class="menu" rowspan="3">รายการ</th>
                <th colspan="5"><b>รอยแตกร้าว</b></th>
                <th colspan="5"><b>ต้นไม้</b></th>
                <th colspan="5"><b>วัชพืช</b></th>
                <th colspan="5"><b>สิ่งกีดขวางทางน้ำ</b></th>
                <th colspan="10" rowspan="2"><b>หมายเหตุ</b></th>
            </tr>
            <tr>
                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>

                <th class="score_template">1</th>
                <th class="score_template">2</th>
                <th class="score_template">3</th>
                <th class="score_template">4</th>
                <th class="score_template">5</th>
            </tr>
            <tr>

                <?php if ($score == ""): //start blank table?>

                    <td class="first block"></td>
                    <td ></td>
                    <td ></td>
                    <td ></td>
                    <td></td>

                    <td class="first"></td>
                    <td ></td>
                    <td ></td>
                    <td ></td>
                    <td></td>

                    <td class="first block"></td>
                    <td></td>
                    <td class="block"></td>
                    <td></td>
                    <td></td>

                    <td class="first"></td>
                    <td class="block"></td>
                    <td ></td>
                    <td class="block"></td>
                    <td></td>

                    <th colspan="10"></th>	


                <?php else: //end blank table, start data table?>


                    <td class="first block"></td>
                    <td ><?php getans($item[6], 2); ?></td>
                    <td ><?php getans($item[6], 3); ?></td>
                    <td ><?php getans($item[6], 4); ?></td>
                    <td><?php getans($item[6], 5); ?></td>

                    <td class="first"><?php getans($item[7], 1); ?></td>
                    <td ><?php getans($item[7], 2); ?></td>
                    <td ><?php getans($item[7], 3); ?></td>
                    <td ><?php getans($item[7], 4); ?></td>
                    <td><?php getans($item[7], 5); ?></td>

                    <td class="first block"></td>
                    <td><?php getans($item[8], 2); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[8], 4); ?></td>
                    <td><?php getans($item[8], 5); ?></td>

                    <td class="first"><?php getans($item[9], 1); ?></td>
                    <td class="block"></td>
                    <td ><?php getans($item[9], 3); ?></td>
                    <td class="block"></td>
                    <td><?php getans($item[9], 5); ?></td>

                    <th colspan="10"><?php echo $item[10]; ?></th>
                <?php endif; //end data table?>

            </tr>		
        </tbody>
    </table>
<?php endif; ?>

<?php if ($data[123] || $data[124] || $data[125] || $data[129]): ?>
    <table class="desc">
        <tbody>
            <tr>
                <td class="col1"><b><u>หมายเหตุ</u></b></td>
                <td class="col2">การกัดเซาะ <sup>3</sup></td>
                <td class="col3">(1) เกิดสนิมกัดกร่อนถึงเนื้อใน (3) เกิดสนิมที่ผิวเหล็ก (5) ไม่เกิดสนิม</td>
            </tr>

            <tr>
                <td></td>
                <td>การเลื่อนไถล</td>
                <td>(2)เกิดการเลื่อนไถล (5) ไม่เกิดการเลื่อนไถล</td>
            </tr>
            <tr>
                <td></td>
                <td>การรั่ว</td>
                <td>(2) เกิดการรั่วและมีน้ำไหลพุ่งออกมา (3) เกิดการรั่วและมีน้ำไหลซึม (5) ไม่เกิดการรั่ว</td>

            </tr>
            <tr>
                <td></td>
                <td>สภาพการใช้งาน<sup>2</sup></td>
                <td>(1) ใช้งานไม่ได้ (5) ใช้งานได้</td>
            </tr>
            <tr>
                <td></td>
                <td>การกัดเซาะ<sup>2</sup></td>
                <td>(1)กัดเซาะเห็นเนื้อเหล็ก (3)เกิดการกัดเซาะ (5)ไม่เกิดการกัดเซาะ</td>

            </tr>
            <tr>
                <td></td>
                <td>การเคลื่อนตัว<sup>2</sup></td>
                <td>(2) เกิดการเคลื่อนตัว (5) ไม่เกิดการเคลื่อนตัว</td>
            </tr>

            <tr>
                <td></td>
                <td>การทรุดตัว<sup>2</sup></td>
                <td>(2) ทรุดตัว &gt; 5 ซม. (3) ทรุดตัวลึกอยู่ระหว่าง 2-5 ซม. (4) ทรุดตัว &gt; 2 ซม. (5) ไม่เกิดการทรุดตัว</td>

            </tr>

            <tr>
                <td></td>
                <td>การบวมตัว<sup>2</sup></td>
                <td>(2) บวมตัว (5) ไม่บวมตัว</td>

            </tr>

            <tr>
                <td></td>
                <td>การระบายน้ำ<sup>2</sup></td>
                <td>(1) ท่อระบายน้ำ/รางระบายน้ำอุดตัน ไม่สามารถระบายน้ำได้โดยสิ้นเชิง (2) น้ำขุ่นแต่ไม่เกิดการอุดตัน (3) น้ำไหลมีตะกอน แต่ไม่เกิดการอุดตัน (5) สามารถดี/น้ำไหลปกติ</td>

            </tr>
            <tr>
                <td></td>
                <td>การรั่ว<sup>2</sup></td>
                <td>(2) เกิดการรั่วและมีน้ำไหลพุ่งออกมา (3) เกิดการรั่วและมีน้ำไหลซึม (5) ไม่เกิดการรั่ว</td>

            </tr>
            <tr>
                <td></td>
                <td>รอยแตกร้าว<sup>2</sup></td>
                <td>(2) เกิดรอยร้าวมีความกว้างและความลึกเป็นทางยาว (3) เกิดรอยร้าวมีความกว้างและความลึกบางจุด  (4) เกิดรอยร้าวเนื่องจากอุณหูมิ (5) ไม่เกิดรอยร้าว</td>

            </tr>

            <tr>
                <td></td>
                <td>ต้นไม้<sup>2</sup></td>
                <td>(1) ต้นไม้สูงกว่าหัว (2) ต้นไม้สูงกว่าเอว (3) ต้นไม้สูงต่ำกว่าเอว (4) ต้นไม้ต่ำกว่าเข่า (5) ไม่มีต้นไม้</td>

            </tr>

            <tr>
                <td></td>
                <td>วัชพืช<sup>2</sup></td>
                <td>(2) มีวัชพืชปกคลุมมากกว่า 50% ของพื้นที่ (4) มีวัชพืชปกคลุมน้อยกว่า 50% ของพื้นที่ (5) ไม่มีวัชพืชปกคลุม</td>

            </tr>


            <tr>
                <td></td>
                <td>สิ่งกีดขวางทางน้ำ<sup>2</sup></td>
                <td>(1) มีสิ่งกีดขวางทางน้ำ และขวางทางเดินน้ำทั้งหมด (3) มีสิ่งกีดขวางทางน้ำ แต่ปิดขวางทางเดินน้ำบางส่วน (5) ไม่มีสิ่งกีดขวางทางน้ำ</td>

            </tr>
        </tbody>
    </table>
    <pagebreak />
<?php endif; ?>