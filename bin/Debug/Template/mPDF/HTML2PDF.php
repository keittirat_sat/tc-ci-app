<?php

require('MPDF56/mpdf.php');

if (!empty($argv) OR count($argv) < 3) {
    // parse command line argument
    $current_path = $argv[0];
    $input_file = preg_replace('/--INPUT_FILE=/', '', $argv[1]);
    $output_file = preg_replace('/--OUTPUT_FILE=/', '', $argv[2]);

    // check input file
    if (file_exists($input_file)) {
        // get file contents
        $html = file_get_contents($input_file);

        // mPDF
        //$mpdf = new mPDF('th');
        $mpdf = new mPDF('th','',0,'',15,15,5,0,0,0,'P');
        $mpdf->SetAutoFont(AUTOFONT_THAIVIET);
        $mpdf->writeHTML($html);
        $mpdf->Output($output_file);
        $mpdf->close();
		
        printf('Completed! output PDF already saved at %s', $output_file);
        exit();
    } else {
        print('Error! input file not found.');
        exit();
    }
} else {
    print('Error! missing argument.');
    exit();
}

exit();