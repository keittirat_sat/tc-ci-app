
<table class="coverpage">
    <tbody>
        <tr>
            <th colspan="6" style="position: relative; background-image:url('<?php echo "{$pro_path}\\Template\\logo.png"; ?>'); background-repeat: no-repeat; background-position: 2px 2px;">
                <span class="fheader1">แบบบันทึกการตรวจสภาพเขื่อน</span><br>
                <span class="fheader2"><i>ส่วนความปลอดภัยเขื่อน</i></span><br>
                <span class="fheader3">กรมชลประทาน</span>
            </th>
        </tr>
        <tr>
            <td class="hidden_template"></td>
            <td class="hidden_template"></td>
            <td class="hidden_template"></td>
            <td class="hidden_template"></td>
            <td class="hidden_template"></td>
            <td class="hidden_template"></td>
        </tr>
        <tr>
            <td colspan="4"><b>ชื่อเขื่อน : <?php echo $dam_info[0][2]; ?></b></td>
            <td colspan="2"><b>รหัสเขื่อน : <?php echo $dam_info[0][1]; ?></b></td>
        </tr>
        <tr>
            <td colspan="2">ที่ตั้งเขื่อน บ้าน : <?php echo $dam_info[0][3]; ?></td>
            <td colspan="2">ตำบล : <?php echo $dam_info[0][25]; ?></td>
            <td>อำเภอ : <?php echo $dam_info[0][26]; ?></td>
            <td>จังหวัด : <?php echo $dam_info[0][23]; ?></td>
        </tr>
        <tr>
            <td class="hidden_template"></td>
            <td class="hidden_template"></td>
            <td class="hidden_template"></td>
            <td class="hidden_template"></td>
            <td class="hidden_template"></td>
            <td class="hidden_template"></td>
        </tr>
        <tr>
            <td colspan="2">ความยาวเขื่อน (เมตร) : <?php echo $dam_info[0][12]; ?></td>
            <td colspan="2">ความสูงเขื่อน (เมตร) : <?php echo $dam_info[0][13]; ?></td>
            <td colspan="2">ความกว้างสันเขื่อน (เมตร)  : <?php echo $dam_info[0][14]; ?></td>
        </tr>
        <tr>
            <td colspan="2"><b>ปริมาณความจุ : ล้าน ลบ.ม.</b></td>
            <td colspan="4">ปริมาณน้ำ ณ วันที่ตรวจ : </td>
        </tr>
        <tr>
            <td colspan="2">ต่ำสุด : <?php echo $dam_info[0][19]; ?></td>
            <td colspan="2">ปกติ : <?php echo $dam_info[0][20]; ?></td>
            <td colspan="2">สูงสุด : <?php echo $dam_info[0][21]; ?></td>
        </tr>
        <tr>
            <td class="hidden_template"></td>
            <td class="hidden_template"></td>
            <td class="hidden_template"></td>
            <td class="hidden_template"></td>
            <td class="hidden_template"></td>
            <td class="hidden_template"></td>
        </tr>
        <tr>
            <td colspan="2"><b>ระดับต่างๆ : <?php if ($dam_info[0][22] == 0): ?>ม. ร.ท.ก.<?php else: ?>ม. ร.ส.ม.<?php endif; ?></b></td>
            <td colspan="4">ปริมาณน้ำ ณ วันที่ตรวจ : </td>
        </tr>
        <tr>
            <td colspan="2">ระดับน้ำต่ำสุด : <?php echo $dam_info[0][15]; ?></td>
            <td colspan="2">ระดับน้ำเก็บกักปกติ : <?php echo $dam_info[0][16]; ?></td>
            <td colspan="2">ระดับน้ำเก็บกักสูงสุด : <?php echo $dam_info[0][17]; ?></td>
        </tr>
        <tr>
            <td colspan="2">ระดับสันเขื่อน : <?php echo $dam_info[0][18]; ?></td>
            <td colspan="4"></td>
        </tr>
        <tr>
            <td colspan="3">โครงการที่รับผิดชอบ : <?php echo $dam_info[0][31]; ?></td>
            <td colspan="3">สำนักชลประทานที่ : <?php echo $dam_info[0][24]; ?></td>
        </tr>
        <tr>
            <td colspan="3">หัวหน้าโครงการ : <?php echo $dam_info[0][32]; ?></td>
            <td colspan="3">ผู้ดูแลรับผิดชอบเขื่อน : <?php echo $dam_info[0][33]; ?></td>
        </tr>
        <tr>
            <td class="hidden_template"></td>
            <td class="hidden_template"></td>
            <td class="hidden_template"></td>
            <td class="hidden_template"></td>
            <td class="hidden_template"></td>
            <td class="hidden_template"></td>
        </tr>
        <tr>
            <td colspan="2">โทรศัพท์ : <?php echo $dam_info[0][34]; ?></td>
            <td colspan="2">โทรสาร : <?php echo $dam_info[0][35]; ?></td>
            <td colspan="2">อีเมลล์ : <?php echo $dam_info[0][36]; ?></td>
        </tr>

        <tr>
            <td colspan="6"><b>รายชื่อผู้ร่วมการตรวจสภาพเขื่อน : </b></td>
        </tr>
        <tr>
            <td colspan="2" style="border: none;">ชื่อ-นามสกุล</td>
            <td colspan="2" style="border: none;">ตำแหน่ง</td>
            <td colspan="2" style="border: none;">สังกัด</td>
        </tr>

        <!-- Start Member -->
        <?php $i = 0; ?>
        <?php foreach ($dam_info[1] as $member): ?>
            <tr>
                <td colspan="2" style="border: none; border-bottom: 1px dotted;"><?php echo $member[0]; ?></td>
                <td colspan="2" style="border: none; border-bottom: 1px dotted;"><?php echo $member[1]; ?></td>
                <td colspan="2" style="border: none; border-bottom: 1px dotted;"><?php echo $member[2]; ?></td>
            </tr>
            <?php $i++; ?>
        <?php endforeach; ?>
        <!-- End Member -->
        <?php if ($i < 25): ?>
            <?php for ($j = $i; $j < 25; $j++): ?>
                <tr>
                    <td colspan="6" style="border: none; border-bottom: 1px dotted;"></td>
                </tr>
            <?php endfor; ?>
        <?php endif; ?>
        <tr>
            <td class="hidden_template"></td>
            <td class="hidden_template"></td>
            <td class="hidden_template"></td>
            <td class="hidden_template"></td>
            <td class="hidden_template"></td>
            <td class="hidden_template"></td>
        </tr>
        <tr>
            <td>วันที่ทำการตรวจสภาพเขื่อน :</td>
            <td colspan="5"> <?php echo ($dam_info[0][39]&&$dam_info[0][40]&&$dam_info[0][41])?$dam_info[0][39]." ".get_month($dam_info[0][40])." ".check_year($dam_info[0][41]):""; ?></td>
        </tr>
        <tr>
            <td class="hidden_template"></td>
            <td class="hidden_template"></td>
            <td class="hidden_template"></td>
            <td class="hidden_template"></td>
            <td class="hidden_template"></td>
            <td class="hidden_template"></td>
        </tr>
        <tr>
            <td>สภาพอากาศ ณ วันที่ตรวจ :</td>
            <td colspan="5"> <?php echo $dam_info[0][37]; ?></td>
        </tr>
        <tr>
            <td class="hidden_template"></td>
            <td class="hidden_template"></td>
            <td class="hidden_template"></td>
            <td class="hidden_template"></td>
            <td class="hidden_template"></td>
            <td class="hidden_template"></td>
        </tr>
        <tr>
            <td>อุณหภูมิ ณ วันที่ตรวจ :</td>
            <td colspan="5"> <?php echo $dam_info[0][38]; ?></td>
        </tr>
        <tr>
            <td class="hidden_template"></td>
            <td class="hidden_template"></td>
            <td class="hidden_template"></td>
            <td class="hidden_template"></td>
            <td class="hidden_template"></td>
            <td class="hidden_template"></td>
        </tr>
    </tbody>
</table>
<pagebreak />