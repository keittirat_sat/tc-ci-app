<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <meta charset="UTF-8">
        <title>template</title>
        <style type="text/css">
            root { 
                display: block;
            }

            table{
                width: 700px;
                border-spacing: 0px;
                border: 1px solid;
                border-collapse:collapse;
            }

            th{
                text-align: center;
                font-size: 11px;
                height: 26px;
                margin: 0px;
                padding: 0px;
                border: 1px solid;
            }

            th.score_template{
                width: 20px;
            }

            th.menu, td.menu{
                width:100px;
            }

            th.distance{
                width: 50px;
            }

            .headtitle{
                font-size: 14px;
                text-align: left;
                height: 32px;
                padding: 0px 5px;
            }

            td.block{
                background-color: #ccc;
                height: 20px;
            }

            td{
                border: 1px solid;
                border-left: 1px dotted;
                margin: 0px;
                padding: 0px;
                height: 20px;
                border-right: none;
                font-size: 12px;
                text-align: center;
            }

            td.distance{
                border: 1px solid;
            }

            td.first{
                border-left: 1px solid;
            }

            tfoot td.notice{
                font-size: 10px;
                text-align: center;
                text-decoration: underline;
            }

            tfoot td, tfoot th,table.desc td,table.desc th{
                border: 0px solid #fff;
                font-size: 11px;
                text-align: left;
            }

            table.desc{
                border: 1px solid;
            }

            table.desc .col1{
                width: 50px;
                font-size: 11px;
                text-align: center;
                text-decoration: underline;
            }

            table.desc .col2{
                width: 90px;
            }

            table.desc .col3{
                width: 560px;
            }			

            table.coverpage{

            }

            table.coverpage td{
                text-align: left;
                padding: 2px 0px 2px 5px;
                border: 1px dotted;				
                font-size: 20px;
            }

            table.coverpage td b{
                font-size: 24px;
            }

            .fheader1{
                font-size: 32px;
            }

            .fheader2{
                font-size: 30px;
            }

            .fheader3{
                font-size: 28px;
                font-weight: normal;
            }

            table.coverpage td.hidden_template{
                width: 166.67px;
                border: none;
                padding: 5px;
                height: 15px;
            }
            tfoot, .desc{
                line-height: 13px;
            }
        </style>
    </head>
    <body>

        <!-- Function -->
        <?php

        function getans($tab_id, $index) {
            if ($tab_id == $index) {
                echo "X";
            }
        }

        function getsingle($stdClass) {
            $x = (array) $stdClass;
            return $x[0];
        }

        function notsee($stdClass) {
            $x = getsingle($stdClass);
            foreach ($x as $item) {
                if ($item != "") {
					echo "&nbsp;&nbsp;&nbsp;";
                    return;
                }
            }
            echo " <b>X</b> ";
        }
        
        function get_month($month){
            $db_tab = array('','มกราคม','กุมภาพันธ์','มีนาคม','เมษายน','พฤษภาคม','มิถุนายน','กรกฏาคม','สิงหาคม','กันยายน','ตุลาคม','พฤศจิกายน','ธันวาคม');
            return $db_tab[$month];
        }
        
        function check_year($input){
            if(($input - 543) < 1900){
                return $input + 543;
            }else{
                return $input;
            }
        }
        ?>

        <!-- Argv -->
        <?php
        $data = preg_replace('/--DAM_PROP=/', '', $argv[1]);
        $pro_path = preg_replace('/--PRO_PATH=/', '', $argv[2]);
        $data = json_decode($data);
        ?>
        
        <!-- File Reader -->    
        <?php
        $filename = "{$pro_path}\\Template\\Param.dat";
        $fp = fopen($filename, "r");
        $dam_info = fread($fp, filesize($filename));
        $dam_info = json_decode(trim($dam_info));
        fclose($fp);
        ?>

        <!-- Read Data Table -->
        <?php
        $filename = "{$pro_path}\\Template\\RawsData.dat";
        if (is_file($filename)) {
            $fp = fopen($filename, "r");
            $score = fread($fp, filesize($filename));
            $score = json_decode($score);
            fclose($fp);
            unlink($filename);
        } else {
            $score = "";
        }
        ?>

        <?php $ext = null; ?>
        <?php if (is_file("{$pro_path}\\Plan\\{$dam_info[0][0]}.jpg")): ?>
            <?php $ext = "jpg"; ?>
        <?php endif; ?>
        <?php if (is_file("{$pro_path}\\Plan\\{$dam_info[0][0]}.JPG")): ?>
            <?php $ext = "JPG"; ?>
        <?php endif; ?>
        <?php if (is_file("{$pro_path}\\Plan\\{$dam_info[0][0]}.bmp")): ?>
            <?php $ext = "bmp"; ?>
        <?php endif; ?>
        <?php if (is_file("{$pro_path}\\Plan\\{$dam_info[0][0]}.BMP")): ?>
            <?php $ext = "BMP"; ?>
        <?php endif; ?>
        <?php if (is_file("{$pro_path}\\Plan\\{$dam_info[0][0]}.png")): ?>
            <?php $ext = "png"; ?>
        <?php endif; ?>
        <?php if (is_file("{$pro_path}\\Plan\\{$dam_info[0][0]}.PNG")): ?>
            <?php $ext = "PNG"; ?>
        <?php endif; ?>

        <?php
        $distance = array();
        if (isset($dam_info[0][12])) {
            $have_distance = true;
            $length = $dam_info[0][12];
            $km = 0;
            $m = 0;
            $limit = $length / 10;
            while (($km * 1000) + ($m * 100) < ($length - 150)) {
                $start = $km . "+" . $m . "00";
                $m++;
                if (($m % 10) == 0) {
                    $km++;
                    $m = 0;
                }
                $end = $km . "+" . $m . "00";
                $temp = array($start, $end);
                array_push($distance, $temp);
            }

            if (($km * 1000) + ($m * 100) < $length) {   //นำเศษที่หารไม่ลงตัวตามระยะวัดวิศวะกรรมเข้าใส่
                $f = $length - (($km * 1000) + ($m * 100));

                $start = $km . "+" . $m . "00";
                $m = ($m * 100) + $f;
                $str = "";
                if ($m < 100) {
                    $str = "0";
                    if ($m < 10) {
                        $str += "0" . $m;
                    } else {
                        $str += $m;
                    }
                } else {
                    $str = $m;
                }
                $end = $km . "+" . $str;

                $temp = array($start, $end);
                array_push($distance, $temp);
            }
        } else {
            $have_distance = false;
        }
        ?>

        <!--Page Cover-->
        <?php include 'cover_page.php'; ?>

        <!--Plan-->
        <?php if ($ext != null): ?>
            <img src="<?php echo "{$pro_path}\\Plan\\{$dam_info[0][0]}.{$ext}" ?>"/>			
			<pagebreak />
		<?php endif; ?>

    <?php for ($ixxx = 1; $ixxx <= 24; $ixxx++): ?>
        <!--Page<?php echo $ixxx; ?>-->  
        <?php include "template_all/page{$ixxx}.php"; ?>
    <?php endfor; ?>
</body>
</html>