<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Final_report</title>
        <style type="text/css">
            .breadbrumb{
                text-align: center;
                font-weight: bold;
            }
            .title_fr{
                font-size: 18px;
            }
            .subtitle_fr{
                font-size: 16px;
            }
            body{
                font-size: 15px;
                width: 700px;
                margin: 0px auto;
                text-align: justify;
                line-height: 20px;
            }
            li{
                list-style: none;
            }
            .section{
                margin-bottom: 10px;
            }
            p.indent{
                text-indent: 40px;
            }
            ul.addition{
                padding-left: 10px;
            }
            ul.addition li{
                text-indent: 25px;
                margin-bottom: 10px;
            }
        </style>
    </head>
    <body>
        <?php

        function score_index($score) {
            if ($score > 80) {
                echo "สภาพดีมาก สามารถทำงานได้ตามปกติ ไม่ซ่อมแซม";
            } elseif ($score > 60) {
                echo "สภาพดี สามารถทำงานได้ สมควรซ่อมแซม แต่รอได้";
            } elseif ($score > 40) {
                echo "สภาพปานกลาง สามารถทำงานได้ซ่อมแซมบางส่วน";
            } elseif ($score > 20) {
                echo "สภาพค่อนข้างแย่ เกือบทำงานไม่ได้ ซ่อมแซมทั้งหมด";
            } else {
                echo "สภาพแย่ ไม่สามารถทำงานได้ ต้องปรับปรุง";
            }
        }

        function score_marker($score) {
            if ($score > 80) {
                echo "<font style='color: green;'><b><i>{$score}%</i></b></font>";
            } else {
                echo "<font style='color: red;'><b><i>{$score}%</i></b></font>";
            }
        }

        function find_name($id, $propeties, $str = "") {
            $name = $propeties[$id]['name'];
            if ($str != "") {
                $str = trim("{$name} &raquo; {$str}");
            } else {
                $str = trim($name);
            }
            if ($propeties[$id]['parent'] != 0) {
                return find_name($propeties[$id]['parent'], $propeties, $str);
            }
            return $str;
        }
        
        function get_month($month){
            $db_tab = array('','มกราคม','กุมภาพันธ์','มีนาคม','เมษายน','พฤษภาคม','มิถุนายน','กรกฏาคม','สิงหาคม','กันยายน','ตุลาคม','พฤศจิกายน','ธันวาคม');
            return $db_tab[$month];
        }
        
        function check_year($input){
            if(($input - 543) < 1900){
                return $input + 543;
            }else{
                return $input;
            }
        }
		
		function correct_score($obj, $id){
			foreach($obj as $rec){
				if($rec->id == $id){
					return $rec->score;
				}
			}
			
			return null;
		}

        $appPath = preg_replace('/--C_FILE=/', '', $argv[1]);
        $path = "{$appPath}/final_param.dat";
        $fp = fopen($path, "r");
        $data = fread($fp, filesize($path));
        fclose($fp);

        //Transform Data
        $data = json_decode($data);
        $data = (array) $data;

        //Transform Propeties
        foreach ($data['Properties'] as $item) {
            $temp = array(
                'parent' => $item->parent,
                'name' => $item->name
            );
            $part[$item->id] = $temp;
        }
        ?>
        <div class="breadbrumb section">
            <p class="title_fr">รายงานวิเคราะห์ความปลอดภัยเขื่อน</p>
            <p class="subtitle_fr">เรื่อง  การตรวจสภาพเขื่อนด้วยสายตาและประเมินสภาพเขื่อนโดยวิธีดัชนีสภาพ</p>
            <p class="subtitle_fr">เขื่อน<?php echo $data['dam_info']->dam_name; ?>  อำเภอ<?php echo $data['dam_info']->district; ?>  จังหวัด<?php echo $data['dam_info']->province; ?></p>
            <p class="subtitle_fr">วันที่ <?php echo $data['dam_info']->day." ".get_month($data['dam_info']->month)." ".check_year($data['dam_info']->year); ?></p>
            <p></p>
            <p class="subtitle_fr">***********</p>
        </div>

        <div class="section">
            <p><b>วัตถุประสงค์</b></p>
            <ul>
                <li>1)	เพื่อตรวจสภาพเขื่อนทางสายตาและประเมินสภาพเขื่อนด้วยวิธีดัชนีสภาพ</li>
                <li>2)	เพื่อเพิ่มพูนความรู้และทักษะด้านการตรวจสภาพเขื่อนให้กับบุคลากรที่ปฏิบัติงาน ณ หัวงานเขื่อน</li>
                <li>3)	เพื่อให้คำแนะนำในการแก้ไขปัญหาเบื้องต้นในกรณีที่พบปัญหา</li>
            </ul>
        </div>

        <div class="section">
            <p><b>วิธีการศึกษา</b></p>
            <p class="indent">ตรวจสภาพเขื่อนด้วยสายตา (Visual Inspection) โดยการเดินตรวจสภาพในทุกองค์ประกอบของเขื่อน เพื่อนำข้อมูลที่ได้จากการตรวจสภาพเขื่อนด้วยสายตามาทำการประเมินสภาพเขื่อนโดยวิธีดัชนีสภาพ (Condition Index) และทำการเพิ่มพูนความรู้และทักษะเบื้องต้นด้านการตรวจสภาพเขื่อนให้กับบุคลากรที่ปฏิบัติงาน ณ หัวงานเขื่อน</p>
        </div>

        <div class="section">
            <p><b>ผลการศึกษา</b></p>
            <p class="indent">จากผลการตรวจสภาพเขื่อน<?php echo $data['dam_info']->dam_name; ?> จังหวัด<?php echo $data['dam_info']->province; ?> สามารถสรุปผลสภาพเขื่อนได้ดังนี้  ค่าดัชนีสภาพเขื่อน<?php echo $data['dam_info']->dam_name; ?>  มีค่าเท่ากับ <?php score_marker($data['dam_info']->score); ?> หมายถึง <?php score_index($data['dam_info']->score); ?> รายละเอียดของค่าดัชนีสภาพ  แบ่งแยกย่อยตามองค์ประกอบหลัก ได้ดังนี้ </p>
            <ul>
                <li>1.	เขื่อน  ค่าดัชนีสภาพ เท่ากับ <?php score_marker(correct_score($data['main_part'],1)); ?> หมายถึง <?php score_index(correct_score($data['main_part'],1)); ?></li>
                <li>2.	อาคารส่งน้ำ/ระบายน้ำ  ค่าดัชนีสภาพ เท่ากับ <?php score_marker(correct_score($data['main_part'],10)); ?> หมายถึง <?php score_index(correct_score($data['main_part'],10)); ?></li>
                <li>3.	อาคารระบายน้ำล้น ค่าดัชนีสภาพ เท่ากับ <?php score_marker(correct_score($data['main_part'],89)); ?> หมายถึง <?php score_index(correct_score($data['main_part'],89)); ?></li>
            </ul>
            <?php if ($data['Top5']): ?>
                <p class="indent"><b>องค์ประกอบส่วนใหญ่อยู่ในสภาพที่ดีมาก (80-100%) 5 อันดับแรก</b> ได้แก่</p>
                <ul class="addition">
                    <?php $top = $data['Top5']; ?>
                    <?php $i = 1; ?>
                    <?php foreach ($top as $rec): ?>
                        <li><?php echo $i; ?>. <?php echo find_name($rec->id, $part); ?>  ค่าดัชนีสภาพ เท่ากับ <?php score_marker($rec->score); ?> หมายถึง <?php echo score_index($rec->score); ?></li>
                        <?php $i++; ?>
                        <?php if ($i > 5): ?>
                            <?php break; ?>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </ul>
            <?php endif; ?>
            <?php if ($data['last5']): ?>
                <p class="indent">แม้ว่าค่าดัชนีสภาพเขื่อน<?php echo $data['dam_info']->dam_name; ?>โดยรวมอยู่ในสภาพ<?php score_index($data['dam_info']->score); ?> เมื่อพิจารณาถึงองค์ประกอบย่อยลงไป  แต่บางองค์ประกอบมีสภาพไม่ดีหรือแย่ที่ต้องทำการปรับปรุง หรือซ่อมแซมดังนี้</p>
                <ul class="addition">
                    <?php $last = $data['last5']; ?>
					<?php $last = array_reverse($last); ?>
                    <?php $i = 1; ?>
                    <?php foreach ($last as $rec): ?>
                        <li><?php echo $i; ?>. <?php echo find_name($rec->id, $part); ?>  ค่าดัชนีสภาพ เท่ากับ <?php score_marker($rec->score); ?> หมายถึง <?php echo score_index($rec->score); ?></li>
                        <?php $i++; ?>
                    <?php endforeach; ?>
                </ul>
            <?php endif; ?>
            <?php if ($data['solve']): ?>
                <p class="indent"><b>จากสภาพปัญหาดังกล่าว เห็นสมควรให้โครงการฯ ดำเนินการดังต่อไปนี้</b></p>
                <ul class="addition">
                    <?php echo $data['solve']; ?>
                </ul>
            <?php endif; ?>
        </div>
        <?php if ($data['conclusion']): ?>
            <div class="section">
                <p><b>สรุป</b></p>
                <ul class="addition">
                    <?php echo $data['conclusion']; ?>
                </ul>
            </div>
        <?php endif; ?>


        <?php if ($data['suggestion']): ?>
            <div class="section">
                <p><b>ข้อเสนอแนะ</b></p>
                <ul class="addition">
                    <?php echo $data['suggestion']; ?>
                </ul>
            </div>
        <?php endif; ?>
    </body>
</html>
