<?php
/*
 * Final Report Generator
 */
$current = $argv[0];
$appPath = preg_replace('/--C_FILE=/', '', $argv[1]);
$save_as = preg_replace('/--SAVE_FILE=/', '', $argv[2]);
$cmd = "{$appPath}\\bin\\php\\php5.3.13\\php {$appPath}\\final_report.php --C_FILE=".$appPath;
$data = shell_exec($cmd);
$fp = fopen("{$appPath}\\final_report_middle_file.html", "w");
fwrite($fp, $data);
fclose($fp);

//HTML2PDF_FReport.php
$cmd = "{$appPath}\\bin\\php\\php5.3.13\\php {$appPath}\\mPDF\\HTML2PDF_FReport.php --INPUT_FILE={$appPath}\\final_report_middle_file.html --OUTPUT_FILE={$save_as}";
exec($cmd);
?>
