﻿namespace Checklist
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            this.DamList = new System.Windows.Forms.TreeView();
            this.addDam = new System.Windows.Forms.Button();
            this.dam_info_panel = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.geo_id = new System.Windows.Forms.ComboBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.dam_id = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.dam_name = new System.Windows.Forms.TextBox();
            this.cancel_btn_dam_info = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.update_btn_dam_info = new System.Windows.Forms.Button();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.water_scale = new System.Windows.Forms.ComboBox();
            this.crest_level = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.max_sea = new System.Windows.Forms.TextBox();
            this.min_sea = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.normal_sea = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dam_province = new System.Windows.Forms.ComboBox();
            this.dam_district = new System.Windows.Forms.ComboBox();
            this.dam_subdistrict = new System.Windows.Forms.ComboBox();
            this.dam_baan = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.max_cap = new System.Windows.Forms.TextBox();
            this.min_cap = new System.Windows.Forms.TextBox();
            this.normal_cap = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label9 = new System.Windows.Forms.Label();
            this.height = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.width = new System.Windows.Forms.TextBox();
            this.length = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.planBox = new System.Windows.Forms.PictureBox();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.Toolstrip_ChangeImage = new System.Windows.Forms.ToolStripMenuItem();
            this.Toolstrip_DelImage = new System.Windows.Forms.ToolStripMenuItem();
            this.dam_properties_panel = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.DamProperties = new System.Windows.Forms.TabControl();
            this.damProp_1 = new System.Windows.Forms.TabPage();
            this.probBox_1 = new System.Windows.Forms.TreeView();
            this.damProp_2 = new System.Windows.Forms.TabPage();
            this.probBox_2 = new System.Windows.Forms.TreeView();
            this.damProp_3 = new System.Windows.Forms.TabPage();
            this.probBox_3 = new System.Windows.Forms.TreeView();
            this.panel3 = new System.Windows.Forms.Panel();
            this.reset_dam_properties_btn = new System.Windows.Forms.Button();
            this.update_dam_properties_btn = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.statusbar_label = new System.Windows.Forms.ToolStripStatusLabel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.profile_btn = new System.Windows.Forms.Button();
            this.dam_name_label = new System.Windows.Forms.Label();
            this.DamListTab = new System.Windows.Forms.TabControl();
            this.DamList_tab1 = new System.Windows.Forms.TabPage();
            this.DamList_tab2 = new System.Windows.Forms.TabPage();
            this.AllDamList = new System.Windows.Forms.TreeView();
            this.orderby_regist = new System.Windows.Forms.TabPage();
            this.view_by_regist = new System.Windows.Forms.TreeView();
            this.OperationTab = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.PropertiesTab = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.panel7 = new System.Windows.Forms.Panel();
            this.ChecklistAdded = new System.Windows.Forms.DataGridView();
            this.ChecklistSet = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dateOfChecklist = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProjectOfChecklist = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel6 = new System.Windows.Forms.Panel();
            this.add_checklist_sheet_wo_project = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.checklist_added_menu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.view_checklistsheet = new System.Windows.Forms.ToolStripMenuItem();
            this.export_checklistsheet = new System.Windows.Forms.ToolStripMenuItem();
            this.export_table = new System.Windows.Forms.ToolStripMenuItem();
            this.export_data_with_data = new System.Windows.Forms.ToolStripMenuItem();
            this.export_report = new System.Windows.Forms.ToolStripMenuItem();
            this.delete_checklistsheet = new System.Windows.Forms.ToolStripMenuItem();
            this.dam_info_panel.SuspendLayout();
            this.panel5.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.planBox)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.dam_properties_panel.SuspendLayout();
            this.panel4.SuspendLayout();
            this.DamProperties.SuspendLayout();
            this.damProp_1.SuspendLayout();
            this.damProp_2.SuspendLayout();
            this.damProp_3.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel1.SuspendLayout();
            this.DamListTab.SuspendLayout();
            this.DamList_tab1.SuspendLayout();
            this.DamList_tab2.SuspendLayout();
            this.orderby_regist.SuspendLayout();
            this.OperationTab.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.PropertiesTab.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChecklistAdded)).BeginInit();
            this.panel6.SuspendLayout();
            this.checklist_added_menu.SuspendLayout();
            this.SuspendLayout();
            // 
            // DamList
            // 
            this.DamList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DamList.Location = new System.Drawing.Point(3, 3);
            this.DamList.Name = "DamList";
            this.DamList.Size = new System.Drawing.Size(272, 570);
            this.DamList.TabIndex = 0;
            this.DamList.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.DamList_AfterSelect);
            // 
            // addDam
            // 
            this.addDam.Dock = System.Windows.Forms.DockStyle.Left;
            this.addDam.Location = new System.Drawing.Point(10, 10);
            this.addDam.Name = "addDam";
            this.addDam.Size = new System.Drawing.Size(105, 23);
            this.addDam.TabIndex = 1;
            this.addDam.Text = "เพิ่มเขื่อนใหม่";
            this.addDam.UseVisualStyleBackColor = true;
            this.addDam.Click += new System.EventHandler(this.button1_Click);
            // 
            // dam_info_panel
            // 
            this.dam_info_panel.Controls.Add(this.panel5);
            this.dam_info_panel.Controls.Add(this.panel2);
            this.dam_info_panel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dam_info_panel.Location = new System.Drawing.Point(3, 3);
            this.dam_info_panel.Name = "dam_info_panel";
            this.dam_info_panel.Size = new System.Drawing.Size(708, 570);
            this.dam_info_panel.TabIndex = 2;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.groupBox1);
            this.panel5.Controls.Add(this.cancel_btn_dam_info);
            this.panel5.Controls.Add(this.label1);
            this.panel5.Controls.Add(this.update_btn_dam_info);
            this.panel5.Controls.Add(this.groupBox5);
            this.panel5.Controls.Add(this.groupBox2);
            this.panel5.Controls.Add(this.groupBox4);
            this.panel5.Controls.Add(this.groupBox3);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(171, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(537, 570);
            this.panel5.TabIndex = 56;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.geo_id);
            this.groupBox1.Controls.Add(this.label19);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.dam_id);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.dam_name);
            this.groupBox1.Location = new System.Drawing.Point(11, 25);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(522, 98);
            this.groupBox1.TabIndex = 35;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "ชื่อและรหัส";
            // 
            // geo_id
            // 
            this.geo_id.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.geo_id.FormattingEnabled = true;
            this.geo_id.Location = new System.Drawing.Point(66, 72);
            this.geo_id.Name = "geo_id";
            this.geo_id.Size = new System.Drawing.Size(448, 21);
            this.geo_id.TabIndex = 6;
            this.geo_id.SelectedIndexChanged += new System.EventHandler(this.geo_id_SelectedIndexChanged);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(20, 74);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(40, 13);
            this.label19.TabIndex = 7;
            this.label19.Text = "ภูมิภาค";
            this.label19.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "รหัสเขื่อน";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // dam_id
            // 
            this.dam_id.Location = new System.Drawing.Point(64, 20);
            this.dam_id.Name = "dam_id";
            this.dam_id.Size = new System.Drawing.Size(450, 20);
            this.dam_id.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(14, 48);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "ชื่อเขื่อน";
            this.label4.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // dam_name
            // 
            this.dam_name.Location = new System.Drawing.Point(64, 46);
            this.dam_name.Name = "dam_name";
            this.dam_name.Size = new System.Drawing.Size(450, 20);
            this.dam_name.TabIndex = 5;
            // 
            // cancel_btn_dam_info
            // 
            this.cancel_btn_dam_info.Location = new System.Drawing.Point(88, 534);
            this.cancel_btn_dam_info.Name = "cancel_btn_dam_info";
            this.cancel_btn_dam_info.Size = new System.Drawing.Size(75, 23);
            this.cancel_btn_dam_info.TabIndex = 41;
            this.cancel_btn_dam_info.Text = "ลบทิ้ง";
            this.cancel_btn_dam_info.UseVisualStyleBackColor = true;
            this.cancel_btn_dam_info.Click += new System.EventHandler(this.cancel_btn_dam_info_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(6, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(337, 20);
            this.label1.TabIndex = 32;
            this.label1.Text = "แบบบันทึกการตรวจสภาพเขื่อน ส่วนความปลอดภัยเขื่อน";
            // 
            // update_btn_dam_info
            // 
            this.update_btn_dam_info.Location = new System.Drawing.Point(8, 534);
            this.update_btn_dam_info.Name = "update_btn_dam_info";
            this.update_btn_dam_info.Size = new System.Drawing.Size(75, 23);
            this.update_btn_dam_info.TabIndex = 40;
            this.update_btn_dam_info.Text = "อัพเดต";
            this.update_btn_dam_info.UseVisualStyleBackColor = true;
            this.update_btn_dam_info.Click += new System.EventHandler(this.update_btn_dam_info_Click);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.water_scale);
            this.groupBox5.Controls.Add(this.crest_level);
            this.groupBox5.Controls.Add(this.label18);
            this.groupBox5.Controls.Add(this.max_sea);
            this.groupBox5.Controls.Add(this.min_sea);
            this.groupBox5.Controls.Add(this.label17);
            this.groupBox5.Controls.Add(this.normal_sea);
            this.groupBox5.Controls.Add(this.label16);
            this.groupBox5.Controls.Add(this.label15);
            this.groupBox5.Location = new System.Drawing.Point(11, 371);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(261, 157);
            this.groupBox5.TabIndex = 39;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "ระดับต่างๆ (เมตร รทก.)";
            // 
            // water_scale
            // 
            this.water_scale.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.water_scale.FormattingEnabled = true;
            this.water_scale.Location = new System.Drawing.Point(66, 19);
            this.water_scale.Name = "water_scale";
            this.water_scale.Size = new System.Drawing.Size(187, 21);
            this.water_scale.TabIndex = 35;
            // 
            // crest_level
            // 
            this.crest_level.Location = new System.Drawing.Point(66, 123);
            this.crest_level.Name = "crest_level";
            this.crest_level.Size = new System.Drawing.Size(187, 20);
            this.crest_level.TabIndex = 20;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(14, 126);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(46, 13);
            this.label18.TabIndex = 34;
            this.label18.Text = "สันเขื่อน";
            this.label18.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // max_sea
            // 
            this.max_sea.Location = new System.Drawing.Point(66, 98);
            this.max_sea.Name = "max_sea";
            this.max_sea.Size = new System.Drawing.Size(187, 20);
            this.max_sea.TabIndex = 19;
            // 
            // min_sea
            // 
            this.min_sea.Location = new System.Drawing.Point(66, 46);
            this.min_sea.Name = "min_sea";
            this.min_sea.Size = new System.Drawing.Size(187, 20);
            this.min_sea.TabIndex = 17;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(32, 75);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(28, 13);
            this.label17.TabIndex = 31;
            this.label17.Text = "ปกติ";
            this.label17.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // normal_sea
            // 
            this.normal_sea.Location = new System.Drawing.Point(66, 72);
            this.normal_sea.Name = "normal_sea";
            this.normal_sea.Size = new System.Drawing.Size(187, 20);
            this.normal_sea.TabIndex = 18;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(30, 101);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(31, 13);
            this.label16.TabIndex = 32;
            this.label16.Text = "สูงสุด";
            this.label16.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(28, 49);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(32, 13);
            this.label15.TabIndex = 30;
            this.label15.Text = "ต่ำสุด";
            this.label15.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dam_province);
            this.groupBox2.Controls.Add(this.dam_district);
            this.groupBox2.Controls.Add(this.dam_subdistrict);
            this.groupBox2.Controls.Add(this.dam_baan);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Location = new System.Drawing.Point(11, 129);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(524, 127);
            this.groupBox2.TabIndex = 36;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "สถานที่ตั้ง";
            // 
            // dam_province
            // 
            this.dam_province.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.dam_province.FormattingEnabled = true;
            this.dam_province.Location = new System.Drawing.Point(64, 100);
            this.dam_province.Name = "dam_province";
            this.dam_province.Size = new System.Drawing.Size(450, 21);
            this.dam_province.TabIndex = 10;
            this.dam_province.SelectedIndexChanged += new System.EventHandler(this.dam_province_SelectedIndexChanged);
            // 
            // dam_district
            // 
            this.dam_district.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.dam_district.FormattingEnabled = true;
            this.dam_district.Location = new System.Drawing.Point(64, 72);
            this.dam_district.Name = "dam_district";
            this.dam_district.Size = new System.Drawing.Size(450, 21);
            this.dam_district.TabIndex = 9;
            this.dam_district.SelectedIndexChanged += new System.EventHandler(this.dam_district_SelectedIndexChanged);
            // 
            // dam_subdistrict
            // 
            this.dam_subdistrict.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.dam_subdistrict.FormattingEnabled = true;
            this.dam_subdistrict.Location = new System.Drawing.Point(64, 46);
            this.dam_subdistrict.Name = "dam_subdistrict";
            this.dam_subdistrict.Size = new System.Drawing.Size(450, 21);
            this.dam_subdistrict.TabIndex = 8;
            this.dam_subdistrict.SelectedIndexChanged += new System.EventHandler(this.dam_subdistrict_SelectedIndexChanged);
            // 
            // dam_baan
            // 
            this.dam_baan.Location = new System.Drawing.Point(64, 20);
            this.dam_baan.Name = "dam_baan";
            this.dam_baan.Size = new System.Drawing.Size(450, 20);
            this.dam_baan.TabIndex = 7;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(26, 49);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(32, 13);
            this.label6.TabIndex = 18;
            this.label6.Text = "ตำบล";
            this.label6.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(22, 75);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(36, 13);
            this.label7.TabIndex = 19;
            this.label7.Text = "อำเภอ";
            this.label7.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(20, 22);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(40, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "ชื่อบ้าน";
            this.label5.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(20, 101);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(38, 13);
            this.label8.TabIndex = 20;
            this.label8.Text = "จังหวัด";
            this.label8.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.max_cap);
            this.groupBox4.Controls.Add(this.min_cap);
            this.groupBox4.Controls.Add(this.normal_cap);
            this.groupBox4.Controls.Add(this.label12);
            this.groupBox4.Controls.Add(this.label14);
            this.groupBox4.Controls.Add(this.label13);
            this.groupBox4.Location = new System.Drawing.Point(274, 262);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(260, 103);
            this.groupBox4.TabIndex = 38;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "ปริมาตรความจุ (ล้าน ลบ.ม)";
            // 
            // max_cap
            // 
            this.max_cap.Location = new System.Drawing.Point(52, 73);
            this.max_cap.Name = "max_cap";
            this.max_cap.Size = new System.Drawing.Size(198, 20);
            this.max_cap.TabIndex = 16;
            // 
            // min_cap
            // 
            this.min_cap.Location = new System.Drawing.Point(52, 21);
            this.min_cap.Name = "min_cap";
            this.min_cap.Size = new System.Drawing.Size(198, 20);
            this.min_cap.TabIndex = 14;
            // 
            // normal_cap
            // 
            this.normal_cap.Location = new System.Drawing.Point(52, 47);
            this.normal_cap.Name = "normal_cap";
            this.normal_cap.Size = new System.Drawing.Size(198, 20);
            this.normal_cap.TabIndex = 15;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(15, 24);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(32, 13);
            this.label12.TabIndex = 24;
            this.label12.Text = "ต่ำสุด";
            this.label12.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(16, 76);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(31, 13);
            this.label14.TabIndex = 26;
            this.label14.Text = "สูงสุด";
            this.label14.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(20, 50);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(28, 13);
            this.label13.TabIndex = 25;
            this.label13.Text = "ปกติ";
            this.label13.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.height);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.width);
            this.groupBox3.Controls.Add(this.length);
            this.groupBox3.Location = new System.Drawing.Point(11, 262);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(260, 103);
            this.groupBox3.TabIndex = 37;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "รายละเอียดทางโครงสร้างเขื่อน (เมตร)";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(33, 50);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(25, 13);
            this.label9.TabIndex = 21;
            this.label9.Text = "ยาว";
            this.label9.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // height
            // 
            this.height.Location = new System.Drawing.Point(64, 73);
            this.height.Name = "height";
            this.height.Size = new System.Drawing.Size(187, 20);
            this.height.TabIndex = 13;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(28, 24);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(30, 13);
            this.label11.TabIndex = 23;
            this.label11.Text = "กว้าง";
            this.label11.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(40, 76);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(18, 13);
            this.label10.TabIndex = 22;
            this.label10.Text = "สูง";
            this.label10.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // width
            // 
            this.width.Location = new System.Drawing.Point(64, 21);
            this.width.Name = "width";
            this.width.Size = new System.Drawing.Size(187, 20);
            this.width.TabIndex = 11;
            // 
            // length
            // 
            this.length.Location = new System.Drawing.Point(64, 47);
            this.length.Name = "length";
            this.length.Size = new System.Drawing.Size(187, 20);
            this.length.TabIndex = 12;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.pictureBox1);
            this.panel2.Controls.Add(this.planBox);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(171, 570);
            this.panel2.TabIndex = 56;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox1.ErrorImage = null;
            this.pictureBox1.InitialImage = null;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(171, 165);
            this.pictureBox1.TabIndex = 33;
            this.pictureBox1.TabStop = false;
            // 
            // planBox
            // 
            this.planBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.planBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.planBox.ContextMenuStrip = this.contextMenuStrip1;
            this.planBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.planBox.Location = new System.Drawing.Point(0, 171);
            this.planBox.MaximumSize = new System.Drawing.Size(171, 210);
            this.planBox.Name = "planBox";
            this.planBox.Size = new System.Drawing.Size(171, 210);
            this.planBox.TabIndex = 55;
            this.planBox.TabStop = false;
            this.planBox.MouseClick += new System.Windows.Forms.MouseEventHandler(this.planBox_MouseClick);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Toolstrip_ChangeImage,
            this.Toolstrip_DelImage});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(141, 48);
            // 
            // Toolstrip_ChangeImage
            // 
            this.Toolstrip_ChangeImage.Name = "Toolstrip_ChangeImage";
            this.Toolstrip_ChangeImage.Size = new System.Drawing.Size(140, 22);
            this.Toolstrip_ChangeImage.Text = "เปลี่ยนรูปภาพ";
            this.Toolstrip_ChangeImage.Click += new System.EventHandler(this.Toolstrip_ChangeImage_Click);
            // 
            // Toolstrip_DelImage
            // 
            this.Toolstrip_DelImage.Name = "Toolstrip_DelImage";
            this.Toolstrip_DelImage.Size = new System.Drawing.Size(140, 22);
            this.Toolstrip_DelImage.Text = "ลบภาพออก";
            this.Toolstrip_DelImage.Click += new System.EventHandler(this.Toolstrip_DelImage_Click);
            // 
            // dam_properties_panel
            // 
            this.dam_properties_panel.Controls.Add(this.panel4);
            this.dam_properties_panel.Controls.Add(this.panel3);
            this.dam_properties_panel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dam_properties_panel.Location = new System.Drawing.Point(3, 3);
            this.dam_properties_panel.Name = "dam_properties_panel";
            this.dam_properties_panel.Size = new System.Drawing.Size(708, 570);
            this.dam_properties_panel.TabIndex = 3;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.DamProperties);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(708, 536);
            this.panel4.TabIndex = 2;
            // 
            // DamProperties
            // 
            this.DamProperties.Alignment = System.Windows.Forms.TabAlignment.Right;
            this.DamProperties.Controls.Add(this.damProp_1);
            this.DamProperties.Controls.Add(this.damProp_2);
            this.DamProperties.Controls.Add(this.damProp_3);
            this.DamProperties.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DamProperties.Location = new System.Drawing.Point(0, 0);
            this.DamProperties.Multiline = true;
            this.DamProperties.Name = "DamProperties";
            this.DamProperties.SelectedIndex = 0;
            this.DamProperties.Size = new System.Drawing.Size(708, 536);
            this.DamProperties.TabIndex = 0;
            // 
            // damProp_1
            // 
            this.damProp_1.Controls.Add(this.probBox_1);
            this.damProp_1.Location = new System.Drawing.Point(4, 4);
            this.damProp_1.Name = "damProp_1";
            this.damProp_1.Padding = new System.Windows.Forms.Padding(3);
            this.damProp_1.Size = new System.Drawing.Size(681, 528);
            this.damProp_1.TabIndex = 0;
            this.damProp_1.Text = "เขื่อน | Dam";
            this.damProp_1.UseVisualStyleBackColor = true;
            this.damProp_1.Click += new System.EventHandler(this.damProp_1_Click);
            // 
            // probBox_1
            // 
            this.probBox_1.CheckBoxes = true;
            this.probBox_1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.probBox_1.Location = new System.Drawing.Point(3, 3);
            this.probBox_1.Name = "probBox_1";
            this.probBox_1.Size = new System.Drawing.Size(675, 522);
            this.probBox_1.TabIndex = 0;
            this.probBox_1.AfterCheck += new System.Windows.Forms.TreeViewEventHandler(this.probBox_1_AfterCheck);
            // 
            // damProp_2
            // 
            this.damProp_2.Controls.Add(this.probBox_2);
            this.damProp_2.Location = new System.Drawing.Point(4, 4);
            this.damProp_2.Name = "damProp_2";
            this.damProp_2.Padding = new System.Windows.Forms.Padding(3);
            this.damProp_2.Size = new System.Drawing.Size(681, 528);
            this.damProp_2.TabIndex = 1;
            this.damProp_2.Text = "อาคารส่งน้ำ/ระบายน้ำ | Outlet";
            this.damProp_2.UseVisualStyleBackColor = true;
            this.damProp_2.Click += new System.EventHandler(this.damProp_2_Click);
            // 
            // probBox_2
            // 
            this.probBox_2.CheckBoxes = true;
            this.probBox_2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.probBox_2.Location = new System.Drawing.Point(3, 3);
            this.probBox_2.Name = "probBox_2";
            this.probBox_2.Size = new System.Drawing.Size(675, 522);
            this.probBox_2.TabIndex = 1;
            this.probBox_2.AfterCheck += new System.Windows.Forms.TreeViewEventHandler(this.probBox_2_AfterCheck);
            // 
            // damProp_3
            // 
            this.damProp_3.Controls.Add(this.probBox_3);
            this.damProp_3.Location = new System.Drawing.Point(4, 4);
            this.damProp_3.Name = "damProp_3";
            this.damProp_3.Padding = new System.Windows.Forms.Padding(3);
            this.damProp_3.Size = new System.Drawing.Size(681, 528);
            this.damProp_3.TabIndex = 2;
            this.damProp_3.Text = "อาคารระบายน้ำล้น | Spillways";
            this.damProp_3.UseVisualStyleBackColor = true;
            this.damProp_3.Click += new System.EventHandler(this.damProp_3_Click);
            // 
            // probBox_3
            // 
            this.probBox_3.CheckBoxes = true;
            this.probBox_3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.probBox_3.Location = new System.Drawing.Point(3, 3);
            this.probBox_3.Name = "probBox_3";
            this.probBox_3.Size = new System.Drawing.Size(675, 522);
            this.probBox_3.TabIndex = 1;
            this.probBox_3.AfterCheck += new System.Windows.Forms.TreeViewEventHandler(this.probBox_3_AfterCheck);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.panel3.Controls.Add(this.reset_dam_properties_btn);
            this.panel3.Controls.Add(this.update_dam_properties_btn);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(0, 536);
            this.panel3.Margin = new System.Windows.Forms.Padding(3, 3, 6, 3);
            this.panel3.Name = "panel3";
            this.panel3.Padding = new System.Windows.Forms.Padding(6);
            this.panel3.Size = new System.Drawing.Size(708, 34);
            this.panel3.TabIndex = 1;
            // 
            // reset_dam_properties_btn
            // 
            this.reset_dam_properties_btn.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.reset_dam_properties_btn.Dock = System.Windows.Forms.DockStyle.Right;
            this.reset_dam_properties_btn.Location = new System.Drawing.Point(521, 6);
            this.reset_dam_properties_btn.MaximumSize = new System.Drawing.Size(75, 27);
            this.reset_dam_properties_btn.MinimumSize = new System.Drawing.Size(75, 27);
            this.reset_dam_properties_btn.Name = "reset_dam_properties_btn";
            this.reset_dam_properties_btn.Size = new System.Drawing.Size(75, 27);
            this.reset_dam_properties_btn.TabIndex = 1;
            this.reset_dam_properties_btn.Text = "คืนค่าเดิม";
            this.reset_dam_properties_btn.UseVisualStyleBackColor = true;
            this.reset_dam_properties_btn.Click += new System.EventHandler(this.reset_dam_properties_btn_Click);
            // 
            // update_dam_properties_btn
            // 
            this.update_dam_properties_btn.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.update_dam_properties_btn.Dock = System.Windows.Forms.DockStyle.Right;
            this.update_dam_properties_btn.Location = new System.Drawing.Point(596, 6);
            this.update_dam_properties_btn.MaximumSize = new System.Drawing.Size(106, 27);
            this.update_dam_properties_btn.MinimumSize = new System.Drawing.Size(106, 27);
            this.update_dam_properties_btn.Name = "update_dam_properties_btn";
            this.update_dam_properties_btn.Size = new System.Drawing.Size(106, 27);
            this.update_dam_properties_btn.TabIndex = 2;
            this.update_dam_properties_btn.Text = "อัพเดตคุณสมบัติ";
            this.update_dam_properties_btn.UseVisualStyleBackColor = true;
            this.update_dam_properties_btn.Click += new System.EventHandler(this.update_dam_properties_btn_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Location = new System.Drawing.Point(0, 649);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1008, 22);
            this.statusStrip1.TabIndex = 5;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // statusbar_label
            // 
            this.statusbar_label.Name = "statusbar_label";
            this.statusbar_label.Size = new System.Drawing.Size(109, 17);
            this.statusbar_label.Text = "toolStripStatusLabel1";
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.profile_btn);
            this.panel1.Controls.Add(this.dam_name_label);
            this.panel1.Controls.Add(this.addDam);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(10);
            this.panel1.Size = new System.Drawing.Size(1008, 47);
            this.panel1.TabIndex = 6;
            // 
            // profile_btn
            // 
            this.profile_btn.Location = new System.Drawing.Point(121, 10);
            this.profile_btn.Name = "profile_btn";
            this.profile_btn.Size = new System.Drawing.Size(102, 23);
            this.profile_btn.TabIndex = 3;
            this.profile_btn.Text = "Profile";
            this.profile_btn.UseVisualStyleBackColor = true;
            this.profile_btn.Click += new System.EventHandler(this.profile_btn_Click);
            // 
            // dam_name_label
            // 
            this.dam_name_label.AutoSize = true;
            this.dam_name_label.Dock = System.Windows.Forms.DockStyle.Right;
            this.dam_name_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.dam_name_label.Location = new System.Drawing.Point(994, 10);
            this.dam_name_label.Name = "dam_name_label";
            this.dam_name_label.Size = new System.Drawing.Size(0, 22);
            this.dam_name_label.TabIndex = 2;
            // 
            // DamListTab
            // 
            this.DamListTab.Controls.Add(this.DamList_tab1);
            this.DamListTab.Controls.Add(this.DamList_tab2);
            this.DamListTab.Controls.Add(this.orderby_regist);
            this.DamListTab.Dock = System.Windows.Forms.DockStyle.Left;
            this.DamListTab.Location = new System.Drawing.Point(0, 47);
            this.DamListTab.Name = "DamListTab";
            this.DamListTab.SelectedIndex = 0;
            this.DamListTab.Size = new System.Drawing.Size(286, 602);
            this.DamListTab.TabIndex = 7;
            // 
            // DamList_tab1
            // 
            this.DamList_tab1.Controls.Add(this.DamList);
            this.DamList_tab1.Location = new System.Drawing.Point(4, 22);
            this.DamList_tab1.Name = "DamList_tab1";
            this.DamList_tab1.Padding = new System.Windows.Forms.Padding(3);
            this.DamList_tab1.Size = new System.Drawing.Size(278, 576);
            this.DamList_tab1.TabIndex = 0;
            this.DamList_tab1.Text = "เรียงตามภูมิภาค";
            this.DamList_tab1.UseVisualStyleBackColor = true;
            // 
            // DamList_tab2
            // 
            this.DamList_tab2.Controls.Add(this.AllDamList);
            this.DamList_tab2.Location = new System.Drawing.Point(4, 22);
            this.DamList_tab2.Name = "DamList_tab2";
            this.DamList_tab2.Padding = new System.Windows.Forms.Padding(3);
            this.DamList_tab2.Size = new System.Drawing.Size(278, 576);
            this.DamList_tab2.TabIndex = 1;
            this.DamList_tab2.Text = "เขื่อนทั้งหมด";
            this.DamList_tab2.UseVisualStyleBackColor = true;
            // 
            // AllDamList
            // 
            this.AllDamList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.AllDamList.Location = new System.Drawing.Point(3, 3);
            this.AllDamList.Name = "AllDamList";
            this.AllDamList.Size = new System.Drawing.Size(272, 570);
            this.AllDamList.TabIndex = 1;
            this.AllDamList.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.AllDamList_AfterSelect);
            // 
            // orderby_regist
            // 
            this.orderby_regist.Controls.Add(this.view_by_regist);
            this.orderby_regist.Location = new System.Drawing.Point(4, 22);
            this.orderby_regist.Name = "orderby_regist";
            this.orderby_regist.Padding = new System.Windows.Forms.Padding(3);
            this.orderby_regist.Size = new System.Drawing.Size(278, 576);
            this.orderby_regist.TabIndex = 2;
            this.orderby_regist.Text = "สำนักชลประทาน";
            this.orderby_regist.UseVisualStyleBackColor = true;
            // 
            // view_by_regist
            // 
            this.view_by_regist.Dock = System.Windows.Forms.DockStyle.Fill;
            this.view_by_regist.Location = new System.Drawing.Point(3, 3);
            this.view_by_regist.Name = "view_by_regist";
            this.view_by_regist.Size = new System.Drawing.Size(272, 570);
            this.view_by_regist.TabIndex = 2;
            this.view_by_regist.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.view_by_regist_AfterSelect);
            // 
            // OperationTab
            // 
            this.OperationTab.Controls.Add(this.tabPage1);
            this.OperationTab.Controls.Add(this.PropertiesTab);
            this.OperationTab.Controls.Add(this.tabPage2);
            this.OperationTab.Dock = System.Windows.Forms.DockStyle.Fill;
            this.OperationTab.Location = new System.Drawing.Point(286, 47);
            this.OperationTab.Name = "OperationTab";
            this.OperationTab.SelectedIndex = 0;
            this.OperationTab.Size = new System.Drawing.Size(722, 602);
            this.OperationTab.TabIndex = 8;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dam_info_panel);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(714, 576);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "รายละเอียด";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // PropertiesTab
            // 
            this.PropertiesTab.Controls.Add(this.dam_properties_panel);
            this.PropertiesTab.Location = new System.Drawing.Point(4, 22);
            this.PropertiesTab.Name = "PropertiesTab";
            this.PropertiesTab.Padding = new System.Windows.Forms.Padding(3);
            this.PropertiesTab.Size = new System.Drawing.Size(714, 576);
            this.PropertiesTab.TabIndex = 1;
            this.PropertiesTab.Text = "องค์ประกอบ";
            this.PropertiesTab.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.panel7);
            this.tabPage2.Controls.Add(this.panel6);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(714, 576);
            this.tabPage2.TabIndex = 2;
            this.tabPage2.Text = "แบบบันทึกการตรวจสภาพเขื่อน";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.ChecklistAdded);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel7.Location = new System.Drawing.Point(3, 3);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(708, 536);
            this.panel7.TabIndex = 3;
            // 
            // ChecklistAdded
            // 
            this.ChecklistAdded.AllowUserToAddRows = false;
            this.ChecklistAdded.AllowUserToDeleteRows = false;
            this.ChecklistAdded.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.ChecklistAdded.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.ChecklistAdded.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.ChecklistAdded.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ChecklistAdded.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ChecklistSet,
            this.dateOfChecklist,
            this.ProjectOfChecklist});
            this.ChecklistAdded.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ChecklistAdded.Location = new System.Drawing.Point(0, 0);
            this.ChecklistAdded.MultiSelect = false;
            this.ChecklistAdded.Name = "ChecklistAdded";
            this.ChecklistAdded.ReadOnly = true;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.ChecklistAdded.RowHeadersDefaultCellStyle = dataGridViewCellStyle11;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.ChecklistAdded.RowsDefaultCellStyle = dataGridViewCellStyle12;
            this.ChecklistAdded.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.ChecklistAdded.ShowEditingIcon = false;
            this.ChecklistAdded.Size = new System.Drawing.Size(708, 536);
            this.ChecklistAdded.TabIndex = 0;
            this.ChecklistAdded.MouseClick += new System.Windows.Forms.MouseEventHandler(this.ChecklistAdded_MouseClick);
            this.ChecklistAdded.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.ChecklistAdded_MouseDoubleClick);
            this.ChecklistAdded.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.ChecklistAdded_CellClick);
            // 
            // ChecklistSet
            // 
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.ChecklistSet.DefaultCellStyle = dataGridViewCellStyle8;
            this.ChecklistSet.HeaderText = "รหัสแบบบันทึกการตรวจสภาพเขื่อน";
            this.ChecklistSet.Name = "ChecklistSet";
            this.ChecklistSet.ReadOnly = true;
            // 
            // dateOfChecklist
            // 
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dateOfChecklist.DefaultCellStyle = dataGridViewCellStyle9;
            this.dateOfChecklist.HeaderText = "บันทึกเมื่อวันที่";
            this.dateOfChecklist.Name = "dateOfChecklist";
            this.dateOfChecklist.ReadOnly = true;
            // 
            // ProjectOfChecklist
            // 
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.ProjectOfChecklist.DefaultCellStyle = dataGridViewCellStyle10;
            this.ProjectOfChecklist.HeaderText = "คะแนนของเขื่อน";
            this.ProjectOfChecklist.Name = "ProjectOfChecklist";
            this.ProjectOfChecklist.ReadOnly = true;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.panel6.Controls.Add(this.add_checklist_sheet_wo_project);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel6.Location = new System.Drawing.Point(3, 539);
            this.panel6.Margin = new System.Windows.Forms.Padding(3, 3, 6, 3);
            this.panel6.Name = "panel6";
            this.panel6.Padding = new System.Windows.Forms.Padding(6);
            this.panel6.Size = new System.Drawing.Size(708, 34);
            this.panel6.TabIndex = 2;
            // 
            // add_checklist_sheet_wo_project
            // 
            this.add_checklist_sheet_wo_project.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.add_checklist_sheet_wo_project.Dock = System.Windows.Forms.DockStyle.Right;
            this.add_checklist_sheet_wo_project.Location = new System.Drawing.Point(574, 6);
            this.add_checklist_sheet_wo_project.MaximumSize = new System.Drawing.Size(128, 27);
            this.add_checklist_sheet_wo_project.MinimumSize = new System.Drawing.Size(128, 27);
            this.add_checklist_sheet_wo_project.Name = "add_checklist_sheet_wo_project";
            this.add_checklist_sheet_wo_project.Size = new System.Drawing.Size(128, 27);
            this.add_checklist_sheet_wo_project.TabIndex = 3;
            this.add_checklist_sheet_wo_project.Text = "เพิ่มใบตรวจเขื่อน";
            this.add_checklist_sheet_wo_project.UseVisualStyleBackColor = true;
            this.add_checklist_sheet_wo_project.Click += new System.EventHandler(this.add_checklist_sheet_wo_project_Click);
            // 
            // checklist_added_menu
            // 
            this.checklist_added_menu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.view_checklistsheet,
            this.export_checklistsheet,
            this.delete_checklistsheet});
            this.checklist_added_menu.Name = "checklist_added_menu";
            this.checklist_added_menu.Size = new System.Drawing.Size(181, 70);
            // 
            // view_checklistsheet
            // 
            this.view_checklistsheet.Name = "view_checklistsheet";
            this.view_checklistsheet.Size = new System.Drawing.Size(180, 22);
            this.view_checklistsheet.Text = "แก้ไข/ดูข้อมูลในตาราง";
            this.view_checklistsheet.Click += new System.EventHandler(this.view_checklistsheet_Click);
            // 
            // export_checklistsheet
            // 
            this.export_checklistsheet.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.export_table,
            this.export_data_with_data,
            this.export_report});
            this.export_checklistsheet.Name = "export_checklistsheet";
            this.export_checklistsheet.Size = new System.Drawing.Size(180, 22);
            this.export_checklistsheet.Text = "ส่งออกเป็น";
            // 
            // export_table
            // 
            this.export_table.Name = "export_table";
            this.export_table.Size = new System.Drawing.Size(273, 22);
            this.export_table.Text = "แบบบันทึกการตรวจสภาพเขื่อน";
            this.export_table.Click += new System.EventHandler(this.export_table_Click);
            // 
            // export_data_with_data
            // 
            this.export_data_with_data.Name = "export_data_with_data";
            this.export_data_with_data.Size = new System.Drawing.Size(273, 22);
            this.export_data_with_data.Text = "แบบบันทึกการตรวจสภาพเขื่อนพร้อมข้อมูล";
            this.export_data_with_data.Click += new System.EventHandler(this.export_data_with_data_Click);
            // 
            // export_report
            // 
            this.export_report.Name = "export_report";
            this.export_report.Size = new System.Drawing.Size(273, 22);
            this.export_report.Text = "รายงานสรุปการบันทึกการตรวจสภาพเขื่อน";
            this.export_report.Click += new System.EventHandler(this.export_report_Click);
            // 
            // delete_checklistsheet
            // 
            this.delete_checklistsheet.Name = "delete_checklistsheet";
            this.delete_checklistsheet.Size = new System.Drawing.Size(180, 22);
            this.delete_checklistsheet.Text = "ลบ";
            this.delete_checklistsheet.Click += new System.EventHandler(this.delete_checklistsheet_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1008, 671);
            this.Controls.Add(this.OperationTab);
            this.Controls.Add(this.DamListTab);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.statusStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "โปรแกรมคำนวณ CI : ส่วนความปลอดภัยเขื่อน";
            this.dam_info_panel.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.planBox)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.dam_properties_panel.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.DamProperties.ResumeLayout(false);
            this.damProp_1.ResumeLayout(false);
            this.damProp_2.ResumeLayout(false);
            this.damProp_3.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.DamListTab.ResumeLayout(false);
            this.DamList_tab1.ResumeLayout(false);
            this.DamList_tab2.ResumeLayout(false);
            this.orderby_regist.ResumeLayout(false);
            this.OperationTab.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.PropertiesTab.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChecklistAdded)).EndInit();
            this.panel6.ResumeLayout(false);
            this.checklist_added_menu.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TreeView DamList;
        private System.Windows.Forms.Button addDam;
        private System.Windows.Forms.Panel dam_info_panel;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox crest_level;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox max_sea;
        private System.Windows.Forms.TextBox min_sea;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox normal_sea;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox max_cap;
        private System.Windows.Forms.TextBox min_cap;
        private System.Windows.Forms.TextBox normal_cap;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox height;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox width;
        private System.Windows.Forms.TextBox length;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox dam_baan;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox dam_id;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox dam_name;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button cancel_btn_dam_info;
        private System.Windows.Forms.Button update_btn_dam_info;
        private System.Windows.Forms.ComboBox dam_province;
        private System.Windows.Forms.ComboBox dam_district;
        private System.Windows.Forms.ComboBox dam_subdistrict;
        private System.Windows.Forms.ComboBox geo_id;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Panel dam_properties_panel;
        private System.Windows.Forms.TabControl DamProperties;
        private System.Windows.Forms.TabPage damProp_1;
        private System.Windows.Forms.TabPage damProp_2;
        private System.Windows.Forms.TabPage damProp_3;
        private System.Windows.Forms.TreeView probBox_1;
        private System.Windows.Forms.TreeView probBox_2;
        private System.Windows.Forms.TreeView probBox_3;
        private System.Windows.Forms.Button update_dam_properties_btn;
        private System.Windows.Forms.Button reset_dam_properties_btn;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel statusbar_label;
        private System.Windows.Forms.PictureBox planBox;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem Toolstrip_ChangeImage;
        private System.Windows.Forms.ToolStripMenuItem Toolstrip_DelImage;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TabControl DamListTab;
        private System.Windows.Forms.TabPage DamList_tab1;
        private System.Windows.Forms.TabPage DamList_tab2;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TabControl OperationTab;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage PropertiesTab;
        private System.Windows.Forms.TreeView AllDamList;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.DataGridView ChecklistAdded;
        private System.Windows.Forms.ContextMenuStrip checklist_added_menu;
        private System.Windows.Forms.ToolStripMenuItem view_checklistsheet;
        private System.Windows.Forms.ToolStripMenuItem export_checklistsheet;
        private System.Windows.Forms.ToolStripMenuItem export_report;
        private System.Windows.Forms.ToolStripMenuItem export_table;
        private System.Windows.Forms.ToolStripMenuItem delete_checklistsheet;
        private System.Windows.Forms.ComboBox water_scale;
        private System.Windows.Forms.TabPage orderby_regist;
        private System.Windows.Forms.Button add_checklist_sheet_wo_project;
        private System.Windows.Forms.TreeView view_by_regist;
        private System.Windows.Forms.ToolStripMenuItem export_data_with_data;
        private System.Windows.Forms.DataGridViewTextBoxColumn ChecklistSet;
        private System.Windows.Forms.DataGridViewTextBoxColumn dateOfChecklist;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProjectOfChecklist;
        private System.Windows.Forms.Label dam_name_label;
        private System.Windows.Forms.Button profile_btn;
    }
}

