﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Collections;
using System.Threading;
//using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json.Linq;

namespace Checklist
{
    public partial class ChecklistPanel : Form
    {
        private Form1 mainForm;
        private ArrayList checklist_properties;
        private int dam_id;
        private bool[] checklist_structure = new bool[200];
        private ArrayList dam;
        private String checklist_id;
        private String dam_name;
        //Calculation Log
        private string[] callog = new string[152];
        private int order_log = 0;
        //All Dam Properties no filter
        private ArrayList all_properties;

        //All kept and cal current ID from DB
        private ArrayList all_kept;

        //Sheet INFO
        private ArrayList sheet1, sheet2, sheet3, sheet4, sheet5, sheet6, sheet7, sheet8, sheet9, sheet10, sheet11, sheet12, sheet13, sheet14, sheet15, sheet16, sheet17, sheet18, sheet19, sheet20, sheet21, sheet22, sheet23, sheet24;

        //Form Onload
        public ChecklistPanel(Form1 x, int dam_auto_id, string checklist_id_form_main)
        {
            mainForm = x;
            dam_id = dam_auto_id;
            InitializeComponent();
    
            //Get All Properties from Main form
            all_properties = mainForm.properties_list;

            //Get Dam Properties
            String sql = "select * from checklist where dam_id = '" + dam_auto_id.ToString() + "' order by checklist_id asc";
            checklist_properties = x.getDB(sql);
            
            /*checklist_id = DateTime.Now.ToString("yyMMddHHmmss");
            sql = "insert into fill_checklist (dam_auto_id, checklist_id, score) values('"+dam_auto_id+"', '"+checklist_id+"', '0')";
            mainForm.insertDB(sql);*/

            checklist_id = checklist_id_form_main;

            get_all_sheet();

            //List of structure which used in this dam
            foreach (ArrayList item in checklist_properties)
            {
                checklist_structure[Convert.ToInt16(item[1])] = true;
            }

            sql = "select * from dam_info where id_auto = " + dam_auto_id.ToString();
            dam = x.getDB(sql);


            foreach (ArrayList item in dam)
            {
                if (item[12].ToString() != string.Empty)
                {
                    generate_damlength(Convert.ToDouble(item[12]));
                }
                else
                {
                    generate_damlength();
                }

                dam_name = item[2].ToString();
            }

            //Console.WriteLine(mainForm.weight_raws);

            create_checklist_panel();
            this.KeyPreview = true;

            //Add Element into Project Page
            sql = "SELECT * FROM fill_checklist WHERE (((fill_checklist.checklist_id)='" + checklist_id + "'));";
            ArrayList ProjectPage = mainForm.getDB(sql);
            foreach (ArrayList item in ProjectPage)
            {
                Project.Text = item[4].ToString();
                HeadProject.Text = item[5].ToString();
                ProjectResponse.Text = item[6].ToString();
                Tel.Text = item[7].ToString();
                Fax.Text = item[8].ToString();
                Email.Text = item[9].ToString();
                try
                {
                    DateTime rp = new DateTime(Convert.ToInt32(item[14]), Convert.ToInt32(item[13]), Convert.ToInt32(item[12]));
                    checkatdate.Value = rp;
                }
                catch
                {
                    checkatdate.Value = DateTime.Now;
                }
                weatheratdate.Text = item[10].ToString();
                tempatdate.Text = item[11].ToString();
            }

            //Add Memeber
            sql = "SELECT * FROM member WHERE (((member.[checklist_id])='" + checklist_id + "'));";
            ArrayList member = mainForm.getDB(sql);
            foreach (ArrayList item in member)
            {
                OperationMember.Rows.Add(item[1], item[2], item[3]);
            }

            label_footer_checklistsheet.Text = "ตารางสำรวจหมายเลข " + checklist_id;
            this.Text = "ตารางสำรวจหมายเลข "+checklist_id;
            mainForm.Cursor = Cursors.Default;
            Form.CheckForIllegalCrossThreadCalls = false;
        }

        private void get_all_sheet()
        {
            String sql = String.Empty;

            for (int i = 1; i <= 24; i++)
            {
                sql = "SELECT * FROM sheet_" + i + " WHERE (((sheet_" + i + ".[checklist_id])='" + checklist_id + "')) ORDER BY sheet_" + i + ".ID;";
                switch (i)
                {
                    case 1: sheet1 = mainForm.getDB(sql); break;
                    case 2: sheet2 = mainForm.getDB(sql); break;
                    case 3: sheet3 = mainForm.getDB(sql); break;
                    case 4: sheet4 = mainForm.getDB(sql); break;
                    case 5: sheet5 = mainForm.getDB(sql); break;
                    case 6: sheet6 = mainForm.getDB(sql); break;
                    case 7: sheet7 = mainForm.getDB(sql); break;
                    case 8: sheet8 = mainForm.getDB(sql); break;
                    case 9: sheet9 = mainForm.getDB(sql); break;
                    case 10: sheet10 = mainForm.getDB(sql); break;
                    case 11: sheet11 = mainForm.getDB(sql); break;
                    case 12: sheet12 = mainForm.getDB(sql); break;
                    case 13: sheet13 = mainForm.getDB(sql); break;
                    case 14: sheet14 = mainForm.getDB(sql); break;
                    case 15: sheet15 = mainForm.getDB(sql); break;
                    case 16: sheet16 = mainForm.getDB(sql); break;
                    case 17: sheet17 = mainForm.getDB(sql); break;
                    case 18: sheet18 = mainForm.getDB(sql); break;
                    case 19: sheet19 = mainForm.getDB(sql); break;
                    case 20: sheet20 = mainForm.getDB(sql); break;
                    case 21: sheet21 = mainForm.getDB(sql); break;
                    case 22: sheet22 = mainForm.getDB(sql); break;
                    case 23: sheet23 = mainForm.getDB(sql); break;
                    case 24: sheet24 = mainForm.getDB(sql); break;
                    default: /* */ break;
                }
            }
        }

        /* สร้างระยะทางในการวัดของความยาวเขื่อน */
        //private void generate_damlength(double length = 0)
        private void generate_damlength()
        {
            generate_damlength(0);
        }

        private void generate_damlength(double length)
        {
            if (length == 0)
            {
                for (int running = 0; running < 20; running++ )
                {
                    ch_1_1_1.Rows.Add();
                    ch_1_1_2.Rows.Add();
                    ch_1_1_3.Rows.Add();
                    ch_1_3.Rows.Add();
                }
            }
            else
            {
                double km = 0, m = 0;
                double limit = length / 10;
                while ((km * 1000) + (m * 100) < (length - 150))
                {
                    string start = km.ToString() + "+" + m.ToString() + "00";
                    m++;
                    if ((m % 10) == 0)
                    {
                        km++;
                        m = 0;
                    }
                    string end = km.ToString() + "+" + m.ToString() + "00";

                    ch_1_1_1.Rows.Add(start, end);
                    ch_1_1_2.Rows.Add(start, end);
                    ch_1_1_3.Rows.Add(start, end);
                    ch_1_3.Rows.Add(start, end);
                }

                if ((km * 1000) + (m * 100) < length)   //นำเศษที่หารไม่ลงตัวตามระยะวัดวิศวะกรรมเข้าใส่
                {
                    double f = length - ((km * 1000) + (m * 100));

                    string start = km.ToString() + "+" + m.ToString() + "00";
                    m = (m * 100) + f;
                    string str = string.Empty;
                    if (m < 100)
                    {
                        str = "0";
                        if (m < 10)
                        {
                            str += "0" + m.ToString();
                        }
                        else
                        {
                            str += m.ToString();
                        }
                    }
                    else
                    {
                        str = m.ToString();
                    }
                    string end = km.ToString() + "+" + str;

                    ch_1_1_1.Rows.Add(start, end);
                    ch_1_1_2.Rows.Add(start, end);
                    ch_1_1_3.Rows.Add(start, end);
                    ch_1_3.Rows.Add(start, end);
                }
            }
        }

        private void fill_data(DataGridView table, ArrayList sheet, String table_id)
        {
            int r = 0;
            foreach (ArrayList item in sheet)
            {
                if (item[2].ToString() == table_id)
                {
                    for (int c = 0; c < table.ColumnCount; c++)
                    {
                        table.Rows[r].Cells[c].Value = item[c + 3];
                    }
                    r++;
                }
            }
        }

        /* ตัวซ่อนข้อของแบบสอบถามในกรณ๊ไม่ได้เลือก */
        private void create_checklist_panel()
        {
            int i = 0, c, r;
            String sql = string.Empty;
            DataGridView table;
            foreach (bool f in checklist_structure)
            {
                switch (i)
                {
                    case 5:
                        if (!f)
                        {
                            ChecklistSheet.TabPages.Remove(tp1);
                        }
                        else
                        {
                            r = 0;
                            table = ch_1_1_1;
                            foreach (ArrayList item in sheet1)
                            {
                                for (c = 2; c < table.ColumnCount; c++)
                                {
                                    table.Rows[r].Cells[c].Value = item[c + 3];
                                }
                                r++;
                            }
                        }
                        break;
                    case 6:
                        if (!f)
                            ChecklistSheet.TabPages.Remove(tp2);
                        else
                        {
                            r = 0;
                            table = ch_1_1_2;
                            foreach (ArrayList item in sheet2)
                            {
                                for (c = 2; c < table.ColumnCount; c++)
                                {
                                    table.Rows[r].Cells[c].Value = item[c + 3];
                                }
                                r++;
                            }
                        }
                        break;
                    case 7:
                        if(!f)
                            ChecklistSheet.TabPages.Remove(tp3);
                        else
                        {
                            r = 0;
                            table = ch_1_1_3;
                            foreach (ArrayList item in sheet3)
                            {
                                for (c = 2; c < table.ColumnCount; c++)
                                {
                                    table.Rows[r].Cells[c].Value = item[c + 3];
                                }
                                r++;
                            }
                        }
                        break;
                    case 8:
                        if (f)
                        {
                            ch_1_2.Rows.Add("ฝั่งซ้าย");
                            r = 0;
                            table = ch_1_2;
                            foreach (ArrayList item in sheet4)
                            {
                                if (item[2].ToString() == i.ToString())
                                {
                                    for (c = 1; c < table.ColumnCount; c++)
                                    {
                                        table.Rows[r].Cells[c].Value = item[c + 3];
                                    }
                                }
                            }
                        }
                        break;
                    case 9:
                        if (f)
                        {
                            ch_1_2.Rows.Add("ฝั่งขวา");
                            if (ch_1_2.RowCount == 1)
                            {
                                r = 0;
                            }
                            else
                            {
                                r = 1;
                            }
                            table = ch_1_2;
                            foreach (ArrayList item in sheet4)
                            {
                                if (item[2].ToString() == i.ToString())
                                {
                                    for (c = 1; c < table.ColumnCount; c++)
                                    {
                                        table.Rows[r].Cells[c].Value = item[c + 3];
                                    }
                                }
                            }
                        }
                        else if (ch_1_2.RowCount == 0)
                        {
                            b_1_2.Hide();
                        }
                        break;
                    case 4:
                        if (!f)
                        {
                            b_1_3.Hide();
                            if (ch_1_2.RowCount == 0)
                            {
                                ChecklistSheet.TabPages.Remove(tp4);
                            }
                        }
                        else
                        {
                            r = 0;
                            table = ch_1_3;
                            foreach (ArrayList item in sheet4)
                            {
                                if (item[2].ToString() == i.ToString())
                                {
                                    for (c = 2; c < table.ColumnCount; c++)
                                    {
                                        table.Rows[r].Cells[c].Value = item[c + 3];
                                    }
                                    r++;
                                }
                            }
                        }
                        break;
                    case 12:
                        if (f)
                        {
                            ch_2_1_1_1.Rows.Add(); 
                            fill_data(ch_2_1_1_1, sheet5, i.ToString());
                        }
                        else
                            b_2_1_1_1.Hide();
                        break;
                    case 14:
                        if (f)
                        {
                            ch_2_1_1_2_1.Rows.Add();
                            fill_data(ch_2_1_1_2_1, sheet5, i.ToString());
                        }
                        else
                            b_2_1_1_2_1.Hide();
                        break;
                    case 15:
                        if (f)
                        {
                            ch_2_1_1_2_2.Rows.Add();
                            fill_data(ch_2_1_1_2_2, sheet5, i.ToString());
                        }
                        else
                            b_2_1_1_2_2.Hide();
                        break;
                    case 16:
                        if (f)
                        {
                            ch_2_1_1_2_3.Rows.Add();
                            fill_data(ch_2_1_1_2_3, sheet5, i.ToString());
                        }
                        else
                            b_2_1_1_2_3.Hide();
                        break;
                    case 18:
                        if (f)
                        {
                            ch_2_1_2_1.Rows.Add();
                            fill_data(ch_2_1_2_1, sheet5, i.ToString());
                        }
                        else
                            b_2_1_2_1.Hide();
                        break;
                    case 23:
                        if (f){
                            ch_2_1_3_1.Rows.Add();
                            fill_data(ch_2_1_3_1, sheet5, i.ToString());
                        }else
                            b_2_1_3_1.Hide(); break;
                    case 24:
                        if (f){
                            ch_2_1_3_2.Rows.Add();
                            fill_data(ch_2_1_3_2, sheet5, i.ToString());
                        }else{
                            b_2_1_3_2.Hide();
                            if ((ch_2_1_1_1.RowCount == 0)&&(ch_2_1_1_2_1.RowCount == 0)&&(ch_2_1_1_2_2.RowCount == 0)&&(ch_2_1_1_2_3.RowCount == 0)&&(ch_2_1_2_1.RowCount == 0)&&(ch_2_1_3_1.RowCount == 0)&&(ch_2_1_3_2.RowCount == 0))
                            {
                                ChecklistSheet.TabPages.Remove(tb5);
                            }
                        }break;
                    case 25:
                        if (f)
                        {
                            ch_2_1_3_3.Rows.Add();
                            fill_data(ch_2_1_3_3, sheet6, i.ToString());
                        }
                        else
                            b_2_1_3_3.Hide();
                        break;
                    case 26:
                        if (f)
                        {
                            ch_2_1_3_4.Rows.Add();
                            fill_data(ch_2_1_3_4, sheet6, i.ToString());
                        }
                        else
                            b_2_1_3_4.Hide(); 
                        break;
                    case 27:
                        if (f)
                        {
                            ch_2_1_3_5.Rows.Add();
                            fill_data(ch_2_1_3_5, sheet6, i.ToString());
                        }
                        else
                            b_2_1_3_5.Hide();
                        break;
                    case 29:
                        if (f)
                        {
                            ch_2_1_3_6_1.Rows.Add();
                            fill_data(ch_2_1_3_6_1, sheet6, i.ToString());
                        }
                        else
                            b_2_1_3_6_1.Hide();
                        break;
                    case 30:
                        if (f)
                        {
                            ch_2_1_3_6_2.Rows.Add();
                            fill_data(ch_2_1_3_6_2, sheet6, i.ToString());
                        }
                        else
                            b_2_1_3_6_2.Hide();
                        break;
                    case 31:
                        if (f)
                        {
                            ch_2_1_3_6_3.Rows.Add();
                            fill_data(ch_2_1_3_6_3, sheet6, i.ToString());
                        }
                        else
                            b_2_1_3_6_3.Hide();
                        break;
                    case 33:
                        if (f){
                            ch_2_1_4_1_1.Rows.Add();
                            fill_data(ch_2_1_4_1_1, sheet6, i.ToString());
                        }else{
                            b_2_1_4_1_1.Hide();
                            if((ch_2_1_3_3.RowCount == 0)&&(ch_2_1_3_4.RowCount == 0)&&(ch_2_1_3_5.RowCount == 0)&&(ch_2_1_3_6_1.RowCount == 0)&&(ch_2_1_3_6_2.RowCount == 0)&&(ch_2_1_3_6_3.RowCount == 0)&&(ch_2_1_4_1_1.RowCount == 0)){
                                ChecklistSheet.TabPages.Remove(tp6);
                            }
                        }break;
                    case 34:
                        if (f)
                        {
                            ch_2_1_4_1_2.Rows.Add();
                            fill_data(ch_2_1_4_1_2, sheet7, i.ToString());
                        }
                        else
                            b_2_1_4_1_2.Hide();
                        break;
                    case 35:
                        if (f)
                        {
                            ch_2_1_5_1.Rows.Add();
                            fill_data(ch_2_1_5_1, sheet7, i.ToString());
                        }
                        else
                            b_2_1_5_1.Hide();
                        break;
                    case 36:
                        if (f)
                        {
                            ch_2_1_5_2.Rows.Add();
                            fill_data(ch_2_1_5_2, sheet7, i.ToString());
                        }
                        else
                            b_2_1_5_2.Hide();
                        break;
                    case 22:
                        if (f)
                        {
                            ch_2_1_6.Rows.Add();
                            fill_data(ch_2_1_6, sheet7, i.ToString());
                        }
                        else
                        {
                            b_2_1_6.Hide();
                            if ((ch_2_1_4_1_2.RowCount == 0) && (ch_2_1_5_1.RowCount == 0) && (ch_2_1_5_2.RowCount == 0) && (ch_2_1_6.RowCount == 0))
                            {
                                ChecklistSheet.TabPages.Remove(tp7);
                            }
                        }
                        break;
                    case 46:
                        if (f)
                        {
                            ch_2_2_1_1_1.Rows.Add();
                            fill_data(ch_2_2_1_1_1, sheet8, i.ToString());
                        }
                        else
                            b_2_2_1_1_1.Hide();
                        break;
                    case 48:
                        if (f)
                        {
                            ch_2_2_1_1_2_1.Rows.Add();
                            fill_data(ch_2_2_1_1_2_1, sheet8, i.ToString());
                        }
                        else
                            b_2_2_1_1_2_1.Hide();
                        break;
                    case 49:
                        if (f)
                        {
                            ch_2_2_1_1_2_2.Rows.Add();
                            fill_data(ch_2_2_1_1_2_2, sheet8, i.ToString());
                        }
                        else
                            b_2_2_1_1_2_2.Hide();
                        break;
                    case 50:
                        if (f)
                        {
                            ch_2_2_1_1_2_3.Rows.Add();
                            fill_data(ch_2_2_1_1_2_3, sheet8, i.ToString());
                        }
                        else
                            b_2_2_1_1_2_3.Hide();
                        break;
                    case 51:
                        if (f)
                        {
                            ch_2_2_1_2_1.Rows.Add();
                            fill_data(ch_2_2_1_2_1, sheet8, i.ToString());
                        }
                        else
                            b_2_2_1_2_1.Hide();
                        break;
                    case 52:
                        if (f)
                        {
                            ch_2_2_1_3_1.Rows.Add();
                            fill_data(ch_2_2_1_3_1, sheet8, i.ToString());
                        }
                        else
                            b_2_2_1_3_1.Hide();
                        break;
                    case 53:
                        if (f)
                        {
                            ch_2_2_1_3_2.Rows.Add();
                            fill_data(ch_2_2_1_3_2, sheet8, i.ToString());
                        }
                        else
                        {
                            b_2_2_1_3_2.Hide();
                            if ((ch_2_2_1_1_1.RowCount == 0) && (ch_2_2_1_1_2_1.RowCount == 0) && (ch_2_2_1_1_2_2.RowCount == 0) && (ch_2_2_1_1_2_3.RowCount == 0) && (ch_2_2_1_2_1.RowCount == 0) && (ch_2_2_1_3_1.RowCount == 0) && (ch_2_2_1_3_2.RowCount == 0))
                            {
                                ChecklistSheet.TabPages.Remove(tp8);
                            }
                        }
                        break;
                    case 54:
                        if (f)
                        {
                            ch_2_2_1_3_3.Rows.Add();
                            fill_data(ch_2_2_1_3_3, sheet9, i.ToString());
                        }
                        else
                            b_2_2_1_3_3.Hide();
                        break;
                    case 55:
                        if (f)
                        {
                            ch_2_2_1_3_4.Rows.Add();
                            fill_data(ch_2_2_1_3_4, sheet9, i.ToString());
                        }
                        else
                            b_2_2_1_3_4.Hide();
                        break;
                    case 56:
                        if (f)
                        {
                            ch_2_2_1_3_5.Rows.Add();
                            fill_data(ch_2_2_1_3_5, sheet9, i.ToString());
                        }
                        else
                            b_2_2_1_3_5.Hide();
                        break;
                    case 58:
                        if (f)
                        {
                            ch_2_2_1_3_6_1.Rows.Add();
                            fill_data(ch_2_2_1_3_6_1, sheet9, i.ToString());
                        }
                        else
                            b_2_2_1_3_6_1.Hide();
                        break;
                    case 59:
                        if (f)
                        {
                            ch_2_2_1_3_6_2.Rows.Add();
                            fill_data(ch_2_2_1_3_6_2, sheet9, i.ToString());
                        }
                        else
                            b_2_2_1_3_6_2.Hide();
                        break;
                    case 60:
                        if (f)
                        {
                            ch_2_2_1_3_6_3.Rows.Add();
                            fill_data(ch_2_2_1_3_6_3, sheet9, i.ToString());
                        }
                        else
                            b_2_2_1_3_6_3.Hide();
                        break;
                    case 62:
                        if (f)
                        {
                            ch_2_2_1_4_1_1.Rows.Add();
                            fill_data(ch_2_2_1_4_1_1, sheet9, i.ToString());
                        }
                        else
                        {
                            b_2_2_1_4_1_1.Hide();
                            if ((ch_2_2_1_3_3.RowCount == 0) && (ch_2_2_1_3_4.RowCount == 0) && (ch_2_2_1_3_5.RowCount == 0) && (ch_2_2_1_3_6_1.RowCount == 0) && (ch_2_2_1_3_6_2.RowCount == 0) && (ch_2_2_1_3_6_3.RowCount == 0) && (ch_2_2_1_4_1_1.RowCount == 0))
                            {
                                ChecklistSheet.TabPages.Remove(tp9);
                            }
                        }
                        break;
                    case 63:
                        if (f)
                        {
                            ch_2_2_1_4_1_2.Rows.Add();
                            fill_data(ch_2_2_1_4_1_2, sheet10, i.ToString());
                        }
                        else
                            b_2_2_1_4_1_2.Hide();
                        break;
                    case 64:
                        if (f)
                        {
                            ch_2_2_1_5_1.Rows.Add();
                            fill_data(ch_2_2_1_5_1, sheet10, i.ToString());
                        }
                        else
                            b_2_2_1_5_1.Hide();
                        break;
                    case 65:
                        if (f)
                        {
                            ch_2_2_1_5_2.Rows.Add();
                            fill_data(ch_2_2_1_5_2, sheet10, i.ToString());
                        }
                        else
                            b_2_2_1_5_2.Hide();
                        break;
                    case 45:
                        if (f)
                        {
                            ch_2_2_1_6.Rows.Add();
                            fill_data(ch_2_2_1_6, sheet10, i.ToString());
                        }
                        else
                        {
                            b_2_2_1_6.Hide();
                            if ((ch_2_2_1_4_1_2.RowCount == 0) && (ch_2_2_1_5_1.RowCount == 0) && (ch_2_2_1_5_2.RowCount == 0) && (ch_2_2_1_6.RowCount == 0))
                            {
                                ChecklistSheet.TabPages.Remove(tp10);
                            }
                        }
                        break;
                    case 72:
                        if (f)
                        {
                            ch_2_2_2_1_1.Rows.Add();
                            fill_data(ch_2_2_2_1_1, sheet11, i.ToString());
                        }
                        else
                            b_2_2_2_1_1.Hide();
                        break;
                    case 74:
                        if (f)
                        {
                            ch_2_2_2_1_2_1.Rows.Add();
                            fill_data(ch_2_2_2_1_2_1, sheet11, i.ToString());
                        }
                        else
                            b_2_2_2_1_2_1.Hide();
                        break;
                    case 75:
                        if (f)
                        {
                            ch_2_2_2_1_2_2.Rows.Add();
                            fill_data(ch_2_2_2_1_2_2, sheet11, i.ToString());
                        }
                        else
                            b_2_2_2_1_2_2.Hide();
                        break;
                    case 76:
                        if (f)
                        {
                            ch_2_2_2_1_2_3.Rows.Add();
                            fill_data(ch_2_2_2_1_2_3, sheet11, i.ToString());
                        }
                        else
                            b_2_2_2_1_2_3.Hide();
                        break;
                    case 77:
                        if (f)
                        {
                            ch_2_2_2_2_1.Rows.Add();
                            fill_data(ch_2_2_2_2_1, sheet11, i.ToString());
                        }
                        else
                            b_2_2_2_2_1.Hide();
                        break;
                    case 78:
                        if (f)
                        {
                            ch_2_2_2_3_1.Rows.Add();
                            fill_data(ch_2_2_2_3_1, sheet11, i.ToString());
                        }
                        else
                            b_2_2_2_3_1.Hide();
                        break;
                    case 79:
                        if (f)
                        {
                            ch_2_2_2_3_2.Rows.Add();
                            fill_data(ch_2_2_2_3_2, sheet11, i.ToString());
                        }
                        else
                        {
                            b_2_2_2_3_2.Hide();
                            if ((ch_2_2_2_1_1.RowCount == 0) && (ch_2_2_2_1_2_1.RowCount == 0) && (ch_2_2_2_1_2_2.RowCount == 0) && (ch_2_2_2_1_2_3.RowCount == 0) && (ch_2_2_2_2_1.RowCount == 0) && (ch_2_2_2_3_1.RowCount == 0) && (ch_2_2_2_3_2.RowCount == 0))
                            {
                                ChecklistSheet.TabPages.Remove(tp11);
                            }
                        }
                        break;
                    case 80:
                        if (f)
                        {
                            ch_2_2_2_3_3.Rows.Add();
                            fill_data(ch_2_2_2_3_3, sheet12, i.ToString());
                        }
                        else
                            b_2_2_2_3_3.Hide();
                        break;
                    case 81:
                        if (f)
                        {
                            ch_2_2_2_3_4.Rows.Add();
                            fill_data(ch_2_2_2_3_4, sheet12, i.ToString());
                        }
                        else
                            b_2_2_2_3_4.Hide();
                        break;
                    case 82:
                        if (f)
                        {
                            ch_2_2_2_3_5.Rows.Add();
                            fill_data(ch_2_2_2_3_5, sheet12, i.ToString());
                        }
                        else
                            b_2_2_2_3_5.Hide();
                        break;
                    case 150:
                        if (f)
                        {
                            ch_2_2_2_3_6_1.Rows.Add();
                            fill_data(ch_2_2_2_3_6_1, sheet12, i.ToString());
                        }
                        else
                            b_2_2_2_3_6_1.Hide();
                        break;
                    case 151:
                        if (f)
                        {
                            ch_2_2_2_3_6_2.Rows.Add();
                            fill_data(ch_2_2_2_3_6_2, sheet12, i.ToString());
                        }
                        else
                            b_2_2_2_3_6_2.Hide();
                        break;
                    case 152:
                        if (f)
                        {
                            ch_2_2_2_3_6_3.Rows.Add();
                            fill_data(ch_2_2_2_3_6_3, sheet12, i.ToString());
                        }
                        else
                            b_2_2_2_3_6_3.Hide();
                        break;
                    case 85:
                        if (f)
                        {
                            ch_2_2_2_4_1_1.Rows.Add();
                            fill_data(ch_2_2_2_4_1_1, sheet12, i.ToString());
                        }
                        else
                        {
                            b_2_2_2_4_1_1.Hide();
                            if ((ch_2_2_2_3_3.RowCount == 0) && (ch_2_2_2_3_4.RowCount == 0) && (ch_2_2_2_3_5.RowCount == 0) && (ch_2_2_2_3_6_1.RowCount == 0) && (ch_2_2_2_3_6_2.RowCount == 0) && (ch_2_2_2_3_6_3.RowCount == 0) && (ch_2_2_2_4_1_1.RowCount == 0))
                            {
                                ChecklistSheet.TabPages.Remove(tp12);
                            }
                        }
                        break;
                    case 86:
                        if (f)
                        {
                            ch_2_2_2_4_1_2.Rows.Add();
                            fill_data(ch_2_2_2_4_1_2, sheet13, i.ToString());
                        }
                        else
                            b_2_2_2_4_1_2.Hide();
                        break;
                    case 87:
                        if (f)
                        {
                            ch_2_2_2_5_1.Rows.Add();
                            fill_data(ch_2_2_2_5_1, sheet13, i.ToString());
                        }
                        else
                            b_2_2_2_5_1.Hide();
                        break;
                    case 88:
                        if (f)
                        {
                            ch_2_2_2_5_2.Rows.Add();
                            fill_data(ch_2_2_2_5_2, sheet13, i.ToString());
                        }
                        else
                            b_2_2_2_5_2.Hide();
                        break;
                    case 71:
                        if (f)
                        {
                            ch_2_2_2_6.Rows.Add();
                            fill_data(ch_2_2_2_6, sheet13, i.ToString());
                        }
                        else
                        {
                            b_2_2_2_6.Hide();
                            if ((ch_2_2_2_4_1_2.RowCount == 0) && (ch_2_2_2_4_1_2.RowCount == 0) && (ch_2_2_2_4_1_2.RowCount == 0) && (ch_2_2_2_4_1_2.RowCount == 0))
                            {
                                ChecklistSheet.TabPages.Remove(tp13);
                            }
                        }
                        break;
                    case 101:
                        if (f)
                        {
                            ch_3_1_1_1_1.Rows.Add();
                            fill_data(ch_3_1_1_1_1, sheet14, i.ToString());
                        }
                        else
                            b_3_1_1_1_1.Hide();
                        break;
                    case 102:
                        if (f)
                        {
                            ch_3_1_1_1_2.Rows.Add();
                            fill_data(ch_3_1_1_1_2, sheet14, i.ToString());
                        }
                        else
                            b_3_1_1_1_2.Hide();
                        break;
                    case 103:
                        if (f)
                        {
                            ch_3_1_1_2_1.Rows.Add();
                            fill_data(ch_3_1_1_2_1, sheet14, i.ToString());
                        }
                        else
                        {
                            b_3_1_1_2_1.Hide();
                            if ((ch_3_1_1_1_1.RowCount == 0) && (ch_3_1_1_1_2.RowCount == 0) && (ch_3_1_1_2_1.RowCount == 0))
                            {
                                ChecklistSheet.TabPages.Remove(tp14);
                            }
                        }
                        break;
                    case 104:
                        if (f)
                        {
                            ch_3_1_1_3_1.Rows.Add();
                            fill_data(ch_3_1_1_3_1, sheet15, i.ToString());
                        }
                        else
                            b_3_1_1_3_1.Hide();
                        break;
                    case 105:
                        if (f)
                        {
                            ch_3_1_1_3_2.Rows.Add();
                            fill_data(ch_3_1_1_3_2, sheet15, i.ToString());
                        }
                        else
                            b_3_1_1_3_2.Hide();
                        break;
                    case 106:
                        if (f)
                        {
                            ch_3_1_1_4_1.Rows.Add();
                            fill_data(ch_3_1_1_4_1, sheet15, i.ToString());
                        }
                        else
                            b_3_1_1_4_1.Hide();
                        break;
                    case 107:
                        if (f)
                        {
                            ch_3_1_1_4_2.Rows.Add();
                            fill_data(ch_3_1_1_4_2, sheet15, i.ToString());
                        }
                        else
                        {
                            b_3_1_1_4_2.Hide();
                            if ((ch_3_1_1_3_1.RowCount == 0) && (ch_3_1_1_3_2.RowCount == 0) && (ch_3_1_1_4_1.RowCount == 0) && (ch_3_1_1_4_2.RowCount == 0))
                            {
                                ChecklistSheet.TabPages.Remove(tp15);
                            }
                        }
                        break;
                    case 99:
                        if (f)
                        {
                            ch_3_1_1_5.Rows.Add();
                            fill_data(ch_3_1_1_5, sheet16, i.ToString());
                        }
                        else
                            b_3_1_1_5.Hide();
                        break;
                    case 108:
                        if (f)
                        {
                            ch_3_1_1_6_1.Rows.Add();
                            fill_data(ch_3_1_1_6_1, sheet16, i.ToString());
                        }
                        else
                            b_3_1_1_6_1.Hide();
                        break;
                    case 109:
                        if (f)
                        {
                            ch_3_1_1_6_2.Rows.Add();
                            fill_data(ch_3_1_1_6_2, sheet16, i.ToString());
                        }
                        else
                            b_3_1_1_6_2.Hide();
                        break;
                    case 110:
                        if (f)
                        {
                            ch_3_1_1_6_3.Rows.Add();
                            fill_data(ch_3_1_1_6_3, sheet16, i.ToString());
                        }
                        else
                        {
                            b_3_1_1_6_3.Hide();
                            if ((ch_3_1_1_5.RowCount == 0) && (ch_3_1_1_6_1.RowCount == 0) && (ch_3_1_1_6_2.RowCount == 0) && (ch_3_1_1_6_3.RowCount == 0))
                            {
                                ChecklistSheet.TabPages.Remove(tp16);
                            }
                        }
                        break;
                    case 117:
                        if (f)
                        {
                            ch_3_1_2_1_1.Rows.Add();
                            fill_data(ch_3_1_2_1_1, sheet17, i.ToString());
                        }
                        else
                            b_3_1_2_1_1.Hide();
                        break;
                    case 118:
                        if (f)
                        {
                            ch_3_1_2_1_2.Rows.Add();
                            fill_data(ch_3_1_2_1_2, sheet17, i.ToString());
                        }
                        else
                            b_3_1_2_1_2.Hide();
                        break;
                    case 119:
                        if (f)
                        {
                            ch_3_1_2_2_1.Rows.Add();
                            fill_data(ch_3_1_2_2_1, sheet17, i.ToString());
                        }
                        else
                            b_3_1_2_2_1.Hide();
                        break;
                    case 126:
                        if (f)
                        {
                            ch_3_1_2_2_2_1.Rows.Add();
                            fill_data(ch_3_1_2_2_2_1, sheet17, i.ToString());
                        }
                        else
                        {
                            b_3_1_2_2_2_1.Hide();
                            if ((ch_3_1_2_1_1.RowCount == 0) && (ch_3_1_2_1_2.RowCount == 0) && (ch_3_1_2_2_1.RowCount == 0) && (ch_3_1_2_2_2_1.RowCount == 0))
                            {
                                ChecklistSheet.TabPages.Remove(tp17);
                            }
                        }
                        break;
                    case 127:
                        if (f)
                        {
                            ch_3_1_2_2_2_2.Rows.Add();
                            fill_data(ch_3_1_2_2_2_2, sheet18, i.ToString());
                        }
                        else
                            b_3_1_2_2_2_2.Hide();
                        break;
                    case 128:
                        if (f)
                        {
                            ch_3_1_2_2_2_3.Rows.Add();
                            fill_data(ch_3_1_2_2_2_3, sheet18, i.ToString());
                        }
                        else
                            b_3_1_2_2_2_3.Hide();
                        break;
                    case 121:
                        if (f)
                        {
                            ch_3_1_2_2_3.Rows.Add();
                            fill_data(ch_3_1_2_2_3, sheet18, i.ToString());
                        }
                        else
                            b_3_1_2_2_3.Hide();
                        break;
                    case 122:
                        if (f)
                        {
                            ch_3_1_2_2_4.Rows.Add();
                            fill_data(ch_3_1_2_2_4, sheet18, i.ToString());
                        }
                        else
                        {
                            b_3_1_2_2_4.Hide();
                            if ((ch_3_1_2_2_2_2.RowCount == 0) && (ch_3_1_2_2_2_3.RowCount == 0) && (ch_3_1_2_2_3.RowCount == 0) && (ch_3_1_2_2_4.RowCount == 0))
                            {
                                ChecklistSheet.TabPages.Remove(tp18);
                            }
                        }
                        break;
                    case 123:
                        if (f)
                        {
                            ch_3_1_2_2_5.Rows.Add();
                            fill_data(ch_3_1_2_2_5, sheet19, i.ToString());
                        }
                        else
                            b_3_1_2_2_5.Hide();
                        break;
                    case 124:
                        if (f)
                        {
                            ch_3_1_2_2_6.Rows.Add();
                            fill_data(ch_3_1_2_2_6, sheet19, i.ToString());
                        }
                        else
                            b_3_1_2_2_6.Hide();
                        break;
                    case 125:
                        if (f)
                        {
                            ch_3_1_2_2_7.Rows.Add();
                            fill_data(ch_3_1_2_2_7, sheet19, i.ToString());
                        }
                        else
                            b_3_1_2_2_7.Hide();
                        break;
                    case 129:
                        if (f)
                        {
                            ch_3_1_2_3_1.Rows.Add();
                            fill_data(ch_3_1_2_3_1, sheet19, i.ToString());
                        }
                        else
                        {
                            b_3_1_2_3_1.Hide();
                            if ((ch_3_1_2_2_5.RowCount == 0) && (ch_3_1_2_2_6.RowCount == 0) && (ch_3_1_2_2_7.RowCount == 0) && (ch_3_1_2_3_1.RowCount == 0))
                            {
                                ChecklistSheet.TabPages.Remove(tp19);
                            }
                        }
                        break;
                    case 130:
                        if (f)
                        {
                            ch_3_1_2_3_2.Rows.Add();
                            fill_data(ch_3_1_2_3_2, sheet20, i.ToString());
                        }
                        else
                            b_3_1_2_3_2.Hide();
                        break;
                    case 131:
                        if (f)
                        {
                            ch_3_1_2_4_1.Rows.Add();
                            fill_data(ch_3_1_2_4_1, sheet20, i.ToString());
                        }
                        else
                            b_3_1_2_4_1.Hide();
                        break;
                    case 132:
                        if (f)
                        {
                            ch_3_1_2_4_2.Rows.Add();
                            fill_data(ch_3_1_2_4_2, sheet20, i.ToString());
                        }
                        else
                        {
                            b_3_1_2_4_2.Hide();
                            if ((ch_3_1_2_3_2.RowCount == 0) && (ch_3_1_2_4_1.RowCount == 0) && (ch_3_1_2_4_2.RowCount == 0))
                            {
                                ChecklistSheet.TabPages.Remove(tp20);
                            }
                        }
                        break;
                    case 115:
                        if (f)
                        {
                            ch_3_1_2_5.Rows.Add();
                            fill_data(ch_3_1_2_5, sheet21, i.ToString());
                        }
                        else
                            b_3_1_2_5.Hide();
                        break;
                    case 133:
                        if (f)
                        {
                            ch_3_1_2_6_1.Rows.Add();
                            fill_data(ch_3_1_2_6_1, sheet21, i.ToString());
                        }
                        else
                            b_3_1_2_6_1.Hide();
                        break;
                    case 134:
                        if (f)
                        {
                            ch_3_1_2_6_2.Rows.Add();
                            fill_data(ch_3_1_2_6_2, sheet21, i.ToString());
                        }
                        else
                            b_3_1_2_6_2.Hide();
                        break;
                    case 135:
                        if (f)
                        {
                            ch_3_1_2_6_3.Rows.Add();
                            fill_data(ch_3_1_2_6_3, sheet21, i.ToString());
                        }
                        else
                        {
                            b_3_1_2_6_3.Hide();
                            if ((ch_3_1_2_5.RowCount == 0) && (ch_3_1_2_6_1.RowCount == 0) && (ch_3_1_2_6_2.RowCount == 0) && (ch_3_1_2_6_3.RowCount == 0))
                            {
                                ChecklistSheet.TabPages.Remove(tp21);
                            }
                        }
                        break;
                    case 136:
                        if (f)
                        {
                            ch_3_1_3_1.Rows.Add();
                            fill_data(ch_3_1_3_1, sheet22, i.ToString());
                        }
                        else
                            b_3_1_3_1.Hide();
                        break;
                    case 141:
                        if (f)
                        {
                            ch_3_1_3_2_1.Rows.Add();
                            fill_data(ch_3_1_3_2_1, sheet22, i.ToString());
                        }
                        else
                            b_3_1_3_2_1.Hide();
                        break;
                    case 142:
                        if (f)
                        {
                            ch_3_1_3_3_1.Rows.Add();
                            fill_data(ch_3_1_3_3_1, sheet22, i.ToString());
                        }
                        else
                            b_3_1_3_3_1.Hide();
                        break;
                    case 143:
                        if (f)
                        {
                            ch_3_1_3_3_2.Rows.Add();
                            fill_data(ch_3_1_3_3_2, sheet22, i.ToString());
                        }
                        else
                        {
                            b_3_1_3_3_2.Hide();
                            if ((ch_3_1_3_1.RowCount == 0) && (ch_3_1_3_2_1.RowCount == 0) && (ch_3_1_3_3_1.RowCount == 0) && (ch_3_1_3_3_2.RowCount == 0))
                            {
                                ChecklistSheet.TabPages.Remove(tp22);
                            }
                        }
                        break;
                    case 144:
                        if (f)
                        {
                            ch_3_1_3_4_1.Rows.Add();
                            fill_data(ch_3_1_3_4_1, sheet23, i.ToString());
                        }
                        else
                            b_3_1_3_4_1.Hide();
                        break;
                    case 145:
                        if (f)
                        {
                            ch_3_1_3_4_2.Rows.Add();
                            fill_data(ch_3_1_3_4_2, sheet23, i.ToString());
                        }
                        else
                            b_3_1_3_4_2.Hide();
                        break;
                    case 140:
                        if (f)
                        {
                            ch_3_1_3_5.Rows.Add();
                            fill_data(ch_3_1_3_5, sheet23, i.ToString());
                        }
                        else
                        {
                            b_3_1_3_5.Hide();
                            if ((ch_3_1_3_4_1.RowCount == 0) && (ch_3_1_3_4_2.RowCount == 0) && (ch_3_1_3_5.RowCount == 0))
                            {
                                ChecklistSheet.TabPages.Remove(tp23);
                            }
                        }
                        break;
                    case 146:
                        if (f)
                        {
                            ch_3_2_1.Rows.Add();
                            fill_data(ch_3_2_1, sheet24, i.ToString());
                        }
                        else
                            b_3_2_1.Hide();
                        break;
                    case 147:
                        if (f)
                        {
                            ch_3_2_2.Rows.Add();
                            fill_data(ch_3_2_2, sheet24, i.ToString());
                        }
                        else
                            b_3_2_2.Hide();
                        break;
                    case 148:
                        if (f)
                        {
                            ch_3_2_3.Rows.Add();
                            fill_data(ch_3_2_3, sheet24, i.ToString());
                        }
                        else
                        {
                            b_3_2_3.Hide();
                            if ((ch_3_2_1.RowCount == 0) && (ch_3_2_2.RowCount == 0) && (ch_3_2_3.RowCount == 0))
                            {
                                ChecklistSheet.TabPages.Remove(tp24);
                            }
                        }
                        break;
                    default: break;
                }
                i++;
            }
        }

        private void ChecklistPanel_Load(object sender, EventArgs e)
        {

        }

        private Boolean checklimit_quationaire(int value, int except)
        {
            if (except == value)
            {
                return false;
            }
            else
            {
                switch (value)
                {
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                    case -99:
                        return true;
                    default:
                        if (value.ToString() == "" || value == null)
                            return true;
                        else
                            return false;
                }
            }
        }

        private void alert_msg()
        {
            MessageBox.Show("การป้อนค่าไม่ถูกต้อง");
        }

        private void limit_value(int grid_val)
        {
            if (grid_val <= 0 || grid_val > 5)
            {
                if(grid_val != -99)
                    MessageBox.Show("การป้อนค่าไม่ถูกต้อง กำหนดอยู่ในช่อง [1~5] : "+ grid_val);
            }
        }

        private void ch_1_1_1_check_questionaire(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView onCheck = ch_1_1_1;
            int rows_index = onCheck.CurrentCell.RowIndex;
            Boolean alert = true;
            for (int col = 0; col < onCheck.ColumnCount; col++)
            {
                int grid_value;
                if (onCheck.Rows[rows_index].Cells[col].Value == null || onCheck.Rows[rows_index].Cells[col].Value == "")
                {
                    grid_value = -99;
                }
                else
                {
                    try
                    {
                        grid_value = Convert.ToInt16(onCheck.Rows[rows_index].Cells[col].Value);
                    }
                    catch
                    {
                        grid_value = -99;
                    }
                }

                onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.White;
                switch (col)
                {
                    case 2:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 3:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 4:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 5:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 6:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 8:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 9:
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    default: limit_value(grid_value); break;
                }

            }
        }

        private void ch_1_1_2_check_questionaire(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView onCheck = ch_1_1_2;
            int rows_index = onCheck.CurrentCell.RowIndex;
            Boolean alert = true;
            for (int col = 0; col < onCheck.ColumnCount; col++)
            {
                int grid_value;
                if (onCheck.Rows[rows_index].Cells[col].Value == null || onCheck.Rows[rows_index].Cells[col].Value == "")
                {
                    grid_value = -99;
                }
                else
                {
                    try
                    {
                        grid_value = Convert.ToInt16(onCheck.Rows[rows_index].Cells[col].Value);
                    }
                    catch
                    {
                        grid_value = -99;
                    }
                }

                onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.White;
                switch (col)
                {
                    case 2:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 3:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 4:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 5:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    /*case 6:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;*/
                    case 7:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 8:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    default: limit_value(grid_value); break;
                }

            }
        }

        private void ch_1_1_3_check_questionaire(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView onCheck = ch_1_1_3;
            int rows_index = onCheck.CurrentCell.RowIndex;
            Boolean alert = true;
            for (int col = 0; col < onCheck.ColumnCount; col++)
            {
                int grid_value;
                if (onCheck.Rows[rows_index].Cells[col].Value == null || onCheck.Rows[rows_index].Cells[col].Value == "")
                {
                    grid_value = -99;
                }
                else
                {
                    try
                    {
                        grid_value = Convert.ToInt16(onCheck.Rows[rows_index].Cells[col].Value);
                    }
                    catch
                    {
                        grid_value = -99;
                    }
                }

                onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.White;
                switch (col)
                {
                    case 2:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 3:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 4:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 5:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 6:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 8:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 9:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    default: limit_value(grid_value); break;
                }

            }
        }

        private void ch_1_2_check_questionaire(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView onCheck = ch_1_2;
            int rows_index = onCheck.CurrentCell.RowIndex;
            Boolean alert = true;
            for (int col = 0; col < onCheck.ColumnCount; col++)
            {
                int grid_value;
                if (onCheck.Rows[rows_index].Cells[col].Value == null || onCheck.Rows[rows_index].Cells[col].Value == "")
                {
                    grid_value = -99;
                }
                else
                {
                    try
                    {
                        grid_value = Convert.ToInt16(onCheck.Rows[rows_index].Cells[col].Value);
                    }
                    catch
                    {
                        grid_value = -99;
                    }
                }

                onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.White;
                switch (col)
                {
                    case 1:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 2:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 3:
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 4:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 5:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 6:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 8:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    default: limit_value(grid_value); break;
                }

            }
        }

        private void ch_1_3_check_questionaire(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView onCheck = ch_1_3;
            int rows_index = onCheck.CurrentCell.RowIndex;
            Boolean alert = true;
            for (int col = 0; col < onCheck.ColumnCount; col++)
            {
                int grid_value;
                if (onCheck.Rows[rows_index].Cells[col].Value == null || onCheck.Rows[rows_index].Cells[col].Value == "")
                {
                    grid_value = -99;
                }
                else
                {
                    try
                    {
                        grid_value = Convert.ToInt16(onCheck.Rows[rows_index].Cells[col].Value);
                    }
                    catch
                    {
                        grid_value = -99;
                    }
                }

                onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.White;
                switch (col)
                {
                    case 2:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 3:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 4:
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 5:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 6:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 7:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 8:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 9:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 11:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    default: limit_value(grid_value); break;
                }

            }
        }

        private void ch_2_1_1_1_check_questionaire(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView onCheck = ch_2_1_1_1;
            int rows_index = onCheck.CurrentCell.RowIndex;
            Boolean alert = true;
            for (int col = 0; col < onCheck.ColumnCount; col++)
            {
                int grid_value;
                if (onCheck.Rows[rows_index].Cells[col].Value == null || onCheck.Rows[rows_index].Cells[col].Value == "")
                {
                    grid_value = -99;
                }
                else
                {
                    try
                    {
                        grid_value = Convert.ToInt16(onCheck.Rows[rows_index].Cells[col].Value);
                    }
                    catch
                    {
                        grid_value = -99;
                    }
                }

                onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.White;
                switch (col)
                {
                    case 0:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 1:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 3:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 4:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 5:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    default: limit_value(grid_value); break;
                }

            }
        }

        private void ch_2_1_1_2_1_check_questionaire(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView onCheck = ch_2_1_1_2_1;
            int rows_index = onCheck.CurrentCell.RowIndex;
            Boolean alert = true;
            for (int col = 0; col < onCheck.ColumnCount; col++)
            {
                int grid_value;
                if (onCheck.Rows[rows_index].Cells[col].Value == null || onCheck.Rows[rows_index].Cells[col].Value == "")
                {
                    grid_value = -99;
                }
                else
                {
                    try
                    {
                        grid_value = Convert.ToInt16(onCheck.Rows[rows_index].Cells[col].Value);
                    }
                    catch
                    {
                        grid_value = -99;
                    }
                }

                onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.White;
                switch (col)
                {
                    case 0:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 1:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 2:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 3:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    default: limit_value(grid_value); break;
                }

            }
        }

        private void ch_2_1_1_2_2_check_questionaire(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView onCheck = ch_2_1_1_2_2;
            int rows_index = onCheck.CurrentCell.RowIndex;
            Boolean alert = true;
            for (int col = 0; col < onCheck.ColumnCount; col++)
            {
                int grid_value;
                if (onCheck.Rows[rows_index].Cells[col].Value == null || onCheck.Rows[rows_index].Cells[col].Value == "")
                {
                    grid_value = -99;
                }
                else
                {
                    try
                    {
                        grid_value = Convert.ToInt16(onCheck.Rows[rows_index].Cells[col].Value);
                    }
                    catch
                    {
                        grid_value = -99;
                    }
                }

                onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.White;
                switch (col)
                {
                    case 0:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 1:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 2:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    default: limit_value(grid_value); break;
                }

            }
        }

        private void ch_2_1_1_2_3_check_questionaire(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView onCheck = ch_2_1_1_2_3;
            int rows_index = onCheck.CurrentCell.RowIndex;
            Boolean alert = true;
            for (int col = 0; col < onCheck.ColumnCount; col++)
            {
                int grid_value;
                if (onCheck.Rows[rows_index].Cells[col].Value == null || onCheck.Rows[rows_index].Cells[col].Value == "")
                {
                    grid_value = -99;
                }
                else
                {
                    try
                    {
                        grid_value = Convert.ToInt16(onCheck.Rows[rows_index].Cells[col].Value);
                    }
                    catch
                    {
                        grid_value = -99;
                    }
                }

                onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.White;
                switch (col)
                {
                    case 0:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 1:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 2:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    default: limit_value(grid_value); break;
                }

            }
        }

        private void ch_2_1_2_1_check_questionaire(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView onCheck = ch_2_1_2_1;
            int rows_index = onCheck.CurrentCell.RowIndex;
            Boolean alert = true;
            for (int col = 0; col < onCheck.ColumnCount; col++)
            {
                int grid_value;
                if (onCheck.Rows[rows_index].Cells[col].Value == null || onCheck.Rows[rows_index].Cells[col].Value == "")
                {
                    grid_value = -99;
                }
                else
                {
                    try
                    {
                        grid_value = Convert.ToInt16(onCheck.Rows[rows_index].Cells[col].Value);
                    }
                    catch
                    {
                        grid_value = -99;
                    }
                }

                onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.White;
                switch (col)
                {
                    case 0:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 1:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 2:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 3:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    default: limit_value(grid_value); break;
                }
            }
        }

        private void ch_2_1_3_1_check_questionaire(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView onCheck = ch_2_1_3_1;
            int rows_index = onCheck.CurrentCell.RowIndex;
            Boolean alert = true;
            for (int col = 0; col < onCheck.ColumnCount; col++)
            {
                int grid_value;
                if (onCheck.Rows[rows_index].Cells[col].Value == null || onCheck.Rows[rows_index].Cells[col].Value == "")
                {
                    grid_value = -99;
                }
                else
                {
                    try
                    {
                        grid_value = Convert.ToInt16(onCheck.Rows[rows_index].Cells[col].Value);
                    }
                    catch
                    {
                        grid_value = -99;
                    }
                }

                onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.White;
                switch (col)
                {
                    case 0:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 1:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 2:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 3:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 4:

                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    default: limit_value(grid_value); break;
                }
            }
        }

        private void ch_2_1_3_2_check_questionaire(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView onCheck = ch_2_1_3_2;
            int rows_index = onCheck.CurrentCell.RowIndex;
            Boolean alert = true;
            for (int col = 0; col < onCheck.ColumnCount; col++)
            {
                int grid_value;
                if (onCheck.Rows[rows_index].Cells[col].Value == null || onCheck.Rows[rows_index].Cells[col].Value == "")
                {
                    grid_value = -99;
                }
                else
                {
                    try
                    {
                        grid_value = Convert.ToInt16(onCheck.Rows[rows_index].Cells[col].Value);
                    }
                    catch
                    {
                        grid_value = -99;
                    }
                }

                onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.White;
                switch (col)
                {
                    case 0:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 1:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 2:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    default: limit_value(grid_value); break;
                }
            }
        }

        private void ch_2_1_3_3_check_questionaire(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView onCheck = ch_2_1_3_3;
            int rows_index = onCheck.CurrentCell.RowIndex;
            Boolean alert = true;
            for (int col = 0; col < onCheck.ColumnCount; col++)
            {
                int grid_value;
                if (onCheck.Rows[rows_index].Cells[col].Value == null || onCheck.Rows[rows_index].Cells[col].Value == "")
                {
                    grid_value = -99;
                }
                else
                {
                    try
                    {
                        grid_value = Convert.ToInt16(onCheck.Rows[rows_index].Cells[col].Value);
                    }
                    catch
                    {
                        grid_value = -99;
                    }
                }

                onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.White;
                switch (col)
                {
                    case 0:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 1:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    default: limit_value(grid_value); break;
                }
            }
        }

        private void ch_2_1_3_4_check_questionaire(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView onCheck = ch_2_1_3_4;
            int rows_index = onCheck.CurrentCell.RowIndex;
            Boolean alert = true;
            for (int col = 0; col < onCheck.ColumnCount; col++)
            {
                int grid_value;
                if (onCheck.Rows[rows_index].Cells[col].Value == null || onCheck.Rows[rows_index].Cells[col].Value == "")
                {
                    grid_value = -99;
                }
                else
                {
                    try
                    {
                        grid_value = Convert.ToInt16(onCheck.Rows[rows_index].Cells[col].Value);
                    }
                    catch
                    {
                        grid_value = -99;
                    }
                }

                onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.White;
                switch (col)
                {
                    case 0:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 1:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 2:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 3:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    default: limit_value(grid_value); break;
                }
            }
        }

        private void ch_2_1_3_5_check_questionaire(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView onCheck = ch_2_1_3_5;
            int rows_index = onCheck.CurrentCell.RowIndex;
            Boolean alert = true;
            for (int col = 0; col < onCheck.ColumnCount; col++)
            {
                int grid_value;
                if (onCheck.Rows[rows_index].Cells[col].Value == null || onCheck.Rows[rows_index].Cells[col].Value == "")
                {
                    grid_value = -99;
                }
                else
                {
                    try
                    {
                        grid_value = Convert.ToInt16(onCheck.Rows[rows_index].Cells[col].Value);
                    }
                    catch
                    {
                        grid_value = -99;
                    }
                }

                onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.White;
                switch (col)
                {
                    case 0:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 1:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 2:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 3:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    default: limit_value(grid_value); break;
                }
            }
        }

        private void ch_2_1_3_6_1_check_questionaire(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView onCheck = ch_2_1_3_6_1;
            int rows_index = onCheck.CurrentCell.RowIndex;
            Boolean alert = true;
            for (int col = 0; col < onCheck.ColumnCount; col++)
            {
                int grid_value;
                if (onCheck.Rows[rows_index].Cells[col].Value == null || onCheck.Rows[rows_index].Cells[col].Value == "")
                {
                    grid_value = -99;
                }
                else
                {
                    try
                    {
                        grid_value = Convert.ToInt16(onCheck.Rows[rows_index].Cells[col].Value);
                    }
                    catch
                    {
                        grid_value = -99;
                    }
                }

                onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.White;
                switch (col)
                {
                    case 0:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 1:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 2:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 3:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    default: limit_value(grid_value); break;
                }
            }
        }

        private void ch_2_1_3_6_2_check_questionaire(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView onCheck = ch_2_1_3_6_2;
            int rows_index = onCheck.CurrentCell.RowIndex;
            Boolean alert = true;
            for (int col = 0; col < onCheck.ColumnCount; col++)
            {
                int grid_value;
                if (onCheck.Rows[rows_index].Cells[col].Value == null || onCheck.Rows[rows_index].Cells[col].Value == "")
                {
                    grid_value = -99;
                }
                else
                {
                    try
                    {
                        grid_value = Convert.ToInt16(onCheck.Rows[rows_index].Cells[col].Value);
                    }
                    catch
                    {
                        grid_value = -99;
                    }
                }

                onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.White;
                switch (col)
                {
                    case 0:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 1:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 2:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 3:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    default: limit_value(grid_value); break;
                }
            }
        }

        private void ch_2_1_3_6_3_check_questionaire(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView onCheck = ch_2_1_3_6_3;
            int rows_index = onCheck.CurrentCell.RowIndex;
            Boolean alert = true;
            for (int col = 0; col < onCheck.ColumnCount; col++)
            {
                int grid_value;
                if (onCheck.Rows[rows_index].Cells[col].Value == null || onCheck.Rows[rows_index].Cells[col].Value == "")
                {
                    grid_value = -99;
                }
                else
                {
                    try
                    {
                        grid_value = Convert.ToInt16(onCheck.Rows[rows_index].Cells[col].Value);
                    }
                    catch
                    {
                        grid_value = -99;
                    }
                }

                onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.White;
                switch (col)
                {
                    case 0:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 1:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 2:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 3:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 5: break;
                }
            }
        }

        private void ch_2_1_4_1_1_check_questionaire(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView onCheck = ch_2_1_4_1_1;
            int rows_index = onCheck.CurrentCell.RowIndex;
            Boolean alert = true;
            for (int col = 0; col < onCheck.ColumnCount; col++)
            {
                int grid_value;
                if (onCheck.Rows[rows_index].Cells[col].Value == null || onCheck.Rows[rows_index].Cells[col].Value == "")
                {
                    grid_value = -99;
                }
                else
                {
                    try
                    {
                        grid_value = Convert.ToInt16(onCheck.Rows[rows_index].Cells[col].Value);
                    }
                    catch
                    {
                        grid_value = -99;
                    }
                }

                onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.White;
                switch (col)
                {
                    case 0:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 1:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 2:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 3:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 4:
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 5:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }

                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 6:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 7: break;
                    case 8:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 9:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    default: limit_value(grid_value); break;
                }
            }
        }

        private void ch_2_1_4_1_2_check_questionaire(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView onCheck = ch_2_1_4_1_2;
            int rows_index = onCheck.CurrentCell.RowIndex;
            Boolean alert = true;
            for (int col = 0; col < onCheck.ColumnCount; col++)
            {
                int grid_value;
                if (onCheck.Rows[rows_index].Cells[col].Value == null || onCheck.Rows[rows_index].Cells[col].Value == "")
                {
                    grid_value = -99;
                }
                else
                {
                    try
                    {
                        grid_value = Convert.ToInt16(onCheck.Rows[rows_index].Cells[col].Value);
                    }
                    catch
                    {
                        grid_value = -99;
                    }
                }

                onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.White;
                switch (col)
                {
                    case 0:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 1:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 2:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 3:
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 4:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 5:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 6:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 8:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    default: limit_value(grid_value); break;
                }
            }
        }

        private void ch_2_1_5_1_check_questionaire(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView onCheck = ch_2_1_5_1;
            int rows_index = onCheck.CurrentCell.RowIndex;
            Boolean alert = true;
            for (int col = 0; col < onCheck.ColumnCount; col++)
            {
                int grid_value;
                if (onCheck.Rows[rows_index].Cells[col].Value == null || onCheck.Rows[rows_index].Cells[col].Value == "")
                {
                    grid_value = -99;
                }
                else
                {
                    try
                    {
                        grid_value = Convert.ToInt16(onCheck.Rows[rows_index].Cells[col].Value);
                    }
                    catch
                    {
                        grid_value = -99;
                    }
                }

                onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.White;
                switch (col)
                {
                    case 0:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 1:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 2:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 3:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 4:
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 5:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 6:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 8:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 9:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    default: limit_value(grid_value); break;
                }
            }
        }

        private void ch_2_1_5_2_check_questionaire(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView onCheck = ch_2_1_5_2;
            int rows_index = onCheck.CurrentCell.RowIndex;
            Boolean alert = true;
            for (int col = 0; col < onCheck.ColumnCount; col++)
            {
                int grid_value;
                if (onCheck.Rows[rows_index].Cells[col].Value == null || onCheck.Rows[rows_index].Cells[col].Value == "")
                {
                    grid_value = -99;
                }
                else
                {
                    try
                    {
                        grid_value = Convert.ToInt16(onCheck.Rows[rows_index].Cells[col].Value);
                    }
                    catch
                    {
                        grid_value = -99;
                    }
                }

                onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.White;
                switch (col)
                {
                    case 0:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 1:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 2:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 3:
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 4:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 5:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 6:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 8:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    default: limit_value(grid_value); break;
                }
            }
        }

        private void ch_2_1_6_check_questionaire(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView onCheck = ch_2_1_6;
            int rows_index = onCheck.CurrentCell.RowIndex;
            Boolean alert = true;
            for (int col = 0; col < onCheck.ColumnCount; col++)
            {
                int grid_value;
                if (onCheck.Rows[rows_index].Cells[col].Value == null || onCheck.Rows[rows_index].Cells[col].Value == "")
                {
                    grid_value = -99;
                }
                else
                {
                    try
                    {
                        grid_value = Convert.ToInt16(onCheck.Rows[rows_index].Cells[col].Value);
                    }
                    catch
                    {
                        grid_value = -99;
                    }
                }

                onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.White;
                switch (col)
                {
                    case 0:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 1:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 2:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 3:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 4:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 5:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 6:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 7:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 8:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 10:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 11:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    default: limit_value(grid_value); break;
                }
            }
        }

        private void ch_2_2_1_1_1_check_questionaire(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView onCheck = ch_2_2_1_1_1;
            int rows_index = onCheck.CurrentCell.RowIndex;
            Boolean alert = true;
            for (int col = 0; col < onCheck.ColumnCount; col++)
            {
                int grid_value;
                if (onCheck.Rows[rows_index].Cells[col].Value == null || onCheck.Rows[rows_index].Cells[col].Value == "")
                {
                    grid_value = -99;
                }
                else
                {
                    try
                    {
                        grid_value = Convert.ToInt16(onCheck.Rows[rows_index].Cells[col].Value);
                    }
                    catch
                    {
                        grid_value = -99;
                    }
                }

                onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.White;
                switch (col)
                {
                    case 0:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 1:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 2:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 4:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 5:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    default: limit_value(grid_value); break;
                }
            }
        }

        private void ch_2_2_1_1_2_1_check_questionaire(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView onCheck = ch_2_2_1_1_2_1;
            int rows_index = onCheck.CurrentCell.RowIndex;
            Boolean alert = true;
            for (int col = 0; col < onCheck.ColumnCount; col++)
            {
                int grid_value;
                if (onCheck.Rows[rows_index].Cells[col].Value == null || onCheck.Rows[rows_index].Cells[col].Value == "")
                {
                    grid_value = -99;
                }
                else
                {
                    try
                    {
                        grid_value = Convert.ToInt16(onCheck.Rows[rows_index].Cells[col].Value);
                    }
                    catch
                    {
                        grid_value = -99;
                    }
                }

                onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.White;
                switch (col)
                {
                    case 0:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 1:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 2:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 3:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 4:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    default: limit_value(grid_value); break;
                }
            }
        }

        private void ch_2_2_1_1_2_2_check_questionaire(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView onCheck = ch_2_2_1_1_2_2;
            int rows_index = onCheck.CurrentCell.RowIndex;
            Boolean alert = true;
            for (int col = 0; col < onCheck.ColumnCount; col++)
            {
                int grid_value;
                if (onCheck.Rows[rows_index].Cells[col].Value == null || onCheck.Rows[rows_index].Cells[col].Value == "")
                {
                    grid_value = -99;
                }
                else
                {
                    try
                    {
                        grid_value = Convert.ToInt16(onCheck.Rows[rows_index].Cells[col].Value);
                    }
                    catch
                    {
                        grid_value = -99;
                    }
                }

                onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.White;
                switch (col)
                {
                    case 0:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 1:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 2:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    default: limit_value(grid_value); break;
                }
            }
        }

        private void ch_2_2_1_1_2_3_check_questionaire(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView onCheck = ch_2_2_1_1_2_3;
            int rows_index = onCheck.CurrentCell.RowIndex;
            Boolean alert = true;
            for (int col = 0; col < onCheck.ColumnCount; col++)
            {
                int grid_value;
                if (onCheck.Rows[rows_index].Cells[col].Value == null || onCheck.Rows[rows_index].Cells[col].Value == "")
                {
                    grid_value = -99;
                }
                else
                {
                    try
                    {
                        grid_value = Convert.ToInt16(onCheck.Rows[rows_index].Cells[col].Value);
                    }
                    catch
                    {
                        grid_value = -99;
                    }
                }

                onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.White;
                switch (col)
                {
                    case 0:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 1:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 2:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    default: limit_value(grid_value); break;
                }
            }
        }

        private void ch_2_2_1_2_1_check_questionaire(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView onCheck = ch_2_2_1_2_1;
            int rows_index = onCheck.CurrentCell.RowIndex;
            Boolean alert = true;
            for (int col = 0; col < onCheck.ColumnCount; col++)
            {
                int grid_value;
                if (onCheck.Rows[rows_index].Cells[col].Value == null || onCheck.Rows[rows_index].Cells[col].Value == "")
                {
                    grid_value = -99;
                }
                else
                {
                    try
                    {
                        grid_value = Convert.ToInt16(onCheck.Rows[rows_index].Cells[col].Value);
                    }
                    catch
                    {
                        grid_value = -99;
                    }
                }

                onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.White;
                switch (col)
                {
                    case 0:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 1:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 2:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 3:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    default: limit_value(grid_value); break;
                }
            }
        }

        private void ch_2_2_1_3_1_check_questionaire(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView onCheck = ch_2_2_1_3_1;
            int rows_index = onCheck.CurrentCell.RowIndex;
            Boolean alert = true;
            for (int col = 0; col < onCheck.ColumnCount; col++)
            {
                int grid_value;
                if (onCheck.Rows[rows_index].Cells[col].Value == null || onCheck.Rows[rows_index].Cells[col].Value == "")
                {
                    grid_value = -99;
                }
                else
                {
                    try
                    {
                        grid_value = Convert.ToInt16(onCheck.Rows[rows_index].Cells[col].Value);
                    }
                    catch
                    {
                        grid_value = -99;
                    }
                }

                onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.White;
                switch (col)
                {
                    case 0:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 1:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 2:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 3:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 4:
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    default: limit_value(grid_value); break;
                }
            }
        }

        private void ch_2_2_1_3_2_check_questionaire(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView onCheck = ch_2_2_1_3_2;
            int rows_index = onCheck.CurrentCell.RowIndex;
            Boolean alert = true;
            for (int col = 0; col < onCheck.ColumnCount; col++)
            {
                int grid_value;
                if (onCheck.Rows[rows_index].Cells[col].Value == null || onCheck.Rows[rows_index].Cells[col].Value == "")
                {
                    grid_value = -99;
                }
                else
                {
                    try
                    {
                        grid_value = Convert.ToInt16(onCheck.Rows[rows_index].Cells[col].Value);
                    }
                    catch
                    {
                        grid_value = -99;
                    }
                }

                onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.White;
                switch (col)
                {
                    case 0:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 1:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 2:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    default: limit_value(grid_value); break;
                }
            }
        }

        private void ch_2_2_1_3_3_check_questionaire(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView onCheck = ch_2_2_1_3_3;
            int rows_index = onCheck.CurrentCell.RowIndex;
            Boolean alert = true;
            for (int col = 0; col < onCheck.ColumnCount; col++)
            {
                int grid_value;
                if (onCheck.Rows[rows_index].Cells[col].Value == null || onCheck.Rows[rows_index].Cells[col].Value == "")
                {
                    grid_value = -99;
                }
                else
                {
                    try
                    {
                        grid_value = Convert.ToInt16(onCheck.Rows[rows_index].Cells[col].Value);
                    }
                    catch
                    {
                        grid_value = -99;
                    }
                }

                onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.White;
                switch (col)
                {
                    case 0:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 1:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    default: limit_value(grid_value); break;
                }
            }
        }

        private void ch_2_2_1_3_4_check_questionaire(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView onCheck = ch_2_2_1_3_4;
            int rows_index = onCheck.CurrentCell.RowIndex;
            Boolean alert = true;
            for (int col = 0; col < onCheck.ColumnCount; col++)
            {
                int grid_value;
                if (onCheck.Rows[rows_index].Cells[col].Value == null || onCheck.Rows[rows_index].Cells[col].Value == "")
                {
                    grid_value = -99;
                }
                else
                {
                    try
                    {
                        grid_value = Convert.ToInt16(onCheck.Rows[rows_index].Cells[col].Value);
                    }
                    catch
                    {
                        grid_value = -99;
                    }
                }

                onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.White;
                switch (col)
                {
                    case 0:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 1:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 2:

                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 3:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    default: limit_value(grid_value); break;
                }
            }
        }

        private void ch_2_2_1_3_5_check_questionaire(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView onCheck = ch_2_2_1_3_5;
            int rows_index = onCheck.CurrentCell.RowIndex;
            Boolean alert = true;
            for (int col = 0; col < onCheck.ColumnCount; col++)
            {
                int grid_value;
                if (onCheck.Rows[rows_index].Cells[col].Value == null || onCheck.Rows[rows_index].Cells[col].Value == "")
                {
                    grid_value = -99;
                }
                else
                {
                    try
                    {
                        grid_value = Convert.ToInt16(onCheck.Rows[rows_index].Cells[col].Value);
                    }
                    catch
                    {
                        grid_value = -99;
                    }
                }

                onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.White;
                switch (col)
                {
                    case 0:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 1:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 2:

                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 3:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    default: limit_value(grid_value); break;
                }
            }
        }

        private void ch_2_2_1_3_6_1_check_questionaire(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView onCheck = ch_2_2_1_3_6_1;
            int rows_index = onCheck.CurrentCell.RowIndex;
            Boolean alert = true;
            for (int col = 0; col < onCheck.ColumnCount; col++)
            {
                int grid_value;
                if (onCheck.Rows[rows_index].Cells[col].Value == null || onCheck.Rows[rows_index].Cells[col].Value == "")
                {
                    grid_value = -99;
                }
                else
                {
                    try
                    {
                        grid_value = Convert.ToInt16(onCheck.Rows[rows_index].Cells[col].Value);
                    }
                    catch
                    {
                        grid_value = -99;
                    }
                }

                onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.White;
                switch (col)
                {
                    case 0:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 1:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 2:

                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 3:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    default: limit_value(grid_value); break;
                }
            }
        }

        private void ch_2_2_1_3_6_2_check_questionaire(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView onCheck = ch_2_2_1_3_6_2;
            int rows_index = onCheck.CurrentCell.RowIndex;
            Boolean alert = true;
            for (int col = 0; col < onCheck.ColumnCount; col++)
            {
                int grid_value;
                if (onCheck.Rows[rows_index].Cells[col].Value == null || onCheck.Rows[rows_index].Cells[col].Value == "")
                {
                    grid_value = -99;
                }
                else
                {
                    try
                    {
                        grid_value = Convert.ToInt16(onCheck.Rows[rows_index].Cells[col].Value);
                    }
                    catch
                    {
                        grid_value = -99;
                    }
                }

                onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.White;
                switch (col)
                {
                    case 0:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 1:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 2:

                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 3:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    default: limit_value(grid_value); break;
                }
            }
        }

        private void ch_2_2_1_3_6_3_check_questionaire(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView onCheck = ch_2_2_1_3_6_3;
            int rows_index = onCheck.CurrentCell.RowIndex;
            Boolean alert = true;
            for (int col = 0; col < onCheck.ColumnCount; col++)
            {
                int grid_value;
                if (onCheck.Rows[rows_index].Cells[col].Value == null || onCheck.Rows[rows_index].Cells[col].Value == "")
                {
                    grid_value = -99;
                }
                else
                {
                    try
                    {
                        grid_value = Convert.ToInt16(onCheck.Rows[rows_index].Cells[col].Value);
                    }
                    catch
                    {
                        grid_value = -99;
                    }
                }

                onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.White;
                switch (col)
                {
                    case 0:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 1:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 2:

                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 3:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    default: limit_value(grid_value); break;
                }
            }
        }

        private void ch_2_2_1_4_1_1_check_questionaire(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView onCheck = ch_2_2_1_4_1_1;
            int rows_index = onCheck.CurrentCell.RowIndex;
            Boolean alert = true;
            for (int col = 0; col < onCheck.ColumnCount; col++)
            {
                int grid_value;
                if (onCheck.Rows[rows_index].Cells[col].Value == null || onCheck.Rows[rows_index].Cells[col].Value == "")
                {
                    grid_value = -99;
                }
                else
                {
                    try
                    {
                        grid_value = Convert.ToInt16(onCheck.Rows[rows_index].Cells[col].Value);
                    }
                    catch
                    {
                        grid_value = -99;
                    }
                }

                onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.White;
                switch (col)
                {
                    case 0:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 1:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 2:

                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 3:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 4:
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 5:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 6:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 7: break;
                    case 8:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 9:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    default: limit_value(grid_value); break;
                }
            }
        }

        private void ch_2_2_1_4_1_2_check_questionaire(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView onCheck = ch_2_2_1_4_1_2;
            int rows_index = onCheck.CurrentCell.RowIndex;
            Boolean alert = true;
            for (int col = 0; col < onCheck.ColumnCount; col++)
            {
                int grid_value;
                if (onCheck.Rows[rows_index].Cells[col].Value == null || onCheck.Rows[rows_index].Cells[col].Value == "")
                {
                    grid_value = -99;
                }
                else
                {
                    try
                    {
                        grid_value = Convert.ToInt16(onCheck.Rows[rows_index].Cells[col].Value);
                    }
                    catch
                    {
                        grid_value = -99;
                    }
                }

                onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.White;
                switch (col)
                {
                    case 0:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 1:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 2:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 3:
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 4:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 5:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 6:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 8:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    default: limit_value(grid_value); break;
                }
            }
        }

        private void ch_2_2_1_5_1_check_questionaire(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView onCheck = ch_2_2_1_5_1;
            int rows_index = onCheck.CurrentCell.RowIndex;
            Boolean alert = true;
            for (int col = 0; col < onCheck.ColumnCount; col++)
            {
                int grid_value;
                if (onCheck.Rows[rows_index].Cells[col].Value == null || onCheck.Rows[rows_index].Cells[col].Value == "")
                {
                    grid_value = -99;
                }
                else
                {
                    try
                    {
                        grid_value = Convert.ToInt16(onCheck.Rows[rows_index].Cells[col].Value);
                    }
                    catch
                    {
                        grid_value = -99;
                    }
                }

                onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.White;
                switch (col)
                {
                    case 0:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 1:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 2:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 3:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 4:
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 5:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 6:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 8:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 9:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    default: limit_value(grid_value); break;
                }
            }
        }

        private void ch_2_2_1_5_2_check_questionaire(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView onCheck = ch_2_2_1_5_2;
            int rows_index = onCheck.CurrentCell.RowIndex;
            Boolean alert = true;
            for (int col = 0; col < onCheck.ColumnCount; col++)
            {
                int grid_value;
                if (onCheck.Rows[rows_index].Cells[col].Value == null || onCheck.Rows[rows_index].Cells[col].Value == "")
                {
                    grid_value = -99;
                }
                else
                {
                    try
                    {
                        grid_value = Convert.ToInt16(onCheck.Rows[rows_index].Cells[col].Value);
                    }
                    catch
                    {
                        grid_value = -99;
                    }
                }

                onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.White;
                switch (col)
                {
                    case 0:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 1:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 2:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 3:
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 4:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 5:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 6:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 8:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    default: limit_value(grid_value); break;
                }
            }
        }

        private void ch_2_2_1_6_check_questionaire(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView onCheck = ch_2_2_1_6;
            int rows_index = onCheck.CurrentCell.RowIndex;
            Boolean alert = true;
            for (int col = 0; col < onCheck.ColumnCount; col++)
            {
                int grid_value;
                if (onCheck.Rows[rows_index].Cells[col].Value == null || onCheck.Rows[rows_index].Cells[col].Value == "")
                {
                    grid_value = -99;
                }
                else
                {
                    try
                    {
                        grid_value = Convert.ToInt16(onCheck.Rows[rows_index].Cells[col].Value);
                    }
                    catch
                    {
                        grid_value = -99;
                    }
                }

                onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.White;
                switch (col)
                {
                    case 0:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 1:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 2:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 3:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 4:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 5:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 6:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 7:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        } 
                        break;
                    case 8:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 10:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        } 
                        break;
                    case 11:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        } 
                        break;
                    default: limit_value(grid_value); break;
                }
            }
        }

        private void ch_2_2_2_1_1_check_questionaire(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView onCheck = ch_2_2_2_1_1;
            int rows_index = onCheck.CurrentCell.RowIndex;
            Boolean alert = true;
            for (int col = 0; col < onCheck.ColumnCount; col++)
            {
                int grid_value;
                if (onCheck.Rows[rows_index].Cells[col].Value == null || onCheck.Rows[rows_index].Cells[col].Value == "")
                {
                    grid_value = -99;
                }
                else
                {
                    try
                    {
                        grid_value = Convert.ToInt16(onCheck.Rows[rows_index].Cells[col].Value);
                    }
                    catch
                    {
                        grid_value = -99;
                    }
                }

                onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.White;
                switch (col)
                {
                    case 0:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 1:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 2:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 4:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 5:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    default: limit_value(grid_value); break;
                }
            }
        }

        private void ch_2_2_2_1_2_1_check_questionaire(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView onCheck = ch_2_2_2_1_2_1;
            int rows_index = onCheck.CurrentCell.RowIndex;
            Boolean alert = true;
            for (int col = 0; col < onCheck.ColumnCount; col++)
            {
                int grid_value;
                if (onCheck.Rows[rows_index].Cells[col].Value == null || onCheck.Rows[rows_index].Cells[col].Value == "")
                {
                    grid_value = -99;
                }
                else
                {
                    try
                    {
                        grid_value = Convert.ToInt16(onCheck.Rows[rows_index].Cells[col].Value);
                    }
                    catch
                    {
                        grid_value = -99;
                    }
                }

                onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.White;
                switch (col)
                {
                    case 0:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 1:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 2:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 3:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 4:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    default: limit_value(grid_value); break;
                }
            }
        }

        private void ch_2_2_2_1_2_2_check_questionaire(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView onCheck = ch_2_2_2_1_2_2;
            int rows_index = onCheck.CurrentCell.RowIndex;
            Boolean alert = true;
            for (int col = 0; col < onCheck.ColumnCount; col++)
            {
                int grid_value;
                if (onCheck.Rows[rows_index].Cells[col].Value == null || onCheck.Rows[rows_index].Cells[col].Value == "")
                {
                    grid_value = -99;
                }
                else
                {
                    try
                    {
                        grid_value = Convert.ToInt16(onCheck.Rows[rows_index].Cells[col].Value);
                    }
                    catch
                    {
                        grid_value = -99;
                    }
                }

                onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.White;
                switch (col)
                {
                    case 0:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 1:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 2:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    default: limit_value(grid_value); break;
                }
            }
        }

        private void ch_2_2_2_1_2_3_check_questionaire(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView onCheck = ch_2_2_2_1_2_3;
            int rows_index = onCheck.CurrentCell.RowIndex;
            Boolean alert = true;
            for (int col = 0; col < onCheck.ColumnCount; col++)
            {
                int grid_value;
                if (onCheck.Rows[rows_index].Cells[col].Value == null || onCheck.Rows[rows_index].Cells[col].Value == "")
                {
                    grid_value = -99;
                }
                else
                {
                    try
                    {
                        grid_value = Convert.ToInt16(onCheck.Rows[rows_index].Cells[col].Value);
                    }
                    catch
                    {
                        grid_value = -99;
                    }
                }

                onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.White;
                switch (col)
                {
                    case 0:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 1:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 2:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    default: limit_value(grid_value); break;
                }
            }
        }

        private void ch_2_2_2_2_1_check_questionaire(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView onCheck = ch_2_2_2_2_1;
            int rows_index = onCheck.CurrentCell.RowIndex;
            Boolean alert = true;
            for (int col = 0; col < onCheck.ColumnCount; col++)
            {
                int grid_value;
                if (onCheck.Rows[rows_index].Cells[col].Value == null || onCheck.Rows[rows_index].Cells[col].Value == "")
                {
                    grid_value = -99;
                }
                else
                {
                    try
                    {
                        grid_value = Convert.ToInt16(onCheck.Rows[rows_index].Cells[col].Value);
                    }
                    catch
                    {
                        grid_value = -99;
                    }
                }

                onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.White;
                switch (col)
                {
                    case 0:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 1:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 2:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 3:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    default: limit_value(grid_value); break;
                }
            }
        }

        private void ch_2_2_2_3_1_check_questionaire(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView onCheck = ch_2_2_2_3_1;
            int rows_index = onCheck.CurrentCell.RowIndex;
            Boolean alert = true;
            for (int col = 0; col < onCheck.ColumnCount; col++)
            {
                int grid_value;
                if (onCheck.Rows[rows_index].Cells[col].Value == null || onCheck.Rows[rows_index].Cells[col].Value == "")
                {
                    grid_value = -99;
                }
                else
                {
                    try
                    {
                        grid_value = Convert.ToInt16(onCheck.Rows[rows_index].Cells[col].Value);
                    }
                    catch
                    {
                        grid_value = -99;
                    }
                }

                onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.White;
                switch (col)
                {
                    case 0:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 1:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 2:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 3:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 4:
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    default: limit_value(grid_value); break;
                }
            }
        }

        private void ch_2_2_2_3_2_check_questionaire(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView onCheck = ch_2_2_2_3_2;
            int rows_index = onCheck.CurrentCell.RowIndex;
            Boolean alert = true;
            for (int col = 0; col < onCheck.ColumnCount; col++)
            {
                int grid_value;
                if (onCheck.Rows[rows_index].Cells[col].Value == null || onCheck.Rows[rows_index].Cells[col].Value == "")
                {
                    grid_value = -99;
                }
                else
                {
                    try
                    {
                        grid_value = Convert.ToInt16(onCheck.Rows[rows_index].Cells[col].Value);
                    }
                    catch
                    {
                        grid_value = -99;
                    }
                }

                onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.White;
                switch (col)
                {
                    case 0:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 1:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 2:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    default: limit_value(grid_value); break;
                }
            }
        }

        private void ch_2_2_2_3_3_check_questionaire(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView onCheck = ch_2_2_2_3_3;
            int rows_index = onCheck.CurrentCell.RowIndex;
            Boolean alert = true;
            for (int col = 0; col < onCheck.ColumnCount; col++)
            {
                int grid_value;
                if (onCheck.Rows[rows_index].Cells[col].Value == null || onCheck.Rows[rows_index].Cells[col].Value == "")
                {
                    grid_value = -99;
                }
                else
                {
                    try
                    {
                        grid_value = Convert.ToInt16(onCheck.Rows[rows_index].Cells[col].Value);
                    }
                    catch
                    {
                        grid_value = -99;
                    }
                }

                onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.White;
                switch (col)
                {
                    case 0:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 1:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    default: limit_value(grid_value); break;
                }
            }
        }

        private void ch_2_2_2_3_4_check_questionaire(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView onCheck = ch_2_2_2_3_4;
            int rows_index = onCheck.CurrentCell.RowIndex;
            Boolean alert = true;
            for (int col = 0; col < onCheck.ColumnCount; col++)
            {
                int grid_value;
                if (onCheck.Rows[rows_index].Cells[col].Value == null || onCheck.Rows[rows_index].Cells[col].Value == "")
                {
                    grid_value = -99;
                }
                else
                {
                    try
                    {
                        grid_value = Convert.ToInt16(onCheck.Rows[rows_index].Cells[col].Value);
                    }
                    catch
                    {
                        grid_value = -99;
                    }
                }

                onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.White;
                switch (col)
                {
                    case 0:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 1:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 2:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 3:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    default: limit_value(grid_value); break;
                }
            }
        }

        private void ch_2_2_2_3_5_check_questionaire(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView onCheck = ch_2_2_2_3_5;
            int rows_index = onCheck.CurrentCell.RowIndex;
            Boolean alert = true;
            for (int col = 0; col < onCheck.ColumnCount; col++)
            {
                int grid_value;
                if (onCheck.Rows[rows_index].Cells[col].Value == null || onCheck.Rows[rows_index].Cells[col].Value == "")
                {
                    grid_value = -99;
                }
                else
                {
                    try
                    {
                        grid_value = Convert.ToInt16(onCheck.Rows[rows_index].Cells[col].Value);
                    }
                    catch
                    {
                        grid_value = -99;
                    }
                }

                onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.White;
                switch (col)
                {
                    case 0:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 1:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 2:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 3:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    default: limit_value(grid_value); break;
                }
            }
        }

        private void ch_2_2_2_3_6_1_check_questionaire(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView onCheck = ch_2_2_2_3_6_1;
            int rows_index = onCheck.CurrentCell.RowIndex;
            Boolean alert = true;
            for (int col = 0; col < onCheck.ColumnCount; col++)
            {
                int grid_value;
                if (onCheck.Rows[rows_index].Cells[col].Value == null || onCheck.Rows[rows_index].Cells[col].Value == "")
                {
                    grid_value = -99;
                }
                else
                {
                    try
                    {
                        grid_value = Convert.ToInt16(onCheck.Rows[rows_index].Cells[col].Value);
                    }
                    catch
                    {
                        grid_value = -99;
                    }
                }

                onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.White;
                switch (col)
                {
                    case 0:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 1:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 2:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 3:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    default: limit_value(grid_value); break;
                }
            }
        }

        private void ch_2_2_2_3_6_2_check_questionaire(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView onCheck = ch_2_2_2_3_6_2;
            int rows_index = onCheck.CurrentCell.RowIndex;
            Boolean alert = true;
            for (int col = 0; col < onCheck.ColumnCount; col++)
            {
                int grid_value;
                if (onCheck.Rows[rows_index].Cells[col].Value == null || onCheck.Rows[rows_index].Cells[col].Value == "")
                {
                    grid_value = -99;
                }
                else
                {
                    try
                    {
                        grid_value = Convert.ToInt16(onCheck.Rows[rows_index].Cells[col].Value);
                    }
                    catch
                    {
                        grid_value = -99;
                    }
                }

                onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.White;
                switch (col)
                {
                    case 0:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 1:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 2:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 3:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    default: limit_value(grid_value); break;
                }
            }
        }

        private void ch_2_2_2_3_6_3_check_questionaire(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView onCheck = ch_2_2_2_3_6_3;
            int rows_index = onCheck.CurrentCell.RowIndex;
            Boolean alert = true;
            for (int col = 0; col < onCheck.ColumnCount; col++)
            {
                int grid_value;
                if (onCheck.Rows[rows_index].Cells[col].Value == null || onCheck.Rows[rows_index].Cells[col].Value == "")
                {
                    grid_value = -99;
                }
                else
                {
                    try
                    {
                        grid_value = Convert.ToInt16(onCheck.Rows[rows_index].Cells[col].Value);
                    }
                    catch
                    {
                        grid_value = -99;
                    }
                }

                onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.White;
                switch (col)
                {
                    case 0:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 1:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 2:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 3:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    default: limit_value(grid_value); break;
                }
            }
        }

        private void ch_2_2_2_4_1_1_check_questionaire(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView onCheck = ch_2_2_2_4_1_1;
            int rows_index = onCheck.CurrentCell.RowIndex;
            Boolean alert = true;
            for (int col = 0; col < onCheck.ColumnCount; col++)
            {
                int grid_value;
                if (onCheck.Rows[rows_index].Cells[col].Value == null || onCheck.Rows[rows_index].Cells[col].Value == "")
                {
                    grid_value = -99;
                }
                else
                {
                    try
                    {
                        grid_value = Convert.ToInt16(onCheck.Rows[rows_index].Cells[col].Value);
                    }
                    catch
                    {
                        grid_value = -99;
                    }
                }

                onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.White;
                switch (col)
                {
                    case 0:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 1:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 2:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 3:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 4:
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 5:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 6:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 8:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 9:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    default: limit_value(grid_value); break;
                }
            }
        }

        private void ch_2_2_2_4_1_2_check_questionaire(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView onCheck = ch_2_2_2_4_1_2;
            int rows_index = onCheck.CurrentCell.RowIndex;
            Boolean alert = true;
            for (int col = 0; col < onCheck.ColumnCount; col++)
            {
                int grid_value;
                if (onCheck.Rows[rows_index].Cells[col].Value == null || onCheck.Rows[rows_index].Cells[col].Value == "")
                {
                    grid_value = -99;
                }
                else
                {
                    try
                    {
                        grid_value = Convert.ToInt16(onCheck.Rows[rows_index].Cells[col].Value);
                    }
                    catch
                    {
                        grid_value = -99;
                    }
                }

                onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.White;
                switch (col)
                {
                    case 0:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 1:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 2:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 3:
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 4:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 5:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 6:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 8:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    default: limit_value(grid_value); break;
                }
            }
        }

        private void ch_2_2_2_5_1_check_questionaire(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView onCheck = ch_2_2_2_5_1;
            int rows_index = onCheck.CurrentCell.RowIndex;
            Boolean alert = true;
            for (int col = 0; col < onCheck.ColumnCount; col++)
            {
                int grid_value;
                if (onCheck.Rows[rows_index].Cells[col].Value == null || onCheck.Rows[rows_index].Cells[col].Value == "")
                {
                    grid_value = -99;
                }
                else
                {
                    try
                    {
                        grid_value = Convert.ToInt16(onCheck.Rows[rows_index].Cells[col].Value);
                    }
                    catch
                    {
                        grid_value = -99;
                    }
                }

                onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.White;
                switch (col)
                {
                    case 0:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 1:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 2:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 3:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 4:
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 5:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 6:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 8:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 9:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    default: limit_value(grid_value); break;
                }
            }
        }

        private void ch_2_2_2_5_2_check_questionaire(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView onCheck = ch_2_2_2_5_2;
            int rows_index = onCheck.CurrentCell.RowIndex;
            Boolean alert = true;
            for (int col = 0; col < onCheck.ColumnCount; col++)
            {
                int grid_value;
                if (onCheck.Rows[rows_index].Cells[col].Value == null || onCheck.Rows[rows_index].Cells[col].Value == "")
                {
                    grid_value = -99;
                }
                else
                {
                    try
                    {
                        grid_value = Convert.ToInt16(onCheck.Rows[rows_index].Cells[col].Value);
                    }
                    catch
                    {
                        grid_value = -99;
                    }
                }

                onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.White;
                switch (col)
                {
                    case 0:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 1:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 2:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 3:
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 4:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 5:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 6:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 8:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    default: limit_value(grid_value); break;
                }
            }
        }

        private void ch_2_2_2_6_check_questionaire(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView onCheck = ch_2_2_2_6;
            int rows_index = onCheck.CurrentCell.RowIndex;
            Boolean alert = true;
            for (int col = 0; col < onCheck.ColumnCount; col++)
            {
                int grid_value;
                if (onCheck.Rows[rows_index].Cells[col].Value == null || onCheck.Rows[rows_index].Cells[col].Value == "")
                {
                    grid_value = -99;
                }
                else
                {
                    try
                    {
                        grid_value = Convert.ToInt16(onCheck.Rows[rows_index].Cells[col].Value);
                    }
                    catch
                    {
                        grid_value = -99;
                    }
                }

                onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.White;
                switch (col)
                {
                    case 0:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 1:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 2:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 3:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 4:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 5:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 6:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 7:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 8:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 10:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 11:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    default: limit_value(grid_value); break;
                }
            }
        }

        private void ch_3_1_1_1_1_check_questionaire(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView onCheck = ch_3_1_1_1_1;
            int rows_index = onCheck.CurrentCell.RowIndex;
            Boolean alert = true;
            for (int col = 0; col < onCheck.ColumnCount; col++)
            {
                int grid_value;
                if (onCheck.Rows[rows_index].Cells[col].Value == null || onCheck.Rows[rows_index].Cells[col].Value == "")
                {
                    grid_value = -99;
                }
                else
                {
                    try
                    {
                        grid_value = Convert.ToInt16(onCheck.Rows[rows_index].Cells[col].Value);
                    }
                    catch
                    {
                        grid_value = -99;
                    }
                }

                onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.White;
                switch (col)
                {
                    case 0:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 1:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 2:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 3:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 4:
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 5:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 6:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 8:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 9:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    default: limit_value(grid_value); break;
                }
            }
        }

        private void ch_3_1_1_1_2_check_questionaire(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView onCheck = ch_3_1_1_1_2;
            int rows_index = onCheck.CurrentCell.RowIndex;
            Boolean alert = true;
            for (int col = 0; col < onCheck.ColumnCount; col++)
            {
                int grid_value;
                if (onCheck.Rows[rows_index].Cells[col].Value == null || onCheck.Rows[rows_index].Cells[col].Value == "")
                {
                    grid_value = -99;
                }
                else
                {
                    try
                    {
                        grid_value = Convert.ToInt16(onCheck.Rows[rows_index].Cells[col].Value);
                    }
                    catch
                    {
                        grid_value = -99;
                    }
                }

                onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.White;
                switch (col)
                {
                    case 0:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 1:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 2:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 3:
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 4:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 5:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 6:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 8:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    default: limit_value(grid_value); break;
                }
            }
        }

        private void ch_3_1_1_2_1_check_questionaire(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView onCheck = ch_3_1_1_2_1;
            int rows_index = onCheck.CurrentCell.RowIndex;
            Boolean alert = true;
            for (int col = 0; col < onCheck.ColumnCount; col++)
            {
                int grid_value;
                if (onCheck.Rows[rows_index].Cells[col].Value == null || onCheck.Rows[rows_index].Cells[col].Value == "")
                {
                    grid_value = -99;
                }
                else
                {
                    try
                    {
                        grid_value = Convert.ToInt16(onCheck.Rows[rows_index].Cells[col].Value);
                    }
                    catch
                    {
                        grid_value = -99;
                    }
                }

                onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.White;
                switch (col)
                {
                    case 0:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 1:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 2:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 3:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 4:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        
                        break;
                    case 6:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 7:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    default: limit_value(grid_value); break;
                }
            }
        }

        private void ch_3_1_1_3_1_check_questionaire(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView onCheck = ch_3_1_1_3_1;
            int rows_index = onCheck.CurrentCell.RowIndex;
            Boolean alert = true;
            for (int col = 0; col < onCheck.ColumnCount; col++)
            {
                int grid_value;
                if (onCheck.Rows[rows_index].Cells[col].Value == null || onCheck.Rows[rows_index].Cells[col].Value == "")
                {
                    grid_value = -99;
                }
                else
                {
                    try
                    {
                        grid_value = Convert.ToInt16(onCheck.Rows[rows_index].Cells[col].Value);
                    }
                    catch
                    {
                        grid_value = -99;
                    }
                }

                onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.White;
                switch (col)
                {
                    case 0:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 1:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 2:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 3:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 4:
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 5:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 6:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 8:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 9:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    default: limit_value(grid_value); break;
                }
            }
        }

        private void ch_3_1_1_3_2_check_questionaire(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView onCheck = ch_3_1_1_3_2;
            int rows_index = onCheck.CurrentCell.RowIndex;
            Boolean alert = true;
            for (int col = 0; col < onCheck.ColumnCount; col++)
            {
                int grid_value;
                if (onCheck.Rows[rows_index].Cells[col].Value == null || onCheck.Rows[rows_index].Cells[col].Value == "")
                {
                    grid_value = -99;
                }
                else
                {
                    try
                    {
                        grid_value = Convert.ToInt16(onCheck.Rows[rows_index].Cells[col].Value);
                    }
                    catch
                    {
                        grid_value = -99;
                    }
                }

                onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.White;
                switch (col)
                {
                    case 0:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 1:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 2:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 3:
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 4:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 5:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 6:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 8:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    default: limit_value(grid_value); break;
                }
            }
        }

        private void ch_3_1_1_4_1_check_questionaire(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView onCheck = ch_3_1_1_4_1;
            int rows_index = onCheck.CurrentCell.RowIndex;
            Boolean alert = true;
            for (int col = 0; col < onCheck.ColumnCount; col++)
            {
                int grid_value;
                if (onCheck.Rows[rows_index].Cells[col].Value == null || onCheck.Rows[rows_index].Cells[col].Value == "")
                {
                    grid_value = -99;
                }
                else
                {
                    try
                    {
                        grid_value = Convert.ToInt16(onCheck.Rows[rows_index].Cells[col].Value);
                    }
                    catch
                    {
                        grid_value = -99;
                    }
                }

                onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.White;
                switch (col)
                {
                    case 0:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 1:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 2:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 3:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 4:
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 5:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 6:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 8:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 9:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    default: limit_value(grid_value); break;
                }
            }
        }

        private void ch_3_1_1_4_2_check_questionaire(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView onCheck = ch_3_1_1_4_2;
            int rows_index = onCheck.CurrentCell.RowIndex;
            Boolean alert = true;
            for (int col = 0; col < onCheck.ColumnCount; col++)
            {
                int grid_value;
                if (onCheck.Rows[rows_index].Cells[col].Value == null || onCheck.Rows[rows_index].Cells[col].Value == "")
                {
                    grid_value = -99;
                }
                else
                {
                    try
                    {
                        grid_value = Convert.ToInt16(onCheck.Rows[rows_index].Cells[col].Value);
                    }
                    catch
                    {
                        grid_value = -99;
                    }
                }

                onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.White;
                switch (col)
                {
                    case 0:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 1:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 2:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 3:
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 4:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 5:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 6:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 8:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    default: limit_value(grid_value); break;
                }
            }
        }

        private void ch_3_1_1_5_check_questionaire(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView onCheck = ch_3_1_1_5;
            int rows_index = onCheck.CurrentCell.RowIndex;
            Boolean alert = true;
            for (int col = 0; col < onCheck.ColumnCount; col++)
            {
                int grid_value;
                if (onCheck.Rows[rows_index].Cells[col].Value == null || onCheck.Rows[rows_index].Cells[col].Value == "")
                {
                    grid_value = -99;
                }
                else
                {
                    try
                    {
                        grid_value = Convert.ToInt16(onCheck.Rows[rows_index].Cells[col].Value);
                    }
                    catch
                    {
                        grid_value = -99;
                    }
                }

                onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.White;
                switch (col)
                {
                    case 0:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 1:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 2:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 3:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 4:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 5:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 6:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 7:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 8:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 10:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 11:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    default: limit_value(grid_value); break;
                }
            }
        }

        private void ch_3_1_1_6_1_check_questionaire(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView onCheck = ch_3_1_1_6_1;
            int rows_index = onCheck.CurrentCell.RowIndex;
            Boolean alert = true;
            for (int col = 0; col < onCheck.ColumnCount; col++)
            {
                int grid_value;
                if (onCheck.Rows[rows_index].Cells[col].Value == null || onCheck.Rows[rows_index].Cells[col].Value == "")
                {
                    grid_value = -99;
                }
                else
                {
                    try
                    {
                        grid_value = Convert.ToInt16(onCheck.Rows[rows_index].Cells[col].Value);
                    }
                    catch
                    {
                        grid_value = -99;
                    }
                }

                onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.White;
                switch (col)
                {
                    case 0:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 1:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 2:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 3:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    default: limit_value(grid_value); break;
                }
            }
        }

        private void ch_3_1_1_6_2_check_questionaire(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView onCheck = ch_3_1_1_6_2;
            int rows_index = onCheck.CurrentCell.RowIndex;
            Boolean alert = true;
            for (int col = 0; col < onCheck.ColumnCount; col++)
            {
                int grid_value;
                if (onCheck.Rows[rows_index].Cells[col].Value == null || onCheck.Rows[rows_index].Cells[col].Value == "")
                {
                    grid_value = -99;
                }
                else
                {
                    try
                    {
                        grid_value = Convert.ToInt16(onCheck.Rows[rows_index].Cells[col].Value);
                    }
                    catch
                    {
                        grid_value = -99;
                    }
                }

                onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.White;
                switch (col)
                {
                    case 0:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 1:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 2:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 3:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    default: limit_value(grid_value); break;
                }
            }
        }

        private void ch_3_1_1_6_3_check_questionaire(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView onCheck = ch_3_1_1_6_3;
            int rows_index = onCheck.CurrentCell.RowIndex;
            Boolean alert = true;
            for (int col = 0; col < onCheck.ColumnCount; col++)
            {
                int grid_value;
                if (onCheck.Rows[rows_index].Cells[col].Value == null || onCheck.Rows[rows_index].Cells[col].Value == "")
                {
                    grid_value = -99;
                }
                else
                {
                    try
                    {
                        grid_value = Convert.ToInt16(onCheck.Rows[rows_index].Cells[col].Value);
                    }
                    catch
                    {
                        grid_value = -99;
                    }
                }

                onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.White;
                switch (col)
                {
                    case 0:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 1:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 2:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 3:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    default: limit_value(grid_value); break;
                }
            }
        }

        private void ch_3_1_2_1_1_check_questionaire(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView onCheck = ch_3_1_2_1_1;
            int rows_index = onCheck.CurrentCell.RowIndex;
            Boolean alert = true;
            for (int col = 0; col < onCheck.ColumnCount; col++)
            {
                int grid_value;
                if (onCheck.Rows[rows_index].Cells[col].Value == null || onCheck.Rows[rows_index].Cells[col].Value == "")
                {
                    grid_value = -99;
                }
                else
                {
                    try
                    {
                        grid_value = Convert.ToInt16(onCheck.Rows[rows_index].Cells[col].Value);
                    }
                    catch
                    {
                        grid_value = -99;
                    }
                }

                onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.White;
                switch (col)
                {
                    case 0:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 1:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 2:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 3:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 4:
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 5:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 6:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 8:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 9:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    default: limit_value(grid_value); break;
                }
            }
        }

        private void ch_3_1_2_1_2_check_questionaire(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView onCheck = ch_3_1_2_1_2;
            int rows_index = onCheck.CurrentCell.RowIndex;
            Boolean alert = true;
            for (int col = 0; col < onCheck.ColumnCount; col++)
            {
                int grid_value;
                if (onCheck.Rows[rows_index].Cells[col].Value == null || onCheck.Rows[rows_index].Cells[col].Value == "")
                {
                    grid_value = -99;
                }
                else
                {
                    try
                    {
                        grid_value = Convert.ToInt16(onCheck.Rows[rows_index].Cells[col].Value);
                    }
                    catch
                    {
                        grid_value = -99;
                    }
                }

                onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.White;
                switch (col)
                {
                    case 0:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 1:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 2:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 3:
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 4:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 5:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 6:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 8:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    default: limit_value(grid_value); break;
                }
            }
        }

        private void ch_3_1_2_2_1_check_questionaire(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView onCheck = ch_3_1_2_2_1;
            int rows_index = onCheck.CurrentCell.RowIndex;
            Boolean alert = true;
            for (int col = 0; col < onCheck.ColumnCount; col++)
            {
                int grid_value;
                if (onCheck.Rows[rows_index].Cells[col].Value == null || onCheck.Rows[rows_index].Cells[col].Value == "")
                {
                    grid_value = -99;
                }
                else
                {
                    try
                    {
                        grid_value = Convert.ToInt16(onCheck.Rows[rows_index].Cells[col].Value);
                    }
                    catch
                    {
                        grid_value = -99;
                    }
                }

                onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.White;
                switch (col)
                {
                    case 0:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 1:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 2:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 3:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 4:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 6:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 7:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    default: limit_value(grid_value); break;
                }
            }
        }

        private void ch_3_1_2_2_2_1_check_questionaire(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView onCheck = ch_3_1_2_2_2_1;
            int rows_index = onCheck.CurrentCell.RowIndex;
            Boolean alert = true;
            for (int col = 0; col < onCheck.ColumnCount; col++)
            {
                int grid_value;
                if (onCheck.Rows[rows_index].Cells[col].Value == null || onCheck.Rows[rows_index].Cells[col].Value == "")
                {
                    grid_value = -99;
                }
                else
                {
                    try
                    {
                        grid_value = Convert.ToInt16(onCheck.Rows[rows_index].Cells[col].Value);
                    }
                    catch
                    {
                        grid_value = -99;
                    }
                }

                onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.White;
                switch (col)
                {
                    case 0:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 1:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 2:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 3:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    default: limit_value(grid_value); break;
                }
            }
        }

        private void ch_3_1_2_2_2_2_check_questionaire(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView onCheck = ch_3_1_2_2_2_2;
            int rows_index = onCheck.CurrentCell.RowIndex;
            Boolean alert = true;
            for (int col = 0; col < onCheck.ColumnCount; col++)
            {
                int grid_value;
                if (onCheck.Rows[rows_index].Cells[col].Value == null || onCheck.Rows[rows_index].Cells[col].Value == "")
                {
                    grid_value = -99;
                }
                else
                {
                    try
                    {
                        grid_value = Convert.ToInt16(onCheck.Rows[rows_index].Cells[col].Value);
                    }
                    catch
                    {
                        grid_value = -99;
                    }
                }

                onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.White;
                switch (col)
                {
                    case 0:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 1:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 2:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 3:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    default: limit_value(grid_value); break;
                }
            }
        }

        private void ch_3_1_2_2_2_3_check_questionaire(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView onCheck = ch_3_1_2_2_2_3;
            int rows_index = onCheck.CurrentCell.RowIndex;
            Boolean alert = true;
            for (int col = 0; col < onCheck.ColumnCount; col++)
            {
                int grid_value;
                if (onCheck.Rows[rows_index].Cells[col].Value == null || onCheck.Rows[rows_index].Cells[col].Value == "")
                {
                    grid_value = -99;
                }
                else
                {
                    try
                    {
                        grid_value = Convert.ToInt16(onCheck.Rows[rows_index].Cells[col].Value);
                    }
                    catch
                    {
                        grid_value = -99;
                    }
                }

                onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.White;
                switch (col)
                {
                    case 0:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 1:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 2:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 3:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    default: limit_value(grid_value); break;
                }
            }
        }

        private void ch_3_1_2_2_3_check_questionaire(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView onCheck = ch_3_1_2_2_3;
            int rows_index = onCheck.CurrentCell.RowIndex;
            Boolean alert = true;
            for (int col = 0; col < onCheck.ColumnCount; col++)
            {
                int grid_value;
                if (onCheck.Rows[rows_index].Cells[col].Value == null || onCheck.Rows[rows_index].Cells[col].Value == "")
                {
                    grid_value = -99;
                }
                else
                {
                    try
                    {
                        grid_value = Convert.ToInt16(onCheck.Rows[rows_index].Cells[col].Value);
                    }
                    catch
                    {
                        grid_value = -99;
                    }
                }

                onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.White;
                switch (col)
                {
                    case 0:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 1:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 2:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 3:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 4:
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 5:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 6:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 8:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 9:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    default: limit_value(grid_value); break;
                }
            }
        }

        private void ch_3_1_2_2_4_check_questionaire(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView onCheck = ch_3_1_2_2_4;
            int rows_index = onCheck.CurrentCell.RowIndex;
            Boolean alert = true;
            for (int col = 0; col < onCheck.ColumnCount; col++)
            {
                int grid_value;
                if (onCheck.Rows[rows_index].Cells[col].Value == null || onCheck.Rows[rows_index].Cells[col].Value == "")
                {
                    grid_value = -99;
                }
                else
                {
                    try
                    {
                        grid_value = Convert.ToInt16(onCheck.Rows[rows_index].Cells[col].Value);
                    }
                    catch
                    {
                        grid_value = -99;
                    }
                }

                onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.White;
                switch (col)
                {
                    case 0:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 1:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 2:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 3:
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 4:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 5:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 6:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 8:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    default: limit_value(grid_value); break;
                }
            }
        }

        private void ch_3_1_2_2_5_check_questionaire(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView onCheck = ch_3_1_2_2_5;
            int rows_index = onCheck.CurrentCell.RowIndex;
            Boolean alert = true;
            for (int col = 0; col < onCheck.ColumnCount; col++)
            {
                int grid_value;
                if (onCheck.Rows[rows_index].Cells[col].Value == null || onCheck.Rows[rows_index].Cells[col].Value == "")
                {
                    grid_value = -99;
                }
                else
                {
                    try
                    {
                        grid_value = Convert.ToInt16(onCheck.Rows[rows_index].Cells[col].Value);
                    }
                    catch
                    {
                        grid_value = -99;
                    }
                }

                onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.White;
                switch (col)
                {
                    case 0:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 1:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 2:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 3:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    default: limit_value(grid_value); break;
                }
            }
        }

        private void ch_3_1_2_2_6_check_questionaire(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView onCheck = ch_3_1_2_2_6;
            int rows_index = onCheck.CurrentCell.RowIndex;
            Boolean alert = true;
            for (int col = 0; col < onCheck.ColumnCount; col++)
            {
                int grid_value;
                if (onCheck.Rows[rows_index].Cells[col].Value == null || onCheck.Rows[rows_index].Cells[col].Value == "")
                {
                    grid_value = -99;
                }
                else
                {
                    try
                    {
                        grid_value = Convert.ToInt16(onCheck.Rows[rows_index].Cells[col].Value);
                    }
                    catch
                    {
                        grid_value = -99;
                    }
                }

                onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.White;
                switch (col)
                {
                    case 0:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 1:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 2:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    default: limit_value(grid_value); break;
                }
            }
        }

        private void ch_3_1_2_2_7_check_questionaire(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView onCheck = ch_3_1_2_2_7;
            int rows_index = onCheck.CurrentCell.RowIndex;
            Boolean alert = true;
            for (int col = 0; col < onCheck.ColumnCount; col++)
            {
                int grid_value;
                if (onCheck.Rows[rows_index].Cells[col].Value == null || onCheck.Rows[rows_index].Cells[col].Value == "")
                {
                    grid_value = -99;
                }
                else
                {
                    try
                    {
                        grid_value = Convert.ToInt16(onCheck.Rows[rows_index].Cells[col].Value);
                    }
                    catch
                    {
                        grid_value = -99;
                    }
                }

                onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.White;
                switch (col)
                {
                    case 0:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 1:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    default: limit_value(grid_value); break;
                }
            }
        }

        private void ch_3_1_2_3_1_check_questionaire(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView onCheck = ch_3_1_2_3_1;
            int rows_index = onCheck.CurrentCell.RowIndex;
            Boolean alert = true;
            for (int col = 0; col < onCheck.ColumnCount; col++)
            {
                int grid_value;
                if (onCheck.Rows[rows_index].Cells[col].Value == null || onCheck.Rows[rows_index].Cells[col].Value == "")
                {
                    grid_value = -99;
                }
                else
                {
                    try
                    {
                        grid_value = Convert.ToInt16(onCheck.Rows[rows_index].Cells[col].Value);
                    }
                    catch
                    {
                        grid_value = -99;
                    }
                }

                onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.White;
                switch (col)
                {
                    case 0:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 1:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 2:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 3:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 4:
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 5:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 6:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 8:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 9:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    default: limit_value(grid_value); break;
                }
            }
        }

        private void ch_3_1_2_3_2_check_questionaire(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView onCheck = ch_3_1_2_3_2;
            int rows_index = onCheck.CurrentCell.RowIndex;
            Boolean alert = true;
            for (int col = 0; col < onCheck.ColumnCount; col++)
            {
                int grid_value;
                if (onCheck.Rows[rows_index].Cells[col].Value == null || onCheck.Rows[rows_index].Cells[col].Value == "")
                {
                    grid_value = -99;
                }
                else
                {
                    try
                    {
                        grid_value = Convert.ToInt16(onCheck.Rows[rows_index].Cells[col].Value);
                    }
                    catch
                    {
                        grid_value = -99;
                    }
                }

                onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.White;
                switch (col)
                {
                    case 0:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 1:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 2:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 3:
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 4:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 5:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 6:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 8:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    default: limit_value(grid_value); break;
                }
            }
        }

        private void ch_3_1_2_4_1_check_questionaire(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView onCheck = ch_3_1_2_4_1;
            int rows_index = onCheck.CurrentCell.RowIndex;
            Boolean alert = true;
            for (int col = 0; col < onCheck.ColumnCount; col++)
            {
                int grid_value;
                if (onCheck.Rows[rows_index].Cells[col].Value == null || onCheck.Rows[rows_index].Cells[col].Value == "")
                {
                    grid_value = -99;
                }
                else
                {
                    try
                    {
                        grid_value = Convert.ToInt16(onCheck.Rows[rows_index].Cells[col].Value);
                    }
                    catch
                    {
                        grid_value = -99;
                    }
                }

                onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.White;
                switch (col)
                {
                    case 0:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 1:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 2:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 3:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 4:
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 5:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 6:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 8:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 9:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    default: limit_value(grid_value); break;
                }
            }
        }

        private void ch_3_1_2_4_2_check_questionaire(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView onCheck = ch_3_1_2_4_2;
            int rows_index = onCheck.CurrentCell.RowIndex;
            Boolean alert = true;
            for (int col = 0; col < onCheck.ColumnCount; col++)
            {
                int grid_value;
                if (onCheck.Rows[rows_index].Cells[col].Value == null || onCheck.Rows[rows_index].Cells[col].Value == "")
                {
                    grid_value = -99;
                }
                else
                {
                    try
                    {
                        grid_value = Convert.ToInt16(onCheck.Rows[rows_index].Cells[col].Value);
                    }
                    catch
                    {
                        grid_value = -99;
                    }
                }

                onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.White;
                switch (col)
                {
                    case 0:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 1:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 2:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 3:
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 4:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 5:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 6:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 8:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    default: limit_value(grid_value); break;
                }
            }
        }

        private void ch_3_1_2_5_check_questionaire(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView onCheck = ch_3_1_2_5;
            int rows_index = onCheck.CurrentCell.RowIndex;
            Boolean alert = true;
            for (int col = 0; col < onCheck.ColumnCount; col++)
            {
                int grid_value;
                if (onCheck.Rows[rows_index].Cells[col].Value == null || onCheck.Rows[rows_index].Cells[col].Value == "")
                {
                    grid_value = -99;
                }
                else
                {
                    try
                    {
                        grid_value = Convert.ToInt16(onCheck.Rows[rows_index].Cells[col].Value);
                    }
                    catch
                    {
                        grid_value = -99;
                    }
                }

                onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.White;
                switch (col)
                {
                    case 0:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 1:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 2:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 3:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 4:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 5:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 6:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 7:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 8:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 10:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 11:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    default: limit_value(grid_value); break;
                }
            }
        }

        private void ch_3_1_2_6_1_check_questionaire(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView onCheck = ch_3_1_2_6_1;
            int rows_index = onCheck.CurrentCell.RowIndex;
            Boolean alert = true;
            for (int col = 0; col < onCheck.ColumnCount; col++)
            {
                int grid_value;
                if (onCheck.Rows[rows_index].Cells[col].Value == null || onCheck.Rows[rows_index].Cells[col].Value == "")
                {
                    grid_value = -99;
                }
                else
                {
                    try
                    {
                        grid_value = Convert.ToInt16(onCheck.Rows[rows_index].Cells[col].Value);
                    }
                    catch
                    {
                        grid_value = -99;
                    }
                }

                onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.White;
                switch (col)
                {
                    case 0:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 1:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 2:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 3:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    default: limit_value(grid_value); break;
                }
            }
        }

        private void ch_3_1_2_6_2_check_questionaire(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView onCheck = ch_3_1_2_6_2;
            int rows_index = onCheck.CurrentCell.RowIndex;
            Boolean alert = true;
            for (int col = 0; col < onCheck.ColumnCount; col++)
            {
                int grid_value;
                if (onCheck.Rows[rows_index].Cells[col].Value == null || onCheck.Rows[rows_index].Cells[col].Value == "")
                {
                    grid_value = -99;
                }
                else
                {
                    try
                    {
                        grid_value = Convert.ToInt16(onCheck.Rows[rows_index].Cells[col].Value);
                    }
                    catch
                    {
                        grid_value = -99;
                    }
                }

                onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.White;
                switch (col)
                {
                    case 0:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 1:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 2:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 3:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    default: limit_value(grid_value); break;
                }
            }
        }

        private void ch_3_1_2_6_3_check_questionaire(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView onCheck = ch_3_1_2_6_3;
            int rows_index = onCheck.CurrentCell.RowIndex;
            Boolean alert = true;
            for (int col = 0; col < onCheck.ColumnCount; col++)
            {
                int grid_value;
                if (onCheck.Rows[rows_index].Cells[col].Value == null || onCheck.Rows[rows_index].Cells[col].Value == "")
                {
                    grid_value = -99;
                }
                else
                {
                    try
                    {
                        grid_value = Convert.ToInt16(onCheck.Rows[rows_index].Cells[col].Value);
                    }
                    catch
                    {
                        grid_value = -99;
                    }
                }

                onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.White;
                switch (col)
                {
                    case 0:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 1:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 2:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 3:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    default: limit_value(grid_value); break;
                }
            }
        }

        private void ch_3_1_3_1_check_questionaire(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView onCheck = ch_3_1_3_1;
            int rows_index = onCheck.CurrentCell.RowIndex;
            Boolean alert = true;
            for (int col = 0; col < onCheck.ColumnCount; col++)
            {
                int grid_value;
                if (onCheck.Rows[rows_index].Cells[col].Value == null || onCheck.Rows[rows_index].Cells[col].Value == "")
                {
                    grid_value = -99;
                }
                else
                {
                    try
                    {
                        grid_value = Convert.ToInt16(onCheck.Rows[rows_index].Cells[col].Value);
                    }
                    catch
                    {
                        grid_value = -99;
                    }
                }

                onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.White;
                switch (col)
                {
                    case 0:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 1:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 2:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 3:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 4:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    default: limit_value(grid_value); break;
                }
            }
        }

        private void ch_3_1_3_2_1_check_questionaire(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView onCheck = ch_3_1_3_2_1;
            int rows_index = onCheck.CurrentCell.RowIndex;
            Boolean alert = true;
            for (int col = 0; col < onCheck.ColumnCount; col++)
            {
                int grid_value;
                if (onCheck.Rows[rows_index].Cells[col].Value == null || onCheck.Rows[rows_index].Cells[col].Value == "")
                {
                    grid_value = -99;
                }
                else
                {
                    try
                    {
                        grid_value = Convert.ToInt16(onCheck.Rows[rows_index].Cells[col].Value);
                    }
                    catch
                    {
                        grid_value = -99;
                    }
                }

                onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.White;
                switch (col)
                {
                    case 0:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 1:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 2:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 3:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    default: limit_value(grid_value); break;
                }
            }
        }

        private void ch_3_1_3_3_1_check_questionaire(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView onCheck = ch_3_1_3_3_1;
            int rows_index = onCheck.CurrentCell.RowIndex;
            Boolean alert = true;
            for (int col = 0; col < onCheck.ColumnCount; col++)
            {
                int grid_value;
                if (onCheck.Rows[rows_index].Cells[col].Value == null || onCheck.Rows[rows_index].Cells[col].Value == "")
                {
                    grid_value = -99;
                }
                else
                {
                    try
                    {
                        grid_value = Convert.ToInt16(onCheck.Rows[rows_index].Cells[col].Value);
                    }
                    catch
                    {
                        grid_value = -99;
                    }
                }

                onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.White;
                switch (col)
                {
                    case 0:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 1:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 2:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 3:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 4:
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 5:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 6:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 8:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 9:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    default: limit_value(grid_value); break;
                }
            }
        }

        private void ch_3_1_3_3_2_check_questionaire(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView onCheck = ch_3_1_3_3_2;
            int rows_index = onCheck.CurrentCell.RowIndex;
            Boolean alert = true;
            for (int col = 0; col < onCheck.ColumnCount; col++)
            {
                int grid_value;
                if (onCheck.Rows[rows_index].Cells[col].Value == null || onCheck.Rows[rows_index].Cells[col].Value == "")
                {
                    grid_value = -99;
                }
                else
                {
                    try
                    {
                        grid_value = Convert.ToInt16(onCheck.Rows[rows_index].Cells[col].Value);
                    }
                    catch
                    {
                        grid_value = -99;
                    }
                }

                onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.White;
                switch (col)
                {
                    case 0:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 1:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 2:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 3:
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 4:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 5:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 6:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 8:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    default: limit_value(grid_value); break;
                }
            }
        }

        private void ch_3_1_3_4_1_check_questionaire(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView onCheck = ch_3_1_3_4_1;
            int rows_index = onCheck.CurrentCell.RowIndex;
            Boolean alert = true;
            for (int col = 0; col < onCheck.ColumnCount; col++)
            {
                int grid_value;
                if (onCheck.Rows[rows_index].Cells[col].Value == null || onCheck.Rows[rows_index].Cells[col].Value == "")
                {
                    grid_value = -99;
                }
                else
                {
                    try
                    {
                        grid_value = Convert.ToInt16(onCheck.Rows[rows_index].Cells[col].Value);
                    }
                    catch
                    {
                        grid_value = -99;
                    }
                }

                onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.White;
                switch (col)
                {
                    case 0:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 1:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 2:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 3:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 4:
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 5:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 6:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 8:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 9:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    default: limit_value(grid_value); break;
                }
            }
        }

        private void ch_3_1_3_4_2_check_questionaire(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView onCheck = ch_3_1_3_4_2;
            int rows_index = onCheck.CurrentCell.RowIndex;
            Boolean alert = true;
            for (int col = 0; col < onCheck.ColumnCount; col++)
            {
                int grid_value;
                if (onCheck.Rows[rows_index].Cells[col].Value == null || onCheck.Rows[rows_index].Cells[col].Value == "")
                {
                    grid_value = -99;
                }
                else
                {
                    try
                    {
                        grid_value = Convert.ToInt16(onCheck.Rows[rows_index].Cells[col].Value);
                    }
                    catch
                    {
                        grid_value = -99;
                    }
                }

                onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.White;
                switch (col)
                {
                    case 0:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 1:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 2:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 3:
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 4:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 5:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 6:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 8:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    default: limit_value(grid_value); break;
                }
            }
        }

        private void ch_3_1_3_5_check_questionaire(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView onCheck = ch_3_1_3_5;
            int rows_index = onCheck.CurrentCell.RowIndex;
            Boolean alert = true;
            for (int col = 0; col < onCheck.ColumnCount; col++)
            {
                int grid_value;
                if (onCheck.Rows[rows_index].Cells[col].Value == null || onCheck.Rows[rows_index].Cells[col].Value == "")
                {
                    grid_value = -99;
                }
                else
                {
                    try
                    {
                        grid_value = Convert.ToInt16(onCheck.Rows[rows_index].Cells[col].Value);
                    }
                    catch
                    {
                        grid_value = -99;
                    }
                }

                onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.White;
                switch (col)
                {
                    case 0:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 1:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 2:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 3:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 4:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 5:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 6:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 7:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 8:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 10:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 11:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    default: limit_value(grid_value); break;
                }
            }
        }

        private void ch_3_2_1_check_questionaire(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView onCheck = ch_3_2_1;
            int rows_index = onCheck.CurrentCell.RowIndex;
            Boolean alert = true;
            for (int col = 0; col < onCheck.ColumnCount; col++)
            {
                int grid_value;
                if (onCheck.Rows[rows_index].Cells[col].Value == null || onCheck.Rows[rows_index].Cells[col].Value == "")
                {
                    grid_value = -99;
                }
                else
                {
                    try
                    {
                        grid_value = Convert.ToInt16(onCheck.Rows[rows_index].Cells[col].Value);
                    }
                    catch
                    {
                        grid_value = -99;
                    }
                }

                onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.White;
                switch (col)
                {
                    case 0:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 1:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 3:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 4:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    default: limit_value(grid_value); break;
                }
            }
        }

        private void ch_3_2_2_check_questionaire(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView onCheck = ch_3_2_2;
            int rows_index = onCheck.CurrentCell.RowIndex;
            Boolean alert = true;
            for (int col = 0; col < onCheck.ColumnCount; col++)
            {
                int grid_value;
                if (onCheck.Rows[rows_index].Cells[col].Value == null || onCheck.Rows[rows_index].Cells[col].Value == "")
                {
                    grid_value = -99;
                }
                else
                {
                    try
                    {
                        grid_value = Convert.ToInt16(onCheck.Rows[rows_index].Cells[col].Value);
                    }
                    catch
                    {
                        grid_value = -99;
                    }
                }

                onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.White;
                switch (col)
                {
                    case 0:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 1:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 2:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 4:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 5:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    default: limit_value(grid_value); break;
                }
            }
        }

        private void ch_3_2_3_check_questionaire(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView onCheck = ch_3_2_3;
            int rows_index = onCheck.CurrentCell.RowIndex;
            Boolean alert = true;
            for (int col = 0; col < onCheck.ColumnCount; col++)
            {
                int grid_value;
                if (onCheck.Rows[rows_index].Cells[col].Value == null || onCheck.Rows[rows_index].Cells[col].Value == "")
                {
                    grid_value = -99;
                }
                else
                {
                    try
                    {
                        grid_value = Convert.ToInt16(onCheck.Rows[rows_index].Cells[col].Value);
                    }
                    catch
                    {
                        grid_value = -99;
                    }
                }

                onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.White;
                switch (col)
                {
                    case 0:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 1:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 3:
                        if (!checklimit_quationaire(grid_value, 1))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 3))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    case 4:
                        if (!checklimit_quationaire(grid_value, 2))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        if (!checklimit_quationaire(grid_value, 4))
                        {
                            onCheck.Rows[rows_index].Cells[col].Style.BackColor = System.Drawing.Color.Red;
                            if (alert == true)
                            {
                                alert_msg();
                            }
                            alert = false;
                        }
                        break;
                    default: limit_value(grid_value); break;
                }
            }
        }

        private void temp_checklist_view(int checklist_id)
        {
            switch (checklist_id)
            {
                case 5: break;
                case 6: break;
                case 7: break;
                case 8: break;
                case 9: break;
                case 4: break;
                case 12: break;
                case 14: break;
                case 15: break;
                case 16: break;
                case 18: break;
                case 23: break;
                case 24: break;
                case 25: break;
                case 26: break;
                case 27: break;
                case 29: break;
                case 30: break;
                case 31: break;
                case 33: break;
                case 34: break;
                case 35: break;
                case 36: break;
                case 22: break;
                case 46: break;
                case 48: break;
                case 49: break;
                case 50: break;
                case 51: break;
                case 52: break;
                case 53: break;
                case 54: break;
                case 55: break;
                case 56: break;
                case 58: break;
                case 59: break;
                case 60: break;
                case 62: break;
                case 63: break;
                case 64: break;
                case 65: break;
                case 45: break;
                case 72: break;
                case 74: break;
                case 75: break;
                case 76: break;
                case 77: break;
                case 78: break;
                case 79: break;
                case 80: break;
                case 81: break;
                case 82: break;
                case 150: break;
                case 151: break;
                case 152: break;
                case 85: break;
                case 86: break;
                case 87: break;
                case 88: break;
                case 71: break;
                case 101: break;
                case 102: break;
                case 103: break;
                case 104: break;
                case 105: break;
                case 106: break;
                case 107: break;
                case 99: break;
                case 108: break;
                case 109: break;
                case 110: break;
                case 117: break;
                case 118: break;
                case 119: break;
                case 126: break;
                case 127: break;
                case 128: break;
                case 121: break;
                case 122: break;
                case 123: break;
                case 124: break;
                case 125: break;
                case 129: break;
                case 130: break;
                case 131: break;
                case 132: break;
                case 115: break;
                case 133: break;
                case 134: break;
                case 135: break;
                case 136: break;
                case 141: break;
                case 142: break;
                case 143: break;
                case 144: break;
                case 145: break;
                case 140: break;
                case 146: break;
                case 147: break;
                case 148: break;
            }
        }

        private String check_sheet(int index)
        {
            String sheet = String.Empty;
            switch (index)
            {
                case 5:
                    sheet = "sheet_1";
                    break;
                case 6:
                    sheet = "sheet_2";
                    break;
                case 7:
                    sheet = "sheet_3";
                    break;
                case 8:
                case 9:
                case 4:
                    sheet = "sheet_4";
                    break;
                case 12:
                case 14:
                case 15:
                case 16:
                case 18:
                case 23:
                case 24:
                    sheet = "sheet_5";
                    break;
                case 25:
                case 26:
                case 27:
                case 29:
                case 30:
                case 31:
                case 33:
                    sheet = "sheet_6";
                    break;
                case 34:
                case 35:
                case 36:
                case 22:
                    sheet = "sheet_7";
                    break;
                case 46:
                case 48:
                case 49:
                case 50:
                case 51:
                case 52:
                case 53:
                    sheet = "sheet_8";
                    break;
                case 54:
                case 55:
                case 56:
                case 58:
                case 59:
                case 60:
                case 62:
                    sheet = "sheet_9";
                    break;
                case 63:
                case 64:
                case 65:
                case 45:
                    sheet = "sheet_10";
                    break;
                case 72:
                case 74:
                case 75:
                case 76:
                case 77:
                case 78:
                case 79:
                    sheet = "sheet_11";
                    break;
                case 80:
                case 81:
                case 82:
                case 150:
                case 151:
                case 152:
                case 85:
                    sheet = "sheet_12";
                    break;
                case 86:
                case 87:
                case 88:
                case 71:
                    sheet = "sheet_13";
                    break;
                case 101:
                case 102:
                case 103:
                    sheet = "sheet_14";
                    break;
                case 104:
                case 105:
                case 106:
                case 107:
                    sheet = "sheet_15";
                    break;
                case 99:
                case 108:
                case 109:
                case 110:
                    sheet = "sheet_16";
                    break;
                case 117:
                case 118:
                case 119:
                case 126:
                    sheet = "sheet_17";
                    break;
                case 127:
                case 128:
                case 121:
                case 122:
                    sheet = "sheet_18";
                    break;
                case 123:
                case 124:
                case 125:
                case 129:
                    sheet = "sheet_19";
                    break;
                case 130:
                case 131:
                case 132:
                    sheet = "sheet_20";
                    break;
                case 115:
                case 133:
                case 134:
                case 135:
                    sheet = "sheet_21";
                    break;
                case 136:
                case 141:
                case 142:
                case 143:
                    sheet = "sheet_22";
                    break;
                case 144:
                case 145:
                case 140:
                    sheet = "sheet_23";
                    break;
                case 146:
                case 147:
                case 148:
                    sheet = "sheet_24";
                    break;
                default:
                    sheet = String.Empty;
                    break;
            }
            return sheet;
        }

        private void clear_old_data()
        {
            String sql = String.Empty;

            for (int sheet = 1; sheet <= 24; sheet++)
            {
                sql = "delete from sheet_" + sheet + " where checklist_id = '" + checklist_id + "'";
                mainForm.getDB(sql);
            }
        }

        private void mark_as_use(double score)
        {
            //Update fill checklist Info
            String sql = "UPDATE fill_checklist SET score = '" + score.ToString() + "', project = '" + Project.Text + "', headproject = '" + HeadProject.Text + "', projectresponse = '" + ProjectResponse.Text + "', tel = '" + Tel.Text + "', fax = '" + Fax.Text + "', email = '" + Email.Text + "', checkatday = '" + checkatdate.Value.Day + "', checkatmonth = '" + checkatdate.Value.Month + "', checkatyear = '" + checkatdate.Value.Year + "', weatheratdate = '" + weatheratdate.Text + "', tempatdate = '" + tempatdate.Text + "' WHERE dam_auto_id = '" + dam_id + "' and checklist_id = '" + checklist_id + "'";
            mainForm.getDB(sql);
            //Delete Old Memeber Record
            sql = "delete from member where [checklist_id] = '" + checklist_id + "'";
            mainForm.getDB(sql);

            //Add New Member Record
            int limit = OperationMember.RowCount;
            for (int i = 0; i < limit; i++)
            {
                if (OperationMember.Rows[i].Cells[0].Value != null && OperationMember.Rows[i].Cells[0].Value != "")
                {
                    sql = "insert into member (member_name, member_position, regist, checklist_id) values('" + OperationMember.Rows[i].Cells[0].Value + "', '" + OperationMember.Rows[i].Cells[1].Value + "', '" + OperationMember.Rows[i].Cells[2].Value + "', '" + checklist_id + "')";
                    mainForm.insertDB(sql);
                }
            }
        }

        private void insert_questionaire_single(DataGridView collect, int property_id, int total_col)
        {
            //fix : remove มองไม่เห็น
            total_col--;
            String sql = String.Empty;
            String sheet = check_sheet(property_id);
            int all_table = property_id, i = 0;
            switch (total_col)
            {
                case 3:
                    sql = "insert into " + sheet + " (checklist_id, element_id, c1, c2, c3) values ('" + checklist_id + "', '" + all_table.ToString() + "', '" + collect.Rows[i].Cells[0].Value + "', '" + collect.Rows[i].Cells[1].Value + "', '" + collect.Rows[i].Cells[2].Value + "')";
                    break;
                case 4:
                    sql = "insert into " + sheet + " (checklist_id, element_id, c1, c2, c3, c4) values ('" + checklist_id + "', '" + all_table.ToString() + "', '" + collect.Rows[i].Cells[0].Value + "', '" + collect.Rows[i].Cells[1].Value + "', '" + collect.Rows[i].Cells[2].Value + "', '" + collect.Rows[i].Cells[3].Value + "')";
                    break;
                case 5:
                    sql = "insert into " + sheet + " (checklist_id, element_id, c1, c2, c3, c4, c5) values ('" + checklist_id + "', '" + all_table.ToString() + "', '" + collect.Rows[i].Cells[0].Value + "', '" + collect.Rows[i].Cells[1].Value + "', '" + collect.Rows[i].Cells[2].Value + "', '" + collect.Rows[i].Cells[3].Value + "', '" + collect.Rows[i].Cells[4].Value + "')";
                    break;
                case 6:
                    sql = "insert into " + sheet + " (checklist_id, element_id, c1, c2, c3, c4, c5, c6) values ('" + checklist_id + "', '" + all_table.ToString() + "', '" + collect.Rows[i].Cells[0].Value + "', '" + collect.Rows[i].Cells[1].Value + "', '" + collect.Rows[i].Cells[2].Value + "', '" + collect.Rows[i].Cells[3].Value + "', '" + collect.Rows[i].Cells[4].Value + "', '" + collect.Rows[i].Cells[5].Value + "')";
                    break;
                case 7:
                    sql = "insert into " + sheet + " (checklist_id, element_id, c1, c2, c3, c4, c5, c6, c7) values ('" + checklist_id + "', '" + all_table.ToString() + "', '" + collect.Rows[i].Cells[0].Value + "', '" + collect.Rows[i].Cells[1].Value + "', '" + collect.Rows[i].Cells[2].Value + "', '" + collect.Rows[i].Cells[3].Value + "', '" + collect.Rows[i].Cells[4].Value + "', '" + collect.Rows[i].Cells[5].Value + "', '" + collect.Rows[i].Cells[6].Value + "')";
                    break;
                case 8:
                    sql = "insert into " + sheet + " (checklist_id, element_id, c1, c2, c3, c4, c5, c6, c7, c8) values ('" + checklist_id + "', '" + all_table.ToString() + "', '" + collect.Rows[i].Cells[0].Value + "', '" + collect.Rows[i].Cells[1].Value + "', '" + collect.Rows[i].Cells[2].Value + "', '" + collect.Rows[i].Cells[3].Value + "', '" + collect.Rows[i].Cells[4].Value + "', '" + collect.Rows[i].Cells[5].Value + "', '" + collect.Rows[i].Cells[6].Value + "', '" + collect.Rows[i].Cells[7].Value + "')";
                    break;
                case 9:
                    sql = "insert into " + sheet + " (checklist_id, element_id, c1, c2, c3, c4, c5, c6, c7, c8, c9) values ('" + checklist_id + "', '" + all_table.ToString() + "', '" + collect.Rows[i].Cells[0].Value + "', '" + collect.Rows[i].Cells[1].Value + "', '" + collect.Rows[i].Cells[2].Value + "', '" + collect.Rows[i].Cells[3].Value + "', '" + collect.Rows[i].Cells[4].Value + "', '" + collect.Rows[i].Cells[5].Value + "', '" + collect.Rows[i].Cells[6].Value + "', '" + collect.Rows[i].Cells[7].Value + "', '" + collect.Rows[i].Cells[8].Value + "')";
                    break;
                case 10:
                    sql = "insert into " + sheet + " (checklist_id, element_id, c1, c2, c3, c4, c5, c6, c7, c8, c9, c10) values ('" + checklist_id + "', '" + all_table.ToString() + "', '" + collect.Rows[i].Cells[0].Value + "', '" + collect.Rows[i].Cells[1].Value + "', '" + collect.Rows[i].Cells[2].Value + "', '" + collect.Rows[i].Cells[3].Value + "', '" + collect.Rows[i].Cells[4].Value + "', '" + collect.Rows[i].Cells[5].Value + "', '" + collect.Rows[i].Cells[6].Value + "', '" + collect.Rows[i].Cells[7].Value + "', '" + collect.Rows[i].Cells[8].Value + "', '" + collect.Rows[i].Cells[9].Value + "')";
                    break;
                case 11:
                    sql = "insert into " + sheet + " (checklist_id, element_id, c1, c2, c3, c4, c5, c6, c7, c8, c9, c10, c11) values ('" + checklist_id + "', '" + all_table.ToString() + "', '" + collect.Rows[i].Cells[0].Value + "', '" + collect.Rows[i].Cells[1].Value + "', '" + collect.Rows[i].Cells[2].Value + "', '" + collect.Rows[i].Cells[3].Value + "', '" + collect.Rows[i].Cells[4].Value + "', '" + collect.Rows[i].Cells[5].Value + "', '" + collect.Rows[i].Cells[6].Value + "', '" + collect.Rows[i].Cells[7].Value + "', '" + collect.Rows[i].Cells[8].Value + "', '" + collect.Rows[i].Cells[9].Value + "', '" + collect.Rows[i].Cells[10].Value + "')";
                    break;
                case 12:
                    sql = "insert into " + sheet + " (checklist_id, element_id, c1, c2, c3, c4, c5, c6, c7, c8, c9, c10, c11, c12) values ('" + checklist_id + "', '" + all_table.ToString() + "', '" + collect.Rows[i].Cells[0].Value + "', '" + collect.Rows[i].Cells[1].Value + "', '" + collect.Rows[i].Cells[2].Value + "', '" + collect.Rows[i].Cells[3].Value + "', '" + collect.Rows[i].Cells[4].Value + "', '" + collect.Rows[i].Cells[5].Value + "', '" + collect.Rows[i].Cells[6].Value + "', '" + collect.Rows[i].Cells[7].Value + "', '" + collect.Rows[i].Cells[8].Value + "', '" + collect.Rows[i].Cells[9].Value + "', '" + collect.Rows[i].Cells[10].Value + "', '" + collect.Rows[i].Cells[11].Value + "')";
                    break;
                case 13:
                    sql = "insert into " + sheet + " (checklist_id, element_id, c1, c2, c3, c4, c5, c6, c7, c8, c9, c10, c11, c12, c13) values ('" + checklist_id + "', '" + all_table.ToString() + "', '" + collect.Rows[i].Cells[0].Value + "', '" + collect.Rows[i].Cells[1].Value + "', '" + collect.Rows[i].Cells[2].Value + "', '" + collect.Rows[i].Cells[3].Value + "', '" + collect.Rows[i].Cells[4].Value + "', '" + collect.Rows[i].Cells[5].Value + "', '" + collect.Rows[i].Cells[6].Value + "', '" + collect.Rows[i].Cells[7].Value + "', '" + collect.Rows[i].Cells[8].Value + "', '" + collect.Rows[i].Cells[9].Value + "', '" + collect.Rows[i].Cells[10].Value + "', '" + collect.Rows[i].Cells[11].Value + "', '" + collect.Rows[i].Cells[12].Value + "')";
                    break;
            }
            mainForm.insertDB(sql);
        }

        //private Double cal_single(DataGridView c, Double weight, Int32 cols, double wq = 1.0, Int32 rows = 0)
        private Double cal_single(DataGridView c, Double weight, Int32 cols)
        {
            return cal_single(c, weight, cols, 1.0, 0);
        }

        private Double cal_single(DataGridView c, Double weight, Int32 cols, double wq)
        {
            return cal_single(c, weight, cols, wq, 0);
        }

        private Double cal_single(DataGridView c, Double weight, Int32 cols, double wq, Int32 rows)
        {
            cols--;
            if (check_val(c, cols, rows))
            {
                if (wq != 0.0)
                {
                    double tx = 0.0;
                    String temp = c.Rows[rows].Cells[cols].Value.ToString();

                    //trycatch
                    //Console.WriteLine(temp);

                    if (temp == "")
                    {
                        tx = 0.0;
                    }
                    else
                    {
                        //tx = Convert.ToDouble(c.Rows[rows].Cells[cols].Value);
                        tx = Convert.ToDouble(temp);
                    }

                    Double w = tx * (weight / wq);
                    return w;
                }
                else
                {
                    return 0.0;
                }
            }
            else
            {
                return 0.0;
            }
        }

        //private Boolean check_val(DataGridView c, Int32 cols, Int32 rows = 0)
        private Boolean check_val(DataGridView c, Int32 cols)
        {
            return check_val(c, cols, 0);
        }

        private Boolean check_val(DataGridView c, Int32 cols, Int32 rows)
        {
            if ((c.Rows[rows].Cells[cols].Value != null) && (c.Rows[rows].Cells[cols].Value != ""))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void clear_cal_weight()
        {
            String sql = "Delete from Layer6_Calculate where [checklist_id] = '" + checklist_id + "'";
            mainForm.getDB(sql);
        }

        private double check_val_double(string input)
        {
            if (input == "")
            {
                return 0.0;
            }
            else
            {
                try
                {
                    double temp = Convert.ToDouble(input);
                    if (temp >= 0 && temp <= 5)
                    {
                        return temp;
                    }
                    else
                    {
                        return 0.0;
                    }
                }
                catch
                {
                    return 0.0;
                }
            }
        }

        private string cal_with_dynamic_weight(DataGridView c, int part_layer6) 
        {
            return cal_with_dynamic_weight(c, part_layer6, 0);
        }

        private string cal_with_dynamic_weight(DataGridView c, int part_layer6, int rows)
        {
            string sql = "INSERT INTO Layer6_Calculate ( [checklist_id], [element_id], [is_have], [val] ) ";
            double wq = 0;
            double calculated = 0;
            string index_weight = part_layer6.ToString();
            JArray temp_weight = (JArray)mainForm.weight.GetValue(index_weight);

            //trycatch_fixing
            //Console.Write(temp_weight);

            for (int n = 1; n <= temp_weight.Count; n++)
            {
                if (check_val(c, (n - 1), rows))
                {
                    wq += (double)temp_weight[n - 1];
                }
            }


            //Console.WriteLine("----------------------");

            //Console.WriteLine(part_layer6.ToString());
            //Console.WriteLine(wq);

            sql += "values('" + checklist_id + "', '" + part_layer6.ToString() + "', 'True', '";
            int index = 1;

            double g = 0;
            foreach (double current_weight in temp_weight)
            {
                g = cal_single(c, current_weight, index, wq, rows);
                calculated += g;
                //Console.WriteLine(g + " | " + calculated);
                index++;
            }

            //Console.WriteLine("----------------------");

            sql += calculated.ToString() + "')";
            return sql;
        }

        private void cal_weight()
        {
            String sql = String.Empty;
            DataGridView c;
            int i;
            int cols = 0;
            int current_index = 0;
            int v1, v2;
            String[] keep;
            double wq = 0.0; //Weight
            double cal_temp = 0.0;
            double calculated = 0;
            JArray temp_weight;
            for (int part_layer6 = 0; part_layer6 < 200; part_layer6++)
            {
                if (checklist_structure[part_layer6])
                {
                    sql = "INSERT INTO Layer6_Calculate ( [checklist_id], [element_id], [is_have], [val] ) ";
                    switch (part_layer6)
                    {
                        case 5:
                            c = ch_1_1_1;
                            keep = new String[8];
                            for (cols = 2; cols < 10; cols++)
                            {
                                keep[cols - 2] = "6";
                                for (i = 0; i < c.RowCount; i++)
                                {
                                    v1 = Convert.ToInt32(keep[cols - 2]);   //Temp Value
                                    if (c.Rows[i].Cells[cols].Value != "" && c.Rows[i].Cells[cols].Value != null)
                                    {
                                        v2 = 6;
                                        if (c.Rows[i].Cells[cols].Value != "")
                                        {
                                            try
                                            {
                                                v2 = Convert.ToInt32(c.Rows[i].Cells[cols].Value);
                                            }
                                            catch
                                            {
                                                v2 = 6;
                                            }
                                        }
                                    }
                                    else
                                        v2 = 6;

                                    if (v1 > v2)
                                    {
                                        keep[cols - 2] = v2.ToString();
                                    }
                                }

                                if (keep[cols - 2] == "6")
                                {
                                    keep[cols - 2] = "0";
                                }
                            }

                            //Weight Avr
                            wq = 0.0;
                            temp_weight = (JArray)mainForm.weight.GetValue(part_layer6.ToString());
                            for (int n = 0; n < 8; n++)
                            {
                                if (check_val_double(keep[n]) > 0)
                                {
                                    wq += (double)temp_weight[n];
                                    /*switch (n)
                                    {
                                        case 0: wq += 0.2; break;
                                        case 1: wq += 0.18; break;
                                        case 2: wq += 0.2; break;
                                        case 3: wq += 0.09; break;
                                        case 4: wq += 0.18; break;
                                        case 5: wq += 0.08; break;
                                        case 6: wq += 0.03; break;
                                        case 7: wq += 0.04; break;
                                    }*/
                                }
                            }

                            cal_temp = 0.0;
                            if (wq > 0)
                            {
                                cal_temp = (check_val_double(keep[0]) * ((double)temp_weight[0] / wq)) + (check_val_double(keep[1]) * ((double)temp_weight[1] / wq)) + (check_val_double(keep[2]) * ((double)temp_weight[2] / wq)) + (check_val_double(keep[3]) * ((double)temp_weight[3] / wq)) + (check_val_double(keep[4]) * ((double)temp_weight[4] / wq)) + (check_val_double(keep[5]) * ((double)temp_weight[5] / wq)) + (check_val_double(keep[6]) * ((double)temp_weight[6] / wq)) + (check_val_double(keep[7]) * ((double)temp_weight[7] / wq));
                            }

                            sql += "values('" + checklist_id + "', '" + part_layer6.ToString() + "', 'True', '" + cal_temp + "')";
                            break;
                        case 6:
                            c = ch_1_1_2;

                            keep = new String[7];
                            for (cols = 2; cols < 9; cols++)
                            {
                                keep[cols - 2] = "6";
                                for (i = 0; i < c.RowCount; i++)
                                {
                                    v1 = Convert.ToInt32(keep[cols - 2]);
                                    if (c.Rows[i].Cells[cols].Value != "" && c.Rows[i].Cells[cols].Value != null)
                                    {
                                        v2 = 6;
                                        if (c.Rows[i].Cells[cols].Value.ToString() != "")
                                        {
                                            try
                                            {
                                                v2 = Convert.ToInt32(c.Rows[i].Cells[cols].Value);
                                            }
                                            catch
                                            {
                                                v2 = 6;
                                            }
                                        }
                                    }
                                    else
                                        v2 = 6;

                                    if (v1 > v2)
                                    {
                                        keep[cols - 2] = v2.ToString();
                                    }
                                }

                                if (keep[cols - 2] == "6")
                                {
                                    keep[cols - 2] = "0";
                                }
                            }

                            //Weight Avr
                            wq = 0.0;
                            temp_weight = (JArray)mainForm.weight.GetValue(part_layer6.ToString());
                            for (int n = 0; n <= 6; n++)
                            {
                                if (check_val_double(keep[n]) > 0)
                                {
                                    wq += (double)temp_weight[n];
                                    /*switch (n)
                                    {
                                        case 0: wq += 0.18; break;
                                        case 1: wq += 0.29; break;
                                        case 2: wq += 0.14; break;
                                        case 3: wq += 0.11; break;
                                        case 4: wq += 0.105; break;
                                        case 5: wq += 0.035; break;
                                        case 6: wq += 0.14; break;
                                    }*/
                                }

                            }
                            
                            cal_temp = 0.0;
                            if (wq > 0)
                            {
                                cal_temp = (check_val_double(keep[0]) * ((double)temp_weight[0] / wq)) + (check_val_double(keep[1]) * ((double)temp_weight[1] / wq)) + (check_val_double(keep[2]) * ((double)temp_weight[2] / wq)) + (check_val_double(keep[3]) * ((double)temp_weight[3] / wq)) + (check_val_double(keep[4]) * ((double)temp_weight[4] / wq)) + (check_val_double(keep[5]) * ((double)temp_weight[5] / wq)) + (check_val_double(keep[6]) * ((double)temp_weight[6] / wq));
                            }
                            sql += "values('" + checklist_id + "', '" + part_layer6.ToString() + "', 'True', '" + cal_temp + "')";
                            break;
                        case 7:
                            c = ch_1_1_3;

                            keep = new String[8];
                            for (cols = 2; cols < 10; cols++)
                            {
                                keep[cols - 2] = "6";
                                for (i = 0; i < c.RowCount; i++)
                                {
                                    v1 = Convert.ToInt32(keep[cols - 2]);
                                    if (c.Rows[i].Cells[cols].Value != "" && c.Rows[i].Cells[cols].Value != null)
                                    {
                                        v2 = 6;
                                        if (c.Rows[i].Cells[cols].Value.ToString() != "")
                                        {
                                            try
                                            {
                                                v2 = Convert.ToInt32(c.Rows[i].Cells[cols].Value);
                                            }
                                            catch
                                            {
                                                v2 = 6;
                                            }
                                        }
                                    }
                                    else
                                        v2 = 6;

                                    if (v1 > v2)
                                    {
                                        keep[cols - 2] = v2.ToString();
                                    }
                                }

                                if (keep[cols - 2] == "6")
                                {
                                    keep[cols - 2] = "0";
                                }
                            }

                            //Weight Avr
                            wq = 0.0; 
                            temp_weight = (JArray)mainForm.weight.GetValue(part_layer6.ToString());
                            current_index = 0;
                            for (int n = 0; n <= 7; n++)
                            {
                                if (check_val_double(keep[n]) > 0)
                                {
                                    wq += (double)temp_weight[current_index];
                                    /*switch (n)
                                    {
                                        case 0: wq += 0.15; break;
                                        case 1: wq += 0.21; break;
                                        case 2: wq += 0.21; break;
                                        case 3: wq += 0.13; break;
                                        case 4: wq += 0.06; break;
                                        case 5: wq += 0.10; break;
                                        case 6: wq += 0.04; break;
                                        case 7: wq += 0.10; break;
                                    }*/
                                }
                                current_index++;
                            }

                            cal_temp = 0.0;
                            if (wq != 0)
                            {
                                cal_temp = (check_val_double(keep[0]) * ((double)temp_weight[0] / wq)) + (check_val_double(keep[1]) * ((double)temp_weight[1] / wq)) + (check_val_double(keep[2]) * ((double)temp_weight[2] / wq)) + (check_val_double(keep[3]) * ((double)temp_weight[3] / wq)) + (check_val_double(keep[4]) * ((double)temp_weight[4] / wq)) + (check_val_double(keep[5]) * ((double)temp_weight[5] / wq)) + (check_val_double(keep[6]) * ((double)temp_weight[6] / wq)) + (check_val_double(keep[7]) * ((double)temp_weight[7] / wq));
                            }
                            sql += "values('" + checklist_id + "', '" + part_layer6.ToString() + "', 'True', '" + cal_temp + "')";
                            break;
                        case 8:
                        case 9:
                            c = ch_1_2;
                            int total = c.RowCount;
                            if (total == 1)
                            {
                                if (checklist_structure[part_layer6])
                                {
                                    i = 0;
                                    wq = 0;
                                    temp_weight = (JArray)mainForm.weight.GetValue(part_layer6.ToString());
                                    current_index = 0;

                                    for (int n = 2; n <= 9; n++)
                                    {                                        
                                        if (check_val(c, (n-1), i))
                                        {
                                            wq += (double)temp_weight[current_index];
                                            /*switch (n)
                                            {
                                                case 2: wq += 0.17; break;
                                                case 3: wq += 0.17; break;
                                                case 4: wq += 0.17; break;
                                                case 5: wq += 0.14; break;
                                                case 6: wq += 0.19; break;
                                                case 7: wq += 0.11; break;
                                                case 8: wq += 0.02; break;
                                                case 9: wq += 0.03; break;
                                            }*/
                                        }
                                        current_index++;
                                    }
                                    sql += "values('" + checklist_id + "', '" + part_layer6.ToString() + "', 'True', '" + (cal_single(c, (double)temp_weight[0], 2, wq, i) + cal_single(c, (double)temp_weight[1], 3, wq, i) + cal_single(c, (double)temp_weight[2], 4, wq, i) + cal_single(c, (double)temp_weight[3], 5, wq, i) + cal_single(c, (double)temp_weight[4], 6, wq, i) + cal_single(c, (double)temp_weight[5], 7, wq, i) + cal_single(c, (double)temp_weight[6], 8, wq, i) + cal_single(c, (double)temp_weight[7], 9, wq, i)).ToString() + "')";
                                }
                            }
                            else
                            {
                                if (part_layer6 == 8)
                                {
                                    i = 0;
                                }
                                else
                                {
                                    i = 1;
                                }

                                wq = 0;
                                temp_weight = (JArray)mainForm.weight.GetValue(part_layer6.ToString());
                                current_index = 0;

                                for (int n = 2; n <= 9; n++)
                                {
                                    if (check_val(c, (n - 1), i))
                                    {
                                        wq += (double)temp_weight[current_index];
                                        /*switch (n)
                                        {
                                            case 2: wq += 0.17; break;
                                            case 3: wq += 0.17; break;
                                            case 4: wq += 0.17; break;
                                            case 5: wq += 0.14; break;
                                            case 6: wq += 0.19; break;
                                            case 7: wq += 0.11; break;
                                            case 8: wq += 0.02; break;
                                            case 9: wq += 0.03; break;
                                        }*/
                                    }
                                    current_index++;
                                }

                                sql += "values('" + checklist_id + "', '" + part_layer6.ToString() + "', 'True', '" + (cal_single(c, (double)temp_weight[0], 2, wq, i) + cal_single(c, (double)temp_weight[1], 3, wq, i) + cal_single(c, (double)temp_weight[2], 4, wq, i) + cal_single(c, (double)temp_weight[3], 5, wq, i) + cal_single(c, (double)temp_weight[4], 6, wq, i) + cal_single(c, (double)temp_weight[5], 7, wq, i) + cal_single(c, (double)temp_weight[6], 8, wq, i) + cal_single(c, (double)temp_weight[7], 9, wq, i)).ToString() + "')";
                            }
                            break;
                        case 4:
                            c = ch_1_3;

                            keep = new String[10];
                            for (cols = 2; cols < 12; cols++)
                            {
                                keep[cols - 2] = "6";
                                for (i = 0; i < c.RowCount; i++)
                                {
                                    v1 = Convert.ToInt32(keep[cols - 2]);
                                    if (c.Rows[i].Cells[cols].Value != "" && c.Rows[i].Cells[cols].Value != null)
                                    {
                                        v2 = 0;
                                        if (c.Rows[i].Cells[cols].Value.ToString() != "")
                                        {
                                            try
                                            {
                                                v2 = Convert.ToInt32(c.Rows[i].Cells[cols].Value);
                                            }
                                            catch
                                            {
                                                v2 = 6;
                                            }
                                        }
                                    }
                                    else
                                        v2 = 6;

                                    if (v1 > v2)
                                    {
                                        keep[cols - 2] = v2.ToString();
                                    }
                                }

                                if (keep[cols - 2] == "6")
                                {
                                    keep[cols - 2] = "0";
                                }
                            }

                            //Weight Avr
                            wq = 0.0;
                            for (int n = 0; n <= 9; n++)
                            {
                                if (check_val_double(keep[n]) > 0)
                                {
                                    switch (n)
                                    {
                                        case 0: wq += 0.13; break;
                                        case 1: wq += 0.16; break;
                                        case 2: wq += 0.15; break;
                                        case 3: wq += 0.15; break;
                                        case 4: wq += 0.11; break;
                                        case 5: wq += 0.05; break;
                                        case 6: wq += 0.06; break;
                                        case 7: wq += 0.06; break;
                                        case 8: wq += 0.085; break;
                                        case 9: wq += 0.045; break;
                                    }
                                }
                            }

                            cal_temp = 0.0;
                            if (wq > 0)
                            {
                                cal_temp = (check_val_double(keep[0]) * (0.13 / wq)) + (check_val_double(keep[1]) * (0.16 / wq)) + (check_val_double(keep[2]) * (0.15 / wq)) + (check_val_double(keep[3]) * (0.15 / wq)) + (check_val_double(keep[4]) * (0.11 / wq)) + (check_val_double(keep[5]) * (0.05 / wq)) + (check_val_double(keep[6]) * (0.06 / wq)) + (check_val_double(keep[7]) * (0.06 / wq)) + (check_val_double(keep[8]) * (0.085 / wq)) + (check_val_double(keep[9]) * (0.045 / wq));
                            }

                            sql += "values('" + checklist_id + "', '" + part_layer6.ToString() + "', 'True', '" + cal_temp + "')";
                            
                            break;
                        case 12:
                            c = ch_2_1_1_1;
                            sql = cal_with_dynamic_weight(c, part_layer6);
                            break;
                        case 14:
                            c = ch_2_1_1_2_1;
                            sql = cal_with_dynamic_weight(c, part_layer6);
                            break;
                        case 15:
                            c = ch_2_1_1_2_2;
                            sql = cal_with_dynamic_weight(c, part_layer6);
                            break;
                        case 16:
                            c = ch_2_1_1_2_3;
                            sql = cal_with_dynamic_weight(c, part_layer6);
                            break;
                        case 18:
                            c = ch_2_1_2_1;
                            sql = cal_with_dynamic_weight(c, part_layer6);
                            break;
                        case 23:
                            c = ch_2_1_3_1;
                            sql = cal_with_dynamic_weight(c, part_layer6);
                            break;
                        case 24:
                            c = ch_2_1_3_2;
                            sql = cal_with_dynamic_weight(c, part_layer6);
                            break;
                        case 25:
                            c = ch_2_1_3_3;
                            sql = cal_with_dynamic_weight(c, part_layer6);
                            break;
                        case 26:
                            c = ch_2_1_3_4;
                            sql = cal_with_dynamic_weight(c, part_layer6);
                            break;
                        case 27:
                            c = ch_2_1_3_5;
                            sql = cal_with_dynamic_weight(c, part_layer6);
                            break;
                        case 29:
                            c = ch_2_1_3_6_1;
                            sql = cal_with_dynamic_weight(c, part_layer6);
                            break;
                        case 30:
                            c = ch_2_1_3_6_2;
                            sql = cal_with_dynamic_weight(c, part_layer6);
                            break;
                        case 31:
                            c = ch_2_1_3_6_3;
                            sql = cal_with_dynamic_weight(c, part_layer6);
                            break;
                        case 33:
                            c = ch_2_1_4_1_1;
                            sql = cal_with_dynamic_weight(c, part_layer6);
                            break;
                        case 34:
                            c = ch_2_1_4_1_2;
                            sql = cal_with_dynamic_weight(c, part_layer6);
                            break;
                        case 35:
                            c = ch_2_1_5_1;
                            sql = cal_with_dynamic_weight(c, part_layer6);
                            break;
                        case 36:
                            c = ch_2_1_5_2;
                            sql = cal_with_dynamic_weight(c, part_layer6);
                            break;
                        case 22:
                            c = ch_2_1_6;
                            sql = cal_with_dynamic_weight(c, part_layer6);
                            break;
                        case 46:
                            c = ch_2_2_1_1_1;
                            sql = cal_with_dynamic_weight(c, part_layer6);
                            break;
                        case 48:
                            c = ch_2_2_1_1_2_1;
                            sql = cal_with_dynamic_weight(c, part_layer6);
                            break;
                        case 49:
                            c = ch_2_2_1_1_2_2;
                            sql = cal_with_dynamic_weight(c, part_layer6);
                            break;
                        case 50:
                            c = ch_2_2_1_1_2_3;
                            sql = cal_with_dynamic_weight(c, part_layer6);
                            break;
                        case 51:
                            c = ch_2_2_1_2_1;
                            sql = cal_with_dynamic_weight(c, part_layer6);
                            break;
                        case 52:
                            c = ch_2_2_1_3_1;
                            sql = cal_with_dynamic_weight(c, part_layer6);
                            break;
                        case 53:
                            c = ch_2_2_1_3_2;
                            sql = cal_with_dynamic_weight(c, part_layer6);
                            break;
                        case 54:
                            c = ch_2_2_1_3_3;
                            sql = cal_with_dynamic_weight(c, part_layer6);
                            break;
                        case 55:
                            c = ch_2_2_1_3_4;
                            sql = cal_with_dynamic_weight(c, part_layer6);
                            break;
                        case 56:
                            c = ch_2_2_1_3_5;
                            sql = cal_with_dynamic_weight(c, part_layer6);
                            break;
                        case 58:
                            c = ch_2_2_1_3_6_1;
                            sql = cal_with_dynamic_weight(c, part_layer6);
                            break;
                        case 59:
                            c = ch_2_2_1_3_6_2;
                            sql = cal_with_dynamic_weight(c, part_layer6);
                            break;
                        case 60:
                            c = ch_2_2_1_3_6_3;
                            sql = cal_with_dynamic_weight(c, part_layer6);
                            break;
                        case 62:
                            c = ch_2_2_1_4_1_1;
                            sql = cal_with_dynamic_weight(c, part_layer6);
                            break;
                        case 63:
                            c = ch_2_2_1_4_1_2;
                            sql = cal_with_dynamic_weight(c, part_layer6);
                            break;
                        case 64:
                            c = ch_2_2_1_5_1;
                            sql = cal_with_dynamic_weight(c, part_layer6);
                            break;
                        case 65:
                            c = ch_2_2_1_5_2;
                            sql = cal_with_dynamic_weight(c, part_layer6);
                            break;
                        case 45:
                            c = ch_2_2_1_6;
                            sql = cal_with_dynamic_weight(c, part_layer6);
                            break;
                        case 72:
                            c = ch_2_2_2_1_1;
                            sql = cal_with_dynamic_weight(c, part_layer6);
                            break;
                        case 74:
                            c = ch_2_2_2_1_2_1;
                            sql = cal_with_dynamic_weight(c, part_layer6);
                            break;
                        case 75:
                            c = ch_2_2_2_1_2_2;
                            sql = cal_with_dynamic_weight(c, part_layer6);
                            break;
                        case 76:
                            c = ch_2_2_2_1_2_3;
                            sql = cal_with_dynamic_weight(c, part_layer6);
                            break;
                        case 77:
                            c = ch_2_2_2_2_1;
                            sql = cal_with_dynamic_weight(c, part_layer6);
                            break;
                        case 78:
                            c = ch_2_2_2_3_1;
                            sql = cal_with_dynamic_weight(c, part_layer6);
                            break;
                        case 79:
                            c = ch_2_2_2_3_2;
                            sql = cal_with_dynamic_weight(c, part_layer6);
                            break;
                        case 80:
                            c = ch_2_2_2_3_3;
                            sql = cal_with_dynamic_weight(c, part_layer6);
                            break;
                        case 81:
                            c = ch_2_2_2_3_4;
                            sql = cal_with_dynamic_weight(c, part_layer6);
                            break;
                        case 82:
                            c = ch_2_2_2_3_5;
                            sql = cal_with_dynamic_weight(c, part_layer6);
                            break;
                        case 150:
                            c = ch_2_2_2_3_6_1;
                            sql = cal_with_dynamic_weight(c, part_layer6);
                            break;
                        case 151:
                            c = ch_2_2_2_3_6_2;
                            sql = cal_with_dynamic_weight(c, part_layer6);
                            break;
                        case 152:
                            c = ch_2_2_2_3_6_3;
                            sql = cal_with_dynamic_weight(c, part_layer6);
                            break;
                        case 85:
                            c = ch_2_2_2_4_1_1;
                            sql = cal_with_dynamic_weight(c, part_layer6);
                            break;
                        case 86:
                            c = ch_2_2_2_4_1_2;
                            sql = cal_with_dynamic_weight(c, part_layer6);
                            break;
                        case 87:
                            c = ch_2_2_2_5_1;
                            sql = cal_with_dynamic_weight(c, part_layer6);
                            break;
                        case 88:
                            c = ch_2_2_2_5_2;
                            sql = cal_with_dynamic_weight(c, part_layer6);
                            break;
                        case 71:
                            c = ch_2_2_2_6;
                            sql = cal_with_dynamic_weight(c, part_layer6);
                            break;
                        case 101:
                            c = ch_3_1_1_1_1;
                            sql = cal_with_dynamic_weight(c, part_layer6);
                            break;
                        case 102:
                            c = ch_3_1_1_1_2;
                            sql = cal_with_dynamic_weight(c, part_layer6);
                            break;
                        case 103:
                            c = ch_3_1_1_2_1;
                            sql = cal_with_dynamic_weight(c, part_layer6);
                            break;
                        case 104:
                            c = ch_3_1_1_3_1;
                            sql = cal_with_dynamic_weight(c, part_layer6);
                            break;
                        case 105:
                            c = ch_3_1_1_3_2;
                            sql = cal_with_dynamic_weight(c, part_layer6);
                            break;
                        case 106:
                            c = ch_3_1_1_4_1;
                            sql = cal_with_dynamic_weight(c, part_layer6);
                            break;
                        case 107:
                            c = ch_3_1_1_4_2;
                            sql = cal_with_dynamic_weight(c, part_layer6);
                            break;
                        case 99:
                            c = ch_3_1_1_5;
                            sql = cal_with_dynamic_weight(c, part_layer6);
                            break;
                        case 108:
                            c = ch_3_1_1_6_1;
                            sql = cal_with_dynamic_weight(c, part_layer6);
                            break;
                        case 109:
                            c = ch_3_1_1_6_2;
                            sql = cal_with_dynamic_weight(c, part_layer6);
                            break;
                        case 110:
                            c = ch_3_1_1_6_3;
                            sql = cal_with_dynamic_weight(c, part_layer6);
                            break;
                        case 117:
                            c = ch_3_1_2_1_1;
                            sql = cal_with_dynamic_weight(c, part_layer6);
                            break;
                        case 118:
                            c = ch_3_1_2_1_2;
                            sql = cal_with_dynamic_weight(c, part_layer6);
                            break;
                        case 119:
                            c = ch_3_1_2_2_1;
                            sql = cal_with_dynamic_weight(c, part_layer6);
                            break;
                        case 126:
                            c = ch_3_1_2_2_2_1;
                            sql = cal_with_dynamic_weight(c, part_layer6);
                            break;
                        case 127:
                            c = ch_3_1_2_2_2_2;
                            sql = cal_with_dynamic_weight(c, part_layer6);
                            break;
                        case 128:
                            c = ch_3_1_2_2_2_3;
                            sql = cal_with_dynamic_weight(c, part_layer6);
                            break;
                        case 121:
                            c = ch_3_1_2_2_3;
                            sql = cal_with_dynamic_weight(c, part_layer6);
                            break;
                        case 122:
                            c = ch_3_1_2_2_4;
                            sql = cal_with_dynamic_weight(c, part_layer6);
                            break;
                        case 123:
                            c = ch_3_1_2_2_5;
                            sql = cal_with_dynamic_weight(c, part_layer6);
                            break;
                        case 124:
                            c = ch_3_1_2_2_6;
                            sql = cal_with_dynamic_weight(c, part_layer6);
                            break;
                        case 125:
                            c = ch_3_1_2_2_7;
                            sql = cal_with_dynamic_weight(c, part_layer6);
                            break;
                        case 129:
                            c = ch_3_1_2_3_1;
                            sql = cal_with_dynamic_weight(c, part_layer6);
                            break;
                        case 130:
                            c = ch_3_1_2_3_2;
                            sql = cal_with_dynamic_weight(c, part_layer6);
                            break;
                        case 131:
                            c = ch_3_1_2_4_1;
                            sql = cal_with_dynamic_weight(c, part_layer6);
                            break;
                        case 132:
                            c = ch_3_1_2_4_2;
                            sql = cal_with_dynamic_weight(c, part_layer6);
                            break;
                        case 115:
                            c = ch_3_1_2_5;
                            sql = cal_with_dynamic_weight(c, part_layer6);
                            break;
                        case 133:
                            c = ch_3_1_2_6_1;
                            sql = cal_with_dynamic_weight(c, part_layer6);
                            break;
                        case 134:
                            c = ch_3_1_2_6_2;
                            sql = cal_with_dynamic_weight(c, part_layer6);
                            break;
                        case 135:
                            c = ch_3_1_2_6_3;
                            sql = cal_with_dynamic_weight(c, part_layer6);
                            break;
                        case 136:
                            c = ch_3_1_3_1;
                            sql = cal_with_dynamic_weight(c, part_layer6);
                            break;
                        case 141:
                            c = ch_3_1_3_2_1;
                            sql = cal_with_dynamic_weight(c, part_layer6);
                            break;
                        case 142:
                            c = ch_3_1_3_3_1;
                            sql = cal_with_dynamic_weight(c, part_layer6);
                            break;
                        case 143:
                            c = ch_3_1_3_3_2;
                            sql = cal_with_dynamic_weight(c, part_layer6);
                            break;
                        case 144:
                            c = ch_3_1_3_4_1;
                            sql = cal_with_dynamic_weight(c, part_layer6);
                            break;
                        case 145:
                            c = ch_3_1_3_4_2;
                            sql = cal_with_dynamic_weight(c, part_layer6);
                            break;
                        case 140:
                            c = ch_3_1_3_5;
                            sql = cal_with_dynamic_weight(c, part_layer6);
                            break;
                        case 146:
                            c = ch_3_2_1;
                            sql = cal_with_dynamic_weight(c, part_layer6);
                            break;
                        case 147:
                            c = ch_3_2_2;
                            sql = cal_with_dynamic_weight(c, part_layer6);
                            break;
                        case 148:
                            c = ch_3_2_3;
                            sql = cal_with_dynamic_weight(c, part_layer6);
                            break;
                        default:
                            sql = string.Empty;
                            break;
                    }
                }
                else
                {
                    switch (part_layer6)
                    {
                        case 5:
                        case 6:
                        case 7:
                        case 8:
                        case 9:
                        case 4:
                        case 12:
                        case 14:
                        case 15:
                        case 16:
                        case 18:
                        case 23:
                        case 24:
                        case 25:
                        case 26:
                        case 27:
                        case 29:
                        case 30:
                        case 31:
                        case 33:
                        case 34:
                        case 35:
                        case 36:
                        case 22:
                        case 46:
                        case 48:
                        case 49:
                        case 50:
                        case 51:
                        case 52:
                        case 53:
                        case 54:
                        case 55:
                        case 56:
                        case 58:
                        case 59:
                        case 60:
                        case 62:
                        case 63:
                        case 64:
                        case 65:
                        case 45:
                        case 72:
                        case 74:
                        case 75:
                        case 76:
                        case 77:
                        case 78:
                        case 79:
                        case 80:
                        case 81:
                        case 82:
                        case 150:
                        case 151:
                        case 152:
                        case 85:
                        case 86:
                        case 87:
                        case 88:
                        case 71:
                        case 101:
                        case 102:
                        case 103:
                        case 104:
                        case 105:
                        case 106:
                        case 107:
                        case 99:
                        case 108:
                        case 109:
                        case 110:
                        case 117:
                        case 118:
                        case 119:
                        case 126:
                        case 127:
                        case 128:
                        case 121:
                        case 122:
                        case 123:
                        case 124:
                        case 125:
                        case 129:
                        case 130:
                        case 131:
                        case 132:
                        case 115:
                        case 133:
                        case 134:
                        case 135:
                        case 136:
                        case 141:
                        case 142:
                        case 143:
                        case 144:
                        case 145:
                        case 140:
                        case 146:
                        case 147:
                        case 148:
                            sql = "insert into Layer6_Calculate ([checklist_id], [element_id], [is_have], [val]) values('" + checklist_id + "', '" + part_layer6.ToString() + "', 'False','0')";
                            break;
                        default:
                            sql = string.Empty;
                            break;
                    }
                }

                if (sql != string.Empty)
                {
                    mainForm.insertDB(sql);
                }
            }
        }

        private void get_child_cal()
        {
            String sql = "select * from Layer6_Calculate where [checklist_id] = '" + checklist_id + "'";
            all_kept = mainForm.getDB(sql);
        }

        private void save_risk_part(int element_id, double score)
        {
            if (score < 3)
            {
                String sql = "insert into risk_part (checklist_id, element_id, score) ";
                sql += "values('" + checklist_id + "', '" + element_id.ToString() + "', '" + score.ToString() + "')";
                mainForm.insertDB(sql);
            }
        }

        private void delete_log_db()
        {
            String sql = "delete from calculation_log where [checklist_id] = '" + checklist_id + "'";
            mainForm.getDB(sql);
        }

        //private Double[] cal_all(int parent = 0, double pWeight = 1, string indent_log = "")
        private Double[] cal_all()
        {
            return cal_all(0, 1, "");
        }

        private Double[] cal_all(int parent)
        {
            return cal_all(parent, 1, "");
        }

        private Double[] cal_all(int parent, double pWeight)
        {
            return cal_all(parent, pWeight, "");
        }

        private Double[] cal_all(int parent, double pWeight, string indent_log)
        {
            Double[] result = new Double[2], temp = new Double[2];
            Boolean found = false;
            List<double> score = new List<double>();
            List<double> weight = new List<double>();

            //Primary Method in case of "Found" Condition
            foreach (ArrayList pList in all_properties)     //Shift pointer in the same level of child node
            {
                //find node
                if (Convert.ToInt32(pList[1]) == parent)
                {
                    found = true;                           //tell the system to know it current in tunk node
                    //Json Form "{"title":"Title 0","info":{"score":"s0","weight":"w0"}}"
                    callog[order_log] = "{\"id\":\"" + pList[0] + "\",\"title\":\"" + indent_log + pList[3].ToString().Trim() + "\",\"info\":";
                    int temp_index = order_log;
                    order_log++;

                    temp = cal_all(Convert.ToInt32(pList[0]), Convert.ToDouble(pList[4]), indent_log + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
                    callog[temp_index] += "{\"score\":\"" + String.Format("{0:F2}", temp[0]) + "\",\"weight\":\"" + pList[4] + "\"}}";

                    //Insert Log
                    String sql = "insert into calculation_log (element_id, element_title, score, weight, checklist_id) values ('" + pList[0] + "','" + pList[3].ToString().Trim() + "','" + String.Format("{0:F2}", temp[0]) + "','" + pList[4] + "','" + checklist_id + "')";
                    mainForm.insertDB(sql);

                    if (temp[0] != 0.0)
                    {
                        score.Add(temp[0]);
                        weight.Add(temp[1]);
                        save_risk_part(Convert.ToInt32(pList[0]), temp[0]);
                    }
                }
            }

            //Secondary Method in case of "Not Found" Condition
            if (!found)
            {
                //Leaf node
                //Looking for node in database
                foreach (ArrayList item in all_kept)
                {
                    if (parent == Convert.ToInt32(item[2]))
                    {
                        if (Convert.ToDouble(item[4]) >= 0)
                        {
                            result[0] = Convert.ToDouble(item[4]);
                        }
                        else
                        {
                            result[0] = 0;
                        }
                        result[1] = 1;                              //Assign weight = 1 because let previous recurrent can calculate with exactly weight
                    }
                }
            }
            else
            {
                //Rearrange Weight
                double sum_weight = 0.0;
                result[0] = 0;
                for (int i = 0; i < weight.Count; i++ )
                {
                    //Double check condition
                    if (score[i] > 0.0)
                    {
                        sum_weight += weight[i];
                    }
                }

                //Check non-selected dam properties
                if (sum_weight == 1)                    
                {
                    //Weight is ok no need to re-arrange
                    for (int i = 0; i < weight.Count; i++)
                    {
                        if (score[i] >= 0)
                        {
                            result[0] += score[i] * weight[i];
                        }
                        else
                        {
                            result[0] += 0;
                        }
                    }
                    result[1] = pWeight;
                }
                else
                {
                    //Weight need to re-arrange
                    for (int i = 0; i < weight.Count; i++)
                    {
                        
                        weight[i] = weight[i] / sum_weight;     //find a new weight
                        //result[0] += score[i] * weight[i];
                        if (score[i] >= 0)
                        {
                            result[0] += score[i] * weight[i];
                        }
                        else
                        {
                            result[0] += 0;
                        }
                    }
                    result[1] = pWeight;
                }
                
            }

            return result;      //Return Score and weight
        }

        private void save_sheet()
        {
            Int32 i;
            DataGridView collect;
            String sql = String.Empty;
            String sheet = String.Empty;
            for (int all_table = 0; all_table < 200; all_table++)
            {
                if (checklist_structure[all_table])
                {
                    sheet = check_sheet(all_table);
                    switch (all_table)
                    {
                        case 5:
                            collect = ch_1_1_1;
                            for (i = 0; i < collect.RowCount; i++)
                            {
                                sql = "insert into " + sheet + " (checklist_id, element_id, c1, c2, c3, c4, c5, c6, c7, c8, c9, c10, c11) values ('" + checklist_id + "', '" + all_table.ToString() + "', '" + collect.Rows[i].Cells[0].Value + "', '" + collect.Rows[i].Cells[1].Value + "', '" + collect.Rows[i].Cells[2].Value + "', '" + collect.Rows[i].Cells[3].Value + "', '" + collect.Rows[i].Cells[4].Value + "', '" + collect.Rows[i].Cells[5].Value + "', '" + collect.Rows[i].Cells[6].Value + "', '" + collect.Rows[i].Cells[7].Value + "', '" + collect.Rows[i].Cells[8].Value + "', '" + collect.Rows[i].Cells[9].Value + "', '" + collect.Rows[i].Cells[10].Value + "')";
                                mainForm.insertDB(sql);
                            }
                            break;
                        case 6:
                            collect = ch_1_1_2;
                            for (i = 0; i < collect.RowCount; i++)
                            {
                                sql = "insert into " + sheet + " (checklist_id, element_id, c1, c2, c3, c4, c5, c6, c7, c8, c9, c10) values ('" + checklist_id + "', '" + all_table.ToString() + "', '" + collect.Rows[i].Cells[0].Value + "', '" + collect.Rows[i].Cells[1].Value + "', '" + collect.Rows[i].Cells[2].Value + "', '" + collect.Rows[i].Cells[3].Value + "', '" + collect.Rows[i].Cells[4].Value + "', '" + collect.Rows[i].Cells[5].Value + "', '" + collect.Rows[i].Cells[6].Value + "', '" + collect.Rows[i].Cells[7].Value + "', '" + collect.Rows[i].Cells[8].Value + "', '" + collect.Rows[i].Cells[9].Value + "')";
                                mainForm.insertDB(sql);
                            }
                            break;
                        case 7:
                            collect = ch_1_1_3;
                            for (i = 0; i < collect.RowCount; i++)
                            {
                                sql = "insert into " + sheet + " (checklist_id, element_id, c1, c2, c3, c4, c5, c6, c7, c8, c9, c10, c11) values ('" + checklist_id + "', '" + all_table.ToString() + "', '" + collect.Rows[i].Cells[0].Value + "', '" + collect.Rows[i].Cells[1].Value + "', '" + collect.Rows[i].Cells[2].Value + "', '" + collect.Rows[i].Cells[3].Value + "', '" + collect.Rows[i].Cells[4].Value + "', '" + collect.Rows[i].Cells[5].Value + "', '" + collect.Rows[i].Cells[6].Value + "', '" + collect.Rows[i].Cells[7].Value + "', '" + collect.Rows[i].Cells[8].Value + "', '" + collect.Rows[i].Cells[9].Value + "', '" + collect.Rows[i].Cells[10].Value + "')";
                                mainForm.insertDB(sql);
                            }
                            break;
                        case 8:
                        case 9:
                            collect = ch_1_2;
                            int total = collect.RowCount;
                            if (total == 1)
                            {
                                if (checklist_structure[all_table])
                                {
                                    i = 0;
                                    sql = "insert into " + sheet + " (checklist_id, element_id, c1, c2, c3, c4, c5, c6, c7, c8, c9, c10) values ('" + checklist_id + "', '" + all_table.ToString() + "', '" + collect.Rows[i].Cells[0].Value + "', '" + collect.Rows[i].Cells[1].Value + "', '" + collect.Rows[i].Cells[2].Value + "', '" + collect.Rows[i].Cells[3].Value + "', '" + collect.Rows[i].Cells[4].Value + "', '" + collect.Rows[i].Cells[5].Value + "', '" + collect.Rows[i].Cells[6].Value + "', '" + collect.Rows[i].Cells[7].Value + "', '" + collect.Rows[i].Cells[8].Value + "', '" + collect.Rows[i].Cells[9].Value + "')";
                                }
                            }
                            else
                            {
                                if (all_table == 8)
                                {
                                    i = 0;
                                }
                                else
                                {
                                    i = 1;
                                }
                                sql = "insert into " + sheet + " (checklist_id, element_id, c1, c2, c3, c4, c5, c6, c7, c8, c9, c10) values ('" + checklist_id + "', '" + all_table.ToString() + "', '" + collect.Rows[i].Cells[0].Value + "', '" + collect.Rows[i].Cells[1].Value + "', '" + collect.Rows[i].Cells[2].Value + "', '" + collect.Rows[i].Cells[3].Value + "', '" + collect.Rows[i].Cells[4].Value + "', '" + collect.Rows[i].Cells[5].Value + "', '" + collect.Rows[i].Cells[6].Value + "', '" + collect.Rows[i].Cells[7].Value + "', '" + collect.Rows[i].Cells[8].Value + "', '" + collect.Rows[i].Cells[9].Value + "')";
                            }
                            mainForm.insertDB(sql);
                            break;
                        case 4:
                            collect = ch_1_3;

                            for (i = 0; i < collect.RowCount; i++)
                            {
                                sql = "insert into " + sheet + " (checklist_id, element_id, c1, c2, c3, c4, c5, c6, c7, c8, c9, c10, c11, c12, c13) values ('" + checklist_id + "', '" + all_table.ToString() + "', '" + collect.Rows[i].Cells[0].Value + "', '" + collect.Rows[i].Cells[1].Value + "', '" + collect.Rows[i].Cells[2].Value + "', '" + collect.Rows[i].Cells[3].Value + "', '" + collect.Rows[i].Cells[4].Value + "', '" + collect.Rows[i].Cells[5].Value + "', '" + collect.Rows[i].Cells[6].Value + "', '" + collect.Rows[i].Cells[7].Value + "', '" + collect.Rows[i].Cells[8].Value + "', '" + collect.Rows[i].Cells[9].Value + "', '" + collect.Rows[i].Cells[10].Value + "', '" + collect.Rows[i].Cells[11].Value + "', '" + collect.Rows[i].Cells[12].Value + "')";
                                mainForm.insertDB(sql);
                            } 
                            break;
                        case 12:
                            insert_questionaire_single(ch_2_1_1_1, all_table, 7);
                            break;
                        case 14:
                            insert_questionaire_single(ch_2_1_1_2_1, all_table, 7);
                            break;
                        case 15:
                            insert_questionaire_single(ch_2_1_1_2_2, all_table, 5);
                            break;
                        case 16:
                            insert_questionaire_single(ch_2_1_1_2_3, all_table, 5);
                            break;
                        case 18:
                            insert_questionaire_single(ch_2_1_2_1, all_table, 6);
                            break;
                        case 23:
                            insert_questionaire_single(ch_2_1_3_1, all_table, 7);
                            break;
                        case 24:
                            insert_questionaire_single(ch_2_1_3_2, all_table, 5);
                            break;
                        case 25:
                            insert_questionaire_single(ch_2_1_3_3, all_table, 4);
                            break;
                        case 26:
                            insert_questionaire_single(ch_2_1_3_4, all_table, 6);
                            break;
                        case 27:
                            insert_questionaire_single(ch_2_1_3_5, all_table, 6);
                            break;
                        case 29:
                            insert_questionaire_single(ch_2_1_3_6_1, all_table, 6);
                            break;
                        case 30:
                            insert_questionaire_single(ch_2_1_3_6_2, all_table, 6);
                            break;
                        case 31:
                            insert_questionaire_single(ch_2_1_3_6_3, all_table, 6);
                            break;
                        case 33:
                            insert_questionaire_single(ch_2_1_4_1_1, all_table, 12);
                            break;
                        case 34:
                            insert_questionaire_single(ch_2_1_4_1_2, all_table, 11); 
                            break;
                        case 35:
                            insert_questionaire_single(ch_2_1_5_1, all_table, 12);
                            break;
                        case 36:
                            insert_questionaire_single(ch_2_1_5_2, all_table, 11);
                            break;
                        case 22:
                            insert_questionaire_single(ch_2_1_6, all_table, 13);
                            break;
                        case 46:
                            insert_questionaire_single(ch_2_2_1_1_1, all_table, 7);
                            break;
                        case 48:
                            insert_questionaire_single(ch_2_2_1_1_2_1, all_table, 7);
                            break;
                        case 49:
                            insert_questionaire_single(ch_2_2_1_1_2_2, all_table, 5);
                            break;
                        case 50:
                            insert_questionaire_single(ch_2_2_1_1_2_3, all_table, 5);
                            break;
                        case 51:
                            insert_questionaire_single(ch_2_2_1_2_1, all_table, 6);
                            break;
                        case 52:
                            insert_questionaire_single(ch_2_2_1_3_1, all_table, 7);
                            break;
                        case 53:
                            insert_questionaire_single(ch_2_2_1_3_2, all_table, 5);
                            break;
                        case 54:
                            insert_questionaire_single(ch_2_2_1_3_3, all_table, 4);
                            break;
                        case 55:
                            insert_questionaire_single(ch_2_2_1_3_4, all_table, 6);
                            break;
                        case 56:
                            insert_questionaire_single(ch_2_2_1_3_5, all_table, 6);
                            break;
                        case 58:
                            insert_questionaire_single(ch_2_2_1_3_6_1, all_table, 6);
                            break;
                        case 59:
                            insert_questionaire_single(ch_2_2_1_3_6_2, all_table, 6);
                            break;
                        case 60:
                            insert_questionaire_single(ch_2_2_1_3_6_3, all_table, 6);
                            break;
                        case 62:
                            insert_questionaire_single(ch_2_2_1_4_1_1, all_table, 12);
                            break;
                        case 63:
                            insert_questionaire_single(ch_2_2_1_4_1_2, all_table, 11);
                            break;
                        case 64:
                            insert_questionaire_single(ch_2_2_1_5_1, all_table, 12);
                            break;
                        case 65:
                            insert_questionaire_single(ch_2_2_1_5_2, all_table, 11);
                            break;
                        case 45:
                            insert_questionaire_single(ch_2_2_1_6, all_table, 13);
                            break;
                        case 72:
                            insert_questionaire_single(ch_2_2_2_1_1, all_table, 7);
                            break;
                        case 74:
                            insert_questionaire_single(ch_2_2_2_1_2_1, all_table, 7);
                            break;
                        case 75:
                            insert_questionaire_single(ch_2_2_2_1_2_2, all_table, 5);
                            break;
                        case 76:
                            insert_questionaire_single(ch_2_2_2_1_2_3, all_table, 5);
                            break;
                        case 77:
                            insert_questionaire_single(ch_2_2_2_2_1, all_table, 6);
                            break;
                        case 78:
                            insert_questionaire_single(ch_2_2_2_3_1, all_table, 7);
                            break;
                        case 79:
                            insert_questionaire_single(ch_2_2_2_3_2, all_table, 5);
                            break;
                        case 80:
                            insert_questionaire_single(ch_2_2_2_3_3, all_table, 4);
                            break;
                        case 81:
                            insert_questionaire_single(ch_2_2_2_3_4, all_table, 6);
                            break;
                        case 82:
                            insert_questionaire_single(ch_2_2_2_3_5, all_table, 6);
                            break;
                        case 150:
                            insert_questionaire_single(ch_2_2_2_3_6_1, all_table, 6);
                            break;
                        case 151:
                            insert_questionaire_single(ch_2_2_2_3_6_2, all_table, 6);
                            break;
                        case 152:
                            insert_questionaire_single(ch_2_2_2_3_6_3, all_table, 6);
                            break;
                        case 85:
                            insert_questionaire_single(ch_2_2_2_4_1_1, all_table, 12);
                            break;
                        case 86:
                            insert_questionaire_single(ch_2_2_2_4_1_2, all_table, 11);
                            break;
                        case 87:
                            insert_questionaire_single(ch_2_2_2_5_1, all_table, 12);
                            break;
                        case 88:
                            insert_questionaire_single(ch_2_2_2_5_2, all_table, 11);
                            break;
                        case 71:
                            insert_questionaire_single(ch_2_2_2_6, all_table, 13);
                            break;
                        case 101:
                            insert_questionaire_single(ch_3_1_1_1_1, all_table, 12);
                            break;
                        case 102:
                            insert_questionaire_single(ch_3_1_1_1_2, all_table, 11);
                            break;
                        case 103:
                            insert_questionaire_single(ch_3_1_1_2_1, all_table, 10);
                            break;
                        case 104:
                            insert_questionaire_single(ch_3_1_1_3_1, all_table, 12);
                            break;
                        case 105:
                            insert_questionaire_single(ch_3_1_1_3_2, all_table, 11);
                            break;
                        case 106:
                            insert_questionaire_single(ch_3_1_1_4_1, all_table, 12);
                            break;
                        case 107:
                            insert_questionaire_single(ch_3_1_1_4_2, all_table, 11);
                            break;
                        case 99:
                            insert_questionaire_single(ch_3_1_1_5, all_table, 13);
                            break;
                        case 108:
                            insert_questionaire_single(ch_3_1_1_6_1, all_table, 6);
                            break;
                        case 109:
                            insert_questionaire_single(ch_3_1_1_6_2, all_table, 6);
                            break;
                        case 110:
                            insert_questionaire_single(ch_3_1_1_6_3, all_table, 6);
                            break;
                        case 117:
                            insert_questionaire_single(ch_3_1_2_1_1, all_table, 12);
                            break;
                        case 118:
                            insert_questionaire_single(ch_3_1_2_1_2, all_table, 11);
                            break;
                        case 119:
                            insert_questionaire_single(ch_3_1_2_2_1, all_table, 10);
                            break;
                        case 126:
                            insert_questionaire_single(ch_3_1_2_2_2_1, all_table, 6);
                            break;
                        case 127:
                            insert_questionaire_single(ch_3_1_2_2_2_2, all_table, 6);
                            break;
                        case 128:
                            insert_questionaire_single(ch_3_1_2_2_2_3, all_table, 6);
                            break;
                        case 121:
                            insert_questionaire_single(ch_3_1_2_2_3, all_table, 12);
                            break;
                        case 122:
                            insert_questionaire_single(ch_3_1_2_2_4, all_table, 11);
                            break;
                        case 123:
                            insert_questionaire_single(ch_3_1_2_2_5, all_table, 6);
                            break;
                        case 124:
                            insert_questionaire_single(ch_3_1_2_2_6, all_table, 5);
                            break;
                        case 125:
                            insert_questionaire_single(ch_3_1_2_2_7, all_table, 4);
                            break;
                        case 129:
                            insert_questionaire_single(ch_3_1_2_3_1, all_table, 12);
                            break;
                        case 130:
                            insert_questionaire_single(ch_3_1_2_3_2, all_table, 11);
                            break;
                        case 131:
                            insert_questionaire_single(ch_3_1_2_4_1, all_table, 12);
                            break;
                        case 132:
                            insert_questionaire_single(ch_3_1_2_4_2, all_table, 11);
                            break;
                        case 115:
                            insert_questionaire_single(ch_3_1_2_5, all_table, 13);
                            break;
                        case 133:
                            insert_questionaire_single(ch_3_1_2_6_1, all_table, 6);
                            break;
                        case 134:
                            insert_questionaire_single(ch_3_1_2_6_2, all_table, 6);
                            break;
                        case 135:
                            insert_questionaire_single(ch_3_1_2_6_3, all_table, 6);
                            break;
                        case 136:
                            insert_questionaire_single(ch_3_1_3_1, all_table, 7);
                            break;
                        case 141:
                            insert_questionaire_single(ch_3_1_3_2_1, all_table, 6);
                            break;
                        case 142:
                            insert_questionaire_single(ch_3_1_3_3_1, all_table, 12);
                            break;
                        case 143:
                            insert_questionaire_single(ch_3_1_3_3_2, all_table, 11);
                            break;
                        case 144:
                            insert_questionaire_single(ch_3_1_3_4_1, all_table, 12);
                            break;
                        case 145:
                            insert_questionaire_single(ch_3_1_3_4_2, all_table, 11);
                            break;
                        case 140:
                            insert_questionaire_single(ch_3_1_3_5, all_table, 13);
                            break;
                        case 146:
                            insert_questionaire_single(ch_3_2_1, all_table, 7);
                            break;
                        case 147:
                            insert_questionaire_single(ch_3_2_2, all_table, 7);
                            break;
                        case 148:
                            insert_questionaire_single(ch_3_2_3, all_table, 7);
                            break;
                    }
                }
            }

        }

        private void thread_save_event()
        {
            label_footer_checklistsheet.Text = "เครียค่าน้ำหนัก";

            clear_cal_weight();
            label_footer_checklistsheet.Text = "ลบข้อมูลคำนวนเก่า";

            clear_old_data();
            delete_log_db();
            label_footer_checklistsheet.Text = "เริ่มทำการเซฟข้อมูล";

            save_sheet();

            label_footer_checklistsheet.Text = "คำนวนค่าถ่วงน้ำหนักพร้อมถ่วงสมดุลค่าน้ำหนัก";

            cal_weight();

            label_footer_checklistsheet.Text = "บันทึกตารางเรียบร้อยแล้ว";

            get_child_cal();

            double[] x = cal_all();
            mark_as_use(x[0]);
            label_footer_checklistsheet.Text = "บันทึกตารางเรียบร้อยแล้ว : ค่าคะแนนของตารางเท่ากับ " + String.Format("{0:F2}", (x[0] * 20).ToString());

            label_footer_checklistsheet.Text = "เขียนข้อมูลประวัติการคำนวน";
            write_callog();
        }

        private void render_new_checlist_main_form()
        {
            ArrayList checklist_sql = mainForm.getDB("select * from fill_checklist where [dam_auto_id] = '" + mainForm.dam_auto_id_global + "'");
            mainForm.Checklist_Added(checklist_sql);
        }

        private void save_event()
        {
            callog = new string[152];
            order_log = 0;
            this.Cursor = Cursors.WaitCursor;
            Thread test = new Thread(
                delegate()
                {
                    thread_save_event();
                });
            test.Start();
            test.Join();
            render_new_checlist_main_form();
            this.Cursor = Cursors.Default;
            Thread.Sleep(1500);
            label_footer_checklistsheet.Text = "";

            string sql = "SELECT fill_checklist.ID, * FROM fill_checklist WHERE (((fill_checklist.dam_auto_id)='" + dam_id + "')) ORDER BY fill_checklist.ID DESC;";
            ArrayList checklist_sql = mainForm.getDB(sql);
            mainForm.Checklist_Added(checklist_sql);
        }

        private void write_callog()
        {
            string log = string.Empty;
            log = "[";
            for (int i = 0; i < callog.Length; i++ )
            {
                if (i != 0)
                {
                    log += ",";
                }
                log += callog[i];
            }
            log += "]";

            Thread genLog = new Thread(delegate()
            {
                //Setup Log
                log = "{\"survey_id\":" + checklist_id + ", \"dam_name\":\"" + dam_name + "\", \"data\":" + log + " }";

                //Write Log
                System.IO.File.WriteAllText(@"Log\Log.txt", log);
                System.Diagnostics.Process process = new System.Diagnostics.Process();
                System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
                startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                startInfo.FileName = @"Template\bin\php\php5.3.13\php.exe";
                String exec = @"Log\gen.php";
                startInfo.Arguments = exec;
                process.StartInfo = startInfo;
                process.Start();
                process.WaitForExit();
            });
            genLog.Start();
            genLog.Join();
        }

        private void exit_checklist_panel()
        {
            this.Close();
        }

        private void Save_btn_Click(object sender, EventArgs e)
        {
            save_event();
            MessageBox.Show("ทำการบันทึกเรียบร้อยแล้ว");
        }

        private void Form_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.S)       // Ctrl-S Save
            {
                e.SuppressKeyPress = true;  // stops bing! also sets handeled which stop event bubbling
                save_event();
                MessageBox.Show("ทำการบันทึกเรียบร้อยแล้ว");
            }

            if (e.Control && e.KeyCode == Keys.Q)       // Ctrl-Q Save
            {
                e.SuppressKeyPress = true;
                exit_checklist_panel();
            }

            if (e.Control && e.KeyCode == Keys.T)       // Ctrl-T Test
            {
                Console.WriteLine(checkatdate.Value.Day+" "+checkatdate.Value.Month+" "+checkatdate.Value.Year);
                Console.WriteLine(new DateTime(checkatdate.Value.Year, checkatdate.Value.Month, checkatdate.Value.Day));
            }
        }

        private void exit_btn_Click(object sender, EventArgs e)
        {
            exit_checklist_panel();
        }

        private void ch_3_1_1_2_1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void export_no_data_btn_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            mainForm.checklist_id = checklist_id;
            Thread x = new Thread(delegate()
            {
                int i = 0;
                while (true)
                {
                    string str = "กำลัง Export PDF file ";
                    switch (i % 6)
                    {
                        case 0: str += ""; break;
                        case 1: str += "."; break;
                        case 2: str += ". ."; break;
                        case 3: str += ". . ."; break;
                        case 4: str += ". . . ."; break;
                        case 5: str += ". . . . ."; break;
                    }
                    label_footer_checklistsheet.Text = str;
                    i++;
                    Thread.Sleep(1000);
                }
            });
            x.Start();
            mainForm.export_no_data();
            this.Cursor = Cursors.Default;
            label_footer_checklistsheet.Text = string.Empty;
        }

        private void export_w_data_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            mainForm.checklist_id = checklist_id;
            save_event();

            Thread x = new Thread(delegate()
            {
                int i = 0;
                while (true)
                {
                    string str = "กำลัง Export PDF file ";
                    switch (i % 6)
                    {
                        case 0: str += ""; break;
                        case 1: str += "."; break;
                        case 2: str += ". ."; break;
                        case 3: str += ". . ."; break;
                        case 4: str += ". . . ."; break;
                        case 5: str += ". . . . ."; break;
                    }
                    label_footer_checklistsheet.Text = str;
                    i++;
                    Thread.Sleep(1000);
                }
            });
            x.Start();

            this.Cursor = Cursors.WaitCursor;
            mainForm.export_table_with_data();
            this.Cursor = Cursors.Default;

            x.Abort();
        }

        private void export_final_report_Click(object sender, EventArgs e)
        {
            save_event();
            Form export_form = new export_result(mainForm, checklist_id);
            export_form.Show();
        }
    }
}
